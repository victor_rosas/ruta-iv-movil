//
//  OrdenServicio.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrdenServicio : NSObject

-(BOOL)ordenServ:(NSString *)nIdOrden;

@end
