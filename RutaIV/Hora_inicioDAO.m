//
//  Hora_inicioDAO.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 03/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "Hora_inicioDAO.h"
#import "AppDelegate.h"
#import "Hora_inicio.h"

@implementation Hora_inicioDAO

-(NSString *) obtenerRutaBD{
    logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(BOOL)ActualizaHora_Inicio:(NSInteger)ID_INSTANCIA ID_OPERACION:(NSInteger)ID_OPERACION HORA_INICIO:(NSString *)Start_hour HORA_INICIO_ENVIADA:(NSInteger)HORA_INICIO_ENVIADA id_sitio:(NSInteger)id_sitio orden_visita:(NSInteger)orden_visita{
    [logProceso escribir:@"#Hora_inicioDAO entro ActualizaHora_Inicio"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    //char dateString = [Start_hour UTF8String];
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //AND HORA_INICIO_ENVIADA = %d
        //NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE DET_ORDEN_SERVICIO SET HORA_INICIO = %@,WHERE ID_INSTANCIA = %d,ID_OPERACION = %d",Start_hour,ID_INSTANCIA,ID_OPERACION];
        NSString *sqlInsert;
        
        if (ID_OPERACION == 1)
        {
            sqlInsert= [NSString stringWithFormat:@"UPDATE DET_ORDEN_SERVICIO SET HORA_INICIO = '%@' WHERE ID_SITIO = %ld AND ID_OPERACION = %ld AND ORDEN = %ld",Start_hour, (long)id_sitio,(long)ID_OPERACION, (long)orden_visita];
        }
        else if(ID_OPERACION == 2)
        {
            sqlInsert= [NSString stringWithFormat:@"UPDATE DET_ORDEN_SERVICIO SET HORA_INICIO = '%@' WHERE ID_INSTANCIA = %ld AND ID_OPERACION = %ld AND ORDEN = %ld",Start_hour, (long)ID_INSTANCIA,(long)ID_OPERACION, (long)orden_visita];
        }
        else if (ID_OPERACION == 3)
        {
            sqlInsert= [NSString stringWithFormat:@"UPDATE DET_ORDEN_SERVICIO SET HORA_INICIO = '%@' WHERE ID_INSTANCIA = %ld AND ID_OPERACION = %ld AND ORDEN = %ld",Start_hour, (long)ID_INSTANCIA,(long)ID_OPERACION, (long)orden_visita];
        }
        else
        {
            sqlInsert= [NSString stringWithFormat:@"UPDATE DET_ORDEN_SERVICIO SET HORA_INICIO = '%@' WHERE ID_SITIO = %ld AND ID_OPERACION = %ld AND ORDEN = %ld",Start_hour, (long)id_sitio,(long)ID_OPERACION, (long)orden_visita];
        }
        
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error sql: %s",sqlite3_errmsg(dataBase));
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio ActualizaHora_Inicio"];
    return bValida;
    
}

//1 LLegada a sitio
//2 llegada instancia
//3 Salida Instancia
//4 Salida Sitio

-(BOOL)ActualizaLLEGADA_Cabecero:(NSInteger)ID_ORDEN_SERVICIO HORA_LLEGADA:(NSInteger)HORA_LLEGADA HORA_LLEGADA_ENV:(NSInteger)HORA_LLEGADA_ENV ID_RUTA:(NSInteger)ID_RUTA{
    [logProceso escribir:@"#Hora_inicioDAO entro ActualizaLLEGADA_Cabecero"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    //char dateString = [Start_hour UTF8String];
    NSString * hora_llegada = [NSString stringWithFormat:@"%ld",(long)HORA_LLEGADA];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE CAB_ORDEN_SERVICIO SET HORA_LLEGADA = '%@',HORA_LLEGADA_ENVIADA = %ld WHERE ID_ORDEN_SERVICIO = %ld AND ID_RUTA = %ld",hora_llegada,(long)HORA_LLEGADA_ENV,(long)ID_ORDEN_SERVICIO,(long)ID_RUTA];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:[NSString stringWithFormat:@"#Hora_inicioDAO #ActualizaLLEGADA_Cabecero Error sql: %s",sqlite3_errmsg(dataBase)]];
        }
    }else{
        
       [logProceso escribir:@"#Hora_inicioDAO #ActualizaLLEGADA_Cabecero no se ha podido abrir la BD"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio ActualizaLLEGADA_Cabecero"];
    return bValida;
    
}

-(BOOL)ActualizaSALIDA_Cabecero:(NSInteger)ID_ORDEN_SERVICIO HORA_SALIDA:(NSString *)HORA_SALIDA HORA_SALIDA_ENV:(NSInteger)HORA_SALIDA_ENV ID_RUTA:(NSInteger)ID_RUTA {
     [logProceso escribir:@"#Hora_inicioDAO entro ActualizaSalida_Cabecero"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    //char dateString = [Start_hour UTF8String];
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE CAB_ORDEN_SERVICIO SET HORA_SALIDA = '%@',HORA_SALIDA_ENVIADA = %ld WHERE ID_ORDEN_SERVICIO = %ld AND ID_RUTA = %ld",HORA_SALIDA,(long)HORA_SALIDA_ENV,(long)ID_ORDEN_SERVICIO,(long)ID_RUTA];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:[NSString stringWithFormat:@"#Hora_inicioDAO #ActualizaSalida_Cabecero Error sql: %s",sqlite3_errmsg(dataBase)]];
            
        }
    }else{
        [logProceso escribir:@"#Hora_inicioDAO #ActualizaSalida_Cabecero No se ha podido abrir la base de datos"];
        
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio ActualizaSalida_Cabecero"];
    return bValida;
    
}


-(BOOL)ActualizaHora_SALIDA:(NSInteger)ID_ORDEN_SERVICIO ID_RUTA:(NSInteger)ID_RUTA HORA_SALIDA:(NSString *)HORA_SALIDA HORA_SALIDA_ENVIADA:(NSInteger)HORA_SALIDA_ENVIADA{
    [logProceso escribir:@"#Hora_inicioDAO entro ActualizaHora_SALIDA"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
               NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE CAB_ORDEN_SERVICIO SET HORA_SALIDA = '%@',HORA_SALIDA_ENVIADA = %ld WHERE ID_ORDEN_SERVICIO = %ld AND ID_RUTA = %ld ",HORA_SALIDA,(long)HORA_SALIDA_ENVIADA,(long)ID_ORDEN_SERVICIO,(long)ID_RUTA];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:[NSString stringWithFormat:@"#Hora_inicioDAO #ActualizaHora_SALIDA Error sql: %s",sqlite3_errmsg(dataBase)]];
        }
    }else{
        [logProceso escribir:@"#Hora_inicioDAO #ActualizaHora_SALIDA No se ha podido abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio ActualizaHora_SALIDA"];
    return bValida;
    
}

-(BOOL)ActualizaHora_LLEGADA:(NSInteger)ID_ORDEN_SERVICIO ID_RUTA:(NSInteger)ID_RUTA HORA_LLEGADA:(NSString *)HORA_LLEGADA HORA_LLEGADA_ENVIADA:(NSInteger)HORA_LLEGADA_ENVIADA{
    [logProceso escribir:@"#Hora_inicioDAO entro ActualizaHora_LLEGADA"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE CAB_ORDEN_SERVICIO SET HORA_LLEGADA = '%@',HORA_LLEGADA_ENVIADA = %ld WHERE ID_ORDEN_SERVICIO = %ld AND ID_RUTA = %ld ",HORA_LLEGADA,(long)HORA_LLEGADA_ENVIADA,(long)ID_ORDEN_SERVICIO,(long)ID_RUTA];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:[NSString stringWithFormat:@"#Hora_inicioDAO #ActualizaHora_LLEGADA Error sql: %s",sqlite3_errmsg(dataBase)]];
        }
    }else{
        [logProceso escribir:@"#Hora_inicioDAO #ActualizaHora_LLEGADA No se ha podido abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio ActualizaHora_LLEGADA"];
    return bValida;
    
}

-(BOOL)update_inicio_instancia :(NSInteger)id_instancia
{
    [logProceso escribir:@"#Hora_inicioDAO entro update_inicio_instancia"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET INICIO = %d WHERE ID_INSTANCIA = %ld",0, (long)id_instancia];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Hora_inicioDAO salio update_inicio_instancia"];
    return bValida;
    
}

@end
