//
//  versionesActuales.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface versionesActuales : NSObject

-(BOOL)borraVersionesActuales;

-(BOOL)insertaVersionesActuales:(int)nVersionMinTp wVersionMaxTp:(int)nVersionMaxTp wVersionMinAleatTp:(int)nVersionMinAleatTp wVersionMaxAleatTp:(int)nVersionMaxAleatTp;

@end
