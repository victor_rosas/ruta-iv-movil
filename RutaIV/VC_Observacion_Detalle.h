//
//  VC_Observacion_Detalle.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBJ_Observaciones.h"
#import "DATA_Observaciones.h"

@interface VC_Observacion_Detalle : UIViewController
{
    __weak IBOutlet UITextView *TextView_Descripcion;
    __weak IBOutlet UILabel *label_instancia;
    __weak IBOutlet UILabel *Label_orden;
    __weak IBOutlet UILabel *label_clasificacion;
    __weak IBOutlet UILabel *label_atencion;
    DATA_Observaciones * data_observaciones_manager;
    __weak IBOutlet UIView *view_detalles;
    __weak IBOutlet UIScrollView *scroll_view;
}
@property OBJ_Observaciones * observacion;
- (IBAction)action_back:(id)sender;

@end
