//
//  OBJ_Cambio_Precio.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Cambio_Precio.h"

@implementation OBJ_Cambio_Precio
@synthesize id_instancia;
@synthesize id_charola;
@synthesize id_espiral;
@synthesize id_seleccion;
@synthesize id_articulo;
@synthesize actual;
@synthesize anterior;

-(id)init :(NSString *)instancia id_charola:(NSString *)charola id_espiral:(NSString *)espiral id_seleccion:(NSString *)seleccion id_articulo:(NSString *)articulo actual:(NSString *)precio_actual anterio:(NSString *)precio_anterior
{
    self.id_instancia = instancia;
    self.id_charola = charola;
    self.id_espiral = espiral;
    self.id_seleccion = seleccion;
    self.id_articulo = articulo;
    self.actual = precio_actual;
    self.anterior = precio_anterior;
    
    return self;
}
@end
