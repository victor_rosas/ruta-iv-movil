//
//  VC_Observaciones.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DATA_Observaciones.h"

@interface VC_Observaciones : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    __weak IBOutlet UIPickerView *picker_categoria;
    __weak IBOutlet UISwitch *switch_atencion;
    DATA_Observaciones * data_observaciones_manager;
    NSMutableDictionary * diccionario_clasificaciones;
}

- (IBAction)action_back_combo:(id)sender;
@property int instancia_observaciones;



@end
