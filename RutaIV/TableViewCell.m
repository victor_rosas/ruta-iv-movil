//
//  TableViewCell.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 15/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TableViewCell.h"
#import "NextViewController.h"

@implementation TableViewCell

- (void)awakeFromNib {
    stringVariable = [[NSString alloc]init];
    label_espiral.layer.cornerRadius = 5;
    label_espiral.layer.masksToBounds = TRUE;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
