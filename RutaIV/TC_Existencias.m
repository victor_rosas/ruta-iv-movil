//
//  TC_Existencias.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TC_Existencias.h"

@implementation TC_Existencias

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setupCell:(NSString*)articulo cantidad:(int)cantidad id_articulo:(NSString *)id_articulo{
    
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    [label_id.layer setCornerRadius:10.0f];
    [label_id.layer setMasksToBounds:YES];
    [label_cantidad.layer setCornerRadius:10.0f];
    [label_cantidad.layer setMasksToBounds:YES];
    
    if (cantidad < 1) {
        //label_id.backgroundColor = [UIColor colorWithRed:240.0/255.0f green:72.0/255.0f blue:52.0/255.0f alpha:1.0];
        label_id.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
    }
    else if (cantidad < 3)
    {
        label_id.backgroundColor = [UIColor colorWithRed:246.0f/255.0f green:236.0f/255.0f blue:179.0f/255.0f alpha:1.0];
    }
    else
    {
        label_id.backgroundColor = [UIColor colorWithRed: 0 green:0 blue:0 alpha:.2];
    }
    
    
    if ([id_articulo integerValue] < 1) {
        //label_id.backgroundColor = [UIColor colorWithRed:240.0/255.0f green:72.0/255.0f blue:52.0/255.0f alpha:1.0];
        label_cantidad.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
    }
    else if ([id_articulo integerValue] < 3)
    {
        label_cantidad.backgroundColor = [UIColor colorWithRed:246.0f/255.0f green:236.0f/255.0f blue:179.0f/255.0f alpha:1.0];
    }
    else
    {
        label_cantidad.backgroundColor = [UIColor colorWithRed: 0 green:0 blue:0 alpha:.2];
    }
    
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        label_articulo.text = [articulo capitalizedString];
    }
    else
    {
        label_articulo.text = articulo;
    }
    label_cantidad.text = id_articulo;
    label_id.text = [NSString stringWithFormat:@"%d", cantidad];
}

@end
