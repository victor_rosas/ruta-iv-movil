//
//  MonederoViewController.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 23/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Monedero_y_contadoresDAO.h"
#import "ProductosDAO.h"
#import "Data_Cafe.h"

@interface MonederoViewController : UIViewController <UITextFieldDelegate>
{
    Monedero_y_contadoresDAO *dao;
    NSMutableArray *Historicos;
    NSMutableArray *Monedas;
    NSMutableArray *suma;
    NSMutableArray *articulosuma;
    __weak IBOutlet UITextField *historicopiezas;
    __weak IBOutlet UITextField *historicoventas;
    BOOL perform_segue;
    int x;
    NSInteger totalprecios;
    NSInteger unidadresultado;
    NSString *hist_pieza;
    NSString *hist_venta;
    int orden_servicio;
    
    UIToolbar* keyboardDoneButtonView;
    NSMutableArray * array_text_fields;
    int current;
    
    ProductosDAO * data_manager_articulos;
    Data_Cafe * data_manager_cafe;
}
- (IBAction)Guardar:(id)sender;
-(NSString *)calcular_vlunidadMax;
- (NSString *)calcular_vlmontoMax;
@property int instancia;
//Variables horas llegada
@property BOOL salida_sitio;
@property NSString * tipo;

@end
