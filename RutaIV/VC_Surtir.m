//
//  VC_Surtir.m
//  RutaIV
//
//  Created by Miguel Banderas on 21/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Surtir.h"
#import "TableViewCell.h"
#import "ProductosDAO.h"
#import "MonederoViewController.h"
#import "VC_Existencias.h"
#import "ExistenciasDAO.h"
#import "OBJ_Articulo.h"
#import "VC_Caduco.h"
#import "VC_Modificar_Inventario.h"
#import "OBJ_Instancia_Doble.h"

#import "CamaraUpload.h"
@interface VC_Surtir ()

@end

@implementation VC_Surtir
@synthesize instancia;
@synthesize inv_ini_array;
@synthesize salida_sitio;
@synthesize primera_visita;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Inicializacion de variables
    parametro_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_producto_orden"];
    inv_ini_array = [[NSUserDefaults standardUserDefaults]objectForKey:@"inv_ini_array"];
    contador = 0;
    revision = true;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    instancia = [[prefs objectForKey:@"instancia"]integerValue];
    
    dao = [[ProductosDAO alloc] init];
    dao_existencias = [[ExistenciasDAO alloc]init];
    arreglo_articulos = [[NSMutableArray alloc]init];
    diccionario_existencias = [[NSMutableDictionary alloc]init];
    arreglo_productos = [[NSMutableDictionary alloc]init];
    
    mostrar_alerta = true;
    
    //Almacen
    NSString *ruta_id=[prefs objectForKey:@"nIdRuta"];
    almacen_id = [dao_existencias CargarIDalmacen:(uint32_t)[ruta_id integerValue]];
    
    //Obtener datos
    [self ObtenerProductos];
    [self surtio_vacio];
    [self ajuste_existencias];
    //Cantidad en la celda
    textFieldContent = [[NSMutableArray alloc]initWithCapacity:[arreglo_articulos count]];
    for (NSInteger i = 0; i< [arreglo_articulos count]; i++) {
        [textFieldContent addObject:@""];
    }
    
    //Preparecion de vista
    [self button_setup];
    btn_siguient.enabled = false;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index = [myTableview indexPathForSelectedRow];
    if (index == nil) {
        index = [NSIndexPath indexPathForRow:0 inSection:0];
    }
    
    [self ObtenerProductos];
    [myTableview reloadData];
    
    [myTableview selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated{
    
}

//Boton de siguiente y navegacion
- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* existencias = [[UIBarButtonItem alloc] initWithTitle:@"Existencias"
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(existencias_clicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];    [existencias setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor blackColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial" size:11.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
    
    UIBarButtonItem *bar_siguiente = [[UIBarButtonItem alloc] initWithCustomView:btn_siguient];
    UIBarButtonItem *bar_pasado = [[UIBarButtonItem alloc]initWithCustomView:btn_pasado];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(0, 0, 5, 0);
    
    UIBarButtonItem * espacio = [[UIBarButtonItem alloc]initWithCustomView:button];
    
    UIBarButtonItem *searchButton         = [[UIBarButtonItem alloc]
                                             initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize
                                             target:self
                                             action:@selector(existencias_clicked:)];
    
    UIBarButtonItem *inventario         = [[UIBarButtonItem alloc]
                                           initWithImage:[UIImage imageNamed:@"interstate_truck-26.png"] style:UIBarButtonItemStylePlain target:self action:@selector(inventario_clicked:)];
    
    
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:bar_siguiente, espacio, searchButton, nil];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:bar_pasado, espacio, inventario, nil];
    [self.navigationItem setHidesBackButton:YES];
}
- (IBAction)action_back_pressed:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             if(primera_visita)
                             {
                                 [self surtio_vacio];
                                 NSArray *array = [self.navigationController viewControllers];
                                 [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
                             }
                             else
                             {
                                 [self surtio_vacio];
                                 [self.navigationController popViewControllerAnimated:true];
                             }
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)btn_siguiente:(id)sender {
    //Modificacion de tabla en seccion de surtio
    
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Continuar"
                                 message:@"Seleccionar una opción."
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* removio = [UIAlertAction
                         actionWithTitle:@"Removio"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             VC_Caduco * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Removio"];
                             controller.id_instancia = instancia;
                             controller.salida_sitio = salida_sitio;
                             controller.removio_caduco = @"removio";
                             controller.aparecio = @"Removio";
                             controller.cantidad_aparecido = 1;
                             [self.navigationController pushViewController:controller animated:YES];
                             [self guardar_informacion];
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* caduco = [UIAlertAction
                         actionWithTitle:@"Caduco"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             VC_Caduco * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Caduco"];
                             controller.id_instancia = instancia;
                             controller.salida_sitio = salida_sitio;
                             controller.removio_caduco = @"caduco";
                             controller.aparecio = @"Caduco";
                             controller.cantidad_aparecido = 1;
                             [self.navigationController pushViewController:controller animated:YES];
                             [self guardar_informacion];
                             [view dismissViewControllerAnimated :YES completion:nil];
                             
                         }];
    UIAlertAction* monedero = [UIAlertAction
                         actionWithTitle:@"Fotografia"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                             controller.instancia = instancia;
                             controller.salida_sitio = salida_sitio;
                             [self.navigationController pushViewController:controller animated:YES];
                             [self guardar_informacion];
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancelar"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:removio];
    [view addAction:caduco];
    [view addAction:monedero];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)guardar_informacion
{
    BOOL repeticion = false;
    OBJ_Instancia_Doble * instancia_repetida = [self getInstanciaRepetida];
    if (!(instancia_repetida == nil))
    {
        repeticion = true;
    }
    
    for (int i = 0; i<[textFieldContent count]; i++) {
        
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        
        NSInteger surtido = [[textFieldContent objectAtIndex:i] integerValue];
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_visita = [articulo.orden_visita integerValue];
        
        NSInteger inv_inicial = [articulo.inventario_inicial_real integerValue];
        
        [dao update_surtio:surtido id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita id_instancia:instancia];
        
        if (repeticion) {
            NSString * maximo = instancia_repetida.maximo;
            [dao update_existencia:inv_inicial surtio:surtido id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita id_instancia:instancia maximo:[maximo integerValue]];
        }
    }
    
}

-(OBJ_Instancia_Doble*)getInstanciaRepetida
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    OBJ_Instancia_Doble * instancia_doble = [dao getInstanciaRepetida:instancia orden:[orden_visita_instancia integerValue]];
    return instancia_doble;
}

-(BOOL) verificacion:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    int cantidad = (uint32_t)[[textFieldContent objectAtIndex:row] integerValue];
    int capacidad_espiral = [self revisarCapacidadEspiral:cantidad index:indexPath];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Capacidad de espiral superada."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertController * alert2=   [UIAlertController
                                   alertControllerWithTitle:@"Error"
                                   message:@"No hay suficiente producto en existencia."
                                   preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             UIView* nextResponder = [self.view viewWithTag:indexPath.row+1];
                             [nextResponder becomeFirstResponder];
                             
                         }];
    UIAlertAction* ok2 = [UIAlertAction
                          actionWithTitle:@"OK"
                          style:UIAlertActionStyleDefault
                          handler:^(UIAlertAction * action)
                          {
                              [alert2 dismissViewControllerAnimated:YES completion:nil];
                              UIView* nextResponder = [self.view viewWithTag:indexPath.row+1];
                              [nextResponder becomeFirstResponder];
                              
                          }];
    [alert addAction:ok];
    [alert2 addAction:ok2];
    
    if(capacidad_espiral == 1)
    {
        textFieldContent[indexPath.row] = @"";
        TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
        cell.myTextbox.text = @"";
        cell.backgroundColor = [UIColor whiteColor];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:alert animated:YES completion:nil];
        });
        
        //[myTableview reloadData];
        return false;
    }
    else if (capacidad_espiral == 2)
    {
        textFieldContent[indexPath.row] = @"";
        TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
        cell.myTextbox.text = @"";
        cell.backgroundColor = [UIColor whiteColor];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self presentViewController:alert2 animated:YES completion:nil];
        });
        //[myTableview reloadData];
        return false;
    }
    else
    {
        TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
        if([cell.myTextbox.text  isEqual: @""])
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        else{
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
        }
        
        //VERIFICACION TODAS LAS CELDAS LLENAS
        contador = 0;
        while (![[textFieldContent objectAtIndex:contador] isEqualToString:@""]) {
            contador++;
            if(contador == [textFieldContent count])
            {
                break;
            }
        }
        if(contador >= [textFieldContent count])
        {
            [btn_siguient setEnabled:true];
        }
        else
        {
            [btn_siguient setEnabled:false];
        }
        return true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    currentSelection = indexPath;
    UIView* nextResponder = [self.view viewWithTag:indexPath.row+1];
    [nextResponder becomeFirstResponder];
}

//SELECCIONAMOS LA CELDA CUANDO SE SELECCIONE EL TEXTFIELD----------
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    revision = [self verificacion:seleccion];
    _scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                   _scrollView.frame.origin.y,
                                   _scrollView.frame.size.width,
                                   _scrollView.frame.size.height - 500 + 500);
    
    
    //CODIGO DE SCROLL-------------------------------------------------
    long int rowTest1 = textField.tag-1;
    seleccion = [NSIndexPath indexPathForRow:rowTest1 inSection:0];
    [myTableview selectRowAtIndexPath:[NSIndexPath indexPathForRow:rowTest1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    
    int offset = 2;
    if (rowTest1+offset>=[arreglo_articulos count]) {
        NSIndexPath *scroll_path = [NSIndexPath indexPathForRow:rowTest1 inSection:0];
        [myTableview scrollToRowAtIndexPath:scroll_path
                           atScrollPosition:UITableViewScrollPositionTop
                                   animated:YES];
    }
    else
    {
        NSIndexPath *scroll_path = [NSIndexPath indexPathForRow:rowTest1+offset inSection:0];
        [myTableview scrollToRowAtIndexPath:scroll_path
                           atScrollPosition:UITableViewScrollPositionMiddle
                                   animated:YES];
    }
    //TERMINA CODIGO DE SCROLL------------------------------------------
    
    //Revision de orden
    if (![parametro_orden isEqualToString:@"0"])
    {
        NSIndexPath * seleccion_path = [myTableview indexPathForSelectedRow];
        int selected = (uint32_t)seleccion_path.row;
        int llenados = 1;
        if (!seleccion_path.row == 0)
        {
            if ([[textFieldContent objectAtIndex:seleccion_path.row-1] isEqualToString:@""]) {
                for (int i =0; i<[textFieldContent count]; i++) {
                    if (![[textFieldContent objectAtIndex:i] isEqualToString:@""]) {
                        llenados++;
                    }
                }
                
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Error"
                                              message:@"Llenar información en orden."
                                              preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Es necesario regresar de poco a poco en el scroll para que las celdas se carguen y el tag funcione. Por eso es ese ciclo
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                         if (selected>llenados+4) {
                                             for (int i = selected; i>llenados; i-=4) {
                                                 NSIndexPath *back = [NSIndexPath indexPathForRow:i inSection:0];
                                                 [myTableview selectRowAtIndexPath:back animated:NO scrollPosition:UITableViewScrollPositionMiddle];
                                                 UIView* nextResponder = [self.view viewWithTag:i];
                                                 [nextResponder becomeFirstResponder];
                                             }
                                         }
                                         
                                         //Estas ultimas lineas son para asegurarse que se presente el offset y que la celda este centrada en la pantalla visible y se pueda ver la informacion previamente llenada
                                         int offset = 2;
                                         if (rowTest1+offset>=[arreglo_articulos count]) {
                                             NSIndexPath *scroll_path = [NSIndexPath indexPathForRow:rowTest1 inSection:0];
                                             [myTableview scrollToRowAtIndexPath:scroll_path
                                                                atScrollPosition:UITableViewScrollPositionTop
                                                                        animated:YES];
                                         }
                                         else
                                         {
                                             NSIndexPath *scroll_path = [NSIndexPath indexPathForRow:rowTest1+offset inSection:0];
                                             [myTableview scrollToRowAtIndexPath:scroll_path
                                                                atScrollPosition:UITableViewScrollPositionMiddle
                                                                        animated:YES];
                                         }
                                         
                                         UIView* nextResponder = [self.view viewWithTag:llenados];
                                         [nextResponder becomeFirstResponder];
                                         
                                     }];
                [alert addAction:ok];
                dispatch_async(dispatch_get_main_queue(), ^ {
                    if (mostrar_alerta) {
                        mostrar_alerta = false;
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    //Codigo para asegurarse que la alerta no se muestre repeditas veces dado que esto se ejecuta cada vez que se selecciona una nueva celda.
                    if (selected < llenados+4) {
                        mostrar_alerta = true;
                    }
                });
                
            }
        }
    }
}

//Conservar el texto de las celdas---------------------------------T

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if ([textField.text isEqualToString:@""]) {
        textFieldContent[textField.tag-1] =@"";
        
        
    }
    textFieldContent[textField.tag-1] = textField.text;
    
    [textField resignFirstResponder];
    
}



//TERMINA SELECCION DE CELDA CON TEXTFIELD---------------------------

//METODOS TABLEVIEW-----------------------------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arreglo_articulos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TC_Surtir" forIndexPath:indexPath];
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    // Configure the cell..
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    NSString *producto_nombre = articulo.articulo;
    
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        cell.myLabel.text = [producto_nombre capitalizedString];
    }
    else
    {
        cell.myLabel.text = producto_nombre;
    }
    
    NSString *id_articulo = articulo.id_articulo;
    int existencias = (uint32_t)[[diccionario_existencias objectForKey:id_articulo] integerValue];
    
    cell.Espiral.text = articulo.espiral;
    cell.label_capacidad.text = [NSString stringWithFormat:@"C.E. -  %@", articulo.capacidad];
    NSString *precio_string = articulo.precio;
    float precio = precio_string.floatValue;
    cell.label_costo.text = [formatter stringFromNumber:[NSNumber numberWithFloat:precio]];
    
    cell.label_inv_ini.text = [NSString stringWithFormat:@"In - %@", articulo.inventario_inicial_real];
    //cell.myTextbox.placeholder = cell.label_inv_ini.text;
    cell.myTextbox.placeholder = [NSString stringWithFormat:@"%d", existencias];
    
    cell.myTextbox.tag = indexPath.row+1;
    
    if([textFieldContent count] < indexPath.row){
        cell.myTextbox.text = @"";
    }
    else{
        if ([textFieldContent count] == 0) {
            cell.myTextbox.text = @"";
            
        }
        else{
            cell.myTextbox.text = [textFieldContent objectAtIndex:indexPath.row];
        }
        
    }
    if([cell.myTextbox.text  isEqual: @""])
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    
    if([cell.myTextbox.text isEqualToString:@""])
    {
        cell.label_cantidad_pasada.text = @"0";
    }
    else
    {
        cell.label_cantidad_pasada.text = cell.myTextbox.text;
    }
    
    cell.myTextbox.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

- (IBAction)doneClicked:(id)sender
{
    //[self tableView:myTableview didDeselectRowAtIndexPath:[myTableview indexPathForSelectedRow]];
    
    TableViewCell *cell;
    NSIndexPath *selectedIndexPath = [myTableview indexPathForSelectedRow];
    NSIndexPath *selectNew;
    /*if (selectedIndexPath.row+1 == [textFieldContent count]) {
        selectNew = [NSIndexPath indexPathForRow:0 inSection:0];
        [myTableview selectRowAtIndexPath:selectNew animated:true scrollPosition:UITableViewScrollPositionTop];
        //[myTableview reloadData];
    }
    else{*/
        selectNew = [NSIndexPath indexPathForRow:selectedIndexPath.row+1 inSection:0];
    //}
    [self textFieldShouldReturn:cell.myTextbox];
    
    /*if(selectedIndexPath.row==[textFieldContent count]-1)
    {
        //La verificacion en la ultima fila es especial y debe manejarse por separado (aparentemente)
        if(![self verificacion:selectedIndexPath])
        {
            //Evita el scroll cuando la verificacion falla.
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedIndexPath.row inSection:0];
            [myTableview scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        else
        {
            [myTableview reloadData];
            [self verificacion:selectedIndexPath];
        }
    }*/
}

- (IBAction)existencias_clicked:(id)sender
{
    VC_Existencias *view_existencias = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Existencias"];
    view_existencias.navigationItem.title = @"";
    
    view_existencias.array_existencias = [[NSMutableDictionary alloc]init];
    view_existencias.array_nombres = [[NSMutableDictionary alloc]init];
    view_existencias.arreglo_articulos = arreglo_articulos;
    view_existencias.origen = @"Articulo";
    
    for (int i = 0; i<[diccionario_existencias count]; i++) {
        NSArray *array = [diccionario_existencias allKeys];
        NSString *id_articulo = [array objectAtIndex:i];
        int existencia_actualizada = (uint32_t)[[diccionario_existencias objectForKey:id_articulo] integerValue];
        [view_existencias.array_existencias setObject:[NSString stringWithFormat:@"%d",existencia_actualizada] forKey:id_articulo];
        [view_existencias.array_nombres setObject:[arreglo_productos objectForKey:id_articulo] forKey:id_articulo];
    }
    [self.navigationController pushViewController:view_existencias animated:YES];
    //[self presentModalViewController:view_existencias animated:YES];
}

- (IBAction)inventario_clicked:(id)sender
{
    VC_Modificar_Inventario * view_inventario = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Modificar_Inventario"];
    view_inventario.navigationItem.title = @"";
    
    view_inventario.instancia = instancia;
    view_inventario.index = [myTableview indexPathForSelectedRow];
    
    [self.navigationController pushViewController:view_inventario animated:YES];
    //[self presentModalViewController:view_existencias animated:YES];
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    TableViewCell *cell = (TableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    //[self setCellColor:[colorWithWhite:0.961 alpha:1.000] ForCell:cell]; //normal color
    if([cell.myTextbox.text  isEqual: @""])
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else{
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    NSIndexPath *selectedIndexPath = [myTableview indexPathForSelectedRow];
    
    NSInteger nextTag = selectedIndexPath.row + 1 +1;
    NSInteger cantidad_productos = [textFieldContent count];
    UIView* nextResponder;
    if (nextTag == cantidad_productos +1) {
        //[self verificacion:selectedIndexPath];
        nextResponder = [self.view viewWithTag:nextTag-2];
    }
    else
    {
        nextResponder = [self.view viewWithTag:nextTag];
    }
    
    [textField resignFirstResponder];
    
    if (nextResponder) {
        [nextResponder becomeFirstResponder];
    }
    if(!revision)
    {
        //Evita el scroll cuando es la primera fila la cual esta siendo verificada
        if (selectedIndexPath.row == 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [myTableview scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        //Evita el scroll en todos los otros casos excepto cuando es la ultima
        else
        {
            nextTag = selectedIndexPath.row;
            UIView* nextResponder = [self.view viewWithTag:nextTag];
            
            [textField resignFirstResponder];
            
            if (nextResponder) {
                [nextResponder becomeFirstResponder];
            }
        }
        [myTableview reloadData];
        
    }
    else if (nextTag == cantidad_productos+1)
    {
        [myTableview reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [myTableview scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    return YES;
}


-(void)ObtenerProductos{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [dao select_articulos:instancia :[orden_visita_instancia integerValue]];
    while(x<[arreglo_articulos count])
    {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:x];
        
        
        NSString * id_articulo = articulo.id_articulo;
        //[dao_existencias actualizarExistencia:[id_articulo integerValue] NUEVAEXISTENCIA:5 ALMACEN:[almacen_id integerValue]];
        NSString *existencias = [dao_existencias CargarExistencias:(uint32_t)[id_articulo integerValue]];
        
        if([diccionario_existencias objectForKey:id_articulo] == nil)
        {
            [diccionario_existencias setObject:existencias forKey:id_articulo];
            //Este arreglo es para tener los productos con su nombre en orden para poder mostrarlo en la pantalla de existencias, no se hizo un objeto por no modificar la estructura original, aunque ahora se estan creando objetos sin necesidad verdadera.
            [arreglo_productos setObject:articulo.articulo forKey:id_articulo];
        }
        x++;
        
    }
    int cantidad_productos = [arreglo_articulos count];
}

-(void)ajuste_existencias{
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:0];
    NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
    NSArray *keys_existencia = [diccionario_existencias allKeys];
    
    for (int i = 0; i<[diccionario_existencias count]; i++) {
        NSString *llave = [keys_existencia objectAtIndex:i];
        NSString *existencia_string = [diccionario_existencias objectForKey:llave];
        
        int articulo = (uint32_t)[llave integerValue];
        int existencias = (uint32_t)[existencia_string integerValue];
        
        //Cargar acumulado de surtimiento
        NSMutableDictionary *surtido = [dao CargarSurtidoAnterior:instancia orden_servicio:orden_servicio id_articulo:articulo];
        //NSMutableDictionary *surtido = [dao CargarSurtidoAnterior:instancia :orden :articulo];
        int surtio = (uint32_t)[[surtido objectForKey:@"suma"]integerValue];
        //int meh = (uint32_t)[[surtido objectForKey:@"existencia"]integerValue];
        
        existencias = existencias - surtio;
        [diccionario_existencias setObject:[NSString stringWithFormat:@"%d", existencias] forKey:llave];
    }
}

-(int)revisarCapacidadEspiral:(int)cantidad index:(NSIndexPath*)indexPath{
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
    int cantidad_pasada = (uint32_t)[cell.label_cantidad_pasada.text integerValue];
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    int espiral = (uint32_t)[articulo.capacidad integerValue];
    
    NSString *id_articulo = articulo.id_articulo;
    int existencias = (uint32_t)[[diccionario_existencias objectForKey:id_articulo] integerValue];
    //int cantidad_ini = (uint32_t)[[inv_ini_array objectAtIndex:indexPath.row] integerValue];
    int cantidad_ini = (uint32_t)[articulo.inventario_inicial_real integerValue];
    int final = cantidad + cantidad_ini;
    
    if(final>espiral)
    {
        int existencia_temporal = cantidad_pasada+existencias;
        NSString *cantidad_nueva = [NSString stringWithFormat:@"%d",existencia_temporal];
        [diccionario_existencias setObject:cantidad_nueva forKey:id_articulo];
        cell.label_cantidad_pasada.text = @"0";
        return 1;
    }
    else
    {
        int existencia_temporal = cantidad_pasada+existencias;
        if (existencia_temporal<cantidad) {
            NSString *cantidad_nueva = [NSString stringWithFormat:@"%d",existencia_temporal];
            [diccionario_existencias setObject:cantidad_nueva forKey:id_articulo];
            cell.label_cantidad_pasada.text = @"0";
            return 2;
        }
        else
        {
            int diferencia = cantidad-cantidad_pasada;
            int nueva_cantidad = existencias-diferencia;
            NSString *cantidad_nueva = [NSString stringWithFormat:@"%d",nueva_cantidad];
            [diccionario_existencias setObject:cantidad_nueva forKey:id_articulo];
            cell.label_cantidad_pasada.text = [NSString stringWithFormat:@"%d",cantidad];
            
            return 0;
        }
        
    }
}

-(void)table_reload:(NSIndexPath *)index_path s_contenido:(NSString *)contenido
{
    int alcance = 9;
    OBJ_Articulo * articulo_modificado = [arreglo_articulos objectAtIndex:index_path.row];
    
    int disminuido = (uint32_t)[contenido integerValue];
    NSString * id_modificado = articulo_modificado.id_articulo;
    
    while ([arreglo_articulos count]-1 < index_path.row + alcance) {
        alcance--;
    }
    
    for (int i = 0; i<alcance; i++) {
        int posicion = (uint32_t)index_path.row+1+i;
        NSIndexPath * index = [NSIndexPath indexPathForRow:posicion inSection:0];
        
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:posicion];
        if ([articulo.id_articulo isEqualToString:id_modificado]) {
            TableViewCell *cell_modificada = (TableViewCell *)[myTableview cellForRowAtIndexPath:index];
            
            int existencia =(uint32_t)[[diccionario_existencias objectForKey:id_modificado] integerValue];
            int nueva_existencia = existencia - disminuido;
            
            cell_modificada.myTextbox.placeholder = [NSString stringWithFormat:@"%d",nueva_existencia];
        }

    }
}

-(void)surtio_vacio
{
    for (int i = 0; i<[textFieldContent count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_visita = [articulo.orden_visita integerValue];
        
        [dao update_surtio:0 id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita id_instancia:instancia];
        //[dao actualizaCargadoinv:inventario_ini ID_ARTICULO:id_art instancia:instancia];
    }
}

//Verificacion no mas de 5 caracteres
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    int tag = (uint32_t)textField.tag;
    NSIndexPath * index = [NSIndexPath indexPathForItem:tag-1 inSection:0];
    NSString * contenido = [NSString stringWithFormat:@"%@%@",textField.text,string];
    [self table_reload:index s_contenido:contenido];
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    return (newLength > 5) ? NO : YES;
    
    
}



@end
