//
//  OBJ_Inventario_Queries.h
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface OBJ_Inventario_Queries : NSObject{
    sqlite3 *bd;
    OBJ_Log_Proceso *logProceso;
}

-(NSString *) obtenerRutaBD;
-(NSMutableArray *)cargar_inventario_cero;
-(BOOL)borrar_surtido;
-(BOOL)insert_surtido;
-(NSMutableArray *)inventario_actual;
-(NSMutableArray *)inventario_inicial;

@end
