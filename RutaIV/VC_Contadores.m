//
//  VC_Contadores.m
//  RutaIV
//
//  Created by Miguel Banderas on 11/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Contadores.h"
#import "TC_Cafe.h"
#import "OBJ_Cafe.h"
#import "VC_Pruebas.h"

@interface VC_Contadores ()

@end

@implementation VC_Contadores
@synthesize id_instancia;
@synthesize salida_sitio;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    arreglo_articulos_cafe = [[NSMutableArray alloc]init];
    data_cafe_manager = [[Data_Cafe alloc]init];
    contador_rellenado = 0;
    cantidad_vasos =(uint32_t) [[data_cafe_manager select_cantidad_vasos:id_instancia] integerValue];
    cantidad_vasos_superada = false;
    ventas_incongruentes = false;
    alerta_presente = false;
    [self inicializacion_alertas];
    NSString * parametro_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_producto_orden"];
    if ([parametro_orden isEqualToString:@"0"]) {
        productos_orden = false;
    }
    else{
        productos_orden = true;
    }
    
    //Inicializacion de vistas
    btn_siguiente.enabled = false;
    [self button_setup];
    
    //Cargar datos
    arreglo_articulos_cafe = [data_cafe_manager select_articulos_contadores:id_instancia];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = [NSIndexPath indexPathForRow:0 inSection:0];
    [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
    
    [self update_en_cero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Aspecto y funciones de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_articulos_cafe count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TC_Cafe *cell = (TC_Cafe *)[myTableView dequeueReusableCellWithIdentifier:@"TC_Cafe_2" forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:indexPath.row];
    //NSString * capacidad = [NSString stringWithFormat:@"C.E. - %@", articulo_cafe.capacidad];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString * precio = [formatter stringFromNumber:[NSNumber numberWithFloat:[articulo_cafe.precio integerValue]]];
    
    //Creacion de la celda
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    cell.text_field_cantidad.text = articulo_cafe.surtio;
    
    NSString * nombre_articulo = articulo_cafe.nombre_articulo;
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        nombre_articulo = [nombre_articulo capitalizedString];
    }
    
    [cell cell_setup:nombre_articulo numeracion:articulo_cafe.id_seleccion capacidad:@"" costo:precio];
    
    NSString *historico_cafe = [[NSUserDefaults standardUserDefaults] objectForKey:@"configurarion_historico_cafe"];
    if ([historico_cafe isEqualToString:@"1"]) {
        NSString * place = articulo_cafe.ventas_cafe;
        cell.text_field_cantidad.placeholder = place;
    }
    
    if (![articulo_cafe.surtio isEqualToString:@""])
    {
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    //Se agrega boton a todos los text fields de las celdas
    cell.text_field_cantidad.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
}

//Seleccion de celda cuando se inicia modificacion en TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    if (ventas_incongruentes) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
            [myTableView selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_path_pasado];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_path_pasado];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_ventas animated:YES completion:nil];
            }
            
        });
    }
    else if (cantidad_vasos_superada) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
            [myTableView selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_path_pasado];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_path_pasado];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_vasos animated:YES completion:nil];
            }
            
        });
    }
    else
    {
        index_path_pasado = index_path;
        
        //Verificacion orden
        BOOL continuar;
        if (productos_orden) {
            continuar = [self verificacion_orden_otros:index_path];
        }
        else
        {
            continuar = true;
        }
        
        if (continuar) {
            [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [self ajuste_scroll:index_path];
        }
        
    }
    
}

//Guardar informacion de TextField y activacion de boton
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:index_path.row];
    articulo_cafe.surtio = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if ([textField.text isEqualToString:@""]&&![string isEqualToString:@""]) {
        contador_rellenado++;
    }
    
    //En caso de borrar es necesario hacer esto
    if ([string isEqualToString:@""]) {
        int lenght =(uint32_t)[textField.text length];
        NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
        articulo_cafe.surtio = texto_actualizado;
        
        if ([texto_actualizado isEqualToString:@""]) {
            contador_rellenado --;
        }
    }
    [arreglo_articulos_cafe replaceObjectAtIndex:index_path.row withObject:articulo_cafe];
    
    //Decide cuando desactivar el boton
    if (!(contador_rellenado == [arreglo_articulos_cafe count])) {
        btn_siguiente.enabled = false;
    }
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
}

//Recargar informacion en cell recien abandonada
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
    
    [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self verificacion_cantidad_vasos:index_path_pasado];
    [self verificacion_ventas_anterior:index_path_pasado];
    
    //Decide cuando se activa el boton
    if (contador_rellenado == [arreglo_articulos_cafe count]) {
        btn_siguiente.enabled = true;
    }
    else
    {
        btn_siguiente.enabled = false;
    }
}

//Setup del boton
- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableView indexPathForSelectedRow];
    NSIndexPath * next;
    
    //Revision orden
    BOOL continuar;
    if (productos_orden) {
        continuar = [self verificacion_orden_next:selected];
    }
    else
    {
        continuar = true;
    }
    
    if (continuar) {
        if (selected.row+1 == [arreglo_articulos_cafe count])
        {
            next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
            [self regresar_anterior:selected posicion_regreso:0];
        }
        else
        {
            next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
        }
        
        [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        [self ajuste_scroll:next];
        
        TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:next];
        UITextField * text_field = cell.text_field_cantidad;
        [text_field becomeFirstResponder];
    }
}

//Metodo para regresar a posicion lejana
-(void)regresar_anterior :(NSIndexPath *)selected posicion_regreso:(int)posicion
{
    while (selected.row > posicion+4) {
        selected = [NSIndexPath indexPathForRow:selected.row-3 inSection:selected.section];
        [myTableView selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
}

//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_articulos_cafe count])
    {
        [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableView scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

//Verificacion orden otros casos
-(BOOL)verificacion_orden_otros :(NSIndexPath *)index_seleccionado
{
    if (!(index_seleccionado.row <= contador_rellenado)) {
        NSIndexPath * index_verdadero = [NSIndexPath indexPathForRow:contador_rellenado inSection:index_seleccionado.section];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_seleccionado posicion_regreso:(uint32_t)index_verdadero.row];
            [myTableView selectRowAtIndexPath:index_verdadero animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_verdadero];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_verdadero];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_orden animated:YES completion:nil];
            }
        });
        
        return false;
    }
    return true;
}

//Verificacion orden_next
-(BOOL)verificacion_orden_next :(NSIndexPath *)index_actual
{
    //Revisa si la celda actual tiene algo escrito, si no es asi no ejecuta el movimiento
    int posicion = (uint32_t)index_actual.row;
    
    OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:posicion];
    NSString * removio = articulo_cafe.surtio;
    
    if ([removio isEqualToString:@""]) {
        [myTableView scrollToRowAtIndexPath:index_actual atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        [self ajuste_scroll:index_actual];
        [self presentViewController:alerta_orden animated:YES completion:nil];
        return false;
    }
    else
    {
        return true;
    }
}

//Verificacion vasos
-(void)verificacion_cantidad_vasos :(NSIndexPath *)index_path
{
    long sumatoria = 0;
    for (int contador = 0; contador<[arreglo_articulos_cafe count]; contador ++) {
        OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:contador];
        int surtido_acumulado = (uint32_t)[articulo_cafe.surtio integerValue];
        int anteriores =(uint32_t) [articulo_cafe.ventas_cafe integerValue];
        int actual = 0;
        if (!(surtido_acumulado == 0)) {
            actual = surtido_acumulado - anteriores;
        }

        sumatoria += actual;
    }
    
    if (sumatoria > cantidad_vasos)
    {
        OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:index_path.row];
        articulo_cafe.surtio = @"";
        contador_rellenado--;
        [arreglo_articulos_cafe replaceObjectAtIndex:index_path.row withObject:articulo_cafe];
        NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
        
        [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
        
        cantidad_vasos_superada = true;
    }
    else
    {
        cantidad_vasos_superada = false;
    }
}

//Verificacion ventas cafe anterior
-(void)verificacion_ventas_anterior :(NSIndexPath *)index_path
{
    OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:index_path.row];
    int surtio = (uint32_t)[articulo_cafe.surtio integerValue];
    int ventas_cafe_ant =(uint32_t) [articulo_cafe.ventas_cafe integerValue];
    
    if (![articulo_cafe.surtio isEqualToString:@""]) {
        if (ventas_cafe_ant > surtio)
        {
            articulo_cafe.surtio = @"";
            contador_rellenado--;
            [arreglo_articulos_cafe replaceObjectAtIndex:index_path.row withObject:articulo_cafe];
            NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
            
            [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
            
            ventas_incongruentes = true;
        }
        else
        {
            ventas_incongruentes = false;
        }
    }
}

-(void)inicializacion_alertas
{
    alerta_ventas = [UIAlertController
                        alertControllerWithTitle:@"Error"
                        message:@"Ventas menores a las anteriores."
                        preferredStyle:UIAlertControllerStyleAlert];
    
    alerta_vasos =  [UIAlertController
                     alertControllerWithTitle:@"Error"
                     message:@"Capacidad de vasos superada."
                     preferredStyle:UIAlertControllerStyleAlert];
    
    alerta_orden =  [UIAlertController
                     alertControllerWithTitle:@"Error"
                     message:@"Llenar la información en orden."
                     preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction* ok_ventas = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       alerta_presente = false;
                                       [alerta_ventas dismissViewControllerAnimated:YES completion:nil];
                                   }];
    UIAlertAction* ok_vasos = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       alerta_presente = false;
                                       [alerta_vasos dismissViewControllerAnimated:YES completion:nil];
                                   }];
    UIAlertAction* ok_orden = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   alerta_presente = false;
                                   [alerta_orden dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alerta_ventas addAction:ok_ventas];
    [alerta_vasos addAction:ok_vasos];
    [alerta_orden addAction:ok_orden];
}


- (IBAction)action_back:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)action_siguiente:(id)sender {
    for (int i = 0; i<[arreglo_articulos_cafe count]; i++) {
        OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:i];
        NSInteger cantidad = [articulo_cafe.surtio integerValue];
        NSInteger id_articulo = [articulo_cafe.id_articulo integerValue];
        NSInteger id_charola = [articulo_cafe.id_charola integerValue];
        NSInteger id_espiral = [articulo_cafe.id_espiral integerValue];
        NSInteger orden_servicio = [articulo_cafe.id_orden_servicio integerValue];
        NSInteger orden_vsita = [articulo_cafe.orden_visita integerValue];
        NSInteger instancia = id_instancia;
        
        [data_cafe_manager update_surtio_contadores:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
    }
     
     VC_Pruebas * vc_pruebas = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Pruebas"];
     vc_pruebas.id_instancia = id_instancia;
     vc_pruebas.salida_sitio = salida_sitio;
    [self.navigationController pushViewController:vc_pruebas animated:YES];
}

-(void)update_en_cero
{
    for (int i = 0; i<[arreglo_articulos_cafe count]; i++) {
        OBJ_Cafe * articulo_cafe = [arreglo_articulos_cafe objectAtIndex:i];
        NSInteger cantidad = 0;
        NSInteger id_articulo = [articulo_cafe.id_articulo integerValue];
        NSInteger id_charola = [articulo_cafe.id_charola integerValue];
        NSInteger id_espiral = [articulo_cafe.id_espiral integerValue];
        NSInteger orden_servicio = [articulo_cafe.id_orden_servicio integerValue];
        NSInteger orden_vsita = [articulo_cafe.orden_visita integerValue];
        NSInteger instancia = id_instancia;
        
        [data_cafe_manager update_surtio_contadores:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
    }
}

@end
