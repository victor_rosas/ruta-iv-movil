//
//  ProductosDAO.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 20/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Instancia_Doble.h"

@interface ProductosDAO : NSObject{
    sqlite3 *bd;
}
-(NSString *)obtenerRutaBD;
//SELECT
-(NSMutableArray *)select_articulos:(NSInteger)id_instancia :(NSInteger)orden_visita;

-(BOOL)verificacion_primera_visita:(NSInteger)id_instancia;
-(NSMutableDictionary *)CargarSurtidoAnterior:(NSInteger)instancia orden_servicio:(NSInteger)orden_servicio id_articulo:(NSInteger)id_articulo;
-(NSString *)obtener_codigo_autorizacion:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita;
-(NSString *)obtener_codigo_autorizacion_sitio:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita;
-(NSString *)obtener_codigo_revisita_instancia:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita;
-(NSString *)obtener_codigo_revisita_sitio:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita;
-(BOOL)actualizar_salteado:(NSInteger)valor_salteado instancia:(NSInteger)instancia;

//UPDATE
-(BOOL)update_inventario_inicial:(NSInteger)inventario_ini id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_surtio:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_caduco:(NSInteger)caduco id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_devolucion:(NSInteger)devolucion id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;

//INSTANCIA DOBLE
-(OBJ_Instancia_Doble*)getInstanciaRepetida:(NSInteger)instancia orden:(NSInteger)orden_visita;
-(BOOL)update_existencia:(NSInteger)inv_ini surtio:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo;
-(BOOL)update_existencia_removio:(NSInteger)inv_ini removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo;
-(BOOL)update_existencia_caduco:(NSInteger)inv_ini caduco:(NSInteger)caduco id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo;

//-(void)actualizarInventarioini:(NSInteger)inventario_ini ID_ARTICULO:(NSInteger)ID_ARTICULO instancia:(NSInteger)instancia;
@end
