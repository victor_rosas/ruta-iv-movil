//
//  TC_Cambio_Precio.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Cambio_Precio : UITableViewCell
{
    __weak IBOutlet UILabel *label_espiral;
    __weak IBOutlet UILabel *label_articulo;
    __weak IBOutlet UILabel *label_precio;
}
-(void)cell_setup :(NSString *)articulo espiral:(NSString *)nombre_espiral precio:(NSString *)precio_nuevo;
@end
