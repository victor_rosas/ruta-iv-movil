//
//  generales.m
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "generales.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation generales

-(BOOL)eliminaDumpDex{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM DUMPDEX"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)eliminaCambioMonedero{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CAMBIO_MONEDERO"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

@end
