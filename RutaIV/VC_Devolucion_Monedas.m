//
//  VC_Devolucion_Monedas.m
//  RutaIV
//
//  Created by Miguel Banderas on 17/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Devolucion_Monedas.h"
#import "CamaraUpload.h"
#import "VC_Devoluciones_Especie.h"
#import "OBJ_Articulo.h"

@interface VC_Devolucion_Monedas ()

@end

@implementation VC_Devolucion_Monedas
@synthesize id_instancia;
@synthesize visita;
@synthesize salida_sitio;
@synthesize tipo;

- (void)viewDidLoad {
    [super viewDidLoad];
    data_articulo_manager = [[ProductosDAO alloc]init];
    data_devoluciones_manager = [[DATA_Devoluciones alloc]init];
    back = false;
    [self inicializacion_alertas];
    actual_seleccion = 0;
}

-(void)viewDidAppear:(BOOL)animated
{
    if (back) {
        [self iniciar_cero];
    }
    [self pruebas];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(float)cantidad_monedas_total
{
    float monedas_50 = [field_monedas_50.text integerValue]*.5;
    NSInteger monedas_1 = [field_monedas_1.text integerValue]*1;
    NSInteger monedas_2 = [field_monedas_2.text integerValue]*2;
    NSInteger monedas_5 = [field_monedas_5.text integerValue]*5;
    
    float cantidad_total = monedas_50 + monedas_1 + monedas_2 + monedas_5;
    return cantidad_total;
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""]) {
        textField.text = @"0";
    }
    float cantidad = [self cantidad_monedas_total];
    field_cantidad.placeholder = [NSString stringWithFormat:@"%.01f", cantidad];
    field_cantidad.text = [NSString stringWithFormat:@"%.01f", cantidad];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1) {
        actual_seleccion = 0;
    }
    else
    {
        actual_seleccion = 1;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (actual_seleccion == 0) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 9) ? NO : YES;
    }
    return YES;
}

- (IBAction)action_back:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)action_next:(id)sender {
    
    if ([field_monedas_50.text isEqualToString:@""]) {
        field_monedas_50.text = @"0";
    }
    if ([field_monedas_1.text isEqualToString:@""]) {
        field_monedas_1.text = @"0";
    }
    if ([field_monedas_2.text isEqualToString:@""]) {
        field_monedas_2.text = @"0";
    }
    if ([field_monedas_5.text isEqualToString:@""]) {
        field_monedas_5.text = @"0";
    }
    
    float cantidad_place = [self cantidad_monedas_total];
    field_cantidad.placeholder = [NSString stringWithFormat:@"%.01f", cantidad_place];
    field_cantidad.text = [NSString stringWithFormat:@"%.01f", cantidad_place];
    
    NSString * contacto = field_contacto.text;
    NSString * telefono = field_telefono.text;
    NSString * cantidad = field_cantidad.text;
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        if ([contacto isEqualToString:@""]) {
            [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else if([telefono isEqualToString:@""])
        {
            [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else if([cantidad isEqualToString:@""])
        {
            [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else
        {
            float cantidad_actual = [field_cantidad.text floatValue];
            float cantidad_ideal = [field_cantidad.placeholder floatValue];
            if (!(cantidad_actual == cantidad_ideal)) {
                [self presentViewController:alerta_cantidad animated:YES completion:nil];
            }
            else
            {
                /*if (!(visita == 2)) {
                    [self alerta_siguiente];
                }
                else
                {
                    CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                    controller.instancia = id_instancia;
                    controller.salida_sitio = salida_sitio;
                    [self guardar_informacion];
                    [self.navigationController pushViewController:controller animated:YES];
                }*/
                [self guardar_informacion];
                [self.navigationController popViewControllerAnimated:true];
            }
            
        }
    });
}

-(void)alerta_siguiente
{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Continuar"
                                 message:@"Seleccionar una opción."
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    if (![tipo isEqualToString:@"Cafe"]) {
        UIAlertAction* action_especie = [UIAlertAction
                                         actionWithTitle:@"Devolución Especie"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             VC_Devoluciones_Especie * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devoluciones_especie"];
                                             controller.id_instancia = id_instancia;
                                             controller.salida_sitio = salida_sitio;
                                             controller.visitados = 2;
                                             [self guardar_informacion];
                                             [self.navigationController pushViewController:controller animated:YES];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
        [view addAction:action_especie];
    }
    
    UIAlertAction * fotografia = [UIAlertAction
                                  actionWithTitle:@"Fotografía"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                                      controller.instancia = id_instancia;
                                      controller.salida_sitio = salida_sitio;
                                      [self guardar_informacion];
                                      [self.navigationController pushViewController:controller animated:YES];
                                      [view dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancelar"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:fotografia];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)guardar_informacion
{
    if (![field_cantidad.text isEqualToString:@"0"]) {
        NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
        arreglo_articulos = [data_articulo_manager select_articulos:id_instancia :[orden_visita_instancia integerValue]];
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:0];
        
        NSString * id_orden_servicio = articulo.id_orden_servicio;
        NSString * orden_visita = articulo.orden_visita;
        NSString * instancia = [NSString stringWithFormat:@"%d", id_instancia];
        NSString * type = @"D";
        
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion];
        id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
        
        NSString * contacto = field_contacto.text;
        NSString * telefono = field_telefono.text;
        NSString * cantidad = field_cantidad.text;
        
        [data_devoluciones_manager insert_cab_devolucion_ruta:id_devolucion id_orden_servicio:id_orden_servicio id_instancia:instancia orden:orden_visita tipo:type];
        [data_devoluciones_manager insert_contacto_devolucion_ruta:id_devolucion nombre:contacto telefono:telefono cantidad:cantidad];
        
        if (![field_monedas_50.text isEqualToString:@"0"]) {
            NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
            id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
            NSString * id_moneda = @"1";
            NSInteger  cantidad_entero = [field_monedas_50.text integerValue] * .5;
            NSString * cantidad = [NSString stringWithFormat:@"%ld", (long)cantidad_entero];
            [data_devoluciones_manager insert_det_devolucion_ruta:id_devolucion id_devuleto:id_moneda cantidad:cantidad charola:0 espiral:0 inv_in:0];
        }
        if (![field_monedas_1.text isEqualToString:@"0"]) {
            NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
            id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
            NSString * id_moneda = @"2";
            NSInteger  cantidad_entero = [field_monedas_1.text integerValue];
            NSString * cantidad = [NSString stringWithFormat:@"%ld", (long)cantidad_entero];
            [data_devoluciones_manager insert_det_devolucion_ruta:id_devolucion id_devuleto:id_moneda cantidad:cantidad charola:0 espiral:0 inv_in:0];
        }
        if (![field_monedas_2.text isEqualToString:@"0"]) {
            NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
            id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
            NSString * id_moneda = @"3";
            NSInteger  cantidad_entero = [field_monedas_2.text integerValue] * 2;
            NSString * cantidad = [NSString stringWithFormat:@"%ld", (long)cantidad_entero];
            [data_devoluciones_manager insert_det_devolucion_ruta:id_devolucion id_devuleto:id_moneda cantidad:cantidad charola:0 espiral:0 inv_in:0];
        }
        if (![field_monedas_5.text isEqualToString:@"0"]) {
            NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
            id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
            NSString * id_moneda = @"4";
            NSInteger  cantidad_entero = [field_monedas_5.text integerValue] * 5;
            NSString * cantidad = [NSString stringWithFormat:@"%ld", (long)cantidad_entero];
            [data_devoluciones_manager insert_det_devolucion_ruta:id_devolucion id_devuleto:id_moneda cantidad:cantidad charola:0 espiral:0 inv_in:0];
        }
        
        back = true;
    }
}

-(void)iniciar_cero
{
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [data_articulo_manager select_articulos:id_instancia :[orden_visita_instancia integerValue]];
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:0];
    
    NSString * id_orden_servicio = articulo.id_orden_servicio;
    NSString * orden_visita = articulo.orden_visita;
    NSString * instancia = [NSString stringWithFormat:@"%d", id_instancia];
    
    [data_devoluciones_manager delete_ultimo_cab:id_orden_servicio instancia:instancia orden_visita:orden_visita];
    
    NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion];
    [data_devoluciones_manager delete_ultimo_contacto:id_devolucion];
    
    if (![field_monedas_50.text isEqualToString:@"0"]) {
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
        [data_devoluciones_manager delete_det_devolucion:id_devolucion];
    }
    if (![field_monedas_1.text isEqualToString:@"0"]) {
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
        [data_devoluciones_manager delete_det_devolucion:id_devolucion];
    }
    if (![field_monedas_2.text isEqualToString:@"0"]) {
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
        [data_devoluciones_manager delete_det_devolucion:id_devolucion];
    }
    if (![field_monedas_5.text isEqualToString:@"0"]) {
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
        [data_devoluciones_manager delete_det_devolucion:id_devolucion];
    }
}

-(void)pruebas
{
    [data_devoluciones_manager select_prueba_cab];
    [data_devoluciones_manager select_prueba_contacto];
    [data_devoluciones_manager select_prueba_det];
}

-(void)inicializacion_alertas
{
    alerta_cantidad = [UIAlertController
                       alertControllerWithTitle:@"Error"
                       message:@"Revisar cantidad total."
                       preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok_cantidad = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [alerta_cantidad dismissViewControllerAnimated:YES completion:nil];
                                  }];
    
    alerta_incompleto = [UIAlertController
                         alertControllerWithTitle:@"Error"
                         message:@"información Incompleta."
                         preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok_incompleta = [UIAlertAction
                                    actionWithTitle:@"Ok"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alerta_incompleto dismissViewControllerAnimated:YES completion:nil];
                                    }];
    
    [alerta_cantidad addAction:ok_cantidad];
    [alerta_incompleto addAction:ok_incompleta];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

@end
