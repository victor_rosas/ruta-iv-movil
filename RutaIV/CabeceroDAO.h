//
//  CabeceroDAO.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 09/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface CabeceroDAO : NSObject{
    sqlite3 *bd;
    
    NSString *Ordendeservicio;
    NSString *dif_hora_min;
    
    NSMutableArray *monedero;
    NSMutableArray *dumpdex;
    NSMutableArray *query09;
    NSMutableArray *cortes;
    NSMutableArray *query14;
    NSMutableArray *query12;
    NSMutableArray *query15;
    NSMutableArray *query16;
    NSMutableArray *query08;
    NSMutableArray *query07;
    NSMutableArray *fotos;
    NSMutableArray *horas;
    NSMutableArray *ventas;
    
    NSMutableDictionary *dicdumpex;
    NSMutableDictionary *dicmonedero;
    NSMutableDictionary *dicventasinstancia;
    NSMutableDictionary *dictablascortes;
    NSMutableDictionary *dicquery07;
    NSMutableDictionary *dicquery08;
    NSMutableDictionary *dicquery09;
    NSMutableDictionary *dicquery12;
    NSMutableDictionary *dicquery13;
    NSMutableDictionary *dicquery14;
    NSMutableDictionary *dicquery15;
    NSMutableDictionary *dicquery16;
    NSMutableDictionary *dicquery18;
    NSMutableDictionary *dicHoraruta;
    NSMutableDictionary *dicHorainstancia;
    NSMutableDictionary *dicfotos;
    
    
}

-(NSString *) obtenerRutaBD;
-(NSString *)CargarOrdendeservicio;
-(NSString *)Cargardifhoraminutos;

-(NSMutableArray *)CargarMonederoup;
-(NSMutableArray *)CargarDUMPDEX;
-(NSMutableArray *)CargarQuery09:(int)vlOrdenservicio;
-(NSMutableArray *)Cargartabla_cortes;

-(NSMutableArray *)Cargarventas_hist_instancia;
-(NSMutableArray *)CargarQuery07;
-(NSMutableArray *)CargarQuery08;
-(NSMutableArray*)CargarQuery12;
-(NSMutableDictionary *)CargarQuery13;
-(NSMutableArray *)CargarQuery14;
-(NSMutableArray *)CargarQuery15;
-(NSMutableArray *)CargarQuery16;
-(NSMutableDictionary *)CargarQuery18;
-(NSMutableDictionary *)Acthorasalidallegadaruta;
-(NSMutableArray *)Acthorasalidallegadainstancia;
-(NSMutableArray *)CargarFOTOS_VISITA;
-(BOOL)ActualizarCargado;
-(BOOL)ActualizarFecha_orden_procesar;
-(BOOL)Actualizarcab_orden_servicio:(int)id_orden_servicio;
@end