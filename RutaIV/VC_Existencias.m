//
//  VC_Existencias.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Existencias.h"
#import "OBJ_Cafe.h"
#import "OBJ_Articulo.h"

@interface VC_Existencias ()

@end

@implementation VC_Existencias
@synthesize array_existencias;
@synthesize array_nombres;
@synthesize arreglo_articulos;
@synthesize origen;

- (void)viewDidLoad {
    [super viewDidLoad];
    array_keys = [array_existencias allKeys];
    arreglo_sugerido = [[NSMutableArray alloc]init];
    [self calcular_sugerencia];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [myTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [array_nombres count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TC_Existencias *cell = [myTableView dequeueReusableCellWithIdentifier:@"Cell_Existencias" forIndexPath:indexPath];
    NSString *llave = [array_keys objectAtIndex:indexPath.row];
    
    int existencia = (uint32_t)[[array_existencias objectForKey:llave] integerValue];
    NSString *producto = [array_nombres objectForKey:llave];
    
    NSString * sugerencia = [arreglo_sugerido objectAtIndex:indexPath.row];
    
    if ([sugerencia integerValue]>existencia) {
        sugerencia = [NSString stringWithFormat:@"%d",existencia];
    }
    
    [cell setupCell:producto cantidad:existencia id_articulo:sugerencia];
    return cell;
}

-(void)calcular_sugerencia
{
    for (int i = 0; i<[array_nombres count]; i++) {
        NSString * llave = [array_keys objectAtIndex:i];
        NSString * nombre = [array_nombres objectForKey:llave];
        int total = 0;
        for (int j = 0; j<[arreglo_articulos count]; j++) {
            if ([origen isEqualToString:@"Cafe"]) {
                OBJ_Cafe * cafe = [arreglo_articulos objectAtIndex:j];
                if ([cafe.nombre_articulo isEqualToString:nombre]) {
                    total += [cafe.capacidad integerValue];
                }
                NSString * total_string = [NSString stringWithFormat:@"%d",total];
                [arreglo_sugerido addObject:total_string];
            }
            else
            {
                OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:j];
                if ([articulo.articulo isEqualToString:nombre]) {
                    total += [articulo.capacidad integerValue] - [articulo.inventario_inicial_real integerValue];
                }
            }
        }
        NSString * total_string = [NSString stringWithFormat:@"%d",total];
        [arreglo_sugerido addObject:total_string];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
