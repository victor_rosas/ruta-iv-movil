//
//  inventariosInstancia.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface inventariosInstancia : NSObject

-(BOOL)borraInvInstancia;

-(BOOL)insertaInvInstancia:(NSString *)sDescripcion wIdInstancia:(int)nIdInstancia wIdEspiral:(int)nIdEspiral wClave:(int)nClave wExistencia:(int)nExistencia wIdOrdenServicio:(int)nIdOrdenServicio wCapacidadEspiral:(int)nCapacidadEspiral wFracciones:(int)nFracciones;

@end
