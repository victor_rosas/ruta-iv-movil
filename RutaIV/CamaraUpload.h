//
//  CamaraUpload.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 13/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagenDAO.h"
#import "GRRequestsManager.h"
#import "Hora_inicioDAO.h"
#import "CabeceroOperaciones.h"
#import "Monedero_y_contadoresDAO.h"
#import "DATA_Progreso_Ruta.h"


@interface CamaraUpload : UIViewController<UIImagePickerControllerDelegate>{
    NSString * fullstring;
    bool reachable;
    NSInteger pendientes;
    NSInteger i;
    NSInteger list;
    NSInteger horaenviada;
    ImagenDAO *dao;
    Hora_inicioDAO *dao_hora;
    CabeceroOperaciones *cabecerooperaciones;
    Monedero_y_contadoresDAO *monederoycontadores;
    DATA_Progreso_Ruta * data_manager_progreso;
    
    NSMutableArray *imagenes;
    BOOL datosmoviles;
    __weak IBOutlet UITextField *db_count;
    __weak IBOutlet UISwitch *_Wifiswitch;
    __weak IBOutlet UILabel *Estatus;
}
@property (nonatomic, strong) IBOutlet UITextField *hostnameTextField;
@property (nonatomic, strong) IBOutlet UITextField *usernameTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property(nonatomic,strong) IBOutlet UIImageView *imageView;
@property(nonatomic,strong) ImagenDAO *dao;
@property(nonatomic,strong) NSMutableArray *imagenes;
@property int instancia;

- (IBAction)listCLick:(id)sender;
- (IBAction)FotoClick:(id)sender;
- (IBAction)action_continuar:(id)sender;


-(void)Uploadall;
-(void)Deleteall;
//Variables horas llegada
@property BOOL salida_sitio;

@end
