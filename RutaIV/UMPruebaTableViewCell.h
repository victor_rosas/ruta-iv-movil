//
//  UMPruebaTableViewCell.h
//  RutaIV
//
//  Created by Miguel Banderas on 19/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
//#import "MGSwipeTableCell.h"

@interface UMPruebaTableViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label_sitio;
@property (weak, nonatomic) IBOutlet UILabel *Label;
@property (weak, nonatomic) IBOutlet UILabel *label_codigo;
@property (weak, nonatomic) IBOutlet UILabel *label_continuar;
@property (weak, nonatomic) IBOutlet UILabel *label_nueva;
@property (weak, nonatomic) IBOutlet UILabel *label_orden;


@end
