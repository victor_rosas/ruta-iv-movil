//
//  cabeceroOrden.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cabeceroOrden : NSObject

-(BOOL)borrarCabecero;

-(BOOL)insertarCabecero:(int)nIdOrdenServicio wIdRuta:(int)nIdRuta wFecha:(NSString *)sFecha wHoraSalida:(NSString *)sHoraSalida wHoraLlegada:(NSString *)sHoraLlegada wInactiva:(int)nInactiva wFechaCreacion:(NSString *)sFechaCreacion wIdUsuario:(int)nIdUsuario wTipo:(NSString *)sTipo wProcesadoCafe:(int)nProcesadoCafe wSugerirDex:(int)nSugerirDex;

@end
