//
//  clavesCodigoBarras.m
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "clavesCodigoBarras.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation clavesCodigoBarras

-(BOOL)borraClavesCodBarras{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CLAVE_CODIGO_BARRAS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaClavesCodBarras:(int)nIdOrdenServicio wOrdenVisita:(int)nOrdenVisita wIdInstancia:(int)nIdInstancia wClave:(int)nClave{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CLAVE_CODIGO_BARRAS(\"ID_ORDEN_SERVICIO\",\"ORDEN_VISITA\",\"ID_INSTANCIA\",\"CLAVE\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\")",nIdOrdenServicio,nOrdenVisita,nIdInstancia,nClave];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;

}

-(NSMutableArray *)cargarClavesCodBarras{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA FROM CLAVE_CODIGO_BARRAS"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                [instancias addObject:instancia];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return instancias;
}


@end
