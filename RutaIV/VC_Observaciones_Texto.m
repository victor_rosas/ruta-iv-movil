//
//  VC_Observaciones_Texto.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Observaciones_Texto.h"

@interface VC_Observaciones_Texto ()

@end

@implementation VC_Observaciones_Texto
@synthesize id_clasificacion;
@synthesize atencion;
@synthesize instancia;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    //Inicializar la vista
    text_observaciones.text = @"Escribir observaciones en este espacio.";
    text_observaciones.textColor = [UIColor lightGrayColor];
    text_observaciones.delegate = self;
    
    //Inicializacion de variables
    data_observaciones_manager = [[DATA_Observaciones alloc]init];
    data_cabecero_manager = [[CabeceroDAO alloc]init];
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    text_observaciones.text = @"";
    text_observaciones.textColor = [UIColor blackColor];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)action_back_texto:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)action_next_texto:(id)sender {
    //[data_observaciones_manager prueba_borrar];
    
    //Establecimiento de las variables a guardar
    int id_reporte = (uint32_t)[[data_observaciones_manager traer_id_reporte] integerValue] + 1;
    
    NSDate * fecha = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *fecha_actual = [dateFormat stringFromDate:fecha];
    
    NSString * texto_descripcion = text_observaciones.text;
    
    int orden_servicio = (uint32_t)[[data_cabecero_manager CargarOrdendeservicio] integerValue];
    
    int visita = (uint32_t)[[data_observaciones_manager trae_orden_visita:instancia] integerValue];
    
    int id_maquina = (uint32_t)[[data_observaciones_manager trae_id_maquina:instancia] integerValue];
    
    //Insert
    [data_observaciones_manager insertar_observacion:id_reporte clasificacion:id_clasificacion empleado:1 fecha:fecha_actual texto_descripcion:texto_descripcion orden_servicio:orden_servicio instancia:instancia visita:visita maquina:id_maquina bit_servicio:atencion fecha_atencion:0 contacto:0 tel_contacto:0 bit_subido:0];
    
    //Regresar a la pantalla inicial
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"La observación ha sido guardada exitosamente."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             NSArray *array = [self.navigationController viewControllers];
                             [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
                         }];

    [alert addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

@end
