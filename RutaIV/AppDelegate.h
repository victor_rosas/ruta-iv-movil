//
//  AppDelegate.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (retain, nonatomic) NSString *dataBaseName;
@property (retain, nonatomic) NSString *dataBasePath;

@end
