//
//  VC_Mapa.h
//  RutaIV
//
//  Created by Miguel Banderas on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DATA_Mapa.h"

@interface VC_Mapa : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>{
    DATA_Mapa * Data_mapa;
}

@property(nonatomic, retain) IBOutlet MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;
@end
