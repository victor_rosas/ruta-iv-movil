//
//  DATA_Cambio.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBJ_Log_Proceso.h"

@interface DATA_Cambio : NSObject{

    OBJ_Log_Proceso *logProceso;

}
-(NSString *) obtenerRutaBD;
-(NSMutableArray *)cargar_cambio_planograma :(int)id_instancia;
-(NSMutableArray *)cargar_cambio_precio :(int)id_instancia;
-(NSString *)cargar_nombre_articulo :(int)id_articulo;
-(NSString *)cargar_nombre_espiral :(int)id_espiral charola:(int)id_charola instancia:(int)id_instancia;
-(BOOL)update_inicial_removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(void)prueba_removio :(int)instancia;

@end
