//
//  DATA_Cambio.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "DATA_Cambio.h"
#import <sqlite3.h>
#import "AppDelegate.h"
#import "OBJ_Cambio_Planograma.h"
#import "OBJ_Cambio_Precio.h"

@implementation DATA_Cambio

-(NSString *) obtenerRutaBD
{   logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSMutableArray *)cargar_cambio_planograma :(int)id_instancia
{  [logProceso escribir:@"#Data_Cambio:Entro cargar_cambio_planograma"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSMutableArray * arreglo_mutable_planograma = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CAMBIO_PLANOGRAMA WHERE ID_INSTANCIA = %d AND CAMBIO = 'V'", id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * surtio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * removio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * clave = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * id_orden_servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * id_espiral = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * id_charola = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * capacidad = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_planograma = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * espiral = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * cambio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                
                OBJ_Cambio_Planograma * cambio_planograma = [[OBJ_Cambio_Planograma alloc]init:id_articulo surtio:surtio removio:removio clave:clave orden:orden_visita id_orden_servicio:id_orden_servicio id_espiral:id_espiral id_charola:id_charola capacidad:capacidad id_planograma:id_planograma articulo:articulo espiral:espiral cambio:cambio id_instancia:id_instancia];
                [arreglo_mutable_planograma addObject:cambio_planograma];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cambio:Salio cargar_cambio_planograma"];
    return arreglo_mutable_planograma;
}


-(NSMutableArray *)cargar_cambio_precio :(int)id_instancia
{
    [logProceso escribir:@"#Data_Cambio:Entro cargar_cambio_precio"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSMutableArray * arreglo_mutable_precios = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CAMBIO_PRECIOS WHERE ID_INSTANCIA = %d", id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_charola = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * id_espiral = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * id_seleccion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * id_articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * actual = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * anterior = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                
                OBJ_Cambio_Precio * cambio_precio = [[OBJ_Cambio_Precio alloc]init:id_instancia id_charola:id_charola id_espiral:id_espiral id_seleccion:id_seleccion id_articulo:id_articulo actual:actual anterio:anterior];
                [arreglo_mutable_precios addObject:cambio_precio];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cambio:Salio cargar_cambio_precio"];
    return arreglo_mutable_precios;
}

-(NSString *)cargar_nombre_articulo :(int)id_articulo
{
    [logProceso escribir:@"#Data_Cambio:Entro cargar_nombre_articulo"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSString * articulo;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ARTICULO FROM MOVIMIENTOS_INSTANCIA  WHERE ID_ARTICULO =%d", id_articulo];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cambio:Salio cargar_nombre_articulo"];
    return articulo;
}

-(NSString *)cargar_nombre_espiral :(int)id_espiral charola:(int)id_charola instancia:(int)id_instancia
{   [logProceso escribir:@"#Data_Cambio:Entro cargar_nombre_espiral"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSString * espiral;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ESPIRAL FROM MOVIMIENTOS_INSTANCIA  WHERE ID_ESPIRAL = %d AND ID_CHAROLA = %d AND ID_INSTANCIA = %d", id_espiral, id_charola, id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                espiral = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cambio:Salio cargar_nombre_articulo"];
    return espiral;
}

-(BOOL)update_inicial_removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{   [logProceso escribir:@"#Data_Cambio:Entro update_inicial_removio"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET REMOVIO = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO = 'V'",(long)removio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#Data_Cambio #update_inicial_removio Error en la creacion del update"];
        }
    }else{
       [logProceso escribir:@"#Data_Cambio #update_inicial_removio No se ha podido abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Data_Cambio:Salio update_inicial_removio"];
    return bValida;
}

-(void)prueba_removio :(int)instancia
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT INV_INI, REMOVIO FROM MOVIMIENTOS_INSTANCIA  WHERE ID_INSTANCIA = %d AND CAMBIO = 'V'", instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
}




@end
