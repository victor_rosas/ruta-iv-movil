//
//  Data_Cafe.h
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface Data_Cafe : NSObject
{
    sqlite3 *bd;
    OBJ_Log_Proceso *logProceso;
}
-(NSString *) obtenerRutaBD;

//SELECT
-(NSMutableArray *)select_articulos:(NSInteger)id_instancia;
-(NSMutableArray *)select_articulos_contadores:(NSInteger)id_instancia;
-(NSString *)select_cantidad_vasos:(NSInteger)id_instancia;
-(NSString *)select_maximo_precio:(NSInteger)id_instancia;

//UPDATE
-(BOOL)update_surtio_insumos:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_surtio_contadores:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;
-(BOOL)update_surtio_pruebas:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia;

@end
