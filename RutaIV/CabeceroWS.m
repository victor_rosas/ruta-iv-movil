//
//  CabeceroWS.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "CabeceroWS.h"


@implementation CabeceroWS


-(NSMutableArray *)borraCadenaGprs: (int)vlOrdenServicio boolean:(BOOL)boolean{
    
    NSMutableArray *cadenagprs;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* _sWebService = [prefs stringForKey:@"sWebService"];
    NSString *urlString = [NSString stringWithFormat:@"%@borraCadenaGprs?nIdOrdenServicio=%d",_sWebService,vlOrdenServicio];
    
    NSLog(@"%@",urlString);
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers error:&error];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontro informacion";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    return cadenagprs;
    
}


-(NSMutableArray *)insertaCadenaGprs: (NSString*)sCadenaCM{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* _sWebService = [prefs stringForKey:@"sWebService"];
    NSString *urlString = [NSString stringWithFormat:@"%@insertacadenaGprs?sCadenaCM=%@",_sWebService,sCadenaCM];
    
    NSLog(@"%@",urlString);
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers error:&error];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontro informacion";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    NSMutableArray *A;
    
    return A;
}

-(NSMutableArray*)actualizaCabOrdenServicio:(NSString *)sCadenaHSE{
    NSMutableArray *A;
    
    return A;
    
}

-(NSMutableArray *)interpretaGprs:(NSString*)sCadenaGprs{
    NSMutableArray *A;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* _sWebService = [prefs stringForKey:@"sWebService"];
    NSString *urlString = [NSString stringWithFormat:@"%@interpretaGprs?sCadenaGeneral=%@",_sWebService,sCadenaGprs];
    
    NSLog(@"%@",urlString);
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers error:&error];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontro informacion";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    
    return A;
    
}

-(NSMutableArray *)interpretaDumDex: (int)vlOrdenServicio boolean:(BOOL)boolean{
    NSMutableArray *A;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* _sWebService = [prefs stringForKey:@"sWebService"];
    NSString *urlString = [NSString stringWithFormat:@"%@interpretaDumDex?IdOrdenServicio=%d",_sWebService,vlOrdenServicio];
    
    NSLog(@"%@",urlString);
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers error:&error];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontro informacion";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            
            
        }
    }
    
    return A;
}


-(void)XMLprueba{
 



}







@end
