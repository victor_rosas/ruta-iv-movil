//
//  OBJ_Articulo.m
//  RutaIV
//
//  Created by Miguel Banderas on 05/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Articulo.h"

@implementation OBJ_Articulo

@synthesize id_instancia;
@synthesize id_articulo;
@synthesize inv_ini;
@synthesize surtio;
@synthesize removio;
@synthesize caduco;
@synthesize clave;
@synthesize orden_visita;
@synthesize id_orden_servicio;
@synthesize id_espiral;
@synthesize id_charola;
@synthesize capacidad;
@synthesize articulo;
@synthesize precio;
@synthesize espiral;
@synthesize cambio;
@synthesize inventario_inicial_real;
@synthesize devolucion;

-(id)init :(NSString *)instancia id_articulo:(NSString *)numero_articulo inv_ini:(NSString *)inventario_inicial surtio:(NSString *)surtimiento removio:(NSString *)removidos caduco:(NSString *)caducos clave:(NSString *)numero_clave orden_visita:(NSString *)visita id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)numero_espiral id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad articulo:(NSString *)nombre_articulo precio:(NSString *)numero_precio espiral:(NSString *)nombre_espiral cambio:(NSString *)cambio_planograma
{
    self.id_instancia = instancia;
    self.id_articulo = numero_articulo;
    self.inv_ini = @"";
    self.surtio = surtimiento;
    self.removio = removidos;
    self.caduco = caducos;
    self.clave = numero_clave;
    self.orden_visita = visita;
    self.id_orden_servicio = orden_servicio;
    self.id_espiral = numero_espiral;
    self.id_charola = charola;
    self.capacidad = numero_capacidad;
    self.articulo = nombre_articulo;
    self.precio = numero_precio;
    self.espiral = nombre_espiral;
    self.cambio = cambio_planograma;
    self.inventario_inicial_real = inventario_inicial;
    self.devolucion = @"0";
    
    return self;
}

@end
