//
//  VC_Configuracion_Camara.m
//  RutaIV
//
//  Created by Miguel Banderas on 28/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Configuracion_Camara.h"

@interface VC_Configuracion_Camara ()

@end

@implementation VC_Configuracion_Camara

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *elementos_camara = [[NSUserDefaults standardUserDefaults] objectForKey:@"elementos_camara"];
    label_direccion.text = [elementos_camara objectAtIndex:0];
    label_usuario.text = [elementos_camara objectAtIndex:1];
    label_password.text = [elementos_camara objectAtIndex:2];
    // Do any additional setup after loading the view.
    
    [self button_setup];
    array_text_fields = [[NSMutableArray alloc]init];
    [array_text_fields addObject:label_direccion];
    [array_text_fields addObject:label_usuario];
    [array_text_fields addObject:label_password];
    
    label_direccion.inputAccessoryView = keyboardDoneButtonView;
    label_usuario.inputAccessoryView = keyboardDoneButtonView;
    label_password.inputAccessoryView = keyboardDoneButtonView;
    [label_direccion becomeFirstResponder];
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 30)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    UILabel * next = [array_text_fields objectAtIndex:current];
    [next becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == label_direccion) {
        current = 1;
    }
    if (textField == label_usuario) {
        current = 2;
    }
    if (textField == label_password) {
        current = 0;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)action_guardar_cambios:(id)sender {
    NSString *direccion = @"ftp://173.192.80.226";//[NSString stringWithFormat:@"ftp://%@",label_direccion.text];
    NSString *password = @"ftpImgBodies";//label_password.text;
    NSString *usuario = @"Ads720510.";//label_usuario.text;
    NSArray *elementos_camara = [[NSArray alloc]initWithObjects:direccion, usuario, password, nil];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:elementos_camara
                     forKey:@"elementos_camara"];
    // – setBool:forKey:
    // – setFloat:forKey:
    // in your case
    [userDefaults synchronize];
    
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}


@end
