//
//  almacenes.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface almacenes : NSObject

-(BOOL)borraAlmacenes;

-(BOOL)insertaAlmacenes:(int)nIdAlmacen wNombre:(NSString *)sNombre;

@end
