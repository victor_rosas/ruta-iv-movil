//
//  planograma.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface planograma : NSObject

-(BOOL)borraPlanograma;

-(BOOL)insertaPlanograma:(int)nIdArticulo wSurtio:(double)dSurtio wRemovio:(double)dRemovio wClave:(int)nClave wOrdenVisita:(int)nOrdenVisita wIdOrdenServicio:(int)nIdOrdenServicio wIdEspiral:(int)nIdEspiral wIdCharola:(int)nIdCharola wCapacidadEspiral:(int)nCapacidadEspiral wIdPlanograma:(int)nIdPlanograma wArticulo:(NSString *)sArticulo wEspiral:(NSString *)sEspiral wCambio:(NSString *)sCambio wIdInstancia:(int)nIdInstancia;

@end
