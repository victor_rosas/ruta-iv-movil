//
//  fechaOrdenServicio.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface fechaOrdenServicio : NSObject

-(BOOL)borraFechaOrdenServicio;

-(BOOL)insertaFechaOrdenServicio:(int)nCargado wFecha:(NSString *)sFecha wNoDia:(int)nNoDia;

@end
