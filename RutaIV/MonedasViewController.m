//
//  MonedasViewController.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 26/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "MonedasViewController.h"
#import "Monedero_y_contadoresDAO.h"
#import "CamaraUpload.h"
#import "VC_Devoluciones_Especie.h"
#import "VC_Devolucion_Monedas.h"
#import "VC_Surtir.h"

@interface MonedasViewController ()

@end

@implementation MonedasViewController
@synthesize instancia;
@synthesize salida_sitio;
@synthesize tipo;

- (void)viewDidLoad {
    [super viewDidLoad];
    dao = [[Monedero_y_contadoresDAO alloc]init];
    orden_servicio =(uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"]integerValue];
    // Do any additional setup after loading the view.
    [self button_setup];
    //tipo = [[NSUserDefaults standardUserDefaults]objectForKey:@"tipo_maquina"];
    
    array_text_fields = [[NSMutableArray alloc]init];
    [array_text_fields addObject:Monedas_50];
    [array_text_fields addObject:Monedas_01];
    [array_text_fields addObject:Monedas_02];
    [array_text_fields addObject:Monedas_05];
    [array_text_fields addObject:Monedas_10];
    
    Monedas_50.inputAccessoryView = keyboardDoneButtonView;
    Monedas_01.inputAccessoryView = keyboardDoneButtonView;
    Monedas_02.inputAccessoryView = keyboardDoneButtonView;
    Monedas_05.inputAccessoryView = keyboardDoneButtonView;
    Monedas_10.inputAccessoryView = keyboardDoneButtonView;
    
    [Monedas_50 becomeFirstResponder];
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 9) ? NO : YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneClicked:(id)sender
{
    UILabel * next = [array_text_fields objectAtIndex:current];
    [next becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == Monedas_50) {
        current = 1;
    }
    if (textField == Monedas_01) {
        current = 2;
    }
    if (textField == Monedas_02) {
        current = 3;
    }
    if (textField == Monedas_05) {
        current = 4;
    }
    if (textField == Monedas_10) {
        current = 0;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Guardar:(id)sender {
    
   int cent50 = (uint32_t)[Monedas_50.text integerValue];
   int peso1 = (uint32_t)[Monedas_01.text integerValue];
   int peso2 = (uint32_t)[Monedas_02.text integerValue];
   int peso5 = (uint32_t)[Monedas_05.text integerValue];
   int peso10 = (uint32_t)[Monedas_10.text integerValue];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"ERROR"
                                  message:@"Llenar todos los campos."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert addAction:ok];
    
    if ([Monedas_01.text isEqualToString:@""]) {
        [self presentViewController:alert animated:YES completion:nil];
        performo_segue = false;
    }
    else if ([Monedas_02.text isEqualToString:@""])
    {
        [self presentViewController:alert animated:YES completion:nil];
        performo_segue = false;
    }
    else if ([Monedas_10.text isEqualToString:@""])
    {
        [self presentViewController:alert animated:YES completion:nil];
        performo_segue = false;
    }
    else if([Monedas_05.text isEqualToString:@""])
    {
        [self presentViewController:alert animated:YES completion:nil];
        performo_segue = false;
    }
    else if([Monedas_50.text isEqualToString:@""])
    {
        [self presentViewController:alert animated:YES completion:nil];
        performo_segue = false;
    }
    else
    {   int ordenvisita =(uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"orden_visita_seleccionado"]integerValue];
        [dao InsertaMoneda:orden_servicio ID_INSTANCIA:instancia ID_MONEDA:1 CANTIDAD:cent50 ORDEN_VISITA:ordenvisita];
        [dao InsertaMoneda:orden_servicio ID_INSTANCIA:instancia ID_MONEDA:2 CANTIDAD:peso1 ORDEN_VISITA:ordenvisita];
        [dao InsertaMoneda:orden_servicio ID_INSTANCIA:instancia ID_MONEDA:3 CANTIDAD:peso2 ORDEN_VISITA:ordenvisita];
        [dao InsertaMoneda:orden_servicio ID_INSTANCIA:instancia ID_MONEDA:4 CANTIDAD:peso5 ORDEN_VISITA:ordenvisita];
        [dao InsertaMoneda:orden_servicio ID_INSTANCIA:instancia ID_MONEDA:6 CANTIDAD:peso10 ORDEN_VISITA:ordenvisita];
        performo_segue = true;
    }
    
    
    if (performo_segue) {
        if (![tipo isEqualToString:@"Cafe"]) {
            VC_Surtir * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_surtir"];
            controller.instancia = instancia;
            controller.salida_sitio = salida_sitio;
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        else
        {
            CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];/*@"VC_Camara"];*/
            controller.instancia = instancia;
            controller.salida_sitio = salida_sitio;
            [self.navigationController pushViewController:controller animated:YES];
        }
        //[self alerta_siguiente];
    }
    
}

//Pasar a la pantalla de surtimiento
-(void)alerta_siguiente
{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Continuar"
                                 message:@"Seleccionar una opción."
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    if (![tipo isEqualToString:@"Cafe"]) {
        UIAlertAction* action_especie = [UIAlertAction
                                         actionWithTitle:@"Devolución Especie"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             VC_Devoluciones_Especie * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devoluciones_especie"];
                                             controller.id_instancia = instancia;
                                             controller.salida_sitio = salida_sitio;
                                             controller.visitados = 1;
                                             [self.navigationController pushViewController:controller animated:YES];
                                             [view dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
        [view addAction:action_especie];
    }
    
    UIAlertAction* action_moneda = [UIAlertAction
                             actionWithTitle:@"Devolución Dinero"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 VC_Devolucion_Monedas * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devolucion_Moneda"];
                                 controller.id_instancia = instancia;
                                 controller.salida_sitio = salida_sitio;
                                 controller.visita = 1;
                                 controller.tipo = tipo;
                                 [self.navigationController pushViewController:controller animated:YES];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    [view addAction:action_moneda];
    
    
    UIAlertAction * fotografia = [UIAlertAction
                               actionWithTitle:@"Fotografía"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                                   controller.instancia = instancia;
                                   controller.salida_sitio = salida_sitio;
                                   [self.navigationController pushViewController:controller animated:YES];
                                   [view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancelar"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:fotografia];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)action_back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

-(int)Traerordenvisita{
    int ordenvisita = (uint32_t)[[dao obtenerorden_visita:instancia]integerValue];
    return ordenvisita;
}

@end
