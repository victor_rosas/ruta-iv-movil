//
//  DATA_Progreso_Ruta.h
//  RutaIV
//
//  Created by Miguel Banderas on 25/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface DATA_Progreso_Ruta : NSObject
{
    sqlite3 * bd;
    OBJ_Log_Proceso *logProceso;
    
}
-(void)mandar_hora :(int)id_orden_servicio orden:(int)orden orden_visita:(int)orden_visita id_instancia:(int)id_instancia id_operacion:(int)id_operacion;
-(NSMutableDictionary *)SELECT_datos_progreso:(int)id_instancia;
-(NSMutableArray *)SELECT_instancias_sitio :(NSInteger)id_sitio;
@end
