//
//  TC_Cafe.m
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TC_Cafe.h"

@implementation TC_Cafe
@synthesize text_field_cantidad;

- (void)awakeFromNib {
    text_field_cantidad.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)cell_setup :(NSString *)articulo numeracion:(NSString *)id_numeracion capacidad:(NSString *)capacidad_espiral costo:(NSString *)numero_costo
{
    label_numeracion.layer.cornerRadius = 5;
    label_numeracion.layer.masksToBounds = TRUE;
    
    label_articulo.text = articulo;
    label_numeracion.text = id_numeracion;
    label_capacidad.text = capacidad_espiral;
    label_costo.text = numero_costo;
}


@end
