//
//  VC_Insumos.h
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data_Cafe.h"
#import "ExistenciasDAO.h"
#import "ProductosDAO.h"

@interface VC_Insumos : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    Data_Cafe * data_cafe_manager;
    ProductosDAO * data_articulo_manager;
    ExistenciasDAO * data_existencias_manager;
    
    NSMutableArray * arreglo_articulos_cafe;
    NSMutableDictionary * diccionario_existencias;
    NSMutableDictionary * arreglo_productos;
    int contador_rellenado;
    BOOL capacidad_espiral_superada;
    BOOL capacidad_existencia_superada;
    BOOL productos_orden;
    BOOL alerta_presente;
    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_orden;
    UIAlertController * alerta_capacidad;
    UIAlertController * alerta_existencia;
    
    UIToolbar* keyboardDoneButtonView;
    __weak IBOutlet UIBarButtonItem *btn_siguiente;
    __weak IBOutlet UITableView *myTableView;
    __weak IBOutlet UIButton *btn_siguient;
}
@property int id_instancia;
@property BOOL salida_sitio;
- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
