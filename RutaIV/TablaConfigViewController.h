//
//  TablaConfigViewController.h
//  RutaIV
//
//  Created by Miguel Banderas on 16/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TablaConfigViewController : UITableViewController
{
    NSMutableArray *menu;
}

@property (copy,nonatomic) NSArray *menu;
- (IBAction)bak_pressed:(id)sender;

@end
