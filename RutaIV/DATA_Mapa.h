//
//  DATA_Mapa.h
//  RutaIV
//
//  Created by Miguel Banderas on 25/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface DATA_Mapa : NSObject{
    sqlite3 *bd;
    OBJ_Log_Proceso *logProceso;
}

-(NSString *) obtenerRutaBD;
-(NSMutableDictionary *)cargar_localizacion;
@end
