//
//  VC_Devoluciones_Especie.m
//  RutaIV
//
//  Created by Miguel Banderas on 17/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Devoluciones_Especie.h"
#import "TC_Cambio_Planograma.h"
#import "OBJ_Articulo.h"
#import "CamaraUpload.h"
#import "VC_Devolucion_Monedas.h"

@interface VC_Devoluciones_Especie ()

@end

@implementation VC_Devoluciones_Especie
@synthesize id_instancia;
@synthesize salida_sitio;
@synthesize visitados;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    arreglo_articulos = [[NSMutableArray alloc]init];
    data_articulo_manager = [[ProductosDAO alloc]init];
    data_devoluciones_manager = [[DATA_Devoluciones alloc]init];
    inventario_superado = false;
    alerta_presente = false;
    field_cantidad.placeholder = @"0";
    back = false;
    field_seleccionado = 0;
    
    [self inicializacion_alertas];
    
    //Inicializacion de vistas
    [self button_setup];
    field_contacto.inputAccessoryView = keyboardDoneButtonView;
    field_telefono.inputAccessoryView = keyboardDoneButtonView;
    field_cantidad.inputAccessoryView = keyboardDoneButtonView;
    
    //Cargar datos
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [data_articulo_manager select_articulos:id_instancia :[orden_visita_instancia integerValue]];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = [NSIndexPath indexPathForRow:0 inSection:0];
    [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
    
    if (back) {
        [self update_en_cero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Aspecto y funciones de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_articulos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identifier = @"TC_Devolucion";
    
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    NSString * capacidad = [NSString stringWithFormat:@"C.E. - %@", articulo.capacidad];
    
    //NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    //NSString * precio = [formatter stringFromNumber:[NSNumber numberWithFloat:[articulo.precio integerValue]]];
    
    //Creacion de la celda
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    cell.text_field_cantidad.text = articulo.devolucion;
    if (![articulo.devolucion isEqualToString:@"0"])
    {
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    NSString * nombre_articulo = articulo.articulo;
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        nombre_articulo = [nombre_articulo capitalizedString];
    }
    
    [cell cell_setup:nombre_articulo espiral:articulo.espiral cantidad:articulo.devolucion capacidad:capacidad];
    
    //Se agrega boton a todos los text fields de las celdas
    cell.text_field_cantidad.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
}

//Seleccion de celda cuando se inicia modificacion en TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int categoria_field = (uint32_t)[self revisar_tipo_field:textField];
    if (categoria_field == 0) {
        TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
        NSIndexPath * index_path = [myTableView indexPathForCell:cell];
        
        if (inventario_superado) {
            dispatch_async(dispatch_get_main_queue(), ^ {
                [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
                [myTableView selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
                
                TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:index_path_pasado];
                UITextField * text_field = cell.text_field_cantidad;
                [text_field becomeFirstResponder];
                
                [self ajuste_scroll:index_path_pasado];
                if (!alerta_presente) {
                    alerta_presente = true;
                    [self presentViewController:alerta_capacidad animated:YES completion:nil];
                }
                
            });
        }
        else
        {
            index_path_pasado = index_path;
            [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
            [self ajuste_scroll:index_path];
        }
    }
    else if (categoria_field == 1)
    {
        field_seleccionado = 1;
    }
    else if (categoria_field == 2)
    {
        field_seleccionado = 2;
    }
    else
    {
        field_seleccionado = 3;
    }
    
}

//Guardar informacion de TextField y activacion de boton
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self revisar_tipo_field:textField] == 0) {
        TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
        NSIndexPath * index_path = [myTableView indexPathForCell:cell];
        
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
        articulo.devolucion = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        //En caso de borrar es necesario hacer esto
        if ([string isEqualToString:@""]) {
            int lenght =(uint32_t)[textField.text length];
            NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
            articulo.devolucion = texto_actualizado;
            
        }
        [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
        
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 5) ? NO : YES;
    }
    else
    {
        return YES;
    }
    
    if ([self revisar_tipo_field:textField] == 3) {
        NSInteger cantidad_actual = [textField.text integerValue];
        NSInteger cantidad_ideal = [textField.placeholder integerValue];
        if (!(cantidad_actual == cantidad_ideal)) {
            dispatch_async(dispatch_get_main_queue(), ^ {
                textField.text = @"";
                [self presentViewController:alerta_cantidad animated:YES completion:nil];
            });
        }
    }
}

//Recargar informacion en cell recien abandonada
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self revisar_tipo_field:textField] == 0) {
        TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
        NSIndexPath * index_path = [myTableView indexPathForCell:cell];
        NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
        
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path_pasado.row];
        if ([articulo.devolucion isEqualToString:@""]) {
            articulo.devolucion = @"0";
        }
        
        [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self verificacion_inventario_inicial:index_path_pasado];
    }
}

-(NSInteger)revisar_tipo_field:(UITextField *)text_field
{
    if(text_field.tag == 1)
        return 1;
    else if(text_field.tag == 2)
        return 2;
    else if(text_field.tag == 3)
        return 3;
    else
        return 0;
}

//Setup del boton
- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableView indexPathForSelectedRow];
    NSIndexPath * next;
    
    //Revision orden
    BOOL continuar = true;
    
    if (field_seleccionado == 1) {
        [field_telefono becomeFirstResponder];
    }
    else if (field_seleccionado == 2)
    {
        [field_cantidad becomeFirstResponder];
    }
    else if (field_seleccionado == 3)
    {
        next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
        [self regresar_anterior:selected posicion_regreso:0];
        
        [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        [self ajuste_scroll:next];
        
        TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:next];
        UITextField * text_field = cell.text_field_cantidad;
        [text_field becomeFirstResponder];
        field_seleccionado = 0;

    }
    else
    {
        if (continuar) {
            if (selected.row+1 == [arreglo_articulos count])
            {
                [field_contacto becomeFirstResponder];
            }
            else
            {
                next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
            }
            
            [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            [self ajuste_scroll:next];
            
            TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:next];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
        }
    }
}

//Metodo para regresar a posicion lejana
-(void)regresar_anterior :(NSIndexPath *)selected posicion_regreso:(int)posicion
{
    while (selected.row > posicion+4) {
        selected = [NSIndexPath indexPathForRow:selected.row-3 inSection:selected.section];
        [myTableView selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
}

//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_articulos count])
    {
        [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableView scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}



//Verificacion ventas cafe anterior
-(void)verificacion_inventario_inicial :(NSIndexPath *)index_path
{
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    int inventario_inicial = (uint32_t)[articulo.inventario_inicial_real integerValue];
    int devolucion_num = (uint32_t)[articulo.devolucion integerValue];
    
    if (devolucion_num > inventario_inicial)
    {
        articulo.devolucion = @"0";
        [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
        NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
        
        [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
        
        inventario_superado = true;
    }
    else
    {
        inventario_superado = false;
        [self sumatoria_cantidad];
    }
}

-(void)inicializacion_alertas
{
    alerta_capacidad = [UIAlertController
                        alertControllerWithTitle:@"Error"
                        message:@"Inventario inicial superado."
                        preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             alerta_presente = false;
                             [alerta_capacidad dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    alerta_cantidad = [UIAlertController
                        alertControllerWithTitle:@"Error"
                        message:@"Revisar cantidad total."
                        preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok_cantidad = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alerta_cantidad dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    alerta_incompleto = [UIAlertController
                       alertControllerWithTitle:@"Error"
                       message:@"información Incompleta."
                       preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok_incompleta = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [alerta_incompleto dismissViewControllerAnimated:YES completion:nil];
                                  }];
    
    [alerta_capacidad addAction:ok];
    [alerta_cantidad addAction:ok_cantidad];
    [alerta_incompleto addAction:ok_incompleta];
}


- (IBAction)action_back:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)action_siguiente:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^ {
        if ([field_contacto.text isEqualToString:@""]) {
             [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else if([field_telefono.text isEqualToString:@""])
        {
             [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else if([field_cantidad.text isEqualToString:@""])
        {
            [self presentViewController:alerta_incompleto animated:YES completion:nil];
        }
        else
        {
            NSInteger cantidad_actual = [field_cantidad.text integerValue];
            NSInteger cantidad_ideal = [field_cantidad.placeholder integerValue];
            if (!(cantidad_actual == cantidad_ideal)) {
                [self presentViewController:alerta_cantidad animated:YES completion:nil];
            }
            else
            {
                /*if (!(visitados == 2)) {
                    [self alerta_siguiente];
                }
                else
                {
                    CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                    controller.instancia = id_instancia;
                    controller.salida_sitio = salida_sitio;
                    [self guardar_informacion];
                    [self.navigationController pushViewController:controller animated:YES];
                }*/
                [self guardar_informacion];
                [self.navigationController popViewControllerAnimated:true];
            }
            
        }
    });
}

-(void)alerta_siguiente
{
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Continuar"
                                 message:@"Seleccionar una opción."
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* action_moneda = [UIAlertAction
                                     actionWithTitle:@"Devolución Dinero"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         VC_Devolucion_Monedas * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devolucion_Moneda"];
                                         controller.id_instancia = id_instancia;
                                         controller.salida_sitio = salida_sitio;
                                         controller.visita = 2;
                                         [self.navigationController pushViewController:controller animated:YES];
                                         [self guardar_informacion];
                                         [view dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
    [view addAction:action_moneda];
    
    
    UIAlertAction * fotografia = [UIAlertAction
                                  actionWithTitle:@"Fotografía"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                                      controller.instancia = id_instancia;
                                      controller.salida_sitio = salida_sitio;
                                      [self.navigationController pushViewController:controller animated:YES];
                                      [self guardar_informacion];
                                      [view dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancelar"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    [view addAction:fotografia];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)sumatoria_cantidad
{
    NSInteger total = 0;
    for (int contador = 0; contador<[arreglo_articulos count]; contador ++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:contador];
        NSInteger cantidad = [articulo.devolucion integerValue];
        total += cantidad;
    }
    field_cantidad.placeholder = [NSString stringWithFormat:@"%ld", (long)total];
    field_cantidad.text = [NSString stringWithFormat:@"%ld", (long)total];
}

-(void)guardar_informacion
{
    if (![field_cantidad.text isEqualToString:@"0"]) {
        for (int i = 0; i<[arreglo_articulos count]; i++) {
            OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
            NSInteger devolucion = 0;
            NSInteger inventario_actualizado;
            
            devolucion = [articulo.devolucion integerValue];
            
            NSInteger inventario_inicial = [articulo.inventario_inicial_real integerValue];
            inventario_actualizado = inventario_inicial - devolucion;
            
            NSInteger id_articulo = [articulo.id_articulo integerValue];
            NSInteger id_charola = [articulo.id_charola integerValue];
            NSInteger id_espiral = [articulo.id_espiral integerValue];
            NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
            NSInteger orden_vsita = [articulo.orden_visita integerValue];
            NSInteger instancia = id_instancia;
            
            [data_articulo_manager update_devolucion:inventario_actualizado id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
            if (![articulo.devolucion isEqualToString:@"0"]) {
                NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
                id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
                
                [data_devoluciones_manager insert_det_devolucion_ruta:id_devolucion id_devuleto:articulo.id_articulo cantidad:articulo.devolucion charola:articulo.id_charola espiral:articulo.id_espiral inv_in:articulo.inventario_inicial_real];
            }
        }
        
        NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion];
        id_devolucion = [NSString stringWithFormat:@"%ld", [id_devolucion integerValue]+1];
        
        NSString * contacto = field_contacto.text;
        NSString * telefono = field_telefono.text;
        NSString * cantidad = field_cantidad.text;
        
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:0];
        NSString * id_orden_servicio = articulo.id_orden_servicio;
        NSString * instancia = [NSString stringWithFormat:@"%d", id_instancia];
        NSString * orden_visita = articulo.orden_visita;
        NSString * tipo = @"E";
        
        [data_devoluciones_manager insert_contacto_devolucion_ruta:id_devolucion nombre:contacto telefono:telefono cantidad:cantidad];
        [data_devoluciones_manager insert_cab_devolucion_ruta:id_devolucion id_orden_servicio:id_orden_servicio id_instancia:instancia orden:orden_visita tipo:tipo];
        
        back = true;
    }
}

-(void)update_en_cero
{
    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        NSInteger devolucion = 0;
        //NSInteger inventario_actualizado;
        
        devolucion = [articulo.devolucion integerValue];
        
        NSInteger inventario_inicial = [articulo.inventario_inicial_real integerValue];
        //inventario_actualizado = inventario_inicial + devolucion;
        
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_vsita = [articulo.orden_visita integerValue];
        NSInteger instancia = id_instancia;
        
        [data_articulo_manager update_devolucion:inventario_inicial id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
        
        if (![articulo.devolucion isEqualToString:@"0"])
        {
            NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion_det];
            [data_devoluciones_manager delete_det_devolucion:id_devolucion];
        }
    }
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:0];
    NSString * id_orden_servicio = articulo.id_orden_servicio;
    NSString * instancia = [NSString stringWithFormat:@"%d", id_instancia];
    NSString * orden_visita = articulo.orden_visita;
    
    NSString * id_devolucion = [data_devoluciones_manager select_id_devolucion];
    [data_devoluciones_manager delete_ultimo_contacto:id_devolucion];
    [data_devoluciones_manager delete_ultimo_cab:id_orden_servicio instancia:instancia orden_visita:orden_visita];
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

@end
