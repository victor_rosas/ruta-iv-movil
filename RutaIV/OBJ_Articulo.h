//
//  OBJ_Articulo.h
//  RutaIV
//
//  Created by Miguel Banderas on 05/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Articulo : NSObject
//Variables
@property NSString * id_instancia;
@property NSString * id_articulo;
@property NSString * inv_ini;
@property NSString * surtio;
@property NSString * removio;
@property NSString * caduco;
@property NSString * clave;
@property NSString * orden_visita;
@property NSString * id_orden_servicio;
@property NSString * id_espiral;
@property NSString * id_charola;
@property NSString * capacidad;
@property NSString * articulo;
@property NSString * precio;
@property NSString * espiral;
@property NSString * cambio;
@property NSString * inventario_inicial_real;
@property NSString * devolucion;

-(id)init :(NSString *)instancia id_articulo:(NSString *)numero_articulo inv_ini:(NSString *)inventario_inicial surtio:(NSString *)surtimiento removio:(NSString *)removidos caduco:(NSString *)caducos clave:(NSString *)numero_clave orden_visita:(NSString *)visita id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)numero_espiral id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad articulo:(NSString *)nombre_articulo precio:(NSString *)numero_precio espiral:(NSString *)nombre_espiral cambio:(NSString *)cambio_planograma;
@end
