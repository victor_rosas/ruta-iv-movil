//
//  OBJ_Cafe.m
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Cafe.h"

@implementation OBJ_Cafe

@synthesize id_instancia;
@synthesize id_articulo;
@synthesize surtio;
@synthesize removio;
@synthesize caduco;
@synthesize clave;
@synthesize orden_visita;
@synthesize id_orden_servicio;
@synthesize id_espiral;
@synthesize id_charola;
@synthesize capacidad;
@synthesize nombre_articulo;
@synthesize precio;
@synthesize cambio;
@synthesize vasos;
@synthesize ventas_cafe;
@synthesize ventas_pruebas;
@synthesize id_seleccion;
@synthesize pruebas_sel_numero;
@synthesize existencias_pasadas;

-(id)init :(NSString *)instancia id_articulo:(NSString *)numero_articulo surtio:(NSString *)surtimiento removio:(NSString *)removidos caduco:(NSString *)caducos clave:(NSString *)numero_clave orden_visita:(NSString *)visita id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)numero_espiral id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad nombre_articulo:(NSString *)articulo precio:(NSString *)numero_precio cambio:(NSString *)cambio_planograma vasos:(NSString *)numero_vasos ventas_cafe:(NSString *)ventas ventas_pruebas:(NSString *)pruebas id_seleccion:(NSString *)seleccion
{
    self.id_instancia = instancia;
    self.id_articulo = numero_articulo;
    self.surtio = @"";
    self.removio = removidos;
    self.caduco = caducos;
    self.clave = numero_clave;
    self.orden_visita = visita;
    self.id_orden_servicio = orden_servicio;
    self.id_espiral = numero_espiral;
    self.id_charola = charola;
    self.capacidad = numero_capacidad;
    self.nombre_articulo = articulo;
    self.precio = numero_precio;
    self.cambio = cambio_planograma;
    self.vasos = numero_vasos;
    self.ventas_cafe = ventas;
    self.ventas_pruebas = pruebas;
    self.id_seleccion = seleccion;
    self.pruebas_sel_numero = @"";
    self.existencias_pasadas = @"0";
    
    return self;
}


@end
