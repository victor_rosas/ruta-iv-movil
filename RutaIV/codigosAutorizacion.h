//
//  codigosAutorizacion.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface codigosAutorizacion : NSObject

-(BOOL)borraCodigosAutorizacion;

-(BOOL)insertaCodigosAutorizacion:(int)nIdOrdenServicio wOrdenVisita:(int)nOrdenVisita wIdInstancia:(int)nIdInstancia wCodigo:(int)nCodigo wIdCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wCodSitio:(int)nCodSitio wCancelaCodigo:(int)nCancelaCodigo wCancelaCodSitio:(int)nCancelaCodSitio;

@end
