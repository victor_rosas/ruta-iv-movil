//
//  OBJ_Observaciones.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Observaciones : NSObject
//Variables de el objeto
@property NSString * id_reporte;
@property NSString * id_clasificacion;
@property NSString * id_empleado;
@property NSString * fecha_incidencias;
@property NSString * descripcion;
@property NSString * id_orden_servicio;
@property NSString * id_instancia;
@property NSString * orden_visita;
@property NSString * id_maquina;
@property NSString * servicio;
@property NSString * fecha_atencion;
@property NSString * contacto;
@property NSString * tel_contacto;
@property NSString * subido;
-(id)init:(NSString *)reporte id_clasificacion:(NSString *)clasificacion id_empleado:(NSString *)empleado fecha_incidencia:(NSString *)fecha descripcion:(NSString *)texto_descripcion id_orden_servicio:(NSString *)orden_servicio id_instancia:(NSString *)instancia orden_visita:(NSString *)visita id_maquina:(NSString *)maquina servicio:(NSString *)bit_servicio fecha_atencion:(NSString *)atencion contacto:(NSString *)texto_contacto tel_contacto:(NSString *)telefono_contacto subido:(NSString *)bit_subido;
@end
