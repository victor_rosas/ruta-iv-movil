//
//  TC_Inventario.h
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Inventario : UITableViewCell{
    
    __weak IBOutlet UILabel *label_producto;
    __weak IBOutlet UILabel *label_cantidad;
}
-(void)cell_setup :(NSString *)producto numero_cantidad:(int)cantidad;
@end
