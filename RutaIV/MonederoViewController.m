//
//  MonederoViewController.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 23/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "MonederoViewController.h"
#import "Monedero_y_contadoresDAO.h"
#import "MonedasViewController.h"
#import "OBJ_Articulo.h"
#import "OBJ_Cafe.h"

@interface MonederoViewController ()

@end

@implementation MonederoViewController
@synthesize instancia;
@synthesize salida_sitio;
@synthesize tipo;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    instancia =(uint32_t)[[prefs objectForKey:@"instancia"]integerValue];
    data_manager_articulos = [[ProductosDAO alloc]init];
    data_manager_cafe = [[Data_Cafe alloc]init];
    
    dao = [[Monedero_y_contadoresDAO alloc]init];
    orden_servicio =(uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"]integerValue];
    Historicos = [[NSMutableArray alloc]init];
    Historicos = [dao CargarHistoricos:instancia orden_visita:(uint32_t)[[[NSUserDefaults standardUserDefaults] stringForKey:@"orden_visita_seleccionado"]integerValue]];
    NSMutableDictionary *dic = [Historicos objectAtIndex:0];
    hist_pieza = [dic objectForKey:@"historico_pieza"];
    hist_venta = [dic objectForKey:@"historico_ventas"];
    NSString *historico = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_historico"];
    if([historico isEqualToString:@"1"])
    {
        historicopiezas.placeholder = hist_pieza;
        historicoventas.placeholder = hist_venta;
    }
    
    array_text_fields = [[NSMutableArray alloc]init];
    [array_text_fields addObject:historicopiezas];
    [array_text_fields addObject:historicoventas];
    
    [self button_setup];
    
    historicopiezas.inputAccessoryView = keyboardDoneButtonView;
    historicoventas.inputAccessoryView = keyboardDoneButtonView;
    [historicopiezas becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    UILabel * next = [array_text_fields objectAtIndex:current];
    [next becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == historicopiezas) {
        current = 1;
    }
    if (textField == historicoventas) {
        current = 0;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 9) ? NO : YES;
}

- (IBAction)Guardar:(id)sender {
    int piezas = (uint32_t)[historicopiezas.text integerValue];
    float ventas = [historicoventas.text floatValue];
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"ERROR"
                                  message:@"Llenar todos los campos."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert addAction:ok];
    
    UIAlertController * alert_piezas=   [UIAlertController
                                  alertControllerWithTitle:@"ERROR"
                                  message:@"Información incongruente en piezas."
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController * alert_ventas=   [UIAlertController
                                         alertControllerWithTitle:@"ERROR"
                                         message:@"Información incongruente en ventas."
                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController * alert_ventas_max=   [UIAlertController
                                         alertControllerWithTitle:@"ERROR"
                                         message:@"Valor máximo de ventas superado."
                                         preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertController * alert_piezas_max=   [UIAlertController
                                         alertControllerWithTitle:@"ERROR"
                                         message:@"Valor máximo de piezas superado."
                                         preferredStyle:UIAlertControllerStyleAlert];
    [alert_piezas addAction:ok];
    [alert_ventas addAction:ok];
    [alert_piezas_max addAction:ok];
    [alert_ventas_max addAction:ok];

    if ([historicopiezas.text isEqualToString:@""]) {
        [self presentViewController:alert animated:YES completion:nil];
        perform_segue = false;
    }
    else if([historicoventas.text isEqualToString:@""])
    {
        [self presentViewController:alert animated:YES completion:nil];
        perform_segue = false;
        
    }
    else
    {
        NSString *ventas_maximas = [self calcular_vlmontoMax];
        NSString *piezas_maximas = [self calcular_vlunidadMax];
        
        if ([ventas_maximas floatValue] < ventas)
        {
            [self presentViewController:alert_ventas_max animated:YES completion:nil];
            perform_segue = false;
        }
        else if ([piezas_maximas integerValue] < piezas)
        {
            [self presentViewController:alert_piezas_max animated:YES completion:nil];
            perform_segue = false;
        }
        else if ([hist_pieza integerValue] > piezas) {
            [self presentViewController:alert_piezas animated:YES completion:nil];
            perform_segue = false;
        }
        else if ([hist_venta floatValue] > ventas)
        {
            [self presentViewController:alert_ventas animated:YES completion:nil];
            perform_segue = false;
        }
        else
        {int ordenvisita =(uint32_t)[[[NSUserDefaults standardUserDefaults] stringForKey:@"orden_visita_seleccionado"]integerValue];
            perform_segue = true;
            [dao actualizaHistoricos:orden_servicio INSTANCIA:instancia ORDENVISITA:ordenvisita UNIDADES:piezas MONTO:ventas RECOGIODINERO:1];
        }
    }
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    return perform_segue;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    MonedasViewController *controller = (MonedasViewController *)segue.destinationViewController;
    controller.instancia = instancia;
    controller.salida_sitio = salida_sitio;
    controller.tipo = tipo;
}

-(NSString *)calcular_vlunidadMax{
    /*suma = [[NSMutableArray alloc]init];
    suma = [dao vlUnidadMax:instancia];
    x=0;
    while(x<[suma count]) {
        NSMutableDictionary *dic = [suma objectAtIndex:x];
        NSString *prod = [dic objectForKey:@"surtio"];
        int iprod = (uint32_t)[prod integerValue];
        x++;
        unidadresultado = unidadresultado + iprod;
    }
    
    NSInteger totalunidadmax = unidadresultado + [hist_pieza integerValue];
    NSString *unidadstrValue = @(totalunidadmax).stringValue; //El valor importante*/
    NSString * suma_string;
    NSMutableArray * articulos = [[NSMutableArray alloc]init];
    NSString * vasos = @"";
    if ([tipo isEqualToString:@"Cafe"]) {
        vasos = [data_manager_cafe select_cantidad_vasos:instancia];
        articulos = [data_manager_cafe select_articulos:instancia];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
        articulos = [data_manager_articulos select_articulos:instancia :[orden_visita_instancia integerValue]];
    }
    
    NSInteger sumatoria = 0;
    for (int contador = 0; contador<[articulos count]; contador++) {
        
        if ([tipo isEqualToString:@"Cafe"]) {
            sumatoria = [vasos integerValue];
            NSInteger anterior = [historicopiezas.placeholder integerValue];
            suma_string = [NSString stringWithFormat:@"%d",sumatoria + anterior];
            break;
        }
        else
        {
            OBJ_Articulo * articulo = [articulos objectAtIndex:contador];
            sumatoria += [articulo.capacidad integerValue];
        }
        
        NSInteger anterior = [hist_pieza integerValue];
        suma_string = [NSString stringWithFormat:@"%d",sumatoria + anterior];
    }
    
    return suma_string;
}

- (NSString *)calcular_vlmontoMax{
    /*suma = [[NSMutableArray alloc]init];
    suma = [dao vlMontoMax:instancia];
    x=0;
    while(x<[suma count]) {
        NSMutableDictionary *dic = [suma objectAtIndex:x];
        NSString *prod = [dic objectForKey:@"surtio"];
        NSString *prec = [dic objectForKey:@"precio"];
        int iprod = (uint32_t)[prod integerValue];
        int iprec = (uint32_t)[prec integerValue];
        int iresul = (iprec * iprod);
        
        x++;
        totalprecios = iresul + totalprecios;
    }
    NSInteger newtotal =totalprecios +[hist_venta integerValue];
    NSString *strValue = @(newtotal).stringValue; //El valor importante
    return strValue;*/
    NSString * suma_string;
    NSMutableArray * articulos = [[NSMutableArray alloc]init];
    NSString * cantidad_vasos;
    NSString * precio;
    if ([tipo isEqualToString:@"Cafe"]) {
        articulos = [data_manager_cafe select_articulos:instancia];
        cantidad_vasos = [data_manager_cafe select_cantidad_vasos:instancia];
        precio = [data_manager_cafe select_maximo_precio:instancia];
    }
    else
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
        articulos = [data_manager_articulos select_articulos:instancia :[orden_visita_instancia integerValue]];
    }
    
    NSInteger sumatoria = 0;
    for (int contador = 0; contador<[articulos count]; contador++) {
        if ([tipo isEqualToString:@"Cafe"]) {
            sumatoria = [precio integerValue]*[cantidad_vasos integerValue];
            NSInteger anterior = [historicoventas.placeholder integerValue];
            suma_string = [NSString stringWithFormat:@"%ld",sumatoria + anterior];
            break;
        }
        else
        {
            OBJ_Articulo * articulo = [articulos objectAtIndex:contador];
            NSInteger capacidad = [articulo.capacidad integerValue];
            NSInteger precio = [articulo.precio integerValue];
            sumatoria += capacidad*precio;
        }
        
        NSInteger anterior = [hist_venta integerValue];
        suma_string = [NSString stringWithFormat:@"%ld",sumatoria + anterior];
    }
    
    
    return suma_string;
}

- (IBAction)action_back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

-(int)Traerordenvisita{
    int ordenvisita = (uint32_t)[[dao obtenerorden_visita:instancia]integerValue];
    return ordenvisita;
}

@end
