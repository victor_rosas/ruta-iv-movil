//
//  dexIV.h
//  RutaIV
//
//  Created by Miguel Banderas on 30/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dexIV : NSObject

-(BOOL)borraDexIV;

-(BOOL)insertaDexIV:(int)nIdInstancia wEspiral:(NSString *)sEspiral wPrecioVta:(double)dPrecioVenta wTitulo:(NSString *)sTitulo wDescripcion:(NSString *)sDescripcion;


@end
