//
//  PruebaTableViewController.h
//  RutaIV
//
//  Created by Miguel Banderas on 19/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "Hora_inicioDAO.h"

@interface PruebaTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate,UIAlertViewDelegate>{
    NSMutableArray *arreglo_visitadas;
    BOOL primer_blanco;
    NSIndexPath *activa;
    //variable horas llegada
    BOOL salida_sitio;
    Hora_inicioDAO *dao;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView2;

@end
