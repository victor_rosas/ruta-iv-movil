//
//  ProductosDAO.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 20/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "ProductosDAO.h"
#import "AppDelegate.h"
#import "OBJ_Articulo.h"
#import "OBJ_Instancia_Doble.h"

@implementation ProductosDAO{


}

-(NSString *) obtenerRutaBD{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSMutableArray *)select_articulos:(NSInteger)id_instancia :(NSInteger)orden_visita
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA, ID_ARTICULO, INV_INI, SURTIO, REMOVIO, CADUCARON, CLAVE, ORDEN_VISITA, ID_ORDEN_SERVICIO, ID_ESPIRAL, ID_CHAROLA, CAPACIDAD_ESPIRAL, ARTICULO, PRECIO, ESPIRAL, CAMBIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND CAMBIO != 'V' AND ORDEN_VISITA = %ld",(long)id_instancia, (long)orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_articulo      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * inv_ini          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * surtio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * removio          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * caduco           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * clave            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * orden_visita     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * id_orden_serv    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_espiral       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * id_charola       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * capacidad        = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * nombre_articulo  = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * precio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                NSString * espiral          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,14)];
                NSString * cambio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,15)];
                
                OBJ_Articulo * articulo = [[OBJ_Articulo alloc]init:id_instancia id_articulo:id_articulo inv_ini:inv_ini surtio:surtio removio:removio caduco:caduco clave:clave orden_visita:orden_visita id_orden_servicio:id_orden_serv id_espiral:id_espiral id_charola:id_charola capacidad:capacidad articulo:nombre_articulo precio:precio espiral:espiral cambio:cambio];
                [arreglo_articulos addObject:articulo];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
    return arreglo_articulos;
}

-(NSMutableDictionary *)CargarSurtidoAnterior:(NSInteger)instancia orden_servicio:(NSInteger)orden_servicio id_articulo:(NSInteger)id_articulo{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT SUM(Case When ID_INSTANCIA= %ld And ORDEN_VISITA= %ld Then 0 Else SURTIO - REMOVIO End) AS SUMA , EXISTENCIAS.EXISTENCIA FROM MOVIMIENTOS_INSTANCIA INNER JOIN EXISTENCIAS ON MOVIMIENTOS_INSTANCIA.ID_ARTICULO = EXISTENCIAS.ID_ARTICULO  WHERE  MOVIMIENTOS_INSTANCIA.ID_ARTICULO = %ld GROUP BY EXISTENCIAS.EXISTENCIA ",(long)instancia, (long)orden_servicio, (long)id_articulo];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {

                NSString *suma= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *existencia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                
                [dic setValue:suma forKey:@"suma"];
                [dic setValue:existencia forKey:@"existencia"];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar inventario");
    return dic;
}

-(BOOL)update_inventario_inicial:(NSInteger)inventario_ini id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia {
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET INV_INI = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)inventario_ini, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
    
}

-(BOOL)update_surtio:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET SURTIO = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)surtio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)update_removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET REMOVIO = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)removio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)update_caduco:(NSInteger)caduco id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET CADUCARON = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)caduco, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)update_devolucion:(NSInteger)devolucion id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET INV_INI = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)devolucion, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)verificacion_primera_visita:(int)id_instancia{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];

    sqlite3 *database;
    sqlite3_stmt *sentencia;
    int valor_registros = 100;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT REGISTROS FROM INSTANCIAS_SERVICIO WHERE ID_INSTANCIA = %d",id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString *Producto= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                valor_registros = (uint32_t)[Producto integerValue];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    
    
    if(valor_registros<1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

-(NSString *)obtener_codigo_autorizacion:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita
{
    NSString *Codigo;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT CODIGO FROM CODIGO_AUTORIZACION WHERE ID_INSTANCIA = %d AND ID_ORDEN_SERVICIO = %d AND ORDEN_VISITA = %d",id_instancia, orden, orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                 Codigo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Codigo;
}

-(NSString *)obtener_codigo_autorizacion_sitio:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita
{
    NSString *Codigo;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT COD_SITIO FROM CODIGO_AUTORIZACION WHERE ID_INSTANCIA = %d AND ID_ORDEN_SERVICIO = %d AND ORDEN_VISITA = %d",id_instancia, orden, orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                Codigo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Codigo;
}

-(NSString *)obtener_codigo_revisita_instancia:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita
{
    NSString *Codigo;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT CANCELA_CODIGO FROM CODIGO_AUTORIZACION WHERE ID_INSTANCIA = %d AND ID_ORDEN_SERVICIO = %d AND ORDEN_VISITA = %d",id_instancia, orden, orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                Codigo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Codigo;
}

-(NSString *)obtener_codigo_revisita_sitio:(int)id_instancia orden_servicio:(int)orden orden_visita:(int)orden_visita
{
    NSString *Codigo;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT CANCELA_COD_SITIO FROM CODIGO_AUTORIZACION WHERE ID_INSTANCIA = %d AND ID_ORDEN_SERVICIO = %d AND ORDEN_VISITA = %d",id_instancia, orden, orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                Codigo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Codigo;
}

-(BOOL)actualizar_salteado:(NSInteger)valor_salteado instancia:(NSInteger)instancia
{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET SALTEADO = %ld WHERE ID_INSTANCIA = %ld",(long)valor_salteado, (long)instancia];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(OBJ_Instancia_Doble*)getInstanciaRepetida:(NSInteger)instancia orden:(NSInteger)orden_visita
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    OBJ_Instancia_Doble * instancia_doble;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"Select ID_INSTANCIA,min(orden) as MINIMO ,max(orden) as MAXIMO From DET_ORDEN_SERVICIO WHERE ID_OPERACION = 1 AND ID_INSTANCIA = %ld Group By ID_INSTANCIA having count(*) > 1 AND MINIMO = %ld", (long)instancia,(long)orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * minimo           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * maximo           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                
                instancia_doble = [[OBJ_Instancia_Doble alloc]init:id_instancia orden_minima:minimo orden_maxima:maximo];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
    return instancia_doble;
}

-(BOOL)update_existencia:(NSInteger)inv_ini surtio:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString * removio;
    NSString * caduco;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT REMOVIO, CADUCARON FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                removio   = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                caduco    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    bool bValida = FALSE;
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    long existencias = inv_ini+surtio-[removio integerValue]-[caduco integerValue];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET EXISTENCIA = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)existencias, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)maximo];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)update_existencia_removio:(NSInteger)inv_ini removio:(NSInteger)removio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString * surtio;
    NSString * caduco;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT SURTIO, CADUCARON FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                surtio   = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                caduco    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    bool bValida = FALSE;
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    long existencias = inv_ini+removio-[surtio integerValue]-[caduco integerValue];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET EXISTENCIA = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)existencias, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)maximo];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)update_existencia_caduco:(NSInteger)inv_ini caduco:(NSInteger)caduco id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia maximo:(NSInteger)maximo{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSString * removio;
    NSString * surtio;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT REMOVIO, SURTIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                removio   = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                surtio    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    bool bValida = FALSE;
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    long existencias = inv_ini+caduco-[removio integerValue]-[surtio integerValue];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET EXISTENCIA = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V'",(long)existencias, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)maximo];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
