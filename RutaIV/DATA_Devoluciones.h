//
//  DATA_Devoluciones.h
//  RutaIV
//
//  Created by Miguel Banderas on 18/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DATA_Devoluciones : NSObject
{
    sqlite3 *bd;
}
-(NSString *) obtenerRutaBD;
//Insert
-(BOOL)insert_contacto_devolucion_ruta:(NSString *)id_devolucion nombre:(NSString *)nombre telefono:(NSString *)telefono cantidad:(NSString *)cantidad;
-(BOOL)insert_cab_devolucion_ruta:(NSString *)id_devolucion id_orden_servicio:(NSString *)id_orden_servicio id_instancia:(NSString *)instancia orden:(NSString *)orden_visita tipo:(NSString *)tipo;
-(BOOL)insert_det_devolucion_ruta:(NSString *)id_devolucion id_devuleto:(NSString *)devuelto cantidad:(NSString *)numero_cantidad charola:(NSString *)nombre_charola espiral:(NSString *)nombre_espiral inv_in:(NSString *)inventario_inicial;

//Select
-(NSString *)select_id_devolucion;
-(NSString *)select_id_devolucion_det;

//Delete
-(BOOL)delete_ultimo_contacto:(NSString *)id_devolucion;
-(BOOL)delete_ultimo_cab:(NSString *)id_orden_servicio instancia:(NSString * )id_instancia orden_visita:(NSString *)id_orden_visita;
-(BOOL)delete_det_devolucion:(NSString *)id_devolucion;

//Pruebas
-(void)select_prueba_det;
-(void)select_prueba_contacto;
-(void)select_prueba_cab;
@end
