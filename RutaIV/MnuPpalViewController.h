//
//  MnuPpalViewController.h
//  RutaIV
//
//  Created by Miguel Banderas on 19/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface MnuPpalViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate>


@end
