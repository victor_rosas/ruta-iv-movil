//
//  MonedasViewController.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 26/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Monedero_y_contadoresDAO.h"

@interface MonedasViewController : UIViewController <UITextFieldDelegate>{

    __weak IBOutlet UITextField *Monedas_50;
    __weak IBOutlet UITextField *Monedas_01;
    __weak IBOutlet UITextField *Monedas_02;
    __weak IBOutlet UITextField *Monedas_05;
    __weak IBOutlet UITextField *Monedas_10;
    Monedero_y_contadoresDAO *dao;
    BOOL performo_segue;
    UIToolbar* keyboardDoneButtonView;
    int orden_servicio;
    
    NSMutableArray * array_text_fields;
    int current;
}
- (IBAction)Guardar:(id)sender;
@property int instancia;
//Variables horas llegada
@property BOOL salida_sitio;
@property NSString * tipo;

@end
