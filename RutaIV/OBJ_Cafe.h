//
//  OBJ_Cafe.h
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Cafe : NSObject

@property NSString * id_instancia;
@property NSString * id_articulo;
@property NSString * surtio;
@property NSString * removio;
@property NSString * caduco;
@property NSString * clave;
@property NSString * orden_visita;
@property NSString * id_orden_servicio;
@property NSString * id_espiral;
@property NSString * id_charola;
@property NSString * capacidad;
@property NSString * nombre_articulo;
@property NSString * precio;
@property NSString * cambio;
@property NSString * vasos;
@property NSString * ventas_cafe;
@property NSString * ventas_pruebas;
@property NSString * id_seleccion;
@property NSString * pruebas_sel_numero;
@property NSString * existencias_pasadas;

-(id)init :(NSString *)instancia id_articulo:(NSString *)numero_articulo surtio:(NSString *)surtimiento removio:(NSString *)removidos caduco:(NSString *)caducos clave:(NSString *)numero_clave orden_visita:(NSString *)visita id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)numero_espiral id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad nombre_articulo:(NSString *)articulo precio:(NSString *)numero_precio cambio:(NSString *)cambio_planograma vasos:(NSString *)numero_vasos ventas_cafe:(NSString *)ventas ventas_pruebas:(NSString *)pruebas id_seleccion:(NSString *)seleccion;

@end
