//
//  ExistenciasDAO.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 29/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "ExistenciasDAO.h"
#import "AppDelegate.h"
#import "Existencias.h"

@implementation ExistenciasDAO

-(NSString *) obtenerRutaBD{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSMutableArray *)getInstancias{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Ainstancias = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA from INSTANCIAS_SERVICIO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                
                [Ainstancias addObject:instancia];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Ainstancias;
}


-(NSString *)CargarIDalmacen:(int)ID_RUTA {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    invrutas = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from TABLA_RUTAS WHERE ID_RUTA = %d",ID_RUTA];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {

                NSString *id_almacen = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];

                Almacen = id_almacen;
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Almacen;
}

//USAR ESTE PARA TRAER EXISTENCIAS SOLAMENTE
-(NSString *)CargarExistencias:(int)ID_ARTICULO {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    invrutas = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from EXISTENCIAS WHERE ID_ARTICULO = %d",ID_ARTICULO];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *existencia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                
                Existencia =existencia;
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    if (Existencia == nil) {
        Existencia = @"0";
    }
    return Existencia;
}

-(BOOL)actualizarExistencia:(int)ID_ARTICULO NUEVAEXISTENCIA:(double)NUEVAEXISTENCIA ALMACEN:(int)ALMACEN{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE EXISTENCIAS SET EXISTENCIA = %f WHERE ID_ARTICULO = %d AND ID_ALMACEN = %d",NUEVAEXISTENCIA,ID_ARTICULO,ALMACEN];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update %s",sqlite3_errmsg(dataBase));
    
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}


-(BOOL)update_todas_existencias :(NSInteger)cantidad
{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE EXISTENCIAS SET EXISTENCIA = %ld",(long)cantidad];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update %s",sqlite3_errmsg(dataBase));
            
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}


//METODO NUEVO TENTATIVO A SER USADO
-(NSMutableArray *)CargarExistenciasnuevo:(int)ID_ARTICULO{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    invrutas = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT DISTINCT EXISTENCIAS.ID_ARTICULO,EXISTENCIAS.EXISTENCIA,MOVIMIENTOS_INSTANCIA.ARTICULO FROM MOVIMIENTOS_INSTANCIA INNER JOIN EXISTENCIAS ON EXISTENCIAS.ID_ARTICULO = MOVIMIENTOS_INSTANCIA.ID_ARTICULO WHERE EXISTENCIAS.ID_ARTICULO = %d",ID_ARTICULO];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableArray *dic = [[NSMutableArray alloc]init];
                NSString *articulo_id = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 0)];
                NSString *existencia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *nombre_articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                
                [dic setValue:articulo_id forKey:@"articulo_id"];
                [dic setValue:existencia forKey:@"existencia"];
                [dic setValue:nombre_articulo forKey:@"nombre_articulo"];
                
                [AExistencias addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return AExistencias;
}




@end
