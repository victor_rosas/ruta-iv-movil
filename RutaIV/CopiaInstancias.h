//
//  CopiaInstancias.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CopiaInstancias : NSObject

-(BOOL)copiaInstancias:(int)nIdInstancia wIdCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wIdUbicacion:(int)nIdUbicacion wIdTipoMaquina:(int)nIdTipoMaquina wIdPlanograma:(int)nIdPlanograma wIdMaquina:(int)nIdMaquina wRegistros:(int)nRegistros wDifHoraMins:(int)nDifHoraMins wCashless:(int)nCashless latitud:(NSString *)sLatitud longitud:(NSString *)sLongitud;

-(BOOL)borraInstancias;

@end
