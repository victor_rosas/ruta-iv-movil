//
//  ExistenciasDAO.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 29/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ExistenciasDAO : NSObject{
    sqlite3 *bd;
    NSMutableArray *Ainstancias;
    NSMutableArray *invrutas;
    NSMutableArray *AExistencias;
    NSString *Existencia;
    NSString *Almacen;
}
-(NSString *) obtenerRutaBD;
-(NSMutableArray *)getInstancias;
-(NSString *)CargarIDalmacen:(int)ID_RUTA;
-(NSString *)CargarExistencias:(int)ID_ARTICULO;
-(BOOL)actualizarExistencia:(int)ID_ARTICULO NUEVAEXISTENCIA:(double)NUEVAEXISTENCIA ALMACEN:(int)ALMACEN;
-(BOOL)update_todas_existencias :(NSInteger)cantidad;
@end
