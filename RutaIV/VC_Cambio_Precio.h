//
//  VC_Cambio_Precio.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DATA_Cambio.h"

@interface VC_Cambio_Precio : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *myTableView;
    DATA_Cambio * data_cambio_manager;
    NSMutableArray * arreglo_mutable_cambio_precio;
    UIAlertController * alert_cambio_precio;
}
//Variables de vista anterior
@property int id_instancia;
@property BOOL salida_sitio;
- (IBAction)action_back:(id)sender;
- (IBAction)action_next:(id)sender;

@end
