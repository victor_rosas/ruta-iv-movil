//
//  NextViewController.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 15/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"


@interface NextViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    __weak IBOutlet UITableView *myTableview;
    NSInteger txtSucursal;
    ProductosDAO *dao;
    NSMutableArray * arreglo_articulos;
    
    __weak IBOutlet UIButton *btn_inventario;
    
    int contador_rellenado;
    BOOL capacidad_espiral_superada;
    BOOL productos_orden;
    BOOL alerta_presente;
    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_orden;
    UIAlertController * alerta_capacidad;
    
    UIToolbar* keyboardDoneButtonView;
}

@property (strong, nonatomic) UIView *view_button;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,atomic) NSMutableArray *textFieldData;
@property (strong,atomic) NSString *stringVariable;
@property int instancia;
@property NSMutableArray * arreglo_cambio_planograma;

- (IBAction)btn_siguiente_pressed:(id)sender;
-(void) ObtenerProductos;
-(BOOL)revisarCapacidadEspiral:(int)cantidad index:(NSIndexPath*)indexPath;
//-(BOOL) revisarCapacidadEspiral:(int)cantidad;

//Variblaes para revision de horas
@property BOOL salida_sitio;
@end
