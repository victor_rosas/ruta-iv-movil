//
//  Imagen.h
//  Fotografia_vending
//
//  Created by Jose Miguel Banderas Lechuga on 10/12/14.
//  Copyright (c) 2014 Jose Miguel Banderas Lechuga. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Imagen : NSObject{
    NSString *Imagen;
    NSString *Path;
    NSInteger Image_id;
    
}

@property (nonatomic,retain) NSString *Imagen;
@property (nonatomic,retain) NSString *Path;
@property (nonatomic,assign) NSInteger Image_id;

@end
