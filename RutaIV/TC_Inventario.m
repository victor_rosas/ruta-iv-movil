//
//  TC_Inventario.m
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TC_Inventario.h"

@implementation TC_Inventario

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)cell_setup :(NSString *)producto numero_cantidad:(int)cantidad{
    [label_cantidad.layer setCornerRadius:10.0f];
    [label_cantidad.layer setMasksToBounds:YES];
    if (cantidad < 1) {
        //label_id.backgroundColor = [UIColor colorWithRed:240.0/255.0f green:72.0/255.0f blue:52.0/255.0f alpha:1.0];
        label_cantidad.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0];
    }
    else if (cantidad < 3)
    {
        label_cantidad.backgroundColor = [UIColor colorWithRed:246.0f/255.0f green:236.0f/255.0f blue:179.0f/255.0f alpha:1.0];
    }
    else
    {
        label_cantidad.backgroundColor = [UIColor colorWithRed: 0 green:0 blue:0 alpha:.2];
    }
    label_producto.text = producto;
    label_cantidad.text = [NSString stringWithFormat:@"%d",cantidad];
}

@end
