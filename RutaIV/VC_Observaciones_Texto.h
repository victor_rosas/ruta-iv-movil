//
//  VC_Observaciones_Texto.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DATA_Observaciones.h"
#import "CabeceroDAO.h"

@interface VC_Observaciones_Texto : UIViewController <UITextViewDelegate>
{
    DATA_Observaciones * data_observaciones_manager;
    CabeceroDAO * data_cabecero_manager;
    __weak IBOutlet UITextView *text_observaciones;
}

- (IBAction)action_back_texto:(id)sender;
- (IBAction)action_next_texto:(id)sender;

//Variables pasadas
@property int id_clasificacion;
@property int atencion;
@property int instancia;
@end
