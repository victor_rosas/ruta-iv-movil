//
//  NextViewController.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 15/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

//EN CASO DE ERRORES REVISAR EL +1 DE LOS TAGS

#import "NextViewController.h"
#import "TableViewCell.h"
#import "ProductosDAO.h"
#import "VC_Surtir.h"
#import "OBJ_Articulo.h"
#import "MonederoViewController.h"


@interface NextViewController ()

@end

@implementation NextViewController
@synthesize instancia;
@synthesize salida_sitio;
@synthesize arreglo_cambio_planograma;

- (void)viewDidLoad {
    //Variables
    dao = [[ProductosDAO alloc] init];
    arreglo_articulos = [[NSMutableArray alloc]init];

    contador_rellenado = 0;
    capacidad_espiral_superada = false;
    alerta_presente = false;
    [self inicializacion_alertas];
    NSString * parametro_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_producto_orden"];
    if ([parametro_orden isEqualToString:@"0"]) {
        productos_orden = false;
    }
    else{
        productos_orden = true;
    }
    
    //Informacion
    [self ObtenerProductos];
    
    //Verificacion de la primera visita
    [self primera_visita];
    
    //Vista
    [self.navigationItem setHidesBackButton:YES];
    [btn_inventario setEnabled:false];
    [self button_setup];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = [NSIndexPath indexPathForRow:0 inSection:0];
    [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

-(void)home:(UIBarButtonItem *)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                         actionWithTitle:@"Cancelar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)action_back_button:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arreglo_articulos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = (TableViewCell *)[myTableview dequeueReusableCellWithIdentifier:@"Mycell" forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    
    
    //Creacion de la celda
     NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if([parametrizacion_mayusculas isEqualToString:@"0"])
    {
        cell.myLabel.text = [articulo.articulo capitalizedString];
    }
    else
    {
        cell.myLabel.text = articulo.articulo;
    }
    
    cell.Espiral.text = articulo.espiral;
    
    cell.label_capacidad.text = [NSString stringWithFormat:@"C.E. -  %@", articulo.capacidad];
    NSString *precio_string = articulo.precio;
    float precio = precio_string.floatValue;
    cell.label_costo.text = [formatter stringFromNumber:[NSNumber numberWithFloat:precio]];
    
    if ([articulo.cambio isEqualToString:@"N"]) {
        cell.backgroundColor = [UIColor colorWithRed:145.0/255.0f green:145.0/255.0f blue:145.0/255.0f alpha:1.0];
        cell.myTextbox.text = @"0";
        articulo.inv_ini = @"0";
        [arreglo_articulos replaceObjectAtIndex:indexPath.row withObject:articulo];
    }
    else
    {
        if (![articulo.inv_ini isEqualToString:@""])
        {
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        cell.myTextbox.text = articulo.inv_ini;
    }
    
    
    
    //Se agrega boton a todos los text fields de las celdas
    cell.myTextbox.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

//Seleccion de celda cuando se inicia modificacion en TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    TableViewCell * cell = (TableViewCell*)textField.superview.superview;
    NSIndexPath * index_path = [myTableview indexPathForCell:cell];
    
    if (capacidad_espiral_superada) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
            [myTableview selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index_path_pasado];
            UITextField * text_field = cell.myTextbox;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_path_pasado];
            
            [self presentViewController:alerta_capacidad animated:YES completion:nil];
        });
    }
    else
    {
        index_path_pasado = index_path;
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
        //Verificacion orden
        if ([articulo.cambio isEqualToString:@"N"]) {
            [self verificacion_orden_cambio:index_path];
        }
        else
        {
            BOOL continuar;
            if (productos_orden) {
                continuar = [self verificacion_orden_otros:index_path];
            }
            else
            {
                continuar = true;
            }
            
            if (continuar) {
                [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
                
                [self ajuste_scroll:index_path];
            }
        }
    }
}

//Guardar informacion de TextField y activacion de boton
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    TableViewCell * cell = (TableViewCell*)textField.superview.superview;
    NSIndexPath * index_path = [myTableview indexPathForCell:cell];
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    
    articulo.inv_ini = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    if ([textField.text isEqualToString:@""]&&![string isEqualToString:@""]) {
        contador_rellenado++;
    }
    
    //En caso de borrar es necesario hacer esto
    if ([string isEqualToString:@""]) {
        int lenght = (uint32_t)[textField.text length];
        NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
        articulo.inv_ini = texto_actualizado;
        
        if ([texto_actualizado isEqualToString:@""]) {
            contador_rellenado --;
        }
    }
    [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
    
    //Decide cuando se desactiva el boton
    if (!(contador_rellenado == [arreglo_articulos count])) {
        btn_inventario.enabled = false;
    }
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
}

//Recargar informacion en cell recien abandonada
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    TableViewCell * cell = (TableViewCell*)textField.superview.superview;
    NSIndexPath * index_path = [myTableview indexPathForCell:cell];
    
    NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
    
    [myTableview reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self verificacion_capacidad_espiral:index_path_pasado];
    
    //Decide cuando se activa el boton
    if (contador_rellenado == [arreglo_articulos count]) {
        btn_inventario.enabled = true;
    }
    else
    {
        btn_inventario.enabled = false;
    }
    
}


- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableview indexPathForSelectedRow];
    NSIndexPath * next;
    
    //Revision orden
    BOOL continuar;
    if (productos_orden) {
        continuar = [self verificacion_orden_next:selected];
    }
    else
    {
        continuar = true;
    }
    
    if (continuar) {
        if (selected.row+1 == [arreglo_articulos count])
        {
            next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
            [self regresar_anterior:selected posicion_regreso:0];
        }
        else
        {
            next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
        }
        
        [myTableview selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        [self ajuste_scroll:next];
        
        TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:next];
        UITextField * text_field = cell.myTextbox;
        [text_field becomeFirstResponder];
    }

}

//Metodo para regresar a posicion lejana
-(void)regresar_anterior :(NSIndexPath *)selected posicion_regreso:(int)posicion
{
    if (selected.row>posicion) {
        while (selected.row > posicion+4) {
            selected = [NSIndexPath indexPathForRow:selected.row-3 inSection:selected.section];
            [myTableview selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
    else
    {
        while (selected.row < posicion-4) {
            selected = [NSIndexPath indexPathForRow:selected.row+3 inSection:selected.section];
            [myTableview selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
    
}



//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_articulos count])
    {
        [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableview scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

//Verificacion orden otros casos
-(BOOL)verificacion_orden_otros :(NSIndexPath *)index_seleccionado
{
    if (!(index_seleccionado.row <= contador_rellenado)) {
        NSIndexPath * index_verdadero = [NSIndexPath indexPathForRow:contador_rellenado inSection:index_seleccionado.section];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_seleccionado posicion_regreso:(uint32_t)index_verdadero.row];
            [myTableview selectRowAtIndexPath:index_verdadero animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index_verdadero];
            UITextField * text_field = cell.myTextbox;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_verdadero];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_orden animated:YES completion:nil];
            }
        });
        
        return false;
    }
    return true;
}

-(void)verificacion_orden_cambio :(NSIndexPath *)index_seleccionado
{
    OBJ_Articulo * articulo;
    int posicion = 0;
    contador_rellenado = 0;
    BOOL espacio = false;
    for (int contador = 0; contador<[arreglo_articulos count]; contador ++)
    {
        articulo = [arreglo_articulos objectAtIndex:contador];
        NSString * inv = articulo.inv_ini;
        if (![inv isEqualToString:@""]){
            contador_rellenado++;
        }
        else
        {
            if (!espacio) {
                posicion=contador_rellenado;
                espacio = true;
            }
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^ {
        
        NSIndexPath * index_movimiento;
        if ([arreglo_articulos count] == posicion) {
            index_movimiento = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        else
        {
           index_movimiento = [NSIndexPath indexPathForRow:posicion inSection:index_seleccionado.section];

        }
        [self regresar_anterior:index_seleccionado posicion_regreso:(uint32_t)index_movimiento.row];
        [myTableview selectRowAtIndexPath:index_movimiento animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index_movimiento];
        UITextField * text_field = cell.myTextbox;
        [text_field becomeFirstResponder];
        
        [self ajuste_scroll:index_movimiento];
        if (!alerta_presente) {
            alerta_presente = true;
            //[self presentViewController:alerta_orden animated:YES completion:nil];
        }
    });
}

//Verificacion orden_next
-(BOOL)verificacion_orden_next :(NSIndexPath *)index_actual
{
    //Revisa si la celda actual tiene algo escrito, si no es asi no ejecuta el movimiento
    int posicion = (uint32_t)index_actual.row;
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:posicion];
    NSString * cantidad = articulo.inv_ini;
    
    if ([cantidad isEqualToString:@""]) {
        [myTableview scrollToRowAtIndexPath:index_actual atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        [self ajuste_scroll:index_actual];
        [self presentViewController:alerta_orden animated:YES completion:nil];
        return false;
    }
    else
    {
        return true;
    }
}

//Verificacion capacidad de espiral
-(void)verificacion_capacidad_espiral :(NSIndexPath *)index_path
{
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    int cantidad = (uint32_t)[articulo.inv_ini integerValue];
    int capacidad_espiral = (uint32_t)[articulo.capacidad integerValue];
    if (cantidad>capacidad_espiral) {
        articulo.inv_ini = @"";
        contador_rellenado--;
        [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
        NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
        
        [myTableview reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
        
        capacidad_espiral_superada = true;
    }
    else
    {
        capacidad_espiral_superada = false;
    }
}

-(void)inicializacion_alertas
{
    alerta_capacidad = [UIAlertController
                        alertControllerWithTitle:@"Error"
                        message:@"Se ha superado la capacidad del espiral."
                        preferredStyle:UIAlertControllerStyleAlert];
    
    alerta_orden =  [UIAlertController
                     alertControllerWithTitle:@"Error"
                     message:@"Llenar la información en orden."
                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok_capacidad = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [alerta_capacidad dismissViewControllerAnimated:YES completion:nil];
                                   }];
    UIAlertAction* ok_orden = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   alerta_presente = false;
                                   [alerta_orden dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alerta_capacidad addAction:ok_capacidad];
    [alerta_orden addAction:ok_orden];
}

- (IBAction)btn_siguiente_pressed:(id)sender {
    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        
        NSInteger inv_inicial = [articulo.inv_ini integerValue];
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_visita = [articulo.orden_visita integerValue];
        
        [dao update_inventario_inicial:inv_inicial id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita id_instancia:instancia];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@(salida_sitio) forKey:@"salida_sitio"];
        [prefs setObject:@(instancia) forKey:@"instancia"];
        [prefs setObject:false forKey:@"primera_visita"];
        NSArray *arrayarticulos = [NSArray arrayWithArray:arreglo_articulos];
        //[prefs setObject:arrayarticulos forKey:@"inv_ini_array"];
        [prefs synchronize];
        /*MonederoViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Historico_Piezas"];
        [self.navigationController pushViewController:controller animated:YES];*/
    }
    //prueba
    MonederoViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Historico_Piezas"];
    controller.instancia = instancia;
    controller.salida_sitio = salida_sitio;
    [self.navigationController pushViewController:controller animated:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    [dao select_articulos:instancia :[orden_visita_instancia integerValue]];
}

/*-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    VC_Surtir *controller = (VC_Surtir *)segue.destinationViewController;
    controller.salida_sitio = salida_sitio;
    controller.instancia = instancia;
    controller.primera_visita = false;
    controller.inv_ini_array = arreglo_articulos;
}*/


-(void)ObtenerProductos{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [dao select_articulos:instancia :[orden_visita_instancia integerValue]];
}

-(BOOL)revisarCapacidadEspiral:(int)cantidad index:(NSIndexPath*)indexPath{
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    int espiral = (uint32_t)[articulo.capacidad integerValue];
    if(cantidad>espiral)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//Primera visita
-(void)primera_visita
{
    NSLog(@"%d",instancia);
    BOOL primera_visita = [dao verificacion_primera_visita:instancia];
    if (primera_visita) {
        [self continuar_en_cero];
    }
}

-(void)continuar_en_cero{
    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];

        NSInteger inventario_ini = 0;
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_visita = [articulo.orden_visita integerValue];
        
        [dao update_inventario_inicial:inventario_ini id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita id_instancia:instancia];
        
    }
    VC_Surtir *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_surtir"];
    controller.salida_sitio = salida_sitio;
    controller.instancia = instancia;
    controller.inv_ini_array = arreglo_articulos;//[[NSMutableArray alloc]init];
    //for (int posicion = 0; posicion < [textFieldContent count]; posicion++) {
    //    [controller.inv_ini_array addObject:@"0"];
    //}
    controller.primera_visita = true;
    [self.navigationController pushViewController: controller animated:YES];
}

@end
