//
//  spGenerales.m
//  RutaIV
//
//  Created by Miguel Banderas on 06/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "spGenerales.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation spGenerales

-(NSString *)Sp_Verificar_SalidaRuta:(int)nRuta{
    NSString *vlResultado = @"";
    NSString *vlOrdenServicio = @"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    sqlite3_stmt *sentenciaSelect;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"SELECT ID_ORDEN_SERVICIO FROM CAB_ORDEN_SERVICIO"];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                vlOrdenServicio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaInsert, 0)];
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:vlOrdenServicio forKey:@"nIdOrdenServicio"];
                [prefs synchronize];
                
                NSString *sqlSelect = [NSString stringWithFormat:@"SELECT CASE WHEN HORA_SALIDA IS NULL THEN 'N' ELSE 'S' END AS Salio FROM CAB_ORDEN_SERVICIO WHERE ID_RUTA = %d AND ID_ORDEN_SERVICIO = %@",nRuta,vlOrdenServicio];
                if(sqlite3_prepare_v2(dataBase, [sqlSelect UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
                    if(sqlite3_step(sentenciaSelect)){
                        vlResultado = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect,0)];
                    }
                }
                else{
                    
                }
                sqlite3_finalize(sentenciaSelect);
            }
            else{
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return vlResultado;
}

-(NSInteger)sp_RevisaEstatusOrden:(int)nIdOrdenServicio wAplica:(NSString *)sAplica{
    NSInteger nResultado;
    NSString *sIdOrdenServicio;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaSelect;
    sqlite3_stmt *sentenciaSelect2;
    if ([sAplica isEqualToString:@"S"]) {
        if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
            NSString *sql = [NSString stringWithFormat:@"SELECT SUM(CASE WHEN NOT HORA_INICIO IS NULL THEN 0 ELSE 1 END) AS ABIERTA FROM DET_ORDEN_SERVICIO WHERE ID_ORDEN_SERVICIO = %d",nIdOrdenServicio];
            if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
                if(sqlite3_step(sentenciaSelect)){
                    nResultado = [[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)] intValue];
                }
                else{
                }
                sqlite3_finalize(sentenciaSelect);
            }
        }else{
            NSLog(@"No se ha podido abrir la base de datos");
        }
        sqlite3_close(dataBase);
    }
    else{
        //Se trae la orden de servicio
        if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
            NSString *sql = [NSString stringWithFormat:@"SELECT ID_ORDEN_SERVICIO FROM CAB_ORDEN_SERVICIO"];
            if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
                if(sqlite3_step(sentenciaSelect)){
                    sIdOrdenServicio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:sIdOrdenServicio forKey:@"nIdOrdenServicio"];
                    [prefs synchronize];
                    NSString *sSql = [NSString stringWithFormat:@"SELECT SUM(CASE WHEN NOT (HORA_INICIO = '01/01/1900 00:00:00' OR HORA_INICIO='') THEN 0 ELSE 1 END) AS ABIERTA FROM DET_ORDEN_SERVICIO WHERE ID_ORDEN_SERVICIO=%@ AND TIPO_AUTORIZACION=''",sIdOrdenServicio];
                    if(sqlite3_prepare_v2(dataBase, [sSql UTF8String], -1, &sentenciaSelect2, NULL) == SQLITE_OK){
                        if(sqlite3_step(sentenciaSelect2)){
                            nResultado = [[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect2, 0)] intValue];
                        }
                        else{
                        }
                        sqlite3_finalize(sentenciaSelect2);
                    }
                }
                else{
                }
                sqlite3_finalize(sentenciaSelect);
            }
        }else{
            NSLog(@"No se ha podido abrir la base de datos");
        }
        sqlite3_close(dataBase);
        
    }
    return nResultado;
}

-(void)sp_RevisaFinInstancias:(int)nIdRuta{
    
    NSString *sFecha, *nNoDia, *nOrdenSitio, *nOrdenVisita, *nIdInstancia;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaSelect;
    sqlite3_stmt *sentenciaSelect2;
    sqlite3_stmt *sentenciaSelect3;
    sqlite3_stmt *sentenciaSelect4;
    sqlite3_stmt *sentenciaSelect5;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM FECHA_ORDEN_PROCESAR"];
        if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaSelect)){
                sFecha = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
                nNoDia = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 1)];
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:sFecha forKey:@"sFecha"];
                [prefs setObject:nNoDia forKey:@"nNoDia"];
                [prefs synchronize];
                NSString *sSql = [NSString stringWithFormat:@"SELECT MAX(SITIOS_ASIGNADOS.ID_ORDEN) AS OrdenSitio FROM SITIOS_ASIGNADOS WHERE SITIOS_ASIGNADOS.ID_RUTA = %d AND NO_GRID = %@",nIdRuta,nNoDia];
                if(sqlite3_prepare_v2(dataBase, [sSql UTF8String], -1, &sentenciaSelect2, NULL) == SQLITE_OK){
                    if(sqlite3_step(sentenciaSelect2)){
                        nOrdenSitio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect2, 0)];
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:nOrdenSitio forKey:@"nOrdenSitio"];
                        [prefs synchronize];
                        
                        NSString *sSql2 = [NSString stringWithFormat:@"SELECT MAX(DETALLE.ORDEN_VISITA) AS ORDEN_VISITA FROM SITIOS_ASIGNADOS INNER JOIN UBICACIONES2 ON SITIOS_ASIGNADOS.ID_SITIO=UBICACIONES2.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=UBICACIONES2.ID_CLIENTE INNER JOIN SITIOS ON SITIOS_ASIGNADOS.ID_SITIO = SITIOS.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE = SITIOS.ID_CLIENTE INNER JOIN DET_SITIOS_ASIGNADOS DETALLE ON SITIOS_ASIGNADOS.ID_SITIO = DETALLE.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE = DETALLE.ID_CLIENTE AND SITIOS_ASIGNADOS.ID_RUTA = DETALLE.ID_RUTA AND SITIOS_ASIGNADOS.ID_ORDEN = DETALLE.ID_ORDEN AND DIA = %@ INNER JOIN INSTANCIAS_SERVICIO SERVICIO ON SITIOS_ASIGNADOS.ID_SITIO = SERVICIO.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE = SERVICIO.ID_CLIENTE AND UBICACIONES2.ID_UBICACION = SERVICIO.ID_UBICACION AND DETALLE.ID_INSTANCIA = SERVICIO.ID_INSTANCIA WHERE SITIOS_ASIGNADOS.ID_RUTA=%d AND NO_GRID=%@ AND SITIOS_ASIGNADOS.ID_ORDEN=%@",nNoDia,nIdRuta,nNoDia,nOrdenSitio];
                        if(sqlite3_prepare_v2(dataBase, [sSql2 UTF8String], -1, &sentenciaSelect3, NULL) == SQLITE_OK){
                            if(sqlite3_step(sentenciaSelect3)){
                                nOrdenVisita = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect3, 0)];
                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                [prefs setObject:nOrdenVisita forKey:@"nOrdenVisita"];
                                [prefs synchronize];
                                NSString *sSql3 = [NSString stringWithFormat:@"SELECT MAX(SERVICIO.ID_INSTANCIA) AS INSTANCIA FROM SITIOS_ASIGNADOS INNER JOIN UBICACIONES2 ON SITIOS_ASIGNADOS.ID_SITIO=UBICACIONES2.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=UBICACIONES2.ID_CLIENTE INNER JOIN SITIOS ON SITIOS_ASIGNADOS.ID_SITIO=SITIOS.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=SITIOS.ID_CLIENTE INNER JOIN DET_SITIOS_ASIGNADOS DETALLE ON  SITIOS_ASIGNADOS.ID_SITIO=DETALLE.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=DETALLE.ID_CLIENTE AND SITIOS_ASIGNADOS.ID_RUTA=DETALLE.ID_RUTA AND SITIOS_ASIGNADOS.ID_ORDEN=DETALLE.ID_ORDEN AND DIA=%@ INNER JOIN INSTANCIAS_SERVICIO SERVICIO ON SITIOS_ASIGNADOS.ID_SITIO=SERVICIO.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=SERVICIO.ID_CLIENTE AND UBICACIONES2.ID_UBICACION=SERVICIO.ID_UBICACION AND DETALLE.ID_INSTANCIA=SERVICIO.ID_INSTANCIA WHERE SITIOS_ASIGNADOS.ID_RUTA=%d AND NO_GRID=%@ AND SITIOS_ASIGNADOS.ID_ORDEN=%@ AND DETALLE.ORDEN_VISITA=%@",nNoDia,nIdRuta,nNoDia,nOrdenSitio,nOrdenVisita];
                                if(sqlite3_prepare_v2(dataBase, [sSql3 UTF8String], -1, &sentenciaSelect4, NULL) == SQLITE_OK){
                                    if(sqlite3_step(sentenciaSelect4)){
                                        nIdInstancia = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect4, 0)];
                                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                        [prefs setObject:nIdInstancia forKey:@"nIdInstancia"];
                                        [prefs synchronize];
                                        NSString *sSql4 = [NSString stringWithFormat:@"SELECT SITIOS_ASIGNADOS.ID_ORDEN, SITIOS_ASIGNADOS.ID_CLIENTE, SITIOS_ASIGNADOS.ID_SITIO, SERVICIO.ID_INSTANCIA FROM SITIOS_ASIGNADOS INNER JOIN UBICACIONES2 ON SITIOS_ASIGNADOS.ID_SITIO=UBICACIONES2.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=UBICACIONES2.ID_CLIENTE INNER JOIN SITIOS ON SITIOS_ASIGNADOS.ID_SITIO=SITIOS.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=SITIOS.ID_CLIENTE INNER JOIN DET_SITIOS_ASIGNADOS DETALLE ON  SITIOS_ASIGNADOS.ID_SITIO=DETALLE.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=DETALLE.ID_CLIENTE AND SITIOS_ASIGNADOS.ID_RUTA=DETALLE.ID_RUTA AND SITIOS_ASIGNADOS.ID_ORDEN=DETALLE.ID_ORDEN AND DIA=%@ INNER JOIN INSTANCIAS_SERVICIO SERVICIO ON SITIOS_ASIGNADOS.ID_SITIO=SERVICIO.ID_SITIO AND SITIOS_ASIGNADOS.ID_CLIENTE=SERVICIO.ID_CLIENTE AND UBICACIONES2.ID_UBICACION=SERVICIO.ID_UBICACION AND DETALLE.ID_INSTANCIA=SERVICIO.ID_INSTANCIA WHERE SITIOS_ASIGNADOS.ID_RUTA=%d AND NO_GRID=%@ AND SITIOS_ASIGNADOS.ID_ORDEN=%@ AND SERVICIO.ID_INSTANCIA=%@",nNoDia,nIdRuta,nNoDia,nOrdenSitio,nIdInstancia];
                                        if(sqlite3_prepare_v2(dataBase, [sSql4 UTF8String], -1, &sentenciaSelect5, NULL) == SQLITE_OK){
                                            if(sqlite3_step(sentenciaSelect5)){
                                                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                [prefs setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect5, 0)] forKey:@"nOrdenFin"];
                                                [prefs setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect5, 1)] forKey:@"nClienteFin"];
                                                [prefs setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect5, 2)] forKey:@"nSitioFin"];
                                                [prefs setObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect5, 3)] forKey:@"nInstanciaFin"];
                                                NSString *sOrdenFin = [prefs stringForKey:@"nOrdenFin"];
                                                NSString *sClienteFin = [prefs stringForKey:@"nClienteFin"];
                                                NSString *sSitioFin = [prefs stringForKey:@"nSitioFin"];
                                                NSString *sInstanciaFin = [prefs stringForKey:@"nInstanciaFin"];
                                                NSLog(@"%@ %@ %@ %@",sOrdenFin,sClienteFin,sSitioFin,sInstanciaFin);
                                                [prefs synchronize];
                                            }
                                            else{
                                            }
                                            sqlite3_finalize(sentenciaSelect5);
                                        }
                                    }
                                    else{
                                    }
                                    sqlite3_finalize(sentenciaSelect4);
                                }
                            }
                            else{
                            }
                            sqlite3_finalize(sentenciaSelect3);
                        }
                    }
                    else{
                    }
                    sqlite3_finalize(sentenciaSelect2);
                }
            }
            else{
            }
            sqlite3_finalize(sentenciaSelect);
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
}

-(NSMutableDictionary *)sp_revisaSitioPorRuta:(int)nIdRuta wIdOrdenServicio:(NSString*)nIdOrdenServicio{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaSelect;
    NSMutableDictionary *sitioPorRuta = [NSMutableDictionary dictionary];
    NSString *actual = @"";
    NSMutableArray *arregloDatos = [NSMutableArray arrayWithObjects:nil];
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT ID_ORDEN, CAST(ID_ORDEN AS TEXT) || '. ' || RTRIM(SITIO) SITIO, SITIOS_TP.ID_CLIENTE, SITIOS_TP.ID_SITIO, %@ AS ID_ORDEN_SERVICIO,UBICACIONES2.NOMBRE || ' - ' || SITIOS_TP.INSTANCIA as NOMBRE, ID_INSTANCIA FROM SITIOS_TP INNER JOIN UBICACIONES2 ON SITIOS_TP.ID_CLIENTE=UBICACIONES2.ID_CLIENTE AND SITIOS_TP.ID_SITIO=UBICACIONES2.ID_SITIO AND SITIOS_TP.ID_UBICACION=UBICACIONES2.ID_UBICACION WHERE ID_ORDEN IN (SELECT DISTINCT ORDEN_VISITA FROM MOVIMIENTOS_INSTANCIA WHERE ID_RUTA = %d AND ((INICIO=1) OR (INICIO=0)) AND ID_ORDEN_SERVICIO=%@) ORDER BY ID_ORDEN,UBICACIONES2.ID_UBICACION",nIdOrdenServicio,nIdRuta,nIdOrdenServicio];
        //NSString *sql = [NSString stringWithFormat:@"SELECT ID_ORDEN, CAST(ID_ORDEN AS TEXT) || '. ' || RTRIM(SITIO) SITIO, SITIOS_TP.ID_CLIENTE, SITIOS_TP.ID_SITIO, %@ AS ID_ORDEN_SERVICIO,UBICACIONES2.NOMBRE || ' - ' || SITIOS_TP.INSTANCIA as NOMBRE, ID_INSTANCIA FROM SITIOS_TP INNER JOIN UBICACIONES2 ON SITIOS_TP.ID_CLIENTE=UBICACIONES2.ID_CLIENTE AND SITIOS_TP.ID_SITIO=UBICACIONES2.ID_SITIO AND SITIOS_TP.ID_UBICACION=UBICACIONES2.ID_UBICACION WHERE ID_ORDEN IN (SELECT DISTINCT ORDEN_VISITA FROM MOVIMIENTOS_INSTANCIA WHERE ID_RUTA = %d AND (INICIO=1 AND SALTEADO=0) AND ID_ORDEN_SERVICIO=%@) ORDER BY ID_ORDEN,UBICACIONES2.ID_UBICACION",nIdOrdenServicio,nIdRuta,nIdOrdenServicio];
        if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
            while(sqlite3_step(sentenciaSelect) == SQLITE_ROW){
                
                if ([actual isEqualToString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)]]) {
                    [arregloDatos addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 6)]];
                }
                else{
                    if ([actual isEqualToString:@""]) {
                        //Primera carga
                        actual = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
                        [arregloDatos addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 6)]];
                    }
                    else{
                        [sitioPorRuta setObject:arregloDatos forKey:actual]; //Sitio
                        actual = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
                        arregloDatos = [NSMutableArray arrayWithObjects:nil];
                        [arregloDatos addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 6)]];
                    }
                    
                }
                /*NSArray *arregloValor = @[[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)],[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 2)],[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 3)]]; //nOrden,nIdCliente,nIdSitio
                
                NSMutableArray *arreglo = [[NSMutableArray alloc] initWithArray:arregloValor];
                
                [sitioPorRuta setObject:arreglo forKey:[NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 1)]]; //Sitio
                 */

            }
            [sitioPorRuta setObject:arregloDatos forKey:actual]; //Sitio
        }
    }
    return sitioPorRuta;
}

-(NSString *)traeSitio:(NSString *)nIdOrden
{
    NSString *bValida = @"";
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaSelect;

    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        NSString *sql = [NSString stringWithFormat:@"SELECT DISTINCT CAST(ID_ORDEN AS TEXT) || '. ' || RTRIM(SITIO) SITIO FROM SITIOS_TP WHERE ID_ORDEN=%@",nIdOrden];
        if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
            while(sqlite3_step(sentenciaSelect) == SQLITE_ROW){
                bValida = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
            }
        }
    }
    
    return bValida;
}

-(NSMutableArray *)traeInstancia:(int)nIdRuta wIdOrdenServicio:(NSString*)nIdOrdenServicio wIdOrden:(NSString *)nIdOrden wIdInstancia:(NSString *)nIdInstancia
{
    NSMutableArray *objetos = [[NSMutableArray alloc]init];
    NSString *sRegresa = @"";
    NSString *tipoMaquina = @"";
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaSelect;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        NSString *sql = [NSString stringWithFormat:@"SELECT UBICACIONES2.NOMBRE || ' - ' || SITIOS_TP.INSTANCIA, INSTANCIAS_SERVICIO.ID_TIPO_MAQUINA FROM SITIOS_TP INNER JOIN UBICACIONES2 ON SITIOS_TP.ID_CLIENTE=UBICACIONES2.ID_CLIENTE AND SITIOS_TP.ID_SITIO=UBICACIONES2.ID_SITIO INNER JOIN INSTANCIAS_SERVICIO ON SITIOS_TP.ID_INSTANCIA=INSTANCIAS_SERVICIO.ID_INSTANCIA WHERE ID_ORDEN IN (SELECT DISTINCT ORDEN_VISITA FROM MOVIMIENTOS_INSTANCIA WHERE ID_RUTA = %d AND ((INICIO=1) OR (INICIO =0)) AND ID_ORDEN_SERVICIO=%@) AND SITIOS_TP.ID_ORDEN=%@ AND SITIOS_TP.ID_INSTANCIA=%@ ORDER BY ID_ORDEN,UBICACIONES2.ID_UBICACION",nIdRuta,nIdOrdenServicio,nIdOrden,nIdInstancia];
        if(sqlite3_prepare_v2(dataBase, [sql UTF8String], -1, &sentenciaSelect, NULL) == SQLITE_OK){
            while(sqlite3_step(sentenciaSelect) == SQLITE_ROW){
                sRegresa = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 0)];
                tipoMaquina = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaSelect, 1)];
                [objetos addObject:sRegresa];
                [objetos addObject:tipoMaquina];
            }
        }
    }
    return objetos;
}

//NSString *sql = [NSString stringWithFormat:@"SELECT UBICACIONES2.NOMBRE || ' - ' || SITIOS_TP.INSTANCIA, INSTANCIAS_SERVICIO.ID_TIPO_MAQUINA FROM SITIOS_TP INNER JOIN UBICACIONES2 ON SITIOS_TP.ID_CLIENTE=UBICACIONES2.ID_CLIENTE AND SITIOS_TP.ID_SITIO=UBICACIONES2.ID_SITIO INNER JOIN INSTANCIAS_SERVICIO ON SITIOS_TP.ID_INSTANCIA=INSTANCIAS_SERVICIO.ID_INSTANCIA WHERE ID_ORDEN IN (SELECT DISTINCT ORDEN_VISITA FROM MOVIMIENTOS_INSTANCIA WHERE ID_RUTA = %d AND (INICIO=1 AND SALTEADO=0) AND ID_ORDEN_SERVICIO=%@) AND SITIOS_TP.ID_ORDEN=%@ AND SITIOS_TP.ID_INSTANCIA=%@ ORDER BY ID_ORDEN,UBICACIONES2.ID_UBICACION",nIdRuta,nIdOrdenServicio,nIdOrden,nIdInstancia];
@end
