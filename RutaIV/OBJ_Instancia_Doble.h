//
//  OBJ_Instancia_Doble.h
//  RutaIV
//
//  Created by Miguel Banderas on 19/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Instancia_Doble : NSObject

@property NSString * instancia;
@property NSString * minimo;
@property NSString * maximo;

-(id)init :(NSString *)id_instancia orden_minima:(NSString *)minima orden_maxima:(NSString *)maxima;

@end
