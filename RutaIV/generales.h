//
//  generales.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface generales : NSObject

-(BOOL)eliminaDumpDex;
-(BOOL)eliminaCambioMonedero;

@end
