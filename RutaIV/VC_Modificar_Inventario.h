//
//  VC_Modificar_Inventario.h
//  RutaIV
//
//  Created by Miguel Banderas on 07/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"

@interface VC_Modificar_Inventario : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    __weak IBOutlet UITableView *myTableview;
    ProductosDAO *dao;
    NSMutableArray * arreglo_articulos;
    UIToolbar* keyboardDoneButtonView;
    int contador_rellenado;
    __weak IBOutlet UIButton *btn_back;
}

@property int instancia;
@property NSString * orden_visita;
@property NSIndexPath * index;
- (IBAction)action_back:(id)sender;

@end






