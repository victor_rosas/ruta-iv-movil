//
//  VC_Pruebas_Inventario.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 13/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Pruebas_Inventario.h"
#import "AppDelegate.h"
#import "OBJ_Articulo.h"

@interface VC_Pruebas_Inventario ()

@end

@implementation VC_Pruebas_Inventario

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)select_articulos:(NSInteger)id_instancia :(NSInteger)orden_visita articulo:(NSInteger)articulo
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA, ID_ARTICULO, INV_INI, SURTIO, REMOVIO, CADUCARON, CLAVE, ORDEN_VISITA, ID_ORDEN_SERVICIO, ID_ESPIRAL, ID_CHAROLA, CAPACIDAD_ESPIRAL, ARTICULO, PRECIO, ESPIRAL, CAMBIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND CAMBIO != 'V' AND ORDEN_VISITA = %d AND ID_ARTICULO = %d",(long)id_instancia, orden_visita,articulo];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_articulo      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * inv_ini          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * surtio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * removio          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * caduco           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * clave            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * orden_visita     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * id_orden_serv    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_espiral       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * id_charola       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * capacidad        = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * nombre_articulo  = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * precio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                NSString * espiral          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,14)];
                NSString * cambio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,15)];
                
                OBJ_Articulo * articulo = [[OBJ_Articulo alloc]init:id_instancia id_articulo:id_articulo inv_ini:inv_ini surtio:surtio removio:removio caduco:caduco clave:clave orden_visita:orden_visita id_orden_servicio:id_orden_serv id_espiral:id_espiral id_charola:id_charola capacidad:capacidad articulo:nombre_articulo precio:precio espiral:espiral cambio:cambio];
                [arreglo_articulos addObject:articulo];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    int meh =(uint32_t)[arreglo_articulos count];
}

-(void)select_all_articulos:(NSInteger)id_instancia :(NSInteger)orden_visita articulo:(NSInteger)articulo
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA, ID_ARTICULO, INV_INI, SURTIO, REMOVIO, CADUCARON, CLAVE, ORDEN_VISITA, ID_ORDEN_SERVICIO, ID_ESPIRAL, ID_CHAROLA, CAPACIDAD_ESPIRAL, ARTICULO, PRECIO, ESPIRAL, CAMBIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND CAMBIO != 'V' AND ORDEN_VISITA = %d",(long)id_instancia, orden_visita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_articulo      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * inv_ini          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * surtio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * removio          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * caduco           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * clave            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * orden_visita     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * id_orden_serv    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_espiral       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * id_charola       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * capacidad        = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * nombre_articulo  = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * precio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                NSString * espiral          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,14)];
                NSString * cambio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,15)];
                
                OBJ_Articulo * articulo = [[OBJ_Articulo alloc]init:id_instancia id_articulo:id_articulo inv_ini:inv_ini surtio:surtio removio:removio caduco:caduco clave:clave orden_visita:orden_visita id_orden_servicio:id_orden_serv id_espiral:id_espiral id_charola:id_charola capacidad:capacidad articulo:nombre_articulo precio:precio espiral:espiral cambio:cambio];
                [arreglo_articulos addObject:articulo];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    int meh = [arreglo_articulos count];
}

- (IBAction)btn_buscar:(id)sender {
    NSInteger instancia = [label_instancia.text integerValue];
    NSInteger orden = [label_orden.text integerValue];
    NSInteger articulo = [label_articulo.text integerValue];
    
    if (articulo == 0) {
        [self select_all_articulos:instancia :orden articulo:articulo];
    }
    else
    {
        [self select_articulos:instancia :orden articulo:articulo];
    }
}
@end
