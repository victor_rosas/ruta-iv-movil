//
//  VC_Existencias.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TC_Existencias.h"

@interface VC_Existencias : UIViewController <UITableViewDataSource,UITableViewDelegate>{
    
    __weak IBOutlet UITableView *myTableView;
    NSMutableArray * arreglo_sugerido;
    NSArray *array_keys;
}
@property NSMutableDictionary *array_existencias;
@property NSMutableDictionary *array_nombres;
@property NSMutableArray * arreglo_articulos;
@property NSString * origen;

@end
