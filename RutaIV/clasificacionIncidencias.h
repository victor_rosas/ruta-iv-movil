//
//  clasificacionIncidencias.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface clasificacionIncidencias : NSObject

-(BOOL)borraClasificacionIncidencias;

-(BOOL)insertaClasificacionIncidencias:(int)nIdClasificacion wDescripcion:(NSString *)sDescripcion;

@end
