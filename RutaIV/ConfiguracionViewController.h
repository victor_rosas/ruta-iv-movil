//
//  ConfiguracionViewController.h
//  RutaIV
//
//  Created by Miguel Banderas on 05/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfiguracionViewController : UIViewController
{
    NSString * sWebService;
    NSString *sUUID;
    IBOutlet UITextField *txtUsuario;
    IBOutlet UITextField *txtSucursal;
    IBOutlet UITextField *txtPassword;
    IBOutlet UILabel *lblUUID;
    NSString * cambio_realizado;
    
    UIToolbar* keyboardDoneButtonView;
    NSMutableArray * array_text_fields;
    int current;
}
- (IBAction)btnGuardar:(id)sender;
- (NSString *)createNewUUID;
- (void) ObtenerDatos;
- (BOOL) validarDatos;
- (IBAction)back_pressed:(id)sender;


@end
