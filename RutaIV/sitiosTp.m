//
//  sitiosTp.m
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "sitiosTp.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation sitiosTp

-(BOOL)borraSitiosTp{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM SITIOS_TP"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;

}

-(BOOL)insertaSitiosTp:(NSString *)sSitio wInstancia:(NSString *)sInstancia wIdPlanograma:(int)nIdPlanograma wIdOrden:(int)nIdOrden wIdInstancia:(int)nIdInstancia wIdUbicacion:(int)nIdUbicacion wOrdenVisita:(int)nOrdenVisita wIdSitio:(int)nIdSitio wIdCliente:(int)nIdCliente{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO SITIOS_TP(\"SITIO\",\"INSTANCIA\",\"ID_PLANOGRAMA\",\"ID_ORDEN\",\"ID_INSTANCIA\",\"ID_UBICACION\",\"ORDEN_VISITA\",\"ID_SITIO\",\"ID_CLIENTE\") VALUES(\"%@\",\"%@\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\")",sSitio,sInstancia,nIdPlanograma,nIdOrden,nIdInstancia,nIdUbicacion,nOrdenVisita,nIdSitio,nIdCliente];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
