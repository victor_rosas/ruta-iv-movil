//
//  VC_Instancias.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import "Hora_inicioDAO.h"
#import "ProductosDAO.h"
#import "CabeceroOperaciones.h"
#import "ImagenDAO.h"
#import "DATA_Progreso_Ruta.h"

//SOLAMENTE PRUEBA
#import "ExistenciasDAO.h"

@interface VC_Instancias : UIViewController <UITableViewDelegate, UITableViewDataSource, SWTableViewCellDelegate,UIAlertViewDelegate>{
    NSInteger horaenviada;
    NSMutableArray *arreglo_visitadas;
    NSMutableArray *arreglo_salteadas;
    NSMutableArray *imagenes;
    BOOL primer_blanco;
    NSIndexPath *activa;
    //variable horas llegada
    BOOL salida_sitio;
    Hora_inicioDAO *dao;
    ProductosDAO *dao_producto;
    CabeceroOperaciones *cabecerooperaciones;
    ImagenDAO *imagendao;
    BOOL reachable;
    
    DATA_Progreso_Ruta * data_manager_progreso;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView2;
- (IBAction)action_map_pressed:(id)sender;


@end
