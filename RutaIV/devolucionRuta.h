//
//  devolucionRuta.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface devolucionRuta : NSObject

-(BOOL)borraCabDevolucionRuta;
-(BOOL)borraDetDevolucionRuta;

@end
