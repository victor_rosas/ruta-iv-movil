//
//  OBJ_Observaciones.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Observaciones.h"

@implementation OBJ_Observaciones
//Variables del objeto
@synthesize id_reporte;
@synthesize id_clasificacion;
@synthesize id_empleado;
@synthesize fecha_incidencias;
@synthesize descripcion;
@synthesize id_orden_servicio;
@synthesize id_instancia;
@synthesize orden_visita;
@synthesize id_maquina;
@synthesize servicio;
@synthesize fecha_atencion;
@synthesize contacto;
@synthesize tel_contacto;
@synthesize subido;

//inicializacion
-(id)init:(NSString *)reporte id_clasificacion:(NSString *)clasificacion id_empleado:(NSString *)empleado fecha_incidencia:(NSString *)fecha descripcion:(NSString *)texto_descripcion id_orden_servicio:(NSString *)orden_servicio id_instancia:(NSString *)instancia orden_visita:(NSString *)visita id_maquina:(NSString *)maquina servicio:(NSString *)bit_servicio fecha_atencion:(NSString *)atencion contacto:(NSString *)texto_contacto tel_contacto:(NSString *)telefono_contacto subido:(NSString *)bit_subido
{
    self.id_reporte = reporte;
    self.id_clasificacion = clasificacion;
    self.id_empleado = empleado;
    self.fecha_incidencias = fecha;
    self.descripcion = texto_descripcion;
    self.id_orden_servicio = orden_servicio;
    self.id_instancia = instancia;
    self.orden_visita = visita;
    self.id_maquina = maquina;
    self.servicio = bit_servicio;
    self.fecha_atencion = atencion;
    self.contacto = texto_contacto;
    self.tel_contacto = telefono_contacto;
    self.subido = bit_servicio;
    return self;
}


@end
