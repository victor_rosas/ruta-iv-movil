//
//  TVC_Inventario_Carga_Actual.h
//  RutaIV
//
//  Created by Miguel Banderas on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBJ_Inventario_Queries.h"

@interface TVC_Inventario_Carga_Actual : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate>{
    NSMutableArray *inventario;
    NSMutableArray *filtrado;
    NSMutableDictionary *dictionary_productos;
    NSArray *headers;
    OBJ_Inventario_Queries *metodos;
    NSString *parametrizacion_mayusculas;
}
- (IBAction)back_pressed:(id)sender;

@end
