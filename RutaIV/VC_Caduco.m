//
//  VC_Caduco.m
//  RutaIV
//
//  Created by Miguel Banderas on 12/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Caduco.h"
#import "TC_Cafe.h"
#import "OBJ_Articulo.h"
#import "MonederoViewController.h"
#import "CamaraUpload.h"
#import "OBJ_Instancia_Doble.h"

@interface VC_Caduco ()

@end

@implementation VC_Caduco
@synthesize id_instancia;
@synthesize salida_sitio;
@synthesize removio_caduco;
@synthesize aparecio;
@synthesize cantidad_aparecido;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    arreglo_articulos = [[NSMutableArray alloc]init];
    data_articulo_manager = [[ProductosDAO alloc]init];
    inventario_superado = false;
    alerta_presente = false;
    
    if ([removio_caduco isEqualToString:@"removio"]) {
        removio = true;
        self.title = @"Remover";
    }
    else
    {
        removio = false;
        self.title = @"Caducos";
    }
    
    [self inicializacion_alertas];
    
    //Inicializacion de vistas
    [self button_setup];
    
    //Cargar datos
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [data_articulo_manager select_articulos:id_instancia :[orden_visita_instancia integerValue]];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = [NSIndexPath indexPathForRow:0 inSection:0];
    [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
    
    [self update_en_cero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Aspecto y funciones de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_articulos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * identifier;
    if (removio) {
        identifier = @"TC_Removio";
    }
    else
    {
        identifier = @"TC_Caduco";
    }
    
    TC_Cafe *cell = (TC_Cafe *)[myTableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    NSString * capacidad = [NSString stringWithFormat:@"C.E. - %@", articulo.capacidad];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString * precio = [formatter stringFromNumber:[NSNumber numberWithFloat:[articulo.precio integerValue]]];
    
    //Creacion de la celda
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    if (removio) {
        cell.text_field_cantidad.text = articulo.removio;
        if (![articulo.removio isEqualToString:@"0"])
        {
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    else
    {
        cell.text_field_cantidad.text = articulo.caduco;
        if(![articulo.caduco isEqualToString:@"0"])
        {
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    
    NSString * nombre_articulo = articulo.articulo;
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        nombre_articulo = [nombre_articulo capitalizedString];
    }
    
    [cell cell_setup:nombre_articulo numeracion:articulo.espiral capacidad:capacidad costo:precio];
    
    //Se agrega boton a todos los text fields de las celdas
    cell.text_field_cantidad.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
}

//Seleccion de celda cuando se inicia modificacion en TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    if (inventario_superado) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
            [myTableView selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:index_path_pasado];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_path_pasado];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_capacidad animated:YES completion:nil];
            }
            
        });
    }
    else
    {
        index_path_pasado = index_path;
        [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self ajuste_scroll:index_path];
    }
    
}

//Guardar informacion de TextField y activacion de boton
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    
    if (removio) {
        articulo.removio = [NSString stringWithFormat:@"%@%@",textField.text,string];
    }
    else
    {
       articulo.caduco = [NSString stringWithFormat:@"%@%@",textField.text,string];
    }
    
    //En caso de borrar es necesario hacer esto
    if ([string isEqualToString:@""]) {
        int lenght =(uint32_t)[textField.text length];
        NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
        if (removio) {
            articulo.removio = texto_actualizado;
        }
        else
        {
            articulo.caduco = texto_actualizado;
        }
    }
    [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
}

//Recargar informacion en cell recien abandonada
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    TC_Cafe * cell = (TC_Cafe*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path_pasado.row];
    
    if (removio) {
        if ([articulo.removio isEqualToString:@""]) {
            articulo.removio = @"0";
        }
    }
    else
    {
        if ([articulo.caduco isEqualToString:@""]) {
            articulo.caduco = @"0";
        }
    }
    
    [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self verificacion_inventario_inicial:index_path_pasado];
    
}

//Setup del boton
- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableView indexPathForSelectedRow];
    NSIndexPath * next;
    
    //Revision orden
    BOOL continuar = true;
    
    if (continuar) {
        if (selected.row+1 == [arreglo_articulos count])
        {
            next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
            [self regresar_anterior:selected posicion_regreso:0];
        }
        else
        {
            next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
        }
        
        [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        [self ajuste_scroll:next];
        
        TC_Cafe *cell = (TC_Cafe *)[myTableView cellForRowAtIndexPath:next];
        UITextField * text_field = cell.text_field_cantidad;
        [text_field becomeFirstResponder];
    }
}

//Metodo para regresar a posicion lejana
-(void)regresar_anterior :(NSIndexPath *)selected posicion_regreso:(int)posicion
{
    while (selected.row > posicion+4) {
        selected = [NSIndexPath indexPathForRow:selected.row-3 inSection:selected.section];
        [myTableView selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
}

//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_articulos count])
    {
        [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableView scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}



//Verificacion ventas cafe anterior
-(void)verificacion_inventario_inicial :(NSIndexPath *)index_path
{
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    int inventario_inicial = (uint32_t)[articulo.inventario_inicial_real integerValue];
    
    
    
    if (removio) {
        int removio_num =(uint32_t)[articulo.removio integerValue];
        if (removio_num > inventario_inicial)
        {
            articulo.removio = @"0";
            [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
            NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
            
            [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
            
            inventario_superado = true;
        }
        else
        {
            inventario_superado = false;
        }
    }
    else
    {
        int caduco = (uint32_t)[articulo.caduco integerValue];
        if (caduco > inventario_inicial)
        {
            articulo.caduco = @"0";
            [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
            NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
            
            [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
            
            inventario_superado = true;
        }
        else
        {
            inventario_superado = false;
        }
    }
}

-(void)inicializacion_alertas
{
    alerta_capacidad = [UIAlertController
                     alertControllerWithTitle:@"Error"
                     message:@"Inventario inicial superado."
                     preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    alerta_presente = false;
                                    [alerta_capacidad dismissViewControllerAnimated:YES completion:nil];
                                }];
    
    [alerta_capacidad addAction:ok];
}


- (IBAction)action_back:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)action_siguiente:(id)sender {
    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:@"Continuar"
                                 message:@"Seleccionar una opción."
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    if (!(cantidad_aparecido == 2)) {
        if (![aparecio isEqualToString:@"Removio"]) {
            UIAlertAction* action_removio = [UIAlertAction
                                             actionWithTitle:@"Removio"
                                             style:UIAlertActionStyleDefault
                                             handler:^(UIAlertAction * action)
                                             {
                                                 VC_Caduco * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Removio"];
                                                 controller.id_instancia = id_instancia;
                                                 controller.salida_sitio = salida_sitio;
                                                 controller.removio_caduco = @"removio";
                                                 controller.cantidad_aparecido = 2;
                                                 [self.navigationController pushViewController:controller animated:YES];
                                                 [self guardar_informacion];
                                                 [view dismissViewControllerAnimated:YES completion:nil];
                                                 
                                             }];
            [view addAction:action_removio];
        }
        
        if (![aparecio isEqualToString:@"Caduco"]) {
            UIAlertAction* caduco = [UIAlertAction
                                     actionWithTitle:@"Caduco"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         VC_Caduco * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Caduco"];
                                         controller.id_instancia = id_instancia;
                                         controller.salida_sitio = salida_sitio;
                                         controller.removio_caduco = @"caduco";
                                         controller.cantidad_aparecido = 2;
                                         [self.navigationController pushViewController:controller animated:YES];
                                         [self guardar_informacion];
                                         [view dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            [view addAction:caduco];
        }

    }
    
    UIAlertAction* monedero = [UIAlertAction
                               actionWithTitle:@"Fotografía"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   CamaraUpload * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Camara"];
                                   controller.instancia = id_instancia;
                                   controller.salida_sitio = salida_sitio;
                                   [self.navigationController pushViewController:controller animated:YES];
                                   [self guardar_informacion];
                                   [view dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancelar"
                             style:UIAlertActionStyleDestructive
                             handler:^(UIAlertAction * action)
                             {
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    
    
    
    [view addAction:monedero];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

-(void)guardar_informacion
{
    BOOL repeticion = false;
    OBJ_Instancia_Doble * instancia_repetida = [self getInstanciaRepetida];
    if (!(instancia_repetida == nil))
    {
        repeticion = true;
    }

    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        NSInteger cantidad = 0;
        if (removio) {
            cantidad = [articulo.removio integerValue];
        }
        else
        {
            cantidad = [articulo.caduco integerValue];
        }
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_vsita = [articulo.orden_visita integerValue];
        NSInteger instancia = id_instancia;
        NSInteger inv_inicial = [articulo.inventario_inicial_real integerValue];
        
        if (removio) {
            [data_articulo_manager update_removio:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
            
            if (repeticion) {
                NSString * maximo = instancia_repetida.maximo;
                [data_articulo_manager update_existencia_removio:inv_inicial removio:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:id_instancia maximo:[maximo integerValue]];
            }
        }
        else
        {
            [data_articulo_manager update_caduco:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
            
            if (repeticion) {
                NSString * maximo = instancia_repetida.maximo;
                [data_articulo_manager update_existencia_caduco:inv_inicial caduco:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:id_instancia maximo:[maximo integerValue]];
            }
        }
    }
}

-(OBJ_Instancia_Doble*)getInstanciaRepetida
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    OBJ_Instancia_Doble * instancia_doble = [data_articulo_manager getInstanciaRepetida:id_instancia orden:[orden_visita_instancia integerValue]];
    return instancia_doble;
}

-(void)update_en_cero
{
    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        NSInteger cantidad = 0;
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_vsita = [articulo.orden_visita integerValue];
        NSInteger instancia = id_instancia;
        
        if (removio) {
            [data_articulo_manager update_removio:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
        }
        else
        {
            [data_articulo_manager update_caduco:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
        }
        
    }
}

@end
