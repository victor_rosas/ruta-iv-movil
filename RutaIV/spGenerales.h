//
//  spGenerales.h
//  RutaIV
//
//  Created by Miguel Banderas on 06/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface spGenerales : NSObject

-(NSString *)Sp_Verificar_SalidaRuta:(int)nRuta;
-(NSInteger)sp_RevisaEstatusOrden:(int)nIdOrdenServicio wAplica:(NSString *)sAplica;
-(void)sp_RevisaFinInstancias:(int)nIdRuta;
-(NSMutableDictionary *)sp_revisaSitioPorRuta:(int)nIdRuta wIdOrdenServicio:(NSString*)nIdOrdenServicio;
-(NSString *)traeSitio:(NSString *)nIdOrden;
-(NSMutableArray *)traeInstancia:(int)nIdRuta wIdOrdenServicio:(NSString*)nIdOrdenServicio wIdOrden:(NSString *)nIdOrden wIdInstancia:(NSString *)nIdInstancia;

@end
