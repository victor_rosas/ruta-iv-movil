//
//  UMPruebaTableViewCell.m
//  RutaIV
//
//  Created by Miguel Banderas on 19/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "UMPruebaTableViewCell.h"

@implementation UMPruebaTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
