//
//  VC_Modificar_Inventario.m
//  RutaIV
//
//  Created by Miguel Banderas on 07/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Modificar_Inventario.h"
#import "TableViewCell.h"
#import "OBJ_Articulo.h"

@interface VC_Modificar_Inventario ()
@end

@implementation VC_Modificar_Inventario
@synthesize orden_visita;
@synthesize instancia;
@synthesize index;

- (void)viewDidLoad {
    [super viewDidLoad];
    dao = [[ProductosDAO alloc] init];
    arreglo_articulos = [[NSMutableArray alloc]init];
    orden_visita = [[NSUserDefaults standardUserDefaults] objectForKey:@"orden_visita_seleccionado"];
    [self ObtenerProductos];
    [self button_setup];
    contador_rellenado = 0;
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = index;
    [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

-(void)ObtenerProductos{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    arreglo_articulos = [dao select_articulos:instancia :[orden_visita_instancia integerValue]];
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

//Metodos de la tabla
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arreglo_articulos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TableViewCell *cell = (TableViewCell *)[myTableview dequeueReusableCellWithIdentifier:@"Cell_Modificar" forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:indexPath.row];
    
    //Creacion de la celda
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    if([parametrizacion_mayusculas isEqualToString:@"0"])
    {
        cell.myLabel.text = [articulo.articulo capitalizedString];
    }
    else
    {
        cell.myLabel.text = articulo.articulo;
    }
    
    cell.Espiral.text = articulo.espiral;
    
    cell.label_capacidad.text = [NSString stringWithFormat:@"C.E. -  %@", articulo.capacidad];
    NSString *precio_string = articulo.precio;
    float precio = precio_string.floatValue;
    cell.label_costo.text = [formatter stringFromNumber:[NSNumber numberWithFloat:precio]];
    
    if ([articulo.cambio isEqualToString:@"N"]) {
        cell.backgroundColor = [UIColor colorWithRed:145.0/255.0f green:145.0/255.0f blue:145.0/255.0f alpha:1.0];
        cell.myTextbox.text = articulo.inventario_inicial_real;
        articulo.inv_ini = articulo.inventario_inicial_real;
        [arreglo_articulos replaceObjectAtIndex:indexPath.row withObject:articulo];
    }
    else
    {
        if (![articulo.inventario_inicial_real isEqualToString:@""])
        {
            cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        cell.myTextbox.text = articulo.inventario_inicial_real;
    }
    
    //Se agrega boton a todos los text fields de las celdas
    cell.myTextbox.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    TableViewCell * cell = (TableViewCell*)textField.superview.superview;
    NSIndexPath * index_path = [myTableview indexPathForCell:cell];
    [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
    [self ajuste_scroll:index_path];
}

//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_articulos count])
    {
        [myTableview selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableview scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableview indexPathForSelectedRow];
    NSIndexPath * next;
    next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
    
    TableViewCell *cell = (TableViewCell *)[myTableview cellForRowAtIndexPath:next];
    UITextField * text_field = cell.myTextbox;
    [text_field becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)action_back:(id)sender {
    [self actualizar_inventario];
    [self.navigationController popViewControllerAnimated:true];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (contador_rellenado == 0) {
        btn_back.enabled = true;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    TableViewCell * cell = (TableViewCell*)textField.superview.superview;
    NSIndexPath * index_path = [myTableview indexPathForCell:cell];
    
    OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:index_path.row];
    
    if ([textField.text isEqualToString:@""]&&(![string isEqualToString:@""])) {
        contador_rellenado--;
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    
    //Verificacion capacidad
    NSString *  inventario_inicial = [NSString stringWithFormat:@"%@%@",textField.text,string];
    if ([inventario_inicial integerValue]>[articulo.capacidad integerValue]) {
        contador_rellenado ++;
        articulo.inventario_inicial_real = @"";
        cell.myTextbox.text = @"";
        UIAlertController * alerta=   [UIAlertController
                                            alertControllerWithTitle:@"Error"
                                            message:@"Capacidad de espiral superada."
                                            preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alerta dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alerta addAction:ok];
        [self presentViewController:alerta animated:YES completion:nil];
        cell.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        articulo.inventario_inicial_real = inventario_inicial;
    }
    
    //En caso de borrar es necesario hacer esto
    if ([string isEqualToString:@""]) {
        int lenght = (uint32_t)[textField.text length];
        NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
        articulo.inventario_inicial_real = texto_actualizado;
        
        if ([texto_actualizado isEqualToString:@""]) {
            contador_rellenado ++;
            articulo.inventario_inicial_real = @"";
            cell.backgroundColor = [UIColor whiteColor];
        }
    }
    [arreglo_articulos replaceObjectAtIndex:index_path.row withObject:articulo];
    
    //Decide cuando se desactiva el boton
    if (!(contador_rellenado == 0)) {
        btn_back.enabled = false;
    }
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
}

-(void)actualizar_inventario
{
    for (int i = 0; i<[arreglo_articulos count]; i++) {
        OBJ_Articulo * articulo = [arreglo_articulos objectAtIndex:i];
        
        NSInteger inv_inicial = [articulo.inventario_inicial_real integerValue];
        NSInteger id_articulo = [articulo.id_articulo integerValue];
        NSInteger id_charola = [articulo.id_charola integerValue];
        NSInteger id_espiral = [articulo.id_espiral integerValue];
        NSInteger orden_servicio = [articulo.id_orden_servicio integerValue];
        NSInteger orden_visita_string = [articulo.orden_visita integerValue];
        
        [dao update_inventario_inicial:inv_inicial id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_visita_string id_instancia:instancia];
    }
}
@end
