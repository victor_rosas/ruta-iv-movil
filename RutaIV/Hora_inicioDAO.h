//
//  Hora_inicioDAO.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 03/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface Hora_inicioDAO : NSObject{
sqlite3 *bd;
    OBJ_Log_Proceso *logProceso;
}

-(NSString *) obtenerRutaBD;
-(BOOL)ActualizaHora_Inicio:(NSInteger)ID_INSTANCIA ID_OPERACION:(NSInteger)ID_OPERACION HORA_INICIO:(NSString *)Start_hour HORA_INICIO_ENVIADA:(NSInteger)HORA_INICIO_ENVIADA id_sitio:(NSInteger)id_sitio orden_visita:(NSInteger)orden_visita;

-(BOOL)ActualizaLLEGADA_Cabecero:(NSInteger)ID_ORDEN_SERVICIO HORA_LLEGADA:(NSInteger)HORA_LLEGADA HORA_LLEGADA_ENV:(NSInteger)HORA_LLEGADA_ENV ID_RUTA:(NSInteger)ID_RUTA;
-(BOOL)ActualizaSALIDA_Cabecero:(NSInteger)ID_ORDEN_SERVICIO HORA_SALIDA:(NSString *)HORA_SALIDA HORA_SALIDA_ENV:(NSInteger)HORA_SALIDA_ENV ID_RUTA:(NSInteger)ID_RUTA;
-(BOOL)ActualizaHora_LLEGADA:(NSInteger)ID_ORDEN_SERVICIO ID_RUTA:(NSInteger)ID_RUTA HORA_LLEGADA:(NSString *)HORA_LLEGADA HORA_LLEGADA_ENVIADA:(NSInteger)HORA_LLEGADA_ENVIADA;
-(BOOL)ActualizaHora_SALIDA:(NSInteger)ID_ORDEN_SERVICIO ID_RUTA:(NSInteger)ID_RUTA HORA_SALIDA:(NSString *)HORA_SALIDA HORA_SALIDA_ENVIADA:(NSInteger)HORA_SALIDA_ENVIADA;
-(BOOL)update_inicio_instancia :(NSInteger)id_instancia;
@end
