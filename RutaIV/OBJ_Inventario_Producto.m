//
//  OBJ_Inventario_Producto.m
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Inventario_Producto.h"

@implementation OBJ_Inventario_Producto
@synthesize producto;
@synthesize cantidad;
@synthesize tipo_articulo;

-(id)init:(NSString *)nombre cantidad:(int)numero tipo:(NSString *)id_tipo{
    producto = nombre;
    cantidad = numero;
    tipo_articulo = id_tipo;
    return self;
}

@end
