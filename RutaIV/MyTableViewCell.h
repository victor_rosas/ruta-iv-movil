//
//  MyTableViewCell.h
//  Keyboard_button_2
//
//  Created by Jose Miguel Banderas Lechuga on 19/11/14.
//  Copyright (c) 2014 NULL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell
{
    NSString *stringVariable;
}
@property (weak, nonatomic) IBOutlet UILabel *myLabel;
@property (weak, nonatomic) IBOutlet UITextField *myTextbox;


@end
