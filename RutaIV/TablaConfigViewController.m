//
//  TablaConfigViewController.m
//  RutaIV
//
//  Created by Miguel Banderas on 16/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "TablaConfigViewController.h"
#import "ConfiguracionViewController.h"
#import "VC_Configuracion_Camara.h"
#import "VC_Configuracion_General.h"
#import "TVC_Sucursal.h"

@interface TablaConfigViewController ()
@property (nonatomic,strong) NSString *strUsuario;

@end

@implementation TablaConfigViewController
@synthesize menu;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /*menu = [[NSMutableArray alloc] init];
    
    NSDictionary *iniciaSesion = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Inicia sesión",@"usuario.png",nil] forKeys:[NSArray arrayWithObjects:@"titulo",@"imagen",nil]];
     NSDictionary *seleccionaSucursal = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Selecciona sucursal",@"sucursales.png",nil] forKeys:[NSArray arrayWithObjects:@"titulo",@"imagen",nil]];
     NSDictionary *generales = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Generales",@"generales.png",nil] forKeys:[NSArray arrayWithObjects:@"titulo",@"imagen",nil]];
    
    [menu addObject:iniciaSesion];
    [menu addObject:seleccionaSucursal];
    [menu addObject:generales];*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return 32;
    
    return [menu count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *celda= (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"reusar"];
     
     if (celda==nil) {
     celda=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"reusar"];
     }
     
     celda.textLabel.text = @"Hola";
     
     return celda;*/
    
    /*NSString *simpleIdentifier = @"SimpleIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
    }
    
    NSDictionary *opciones = [menu objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [opciones objectForKey:@"titulo"];
    cell.imageView.image = [UIImage imageNamed:[opciones objectForKey:@"imagen"]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*NSDictionary *opciones = [menu objectAtIndex:indexPath.row];
    NSString *rowValue = [opciones objectForKey:@"titulo"];;
    
    
    
    if ([rowValue  isEqual: @"Inicia sesión"]) {
        ConfiguracionViewController *configuracion = [self.storyboard instantiateViewControllerWithIdentifier:@"vLogin"];
        [self.navigationController pushViewController:configuracion animated:YES];
    }else
    {
        if (_strUsuario == NULL) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe iniciar sesión en este dispositivo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
            [alert show];
        }
        else{
            if ([rowValue  isEqual: @"Selecciona sucursal"]){
                SucursalConfigViewController *sucursal = [self.storyboard instantiateViewControllerWithIdentifier:@"vSucursalConfig"];
                [self.navigationController pushViewController:sucursal animated:YES];
            
            }else if ([rowValue  isEqual: @"Generales"]){
            
            }
        }
    }*/
    
    [self ObtenerDatos];
    if(indexPath.row == 0)
    {
        ConfiguracionViewController *configuracion = [self.storyboard instantiateViewControllerWithIdentifier:@"vLogin"];
        [self.navigationController pushViewController:configuracion animated:YES];
    }
    if (_strUsuario == NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe iniciar sesión en este dispositivo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    if(indexPath.row == 1)
    {
        NSString *descargado = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_descargado"];
        if ([descargado isEqualToString:@"1"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"La información ya fue descargada. Acceder a configuraciones generales en caso de querer volver a hacerlo." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else
        {
            TVC_Sucursal *sucursal = [self.storyboard instantiateViewControllerWithIdentifier:@"vSucursalConfig"];
            [self.navigationController pushViewController:sucursal animated:YES];
        }
    }
    
    
    if(indexPath.row == 2)
    {
        VC_Configuracion_General *general = [self.storyboard instantiateViewControllerWithIdentifier:@"vGeneral"];
        [self.navigationController pushViewController:general animated:YES];
    }
    
    if(indexPath.row == 3)
    {
        VC_Configuracion_Camara *camara = [self.storyboard instantiateViewControllerWithIdentifier:@"camara_configuracion"];
        [self.navigationController pushViewController:camara animated:YES];
    }
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) ObtenerDatos
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    _strUsuario = [prefs stringForKey:@"sUsuario"];
    
}


- (void) limpiar
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

    [prefs setObject:NULL forKey:@"sUsuario"];
    [prefs setObject:NULL forKey:@"sContrasena"];
    [prefs setObject:NULL forKey:@"sSucursal"];
    [prefs setObject:NULL forKey:@"sWebService"];
    [prefs setObject:NULL forKey:@"sUUID"];
    [prefs synchronize];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)bak_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
@end
