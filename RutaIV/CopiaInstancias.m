//
//  CopiaInstancias.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "CopiaInstancias.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation CopiaInstancias

-(BOOL)borraInstancias
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM INSTANCIAS_SERVICIO"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
    
}

-(BOOL)copiaInstancias:(int)nIdInstancia wIdCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wIdUbicacion:(int)nIdUbicacion wIdTipoMaquina:(int)nIdTipoMaquina wIdPlanograma:(int)nIdPlanograma wIdMaquina:(int)nIdMaquina wRegistros:(int)nRegistros wDifHoraMins:(int)nDifHoraMins wCashless:(int)nCashless latitud:(NSString *)sLatitud longitud:(NSString *)sLongitud{
    
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO INSTANCIAS_SERVICIO(\"ID_INSTANCIA\",\"ID_CLIENTE\",\"ID_SITIO\",\"ID_UBICACION\",\"ID_TIPO_MAQUINA\",\"ID_PLANOGRAMA\",\"ID_MAQUINA\",\"REGISTROS\",\"DIF_HORA_MINS\",\"CASHLESS\",\"LATITUD\",\"LONGITUD\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\",\"%@\")",nIdInstancia,nIdCliente,nIdSitio,nIdUbicacion,nIdTipoMaquina,nIdPlanograma,nIdMaquina,nRegistros,nDifHoraMins,nCashless,sLatitud,sLongitud];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
    
}


-(BOOL)algo{
    return TRUE;
}

@end
