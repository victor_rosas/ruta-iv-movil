//
//  CabeceroDAO.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 09/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "CabeceroDAO.h"
#import "AppDelegate.h"

@implementation CabeceroDAO

-(NSString *) obtenerRutaBD{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSString *)CargarOrdendeservicio{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT Id_Orden_Servicio FROM CAB_ORDEN_SERVICIO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *Orden = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                
                Ordendeservicio = Orden;
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Ordendeservicio;
}

-(NSString *)Cargardifhoraminutos{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT max(dif_hora_mins) from INSTANCIAS_SERVICIO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *dif = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                
                dif_hora_min = dif;
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    
    return dif_hora_min;
}


-(NSMutableArray*)CargarMonederoup{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    monedero = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from CAMBIO_MONEDERO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicmonedero = [[NSMutableDictionary alloc]init];
                
                NSString *Id_instancia= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_moneda= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *cantidad= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *ordenvisita= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                
                [dicmonedero setValue:Id_instancia forKey:@"id_instancia"];
                [dicmonedero setValue:Id_moneda forKey:@"id_moneda"];
                [dicmonedero setValue:cantidad forKey:@"cantidad"];
                [dicmonedero setValue:ordenvisita forKey:@"ordenvisita"];
                
                [monedero addObject:dicmonedero];
                
            }NSLog(@"monedero Error sql: %s",sqlite3_errmsg(database));
        }NSLog(@"monedero Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return monedero;
}

-(NSMutableArray *)CargarDUMPDEX{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    dumpdex = [[NSMutableArray alloc]init];
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from MOVIMIENTOS_INSTANCIA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicdumpex = [[NSMutableDictionary alloc]init];
                
                NSString *Campo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_instancia= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Id_sitio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Id_cliente = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Ordenvisita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                
                
                [dicdumpex setValue:Campo forKey:@"campo"];
                [dicdumpex setValue:Id_instancia forKey:@"id_instancia"];
                [dicdumpex setValue:Id_sitio forKey:@"id_sitio"];
                [dicdumpex setValue:Id_cliente forKey:@"id_cliente"];
                [dicdumpex setValue:Ordenvisita forKey:@"ordenvisita"];
                
                [dumpdex addObject:dicdumpex];
                
            }
        }
        NSLog(@"Dumpex Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return dumpdex;
}

-(NSMutableArray *)Cargarventas_hist_instancia{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    ventas = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from VENTAS_HIST_INSTANCIA WHERE RECOGIO_DINERO IS NOT NULL"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicventasinstancia = [[NSMutableDictionary alloc]init];
                
                NSString *Id_instancia= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Ordenvisita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Unidades = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Monto = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Recogiodinero = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                
                
                [dicventasinstancia setValue:Id_instancia forKey:@"id_instancia"];
                [dicventasinstancia setValue:Ordenvisita forKey:@"ordenvisita"];
                [dicventasinstancia setValue:Unidades forKey:@"unidades"];
                [dicventasinstancia setValue:Monto forKey:@"monto"];
                [dicventasinstancia setValue:Recogiodinero forKey:@"recogiodinero"];
                
                [ventas addObject:dicventasinstancia];
                
            }
        }
        
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return ventas;
}


-(NSMutableArray *)Cargartabla_cortes{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    cortes = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from VENTAS_HIST_INSTANCIA WHERE RECOGIO_DINERO IS NOT NULL"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dictablascortes = [[NSMutableDictionary alloc]init];
                
                NSString *Campo= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Id_sitio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Id_cliente = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                
                
                [dictablascortes setValue:Campo forKey:@"campo"];
                [dictablascortes setValue:Id_instancia forKey:@"id_instancia"];
                [dictablascortes setValue:Id_sitio forKey:@"id_sitio"];
                [dictablascortes setValue:Id_cliente forKey:@"id_cliente"];
                [dictablascortes setValue:Orden_visita forKey:@"orden_visita"];
                
                [cortes addObject:dictablascortes];
                
            }
        }
        
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return cortes;
}

-(NSMutableArray *)CargarQuery07{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    query07 = [[NSMutableArray alloc]init];
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT CLAVE,INV_INI,SURTIO,REMOVIO,CADUCARON,INICIO,SALTEADO,PRECIO_DEX, PRUEBAS_SEL,VENTAS_CAFE_ANT,VENTAS_PRUEBAS_ANT,INSTANCIA_NUEVA FROM MOVIMIENTOS_INSTANCIA ORDER BY CLAVE"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery07 = [[NSMutableDictionary alloc]init];
                
                NSString *Clave= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Inv_ini = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Surtio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Removio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Caducaron = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Inicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString *Salteado = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *PrecioDex= @"0";//[NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString *Pruebas_Sel = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString *Ventas_cafe_ant = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString *Ventas_pruebas_ant = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString *Instancia_nueva = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                
                
                [dicquery07 setValue:Clave forKey:@"clave"];
                [dicquery07 setValue:Surtio forKey:@"surtio"];
                [dicquery07 setValue:Removio forKey:@"removio"];
                [dicquery07 setValue:Caducaron forKey:@"caducaron"];
                [dicquery07 setValue:Salteado forKey:@"salteado"];
                [dicquery07 setValue:PrecioDex forKey:@"preciodex"];
                [dicquery07 setValue:Inicio forKey:@"inicio"];
                [dicquery07 setValue:Pruebas_Sel forKey:@"pruebas_sel"];
                [dicquery07 setValue:Inv_ini forKey:@"inv_ini"];
                [dicquery07 setValue:Ventas_cafe_ant forKey:@"ventas_cafe_ant"];
                [dicquery07 setValue:Ventas_pruebas_ant forKey:@"ventas_pruebas_ant"];
                [dicquery07 setValue:Instancia_nueva forKey:@"instancia_nueva"];
                
                [query07 addObject:dicquery07];
                
                
            }
        }
        
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query07;
}

-(NSMutableArray *)CargarQuery08{//REVISAR SUM
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    query08 = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_ORDEN_SERVICIO,ID_INSTANCIA,CLAVE,SUM(EXISTENCIA_TEORICA),SUM(EXISTENCIA_FISICA),SUM(AJUSTE),CONTENEDOR FROM INVENTARIO_INSTANCIAS GROUP BY ID_ORDEN_SERVICIO,ID_INSTANCIA,CLAVE,CONTENEDOR ORDER BY ID_ORDEN_SERVICIO,ID_INSTANCIA,CONTENEDOR,CLAVE"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery08 = [[NSMutableDictionary alloc]init];
                
                NSString *Id_orden_servicio= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Clave = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Existencia_teorica = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Existencia_fisica = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Ajuste = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString *Contenedor = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                
                
                
                [dicquery08 setValue:Id_orden_servicio forKey:@"id_orden_servicio"];
                [dicquery08 setValue:Id_instancia forKey:@"id_instancia"];
                [dicquery08 setValue:Clave forKey:@"clave"];
                [dicquery08 setValue:Existencia_teorica forKey:@"existencia_teorica"];
                [dicquery08 setValue:Existencia_fisica forKey:@"existencia_fisica"];
                [dicquery08 setValue:Ajuste forKey:@"ajuste"];
                [dicquery08 setValue:Contenedor forKey:@"contenedor"];
                [query08 addObject:dicquery08];
                
            }NSLog(@"Query08 Error sql: %s",sqlite3_errmsg(database));
        }
        NSLog(@"Query08 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query08;
}

-(NSMutableArray *)CargarQuery09:(int)vlOrdenservicio{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    query09 = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT %d As ID_ORDEN_SERVICIO, INSTANCIA,NOMENCLATURA_IV,PRECIO_IV,NOMENCLATURA_DEX,PRECIO_DEX FROM DEX_IV ORDER BY INSTANCIA,NOMENCLATURA_IV",vlOrdenservicio];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery09 = [[NSMutableDictionary alloc]init];
                
                NSString *Id_orden_servicio= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Nomenclatura_IV = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Precio_IV = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Nomenclatura_dex = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Precio_dex = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                
                
                
                
                [dicquery09 setValue:Id_orden_servicio forKey:@"id_orden_servicio"];
                [dicquery09 setValue:Id_instancia forKey:@"id_instancia"];
                [dicquery09 setValue:Nomenclatura_IV forKey:@"nomenclatura_iv"];
                [dicquery09 setValue:Precio_IV forKey:@"precio_iv"];
                [dicquery09 setValue:Nomenclatura_dex forKey:@"nomenclatura_dex"];
                [dicquery09 setValue:Precio_dex forKey:@"precio_dex"];
                
                [query09 addObject:dicquery09];
            }
        }
        NSLog(@"Query09 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query09;
}

-(NSMutableArray*)CargarQuery12{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    query12 = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM REPORTE_INCIDENCIAS"]; //WHERE SUBIDO =0"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery12 = [[NSMutableDictionary alloc]init];
                
                NSString *Id_clasificacion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_empleado = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Descripcion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *Orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString *Id_maquina = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString *Servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                
                
                [dicquery12 setValue:Id_clasificacion forKey:@"id_clasificacion"];
                [dicquery12 setValue:Id_empleado forKey:@"id_empleado"];
                [dicquery12 setValue:Descripcion forKey:@"descripcion"];
                [dicquery12 setValue:Id_instancia forKey:@"id_instancia"];
                [dicquery12 setValue:Orden_visita forKey:@"orden_visita"];
                [dicquery12 setValue:Id_maquina forKey:@"id_maquina"];
                [dicquery12 setValue:Servicio forKey:@"servicio"];
                
                [query12 addObject:dicquery12];
            }
        }
        NSLog(@"Query 12 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query12;
}

-(NSMutableDictionary *)CargarQuery13{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM REPORTE_INCIDENCIAS"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery13 = [[NSMutableDictionary alloc]init];
                
                NSString *Id_clasificacion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_empleado = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Descripcion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *Orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString *Id_maquina = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                
                NSString *Servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                
                [dicquery13 setValue:Id_clasificacion forKey:@"id_clasificacion"];
                [dicquery13 setValue:Id_empleado forKey:@"id_empleado"];
                [dicquery13 setValue:Descripcion forKey:@"descripcion"];
                [dicquery13 setValue:Id_instancia forKey:@"id_instancia"];
                [dicquery13 setValue:Orden_visita forKey:@"orden_visita"];
                [dicquery13 setValue:Id_maquina forKey:@"id_maquina"];
                [dicquery13 setValue:Servicio forKey:@"servicio"];
                
            }
        }
        NSLog(@"Query 13 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return dicquery13;
}

-(NSMutableArray *)CargarQuery14{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CAB_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery14 = [[NSMutableDictionary alloc]init];
                
                NSString *Id_orden_servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Tipo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                
                [dicquery14 setValue:Id_orden_servicio forKey:@"id_orden_servicio"];
                [dicquery14 setValue:Id_instancia forKey:@"id_instancia"];
                [dicquery14 setValue:Orden_visita forKey:@"orden_visita"];
                [dicquery14 setValue:Tipo forKey:@"tipo"];
                
                [query14 addObject:dicquery14];
            }
        }
        NSLog(@"Query 14 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query14;
}

-(NSMutableArray*)CargarQuery15{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    query15 = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        //NSString *sentencianonulos = [NSString stringWithFormat:@"SELECT IFNULL(CHAROLA,0),IFNULL(ESPIRAL,0),IFNULL (INV_INI,0) FROM DET_DEVOLUCION_RUTA"];
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM DET_DEVOLUCION_RUTA "];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery15 = [[NSMutableDictionary alloc]init];
                
                //NSString *Clave = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *Id_devolucion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Id_devuelto = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Cantidad = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Charola = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Espiral = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString *Inv_ini = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                if ([Charola isEqualToString:@"(null)"]) {
                    Charola = [NSString stringWithFormat:@"0"];
                }
                if ([Espiral isEqualToString:@"(null)"]) {
                    Espiral = [NSString stringWithFormat:@"0"];
                }
                if ([Inv_ini isEqualToString:@"(null)"]) {
                    Inv_ini = [NSString stringWithFormat:@"0"];
                }
                
                //[dicquery15 setValue:Clave forKey:@"clave"];
                [dicquery15 setValue:Id_devolucion forKey:@"id_devolucion"];
                [dicquery15 setValue:Id_devuelto forKey:@"id_devuelto"];
                [dicquery15 setValue:Cantidad forKey:@"cantidad"];
                [dicquery15 setValue:Charola forKey:@"charola"];
                [dicquery15 setValue:Espiral forKey:@"espiral"];
                [dicquery15 setValue:Inv_ini forKey:@"inv_ini"];
                
                [query15 addObject:dicquery15];
            }
        }
        NSLog(@"Query 15 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query15;
}

-(NSMutableArray *)CargarQuery16{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    query16 = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CONTACTO_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery16 = [[NSMutableDictionary alloc]init];
                
                //NSString *Clave = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *Id_devolucion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Nombre = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Telefono = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Cantidad = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                
                
                //[dicquery16 setValue:Clave forKey:@"clave"];
                [dicquery16 setValue:Id_devolucion forKey:@"id_devolucion"];
                [dicquery16 setValue:Nombre forKey:@"nombre"];
                [dicquery16 setValue:Telefono forKey:@"telefono"];
                [dicquery16 setValue:Cantidad forKey:@"cantidad"];
                
                [query16 addObject:dicquery16];
            }
        }
        NSLog(@"Query 16 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return query16;
}

-(BOOL)ActualizarCargado{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE FECHA_ORDEN_PROCESAR SET CARGADO= %d",1];//WHERE CARGADO = 1//SE AGREGO ESTO PARA HACER EL UPDATE
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Actualizar cargadoError sql: %s",sqlite3_errmsg(dataBase));
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
    
}

-(NSMutableDictionary *)CargarQuery18{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CONTACTO_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicquery18 = [[NSMutableDictionary alloc]init];
                
                NSString *Existencia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Clave = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                
                [dicquery18 setValue:Clave forKey:@"clave"];
                [dicquery18 setValue:Existencia forKey:@"existencia"];
                
            }
        }
        NSLog(@"Query 18 Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return dicquery18;
}

-(NSMutableDictionary *)Acthorasalidallegadaruta{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CAB_ORDEN_SERVICIO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicHoraruta = [[NSMutableDictionary alloc]init];
                
                NSString *Hora_salida = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *Hora_llegada = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString *Hora_salida_enviada = @"1";//[NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString *Hora_llegada_enviada = @"1";//[NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString *Id_orden_servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                //NSString *Valida = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                
                [dicHoraruta setValue:Hora_salida forKey:@"hora_salida"];
                [dicHoraruta setValue:Hora_llegada forKey:@"hora_llegada"];
                [dicHoraruta setValue:Hora_salida_enviada forKey:@"hora_salida_enviada"];
                [dicHoraruta setValue:Hora_llegada_enviada forKey:@"hora_llegada_enviada"];
                [dicHoraruta setValue:Id_orden_servicio forKey:@"id_orden_servicio"];
            }
        }
        NSLog(@"Acthorasaildallegadaruta Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return dicHoraruta;
    
}

-(NSMutableArray *)Acthorasalidallegadainstancia{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    horas = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM DET_ORDEN_SERVICIO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicHorainstancia = [[NSMutableDictionary alloc]init];
                
                NSString *Hora_Inicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString *Orden = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString *Id_operacion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                
                
                [dicHorainstancia setValue:Hora_Inicio forKey:@"hora_inicio"];
                [dicHorainstancia setValue:Orden forKey:@"orden"];
                [dicHorainstancia setValue:Id_instancia forKey:@"id_instancia"];
                [dicHorainstancia setValue:Id_operacion forKey:@"id_operacion"];
                
                [horas addObject:dicHorainstancia];
            }
        }
        NSLog(@"Acthorasalidallegadaruta Error sql: %s",sqlite3_errmsg(database));
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return horas;
    
}

-(NSMutableArray *)CargarFOTOS_VISITA{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    fotos = [[NSMutableArray alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from FOTOS_VISITA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                dicfotos = [[NSMutableDictionary alloc]init];
                
                NSString *Id_orden_servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Id_foto = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString *URL = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                
                
                [dicfotos setValue:Id_orden_servicio forKey:@"id_orden_servicio"];
                [dicfotos setValue:Id_instancia forKey:@"id_instancia"];
                [dicfotos setValue:Orden_visita forKey:@"orden_visita"];
                [dicfotos setValue:Id_foto forKey:@"id_foto"];
                [dicfotos setValue:URL forKey:@"url"];
            
                
                [fotos addObject:dicfotos];
                
            }
        }
        
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return fotos;
}

-(BOOL)ActualizarFecha_orden_procesar{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE FECHA_ORDEN_PROCESAR SET CARGADO = 1"];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Fecha orden procesar Error sql: %s",sqlite3_errmsg(dataBase));
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
    
}

-(BOOL)Actualizarcab_orden_servicio:(int)id_orden_servicio {
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"BEGIN TRANSACTION; UPDATE CAB_ORDEN_SERVICIO SET HORA_SALIDA = NULL, HORA_LLEGADA = NULL WHERE ID_ORDEN_SERVICIO = %d IF @@ERROR <> 0 BEGIN ROLLBACK TRANSACTION; SELECT 999; END ELSE BEGIN COMMIT TRANSACTION; SELECT 0; END ",id_orden_servicio];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Actualizar orden de servicio Error sql: %s",sqlite3_errmsg(dataBase));
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
    
}


@end
