//
//  sitiosTp.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface sitiosTp : NSObject

-(BOOL)borraSitiosTp;

-(BOOL)insertaSitiosTp:(NSString *)sSitio wInstancia:(NSString *)sInstancia wIdPlanograma:(int)nIdPlanograma wIdOrden:(int)nIdOrden wIdInstancia:(int)nIdInstancia wIdUbicacion:(int)nIdUbicacion wOrdenVisita:(int)nOrdenVisita wIdSitio:(int)nIdSitio wIdCliente:(int)nIdCliente;


@end
