//
//  VC_Mapa.m
//  RutaIV
//
//  Created by Miguel Banderas on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Mapa.h"
#import "DATA_Mapa.h"

@interface VC_Mapa ()
@end

@implementation VC_Mapa
@synthesize mapView;
@synthesize locationManager;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    mapView.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
#endif
    [self.locationManager startUpdatingLocation];
    
    mapView.showsUserLocation = YES;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    NSLog(@"%@", [self deviceLocation]);
    
    //View Area
    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = self.locationManager.location.coordinate.latitude;
    region.center.longitude = self.locationManager.location.coordinate.longitude;
    region.span.longitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [mapView setRegion:region animated:YES];
    
    
    //Muestra la localizacion de las instancias
    Data_mapa = [[DATA_Mapa alloc]init];
    NSMutableDictionary * localizaciones = [[NSMutableDictionary alloc]init];
    localizaciones = [Data_mapa cargar_localizacion];
    
    //Mostrar localizaciones en el mapa
    NSArray * sitios = [[NSArray alloc]init];
    sitios = [localizaciones allKeys];
    
    int offset = 0;
    
    for (int i = 0; i<[sitios count]; i++) {
        NSString * sitio = [sitios objectAtIndex:i];
        NSMutableDictionary * d_sitio = [localizaciones objectForKey:sitio];
        NSArray * instancias = [[NSArray alloc]init];
        instancias = [d_sitio allKeys];
        
        NSValue * coordenadas = [d_sitio objectForKey:[instancias objectAtIndex:0]];
        CGPoint point = [coordenadas CGPointValue];
        //offset++;
        
        NSString *address = sitio;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(point.x+offset, point.y+offset);
        MKPlacemark *mPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
       
        // Create an editable PointAnnotation, using placemark's coordinates, and set your own title/subtitle
        MKPointAnnotation * punto = [[MKPointAnnotation alloc]init];
        punto.coordinate = mPlacemark.coordinate;
        punto.title = address;
        
        NSString * subtitulo = @"";
        for (int e = 0; e<[instancias count]; e++) {
            NSString * temporal = subtitulo;
            if (e == 0) {
                subtitulo = [NSString stringWithFormat:@"Instancias: %@",[instancias objectAtIndex:e]];
            }
            else
            {
                subtitulo = [NSString stringWithFormat:@"%@, %@",temporal, [instancias objectAtIndex:e]];
            }
        }
        punto.subtitle = subtitulo;
        
        [mapView addAnnotation:punto];
        [mapView selectAnnotation:mPlacemark animated:NO];
    }
    
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}

@end
