//
//  maquinas.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface maquinas : NSObject

-(BOOL)borraMaquinas;

-(BOOL)insertaMaquinas:(int)nIdTipoMaquina wDescripcionCorta:(NSString *)sDescripcionCorta wBebida:(int)nBebida wSnack:(int)nSnack wCafe:(int)nCafe;

@end
