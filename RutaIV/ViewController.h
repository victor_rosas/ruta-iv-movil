//
//  ViewController.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CabeceroDAO.h"
#import "CabeceroOperaciones.h"
#import "CamaraUpload.h"
#import "XMLDictionary.h"
#import "Hora_inicioDAO.h"
#import "GRRequestsManager.h"
#import "Monedero_y_contadoresDAO.h"
#import "OBJ_Log_Proceso.h"
#import "CustomBadge.h"

@interface ViewController : UIViewController
{
    NSInteger list;
   NSMutableArray *imagenes;
    NSString *pendientes;
    MBProgressHUD *HUD;
    MBProgressHUD *uHUD;
    __weak IBOutlet UIScrollView *scroll_view;
    BOOL descargo_info;
    __weak IBOutlet UIImageView *image_view;
    BOOL perform_segue;
    __weak IBOutlet UIBarButtonItem *btn_descarga;
    CustomBadge *badge;
    Monedero_y_contadoresDAO *mdao;
    ImagenDAO *dao;
    CabeceroDAO *cabecerodao;
    CabeceroWS *cabecerows;
    CabeceroOperaciones *cabecerooperaciones;
    CamaraUpload *camaraupload;
    Hora_inicioDAO *horasdao;
    OBJ_Log_Proceso *logProceso;
    __weak IBOutlet UIToolbar *Toolbar;
    __weak IBOutlet UIBarButtonItem *btn_up;
    __weak IBOutlet UIBarButtonItem *btn_surtir;
    
    //Botones ocultos
    __weak IBOutlet UIButton *btn_galeria;
    __weak IBOutlet UIButton *btn_surtir_oculto;
    __weak IBOutlet UIButton *btn_surtir_oculto_2;
    
    
    //Variables auxiliares
    BOOL informacion_incompleta;
}
- (IBAction)btnSubirRuta:(id)sender;
- (IBAction)btnConfiguracion:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblRuta;
@property (weak, nonatomic) IBOutlet UILabel *lblFecha;
- (IBAction)btnDescargaInformacion:(id)sender;
-(IBAction)btnupload:(id)sender;
- (IBAction)btnSurtirRuta:(id)sender;

@property (weak, nonatomic) IBOutlet UIToolbar *barraPrincipal;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
