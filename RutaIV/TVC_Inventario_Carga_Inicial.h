//
//  TVC_Inventario_Carga_Inicial.h
//  RutaIV
//
//  Created by Miguel Banderas on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBJ_Inventario_Queries.h"

@interface TVC_Inventario_Carga_Inicial : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate>{
    NSMutableArray *inventario;
    NSMutableArray *filtrado;
    OBJ_Inventario_Queries *metodos;
    NSMutableDictionary *dictionary_productos;
    NSArray *headers;
    NSString *parametrizacion_mayusculas;
}
- (IBAction)back_pressed:(id)sender;

@end
