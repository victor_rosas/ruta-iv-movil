//
//  CabeceroOperaciones.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 09/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CabeceroDAO.h"
#import "CabeceroWS.h"

@interface CabeceroOperaciones : NSObject{
    int nindice;
    CabeceroDAO *cabecerodao;
    CabeceroWS *cabecerows;
    NSString *varreporteincidencias;
    NSString *varmedida;
    NSString *varmonedas;
    NSString *vardumpdex;
    NSString *varquery09;
    NSString *varventas;
    NSString *varmedidamonedas;
    NSString *varstring;
    NSString *varcortes;
    NSString *varfoto;
    NSString *SCadenaCDR;
    NSString *vlDiferencia;
    NSString *vlOrdenServicio;
    NSString *vlHoraSalida;
    NSString *vlHoraLlegada;
    NSString *vlHora_inicio;
    NSString *sCadenaHSEhoraruta;
    NSString *sCadenaGPRS;
    NSString *sCadenaHSE;
    NSString *sCadenaDOSe;
    NSString *Concadena;
    NSString *cadena;
    int vl_intNoPaquetesSubidos;
    int x;
    int vlIndice;
    NSMutableArray *Avarmedida;
    NSMutableArray *Avardumpex;
    NSMutableArray *dsInterpretaGPRS;
    NSMutableArray *dsInterpretaDumDex;
    NSMutableArray *dsCabOrdenServ;
    NSMutableArray *dsInsertDetOrden;
    NSMutableArray *Amonedas;
    NSMutableArray *Adumpdex;
    NSMutableArray *Aquery09;
    NSMutableArray *Acortes;
    NSMutableArray *Aquery12;
    NSMutableArray *Aquery14;
    NSMutableArray *Aquery15;
    NSMutableArray *Aquery16;
    NSMutableArray *Aquery08;
    NSMutableArray *Aquery07;
    NSMutableArray *Afotos;
    NSMutableArray *Ahoras;
    NSMutableArray *Aventas;
    BOOL cargado;
    BOOL pEnviaInformacionWSADS;
    BOOL VALIDA;
    NSInteger newTable;
    NSString *cadenaconindice;
    NSString *cadenaconindice2;
    NSString *cadenaconindice3;
    NSString *cadenaconindice4;
    NSString *cadenasjuntas;
    NSString *cadenaind;
    NSString *cadenaind2;
    NSString *cadenaind3;
    NSString *cadenaind4;
    NSString *pcadena;
}

-(NSString*)obtenerOrdenservicio;
-(NSString*)obtenerdiferencia;
-(NSString*)ObtenerCortes;
-(NSString*)VARMEDIDAMONEDAS:(NSMutableDictionary *)Dicmonedas;
-(NSString*)VARMEDIDADumpex:(NSMutableDictionary *)DicDumpdex;
-(NSString*)VARMEDIDAVentashistinstancia:(NSMutableDictionary *)dic;
-(NSString*)VARMEDIDAQuery07:(NSMutableDictionary *)dic;
-(NSString*)VARMEDIDAQuery08:(NSMutableDictionary *)dic;
-(NSString*)VARMEDIDAQuery09:(NSMutableDictionary *)dicquery09;
-(NSString*)VARMEDIDAQuery12:(NSMutableDictionary *)fechareporte;
-(NSString*)VARMEDIDAQuery13;
-(NSString*)SCadenaCDRQuery14:(NSMutableDictionary *)dic;
-(NSString*)SCadenaCDRQuery15:(NSMutableDictionary *)dic;
-(BOOL)ActualizarCargado;
-(BOOL)Fecha_orden_procesar;
-(BOOL)cab_orden_servicio:(int)orden_de_servicio;
-(NSString *)prollbackenvio:(int)orden_de_servicio;
-(NSString *)newvarmedida;
//-(NSString *)fullstring:(NSString *)newvarmedida;
-(NSMutableArray *)getvarmedidaarray;
@end
