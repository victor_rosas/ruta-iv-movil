//
//  Monedero_y_contadoresDAO.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 23/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "Monedero_y_contadoresDAO.h"
#import "AppDelegate.h"
#import "Monedero_y_contadores.h"

@implementation Monedero_y_contadoresDAO

-(NSString *) obtenerRutaBD{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}


-(NSMutableArray *)CargarHistoricos:(int) instancia orden_visita:(int)ordenvisita{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    Historicos =[[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"select UNIDADES,MONTO,ID_INSTANCIA,ORDEN_VISITA from VENTAS_HIST_INSTANCIA WHERE ID_INSTANCIA = %d AND ORDEN_VISITA = %d",instancia,ordenvisita];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                NSString *Historico_Pieza= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *Historico_Ventas= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *instancia= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *orden= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                [dic setValue:Historico_Pieza forKey:@"historico_pieza"];
                [dic setValue:Historico_Ventas forKey:@"historico_ventas"];
                
                [Historicos addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Historicos;
}



-(BOOL)InsertaMoneda:(int)ID_ORDEN_SERVICIO ID_INSTANCIA:(int)ID_INSTANCIA ID_MONEDA:(int)ID_MONEDA CANTIDAD:(int)CANTIDAD ORDEN_VISITA:(int)ORDEN_VISITA{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CAMBIO_MONEDERO (ID_ORDEN_SERVICIO,ID_INSTANCIA,ID_MONEDA,CANTIDAD,ORDEN_VISITA) VALUES (%d,%d,%d,%d,%d)",ID_ORDEN_SERVICIO,ID_INSTANCIA,ID_MONEDA,CANTIDAD,ORDEN_VISITA];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert %s",sqlite3_errmsg(dataBase));
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)actualizaHistoricos:(NSInteger)ID_ORDEN_SERVICIO INSTANCIA:(NSInteger)INSTANCIA ORDENVISITA:(NSInteger)ORDENVISITA UNIDADES:(NSInteger)UNIDADES MONTO:(double)MONTO RECOGIODINERO:(NSInteger)RECOGIODINERO{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE VENTAS_HIST_INSTANCIA SET ID_ORDEN_SERVICIO = %ld,ORDEN_VISITA = %ld,UNIDADES =%ld,MONTO= %f,RECOGIO_DINERO =%ld WHERE ID_INSTANCIA = %ld AND ORDEN_VISITA = %d",(long)ID_ORDEN_SERVICIO,(long)ORDENVISITA,(long)UNIDADES,(double)MONTO,(long)RECOGIODINERO,(long)INSTANCIA,ORDENVISITA];/*@"INSERT INTO VENTAS_HIST_INSTANCIA (ID_ORDEN_SERVICIO,ID_INSTANCIA,ORDEN_VISITA,UNIDADES,MONTO,RECOGIO_DINERO) VALUES (%d,%d,%d,%d,%d,%d)",ID_ORDEN_SERVICIO,INSTANCIA,ORDENVISITA,UNIDADES,MONTO,RECOGIODINERO];*/
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{ 
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del update");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)Borraventas_hist_instancia{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"DELETE FROM VENTAS_HIST_INSTANCIA WHERE RECOGIO_DINERO is null"];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion DELETE");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}



-(NSMutableArray *)vlUnidadMax:(int) instancia{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    suma =[[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_ARTICULO,SURTIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %d",instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                NSString *id_articulos= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *surtio= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                
                [dic setValue:surtio forKey:@"surtio"];
                [dic setValue:id_articulos forKey:@"id_articulos"];
                
                
                [suma addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return suma;
}

-(NSMutableArray *)vlMontoMax:(int) instancia{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    suma =[[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_ARTICULO,SURTIO,PRECIO FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %d",instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                NSString *id_articulos= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *surtio= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *price = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 2)];
                
                [dic setValue:surtio forKey:@"surtio"];
                [dic setValue:id_articulos forKey:@"id_articulos"];
                [dic setValue:price forKey:@"precio"];
                
                [suma addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return suma;
}

-(NSString*)obtenerorden_visita:(int)Instancia{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ORDEN_VISITA FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %d",Instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *Ordenvisita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                
                Orden_visita = Ordenvisita;
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return Orden_visita;
    
}

@end
