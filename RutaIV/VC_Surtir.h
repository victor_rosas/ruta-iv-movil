//
//  VC_Surtir.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"
#import "ExistenciasDAO.h"

@interface VC_Surtir : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    //__weak IBOutlet UIButton *btn_next;
    NSMutableArray *textFieldContent;
    NSIndexPath *currentSelection;
    __weak IBOutlet UITableView *myTableview;
    NSMutableArray *text;
    NSInteger txtSucursal;
    ProductosDAO *dao;
    ExistenciasDAO *dao_existencias;

    NSMutableArray * arreglo_articulos;
    NSString *almacen_id;
    NSString *placeholder_original;
    
    NSMutableDictionary *diccionario_existencias;
    NSMutableDictionary * arreglo_productos;
    
    NSInteger x;
    int contador;
    NSIndexPath *seleccion;
    BOOL revision;
    
    __weak IBOutlet UIButton *btn_siguient;
    __weak IBOutlet UIButton *btn_pasado;
    UIToolbar* keyboardDoneButtonView;
    BOOL mostrar_alerta;
    NSString *parametro_orden;
}

//- (IBAction)next_pressed:(id)sender;
- (IBAction)btn_siguiente:(id)sender;
-(int)revisarCapacidadEspiral:(int)cantidad index:(NSIndexPath*)indexPath;
@property (strong, nonatomic) UIWindow *topMostWindow;
@property (strong, nonatomic) UIView *view_button;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,atomic) NSMutableArray *textFieldData;
@property (strong,atomic) NSString *stringVariable;
@property int instancia;
@property BOOL primera_visita;
@property NSMutableArray *inv_ini_array;
-(void) ObtenerProductos;
-(BOOL) verificacion:(NSIndexPath *)indexPath;
//Variable horas de llegada
@property BOOL salida_sitio;

@end
