//
//  VC_Observacion_Detalle.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Observacion_Detalle.h"

@interface VC_Observacion_Detalle ()

@end

@implementation VC_Observacion_Detalle
@synthesize observacion;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    data_observaciones_manager = [[DATA_Observaciones alloc]init];
    
    //Inicializacion de la vista
    self.automaticallyAdjustsScrollViewInsets = false;
    
    //Cargado de contenido
    NSString * descripcion = observacion.descripcion;
    NSString * id_orden_servicio = observacion.id_orden_servicio;
    NSString * id_instancia = observacion.id_instancia;
    NSString * id_clasficicacion = observacion.id_clasificacion;
    NSString * clasificacion = [self cargar_clasificacion_nombre:id_clasficicacion];
    NSString * atencion_bit = observacion.servicio;
    NSString * atencion;
    if ([atencion_bit isEqualToString:@"0"])
    {
        atencion = @"No requiere";
    }
    else
    {
        atencion = @"Si requiere";
    }
    
    //Establecer contenido
    TextView_Descripcion.text = descripcion;
    label_instancia.text = id_instancia;
    Label_orden.text = id_orden_servicio;
    label_clasificacion.text = clasificacion;
    label_atencion.text = atencion;
}

-(void)viewDidAppear:(BOOL)animated
{
    //Ajuste tamaño de TextView
    TextView_Descripcion.translatesAutoresizingMaskIntoConstraints = YES;
    CGRect frame = TextView_Descripcion.frame;
    frame.size.height = TextView_Descripcion.contentSize.height;
    TextView_Descripcion.frame=frame;
    
    //Setup del scroll
    CGRect frame_view = view_detalles.frame;
    frame_view.size.height = view_detalles.frame.size.height;
    CGRect frame_compuesto = TextView_Descripcion.frame;
    frame_compuesto.size.height = frame_view.size.height + frame.size.height + 150;
    scroll_view.contentSize = frame_compuesto.size;
}

-(NSString *)cargar_clasificacion_nombre:(NSString *)id_clasificacion
{
    NSMutableDictionary * diccionario_clasificaciones = [[NSMutableDictionary alloc]init];
    diccionario_clasificaciones = [data_observaciones_manager traer_clasificacion];
    NSArray * array_clasificaciones = [[NSArray alloc]init];
    array_clasificaciones = [diccionario_clasificaciones allKeysForObject:id_clasificacion];
    NSString * clasificacion = [array_clasificaciones objectAtIndex:0];
    return clasificacion;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)action_back:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
@end
