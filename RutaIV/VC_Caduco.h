//
//  VC_Caduco.h
//  RutaIV
//
//  Created by Miguel Banderas on 12/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"

@interface VC_Caduco : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    ProductosDAO * data_articulo_manager;
    NSMutableArray * arreglo_articulos;
    BOOL inventario_superado;
    BOOL alerta_presente;
    BOOL removio;

    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_capacidad;
    
    UIToolbar* keyboardDoneButtonView;
    __weak IBOutlet UIBarButtonItem *btn_siguiente;
    __weak IBOutlet UITableView *myTableView;
}
@property int id_instancia;
@property BOOL salida_sitio;
@property NSString * removio_caduco;
@property NSString * aparecio;
@property NSInteger cantidad_aparecido;

- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
