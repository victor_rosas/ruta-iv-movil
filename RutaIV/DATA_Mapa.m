//
//  DATA_Mapa.m
//  RutaIV
//
//  Created by Miguel Banderas on 25/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "DATA_Mapa.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation DATA_Mapa

-(NSString *) obtenerRutaBD{
    logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSMutableDictionary *)cargar_localizacion
{   [logProceso escribir:@"#Data_Mapa entro cargar_localizacion"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSMutableDictionary * location_dictionary = [[NSMutableDictionary alloc]init];
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT SITIOS_TP.SITIO, INSTANCIAS_SERVICIO.ID_INSTANCIA , LATITUD, LONGITUD FROM INSTANCIAS_SERVICIO INNER JOIN SITIOS_TP ON SITIOS_TP.ID_INSTANCIA = INSTANCIAS_SERVICIO.ID_INSTANCIA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString *sitio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Latitud = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString *Longitud = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                
                NSValue * location = [NSValue valueWithCGPoint:CGPointMake([Latitud floatValue], [Longitud floatValue])];
                
                if([location_dictionary objectForKey:sitio] == nil)
                {
                    NSMutableDictionary * d_instancia = [[NSMutableDictionary alloc]init];
                    [d_instancia setObject:location forKey:instancia];
                    [location_dictionary setObject:d_instancia forKey:sitio];
                }
                else
                {
                    NSMutableDictionary * d_instancia = [[NSMutableDictionary alloc]init];
                    d_instancia = [location_dictionary objectForKey:sitio];
                    [d_instancia setObject:location forKey:instancia];
                    [location_dictionary setObject:d_instancia forKey:sitio];
                }
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Mapa salio cargar_localizacion"];
    return location_dictionary;
}

@end
