//
//  revisaInfoCargada.m
//  RutaIV
//
//  Created by Miguel Banderas on 22/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "revisaInfoCargada.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation revisaInfoCargada
-(BOOL)revisaCargada{

    BOOL bValida = false;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"SELECT CARGADO FROM FECHA_ORDEN_PROCESAR"];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                NSString *sCargado = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentenciaInsert, 0)];
                if ([sCargado isEqualToString:[NSString stringWithFormat:@"1"]]) {
                    bValida = true;
                }
                else{
                    bValida = false;
                }
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);


    return bValida;
    
}


@end
