//
//  ViewController.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

/*
 ================
       LEER
 ================
 
 A quien le toque continuar con este proyecto:
 
 *La desorganizada programacion de esta aplicacion se debe en gran parte a la falta de especificacion de los requerimientos o funciones
 por lo tanto fue necesario hacer parche sobre parche.
 
 *Por la misma razon, sera necesario asegurarse de conocer desde diferentes fuentes como quieren que sea la aplicacion o funcion y dejar
 bien en claro que es lo que se realizara para evitar desacuerdos.
 
 *A pesar de las precauciones anteriores, les encanta cambiar de parecer y en muchos casos, no saben lo que quieren o como funcionan las
 cosas, por lo tanto, habra problemas.
 
 */

#import "ViewController.h"
#import "ConfiguracionViewController.h"
#import "TVC_Iventario_Carga_Cero.h"
#import "TVC_Inventario_Carga_Inicial.h"
#import "TVC_Inventario_Carga_Actual.h"
#import "MnuPpalViewController.h"
#import <sqlite3.h>
#import "AppDelegate.h"
#import "CopiaInstancias.h"
#import "OrdenServicio.h"
#import "copiaSitios.h"
#import "cabeceroOrden.h"
#import "detalleOrden.h"
#import "ubicaciones.h"
#import "sitiosAsignados.h"
#import "detSitiosAsignados.h"
#import "dexIV.h"
#import "planogramaMovs.h"
#import "almacenes.h"
#import "rutas.h"
#import "maquinas.h"
#import "MBProgressHUD.h"
#import "monedas.h"
#import "ventasHistoricos.h"
#import "generales.h"
#import "parametros.h"
#import "planograma.h"
#import "cambiosDePrecios.h"
#import "existenciasRuta.h"
#import "codigosAutorizacion.h"
#import "clavesCodigoBarras.h"
#import "registrosPrekit.h"
#import "devolucionRuta.h"
#import "clasificacionIncidencias.h"
#import "borraVarios.h"
#import "historicosCafe.h"
#import "versionesActuales.h"
#import "sitiosTp.h"
#import "inventariosInstancia.h"
#import "cargasIguales.h"
#import "fechaOrdenServicio.h"
#import "revisaInfoCargada.h"
#import "revisaInfo.h"
#import "actualizaCargado.h"
#import "spGenerales.h"
#import "PruebaTableViewController.h"
#import "VC_Instancias.h"
#import <Parse/Parse.h>
#import "CustomBadge.h"
#import "TVC_Sucursal.h"
#import "GRRequestsManager.h"
#import "ExistenciasDAO.h"
#import "OBJ_Log_Proceso.h"

#define SCREEN_FRAME [[UIScreen mainScreen] applicationFrame]

@interface ViewController ()<GRRequestsManagerDelegate>
@property (nonatomic, strong) GRRequestsManager *requestsManager;

@property(strong,nonatomic) NSString *strUsuario, *sNombreRuta, *sIdRuta, *sIdSucursal, *sWebService, *nConectaInternet;
@end

@implementation ViewController
@synthesize lblFecha,lblRuta;

- (void)viewDidLoad
{
    

    
    //Prueba
    //OBJ_Instancia * hi = [[OBJ_Instancia alloc]init:@"2" orden_visita:@"1"];
    
    logProceso = [[OBJ_Log_Proceso alloc]init];
    informacion_incompleta = false;
    dao = [[ImagenDAO alloc]init];
    mdao = [[Monedero_y_contadoresDAO alloc]init];
    imagenes = [[NSMutableArray alloc] init];
    camaraupload = [[CamaraUpload alloc]init];
    cabecerodao = [[CabeceroDAO alloc]init];
    cabecerooperaciones = [[CabeceroOperaciones alloc]init];
    descargo_info = NO;
    [super viewDidLoad];
    scroll_view.scrollEnabled = true;
    scroll_view.contentSize = CGSizeMake(scroll_view.frame.size.width, self.view.frame.size.height - Toolbar.frame.size.height*2);
	// Valida que exista sesión
    //[self limpiarPrefs];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    _nConectaInternet = [NSString stringWithFormat:@"0"];
    [prefs setObject:_nConectaInternet forKey:@"nConectaInternet"];
    
    [prefs synchronize];
    [self validaSesion];
    [self ObtenerDatos];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:@"es_MX"];
    [dateFormatter setLocale:loc];
    
    [dateFormatter setDateFormat:@"EEEE dd 'de' MMMM 'del' yyyy"];
    
    NSString *fecha = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    NSString *firstCapChar = [[fecha substringToIndex:1] capitalizedString];
    NSString *cappedString = [fecha stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
    
    NSDateFormatter *hourformat = [[NSDateFormatter alloc]init];
    [hourformat setDateFormat:@"HH:mm:ss"];
    
    lblFecha.text = cappedString;
    [lblFecha.layer setCornerRadius:5.0f];
    [lblFecha.layer setMasksToBounds:YES];
    
    [logProceso escribir:cappedString];
    [logProceso escribir:[hourformat stringFromDate:[NSDate date]]];
    
    if (_sNombreRuta == NULL) {
        self.lblRuta.text = @"[Seleccione una ruta en configuración]";
    }
    else{
        self.lblRuta.text =[NSString stringWithFormat:@"Ruta: %@",_sNombreRuta ];
    }
    
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    
    _barraPrincipal.frame = CGRectMake(0, SCREEN_FRAME.size.height-_barraPrincipal.frame.size.height, SCREEN_FRAME.size.width, _barraPrincipal.frame.size.height);
    
    _barraPrincipal.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    _logoImage.frame = CGRectMake(0, 0, 20, 20);
    _logoImage.center = _logoImage.superview.center;
    
    
    //Variables configuracion primera visita
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *instancia_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_instancia_orden"];
    if (instancia_orden == nil) {
        NSString * encendido = @"1";
        NSString * apagado = @"0";
        [userDefaults setObject:encendido forKey:@"configuracion_instancia_orden"];
        [userDefaults setObject:encendido forKey:@"configuracion_producto_orden"];
        [userDefaults setObject:apagado forKey:@"configuracion_historico"];//Se cambio para la muestra
        [userDefaults setObject:apagado forKey:@"configuracion_3G"];
        [userDefaults setObject:apagado forKey:@"configuracion_apariencia_mayusculas"];
        [userDefaults setObject:encendido forKey:@"configurarion_apariencia_progreso"];
        [userDefaults setObject:apagado forKey:@"configuracion_historico_cafe"];
        [userDefaults synchronize];
    }
    
    CALayer *layer = btn_galeria.layer;
    layer.backgroundColor = [[UIColor clearColor] CGColor];
    layer.borderColor = [[UIColor clearColor] CGColor];
    layer.cornerRadius = 8.0f;
    layer.borderWidth = 1.0f;
    
    CALayer *layer2 = btn_surtir_oculto.layer;
    layer2.backgroundColor = [[UIColor clearColor] CGColor];
    layer2.borderColor = [[UIColor clearColor] CGColor];
    layer2.cornerRadius = 8.0f;
    layer2.borderWidth = 1.0f;
    
    CALayer *layer3 = btn_surtir_oculto_2.layer;
    layer3.backgroundColor = [[UIColor clearColor] CGColor];
    layer3.borderColor = [[UIColor clearColor] CGColor];
    layer3.cornerRadius = 8.0f;
    layer3.borderWidth = 1.0f;
 
   }


+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)viewDidAppear:(BOOL)animated
{
    [self ObtenerDatos];
    if (_sNombreRuta == NULL) {
        
    }
    else{
        lblRuta.text = [NSString stringWithFormat:@"Ruta: %@",_sNombreRuta ];
    }
    //Animacion barra
    NSMutableArray *starImageArray = [@[] mutableCopy];
    for (int i = 1; i < 117; i++) {
        UIImage *starImage = [UIImage imageNamed:[NSString stringWithFormat:@"Linea_%d", i]];
        [starImageArray addObject:starImage];
    }
    
    image_view.animationImages = starImageArray;
    image_view.animationRepeatCount = 4;
    image_view.animationDuration = 6;
    image_view.image = starImageArray[0];
    [image_view startAnimating];
    [self Badge];
}

-(void)Badge{
    NSMutableArray *Avisitadas = [[NSMutableArray alloc]init];
    Avisitadas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    if ([Avisitadas count] > 0) {
    [Avisitadas removeObject:[Avisitadas objectAtIndex:([Avisitadas count] - 1)]];
    pendientes =[@([Avisitadas count])stringValue];//[[NSUserDefaults standardUserDefaults] objectForKey:@"imagenes_pendientes"];
    if (!(pendientes == nil)) {
        if (![pendientes isEqualToString:@"0"]) {
            badge = [CustomBadge customBadgeWithString:pendientes];
            CGSize size = Toolbar.bounds.size;
            NSInteger width = (size.width/3)*2 + 10;
            NSInteger height = size.height-50;
            
            CGRect rect = CGRectMake(width, height, 30, 30);
            [badge setFrame:rect];
            [Toolbar addSubview:badge];
        }
        else
        {
            badge = [CustomBadge customBadgeWithString:@"0"];
            CGSize size = Toolbar.bounds.size;
            NSInteger width = (size.width/3)*2 + 10;
            NSInteger height = size.height-50;
            
            CGRect rect = CGRectMake(width, height, 30, 30);
            [badge setFrame:rect];
            [Toolbar addSubview:badge];
            btn_up.enabled = FALSE;
        }
    }
}
    else{}
    [Toolbar setNeedsDisplay];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self ObtenerDatos];
    if (_sNombreRuta == NULL) {
        
    }
    else{
        lblRuta.text = [NSString stringWithFormat:@"Ruta: %@",_sNombreRuta ];
    }

    imagenes = [dao CargarImagenes];
    if (/*[imagenes count]*/[pendientes integerValue]<=0) {
        btn_up.enabled = false;
    }
    else{
        btn_up.enabled = true;
    }
    NSString *descargado = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_descargado"];
    
    if ([descargado isEqualToString:@"1"]) {
        btn_descarga.enabled = false;
    }
    else
    {
        btn_descarga.enabled = true;
        //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        //[userDefaults setObject:@"0" forKey:@"imagenes_pendientes"];
        //[userDefaults synchronize];
        //btn_up.enabled = false;
    }
    
    //Deshabilitar bajar informacion si no se ha seleccionado una sucursal
    if ([self.lblRuta.text isEqualToString:@"[Seleccione una ruta en configuración]"]) {
        btn_descarga.enabled = false;
        btn_surtir.enabled = false;
        btn_surtir_oculto.enabled = false;
        btn_surtir_oculto_2.enabled = false;
        
    }
    else if ([btn_descarga isEnabled])
    {
        btn_surtir.enabled = false;
        btn_surtir_oculto.enabled = false;
        btn_surtir_oculto_2.enabled = false;
        
    }
    
    NSString * cambio_login = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_cambio_sesion"];
    if ([cambio_login isEqualToString:@"1"]) {
        TVC_Sucursal *vConfiguracion = [self.storyboard instantiateViewControllerWithIdentifier:@"vSucursalConfig"];
        vConfiguracion.navigationItem.title = @"Configurar Sucursal";
        [self.navigationController pushViewController:vConfiguracion animated:YES];
    }
    
}
- (void)limpiarPrefs
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:NULL forKey:@"sUsuario"];
    [prefs setObject:NULL forKey:@"sContrasena"];
    [prefs setObject:NULL forKey:@"sSucursal"];
    [prefs setObject:NULL forKey:@"sWebService"];
    [prefs setObject:NULL forKey:@"sUUID"];
    [prefs setObject:NULL forKey:@"sNombreRuta"];
    [prefs setObject:NULL forKey:@"nIdRuta"];
    [prefs setObject:NULL forKey:@"nIdSucursal"];
    [prefs setObject:NULL forKey:@"nIdPosPicker"];
    [prefs setObject:NULL forKey:@"nConectaInternet"];
    [prefs setObject:NULL forKey:@"nIdEncargado"];
    [prefs setObject:NULL forKey:@"nIdOrdenServicio"];
    [prefs setObject:NULL forKey:@"sFecha"];
    [prefs setObject:NULL forKey:@"nNoDia"];
    [prefs setObject:NULL forKey:@"nOrdenSitio"];
    [prefs setObject:NULL forKey:@"nOrdenFin"];
    [prefs setObject:NULL forKey:@"nClienteFin"];
    [prefs setObject:NULL forKey:@"nSitioFin"];
    [prefs setObject:NULL forKey:@"nInstanciaFin"];
    [prefs setObject:NULL forKey:@"uBadge"];
    
    [prefs synchronize];
}

- (void) validaSesion
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    _strUsuario = [prefs stringForKey:@"sUsuario"];
    
    if (_strUsuario==NULL) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe iniciar sesión en este dispositivo" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
        [alert show];
        ConfiguracionViewController *vLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"vLogin"];
        vLogin.navigationItem.title = @"Carga Cero";
        [self.navigationController pushViewController:vLogin animated:YES];
    }
    
}

- (void)showWithDetailsLabel{
	 NSString *barra_progreso = [[NSUserDefaults standardUserDefaults] objectForKey:@"configurarion_apariencia_progreso"];
    if([barra_progreso isEqualToString:@"0"])
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        
        HUD.labelText = @"Cargando";
        HUD.detailsLabelText = @"cargando información";
        HUD.square = YES;
        
        [HUD showWhileExecuting:@selector(descargaInfo) onTarget:self withObject:nil animated:YES];
        
    }
    else
    {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        HUD.labelText = @"Cargando";
        HUD.detailsLabelText = @"Cargando información";
        HUD.mode = MBProgressHUDModeAnnularDeterminate;
        HUD.square = YES;
        [self.view addSubview:HUD];
        
        [HUD showWhileExecuting:@selector(descargaInfo) onTarget:self withObject:nil animated:YES];
    }
    
}

- (void)uploadshowWithDetailsLabel{
    //NSString *barra_progreso = [[NSUserDefaults standardUserDefaults] objectForKey:@"configurarion_apariencia_progreso"];
   // if([barra_progreso isEqualToString:@"0"])
   // {
        uHUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:uHUD];
        
        uHUD.labelText = @"Subiendo";
        uHUD.detailsLabelText = @"Subiendo información";
        uHUD.square = YES;
        
        [uHUD showWhileExecuting:@selector(Upload) onTarget:self withObject:nil animated:YES];
        
    //}
   /* else
    {
        uHUD = [[MBProgressHUD alloc] initWithView:self.view];
        uHUD.labelText = @"Subiendo";
        uHUD.detailsLabelText = @"Subiendo información";
        uHUD.mode = MBProgressHUDModeAnnularDeterminate;
        uHUD.square = YES;
        [self.view addSubview:uHUD];
        
        [uHUD showWhileExecuting:@selector(Upload) onTarget:self withObject:nil animated:YES];
    }*/
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSubirRuta:(id)sender {
    UIAlertView *mensaje = [[UIAlertView alloc] initWithTitle:@"Inventario de Camión" message:nil delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Carga Cero",@"Carga Inicial",@"Existencia Actual", nil];
    
    [mensaje show];
}

- (void) ObtenerDatos
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    _sNombreRuta = [prefs stringForKey:@"sNombreRuta"];
    _sIdRuta = [prefs stringForKey:@"nIdRuta"];
    _sIdSucursal = [prefs stringForKey:@"nIdSucursal"];
    _sWebService = [prefs stringForKey:@"sWebService"];
    _nConectaInternet = [prefs stringForKey:@"nConectaInternet"];
    
}

- (IBAction)btnConfiguracion:(id)sender {
    ConfiguracionViewController *vConfiguracion = [self.storyboard instantiateViewControllerWithIdentifier:@"vConfiguracion"];
    vConfiguracion.navigationItem.title = @"Configuración";
    [self.navigationController pushViewController:vConfiguracion animated:YES];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Carga Cero"]) {
        TVC_Iventario_Carga_Cero *vCargaCero = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_carga_cero"];
        vCargaCero.navigationItem.title = @"Carga Cero";
        [self.navigationController pushViewController:vCargaCero animated:YES];
    }
    else if([title isEqualToString:@"Carga Inicial"]) {
        TVC_Inventario_Carga_Inicial *vInventarioInicial = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_carga_inicial"];
        vInventarioInicial.navigationItem.title = @"Carga Inicial";
        [self.navigationController pushViewController:vInventarioInicial animated:YES];
    }
    else if([title isEqualToString:@"Existencia Actual"]) {
        TVC_Inventario_Carga_Actual *vInventarioActual = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_carga_actual"];
        vInventarioActual.navigationItem.title = @"Existencia Actual";
        [self.navigationController pushViewController:vInventarioActual animated:YES];
    }
}

-(void)descargaInfo{
    [camaraupload Deleteall];
    pendientes = @"0";//MOVER CUANDO SEAN MAS DE UNA INSTANCIA
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:pendientes forKey:@"imagenes_pendientes"];
    [userDefaults synchronize];
    //Clases
    CopiaInstancias *copiaInstancias = [CopiaInstancias alloc];
    OrdenServicio *ordenServicio = [OrdenServicio alloc];
    copiaSitios *traeSitios = [copiaSitios alloc];
    cabeceroOrden *cabOrden = [cabeceroOrden alloc];
    detalleOrden *detOrden = [detalleOrden alloc];
    ubicaciones *traeUbicaciones = [ubicaciones alloc];
    sitiosAsignados *sitiosAsig = [sitiosAsignados alloc];
    detSitiosAsignados *detSitiosAsig = [detSitiosAsignados alloc];
    dexIV *traeDex = [dexIV alloc];
    planogramaMovs *movsInstancias = [planogramaMovs alloc];
    almacenes *copiaAlmacenes = [almacenes alloc];
    rutas *copiaRutas = [rutas alloc];
    maquinas *traeMaquinas = [maquinas alloc];
    monedas *traeMonedas = [monedas alloc];
    ventasHistoricos *traeVentasHist = [ventasHistoricos alloc];
    generales *traeGenerales = [generales alloc];
    parametros *traeParametros = [parametros alloc];
    planograma *traePlanograma = [planograma alloc];
    cambiosDePrecios *cambioPrecios = [cambiosDePrecios alloc];
    existenciasRuta *traeExistencias = [existenciasRuta alloc];
    codigosAutorizacion *traeCodigosAut = [codigosAutorizacion alloc];
    clavesCodigoBarras *traeClaves = [clavesCodigoBarras alloc];
    registrosPrekit *traeRegistrosPrekit = [registrosPrekit alloc];
    devolucionRuta *devRuta = [devolucionRuta alloc];
    clasificacionIncidencias *traeIncidencias = [clasificacionIncidencias alloc];
    borraVarios *borrar = [borraVarios alloc];
    historicosCafe *traeHistCafe = [historicosCafe alloc];
    versionesActuales *traeVersiones = [versionesActuales alloc];
    sitiosTp *traeSitiosTp = [sitiosTp alloc];
    inventariosInstancia *traeInvInstancia = [inventariosInstancia alloc];
    fechaOrdenServicio *traeFechaOrden = [fechaOrdenServicio alloc];
    actualizaCargado *cargado = [actualizaCargado alloc];
    //revisaInfoCargada *revisaInfo = [revisaInfoCargada alloc];//Investigar que hace esta variable
    
    /*f ([revisaInfo revisaCargada]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"La información generada en la terminal aún no se ha subido al servidor" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{*/
    //Get current date
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:date];
    
    NSLog(@"Todays date is = %@",dateString);
    
    NSString *urlString = [NSString stringWithFormat:@"%@cargaInfo?nIdSucursal=%@&nIdRuta=%@&sFechaTerminal=%@",_sWebService,_sIdSucursal,_sIdRuta,dateString];
    [logProceso escribir:[NSString stringWithFormat:@"Se descargo la ruta %@ de la sucursal %@",_sIdRuta,_sIdSucursal]];
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers error:&error];
        NSMutableArray *array = [json objectForKey:@"cargaInfoResult"];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontro informacion";
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            
            for (int i=0; i<array.count; i++) {
                NSDictionary *validaDatos = [array objectAtIndex:i];
                NSMutableArray *mCopiaInstancias = [validaDatos objectForKey:@"copiaInstancias"];
                NSMutableArray *mOrdenServicio = [validaDatos objectForKey:@"ordenServicio"];
                NSMutableArray *mCopiaSitios = [validaDatos objectForKey:@"traeSitios"];
                NSMutableArray *mCabOrden = [validaDatos objectForKey:@"cabeceroOrden"];
                NSMutableArray *mDetOrden = [validaDatos objectForKey:@"detalleOrden"];
                NSMutableArray *mTraeUbicaciones = [validaDatos objectForKey:@"ubicaciones"];
                NSMutableArray *mSitios = [validaDatos objectForKey:@"sitiosAsignados"];
                NSMutableArray *mDetSitios = [validaDatos objectForKey:@"detSitiosAsignados"];
                NSMutableArray *mDexIV = [validaDatos objectForKey:@"dexIV"];
                NSMutableArray *mPlanogramaMovs = [validaDatos objectForKey:@"planogramaMovs"];
                NSMutableArray *mAlmacenes = [validaDatos objectForKey:@"almacenes"];
                NSMutableArray *mRutas = [validaDatos objectForKey:@"cargaRutas"];
                NSMutableArray *mMaquinas = [validaDatos objectForKey:@"traeMaquinas"];
                NSMutableArray *mMonedas = [validaDatos objectForKey:@"traeMonedas"];
                NSMutableArray *mVentasHistoricos = [validaDatos objectForKey:@"auxiliarVentasHist"];
                NSMutableArray *mParametros = [validaDatos objectForKey:@"parametros"];
                NSMutableArray *mPlanograma = [validaDatos objectForKey:@"copiaPlanograma"];
                NSMutableArray *mCambioPrecios = [validaDatos objectForKey:@"cambioPrecio"];
                NSMutableArray *mExistenciasRuta = [validaDatos objectForKey:@"existenciasRuta"];
                NSMutableArray *mCodigosAutorizacion = [validaDatos objectForKey:@"codigosAutorizacion"];
                NSMutableArray *mClavesCodBarras = [validaDatos objectForKey:@"clavesCodigoBarras"];
                NSMutableArray *mRegistroPrekit = [validaDatos objectForKey:@"copiaPrekit"];
                NSMutableArray *mClasificacionIncidencia = [validaDatos objectForKey:@"clasificacionIncidencia"];
                NSMutableArray *mHistoricosCafe = [validaDatos objectForKey:@"historicosCafe"];
                NSMutableArray *mCopiaVersiones = [validaDatos objectForKey:@"copiaVersiones"];
                NSMutableArray *mSitiosTp = [validaDatos objectForKey:@"copiaSitios"];
                NSMutableArray *mInvInstancia = [validaDatos objectForKey:@"inventariosInstancia"];
                //NSMutableArray *mCargasIguales = [validaDatos objectForKey:@"verificaExistenciaRuta"];
                NSMutableArray *mFechaOrden = [validaDatos objectForKey:@"fechaOrden"];
                
                
                if (mCopiaInstancias.count == 0) {
                    NSString *algo = @"Error";
                    NSLog(@"%@",algo);
                }
                else{
                    if ([copiaInstancias borraInstancias]) {
                    for (int i=0; i<mCopiaInstancias.count; i++) {
                        NSDictionary *validaDatos2 = [mCopiaInstancias objectAtIndex:i];
                        int nIdInstancia = [[validaDatos2 objectForKey:@"nIdInstancia"] intValue];
                        int nIdCliente = [[validaDatos2 objectForKey:@"nIdCliente"] intValue];
                        int nIdSitio = [[validaDatos2 objectForKey:@"nIdSitio"] intValue];
                        int nIdUbicacion = [[validaDatos2 objectForKey:@"nIdUbicacion"] intValue];
                        int nIdTipoMaquina = [[validaDatos2 objectForKey:@"nIdTipoMaquina"] intValue];
                        int nIdPlanograma = [[validaDatos2 objectForKey:@"nIdPlanograma"] intValue];
                        int nIdMaquina = [[validaDatos2 objectForKey:@"nIdMaquina"] intValue];
                        int nRegistros = [[validaDatos2 objectForKey:@"nRegistros"] intValue];
                        int nDifHoraMins = [[validaDatos2 objectForKey:@"nDifHoraMins"] intValue];
                        int nCashless = [[validaDatos2 objectForKey:@"nCashless"] intValue];
                        NSString * sLatitud = [validaDatos2 objectForKey:@"sLatitud"];
                        NSString * sLongitud = [validaDatos2 objectForKey:@"sLongitud"];
                        
                        if([copiaInstancias copiaInstancias:nIdInstancia wIdCliente:nIdCliente wIdSitio:nIdSitio wIdUbicacion:nIdUbicacion wIdTipoMaquina:nIdTipoMaquina wIdPlanograma:nIdPlanograma wIdMaquina:nIdMaquina wRegistros:nRegistros wDifHoraMins:nDifHoraMins wCashless:nCashless latitud:sLatitud longitud:sLongitud])
                            {
                                NSLog(@"INSTANCIAS GUARDADAS");
                                
                            }
                            else{
                                NSLog(@"NO SE GUARDARON LAS INSTANCIAS!!!!!");
                                informacion_incompleta = true;
                            }

                        }
                        
                    }
                    else{
                        NSLog(@"NO SE BORRARON LAS INSTANCIAS");
                    }
                }
                
            
                //Orden de servicio
                if (mOrdenServicio.count == 0) {
                    NSLog(@"No se encontró el nodo de ordenServicio");
                    informacion_incompleta = true;
                }
                else{
                    for (int i=0; i<mOrdenServicio.count; i++) {
                        NSDictionary *validaOrdenServicio = [mOrdenServicio objectAtIndex:i];
                        NSString *nIdOrdenServicio = [validaOrdenServicio objectForKey:@"nIdOrdenServicio"];
                        
                        if ([ordenServicio ordenServ:nIdOrdenServicio]) {
                            NSLog(@"ORDEN DE SERVICIO GUARDADA");
                            
                        }
                        else{
                            NSLog(@"NO SE GUARDO LA ORDEN DE SERVICIO!!!!");
                        }
                    }
                }
                
                //copia los sitios
                if (mCopiaSitios.count == 0) {
                    NSLog(@"No se encontró el nodo de traeSitios");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeSitios borrarSitios]) {
                        for (int i=0; i<mCopiaSitios.count; i++) {
                            NSDictionary *validaCopiaSitios = [mCopiaSitios objectAtIndex:i];
                            int nIdCliente = [[validaCopiaSitios objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaCopiaSitios objectForKey:@"nIdSitio"] intValue];
                            NSString *sNombreCorto = [validaCopiaSitios objectForKey:@"sNombreCorto"];
                            int nIdLista = [[validaCopiaSitios objectForKey:@"nIdLista"] intValue];
                            
                            if ([traeSitios copiaSitios:nIdCliente wIdSitio:nIdSitio wNombreCorto:sNombreCorto wIdLista:nIdLista]) {
                                NSLog(@"SITIOS GUARDADOS");
                                
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS SITIOS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LA TABLA SITIOS!!!!");
                    }
                    
                }
                
                //Progreso 10%
                HUD.progress = 0.1;
                //COPIA EL CABECERO DE LA ORDEN DE SERVICIO
                if (mCabOrden.count == 0) {
                    NSLog(@"No se encontró el nodo de cabeceroOrden");
                    informacion_incompleta = true;
                }
                else{
                    if ([cabOrden borrarCabecero]) {
                        for (int i=0; i<mCabOrden.count; i++) {
                            NSDictionary *validaCabOrden = [mCabOrden objectAtIndex:i];
                            int nIdOrdenServicio = [[validaCabOrden objectForKey:@"nIdOrdenServicio"] intValue];
                            int nIdRuta = [[validaCabOrden objectForKey:@"nIdRuta"] intValue];
                            NSString *sFecha = [[validaCabOrden objectForKey:@"sFecha"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            NSString *sHoraLlegada = [[validaCabOrden objectForKey:@"sHoraLlegada"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            NSString *sHoraSalida = [[validaCabOrden objectForKey:@"sHoraSalida"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            int nInactiva = [[validaCabOrden objectForKey:@"nInactiva"] intValue];
                            NSString *sFechaCreacion = [[validaCabOrden objectForKey:@"sFechaCreacion"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            int nIdUsuario = [[validaCabOrden objectForKey:@"nIdUsuario"] intValue];
                            NSString *sTipo = [validaCabOrden objectForKey:@"sTipo"];
                            int nProcesadoCafe = [[validaCabOrden objectForKey:@"nProcesadoCafe"] intValue];
                            int nSugerirDex = [[validaCabOrden objectForKey:@"nSugerirDex"] intValue];
                            
                            if ([cabOrden insertarCabecero:nIdOrdenServicio wIdRuta:nIdRuta wFecha:sFecha wHoraSalida:sHoraSalida wHoraLlegada:sHoraLlegada wInactiva:nInactiva wFechaCreacion:sFechaCreacion wIdUsuario:nIdUsuario wTipo:sTipo wProcesadoCafe:nProcesadoCafe wSugerirDex:nSugerirDex]) {
                                NSLog(@"CABECERO DE ORDEN GUARDADOS");
                                
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS CABECEROS DE ORDEN!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LA TABLA CABECERO DE ORDEN!!!!");
                    }
                    
                }
                
                //COPIA EL DETALLE DE LA ORDEN DE SERVICIO
                if (mDetOrden.count == 0) {
                    NSLog(@"No se encontró el nodo de detalleOrden");
                    informacion_incompleta = true;
                }
                else{
                    if ([detOrden borraDetalleOrden]) {
                        for (int i=0; i<mDetOrden.count; i++) {
                            NSDictionary *validaDetOrden = [mDetOrden objectAtIndex:i];
                            int nIdOrdenServicio = [[validaDetOrden objectForKey:@"nIdOrdenServicio"] intValue];
                            int nOrden = [[validaDetOrden objectForKey:@"nOrden"] intValue];
                            int nIdCliente = [[validaDetOrden objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaDetOrden objectForKey:@"nIdSitio"] intValue];
                            int nIdUbicacion = [[validaDetOrden objectForKey:@"nIdUbicacion"] intValue];
                            int nIdInstancia = [[validaDetOrden objectForKey:@"nIdInstancia"] intValue];
                            int nIdOperacion = [[validaDetOrden objectForKey:@"nIdOperacion"] intValue];
                            NSString *sHoraInicio = [[validaDetOrden objectForKey:@"sHoraInicio"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            int nCodigoAutorizacion = [[validaDetOrden objectForKey:@"nCodigoAutorizacion"] intValue];
                            NSString *sTipoAutorizacion = [validaDetOrden objectForKey:@"sTipoAutorizacion"];
                            int nOrdenVisita = [[validaDetOrden objectForKey:@"nOrdenVisita"] intValue];
                            int nRecaudo = [[validaDetOrden objectForKey:@"nRecaudo"] intValue];
                            
                            if ([detOrden insertaDetalleOrden:nIdOrdenServicio wOrden:nOrden wCliente:nIdCliente wIdSitio:nIdSitio wIdUbicacion:nIdUbicacion wIdInstancia:nIdInstancia wIdOperacion:nIdOperacion wHoraInicio:sHoraInicio wCodigoAutorizacion:nCodigoAutorizacion wTipoAutorizacion:sTipoAutorizacion wOrdenVisita:nOrdenVisita wRecaudo:nRecaudo]) {
                                NSLog(@"DETALLE DE ORDEN GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS DETALLES DE ORDEN!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO EL DETALLE DE LA ORDEN!!!!");
                    }
                    
                }
                
                //COPIA LAS UBICACIONES
                if (mTraeUbicaciones.count == 0) {
                    NSLog(@"No se encontró el nodo de ubicaciones");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeUbicaciones borraUbicaciones]) {
                        for (int i=0; i<mTraeUbicaciones.count; i++) {
                            NSDictionary *validaUbicaciones = [mTraeUbicaciones objectAtIndex:i];
                            int nIdCliente = [[validaUbicaciones objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaUbicaciones objectForKey:@"nIdSitio"] intValue];
                            int nIdUbicacion = [[validaUbicaciones objectForKey:@"nIdUbicacion"] intValue];
                            NSString *sNombre = [validaUbicaciones objectForKey:@"sNombre"];
                            
                            if ([traeUbicaciones insertaUbicaciones:nIdCliente wIdSitio:nIdSitio wIdUbicacion:nIdUbicacion wNombre:sNombre]) {
                                NSLog(@"UBICACIONES GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS UBICACIONES!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LAS UBICACIONES!!!!");
                    }
                    
                }
                //Progreso 20%
                HUD.progress = 0.2;
                
                //COPIA LOS SITIOS ASIGNADOS
                if (mSitios.count == 0) {
                    NSLog(@"No se encontró el nodo de sitiosAsignados");
                    informacion_incompleta = true;
                }
                else{
                    if ([sitiosAsig borraSitios]) {
                        for (int i=0; i<mSitios.count; i++) {
                            NSDictionary *validaSitios = [mSitios objectAtIndex:i];
                            int nOrden = [[validaSitios objectForKey:@"nOrden"] intValue];
                            int nIdCliente = [[validaSitios objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaSitios objectForKey:@"nIdSitio"] intValue];
                            int nRuta = [[validaSitios objectForKey:@"nRuta"] intValue];
                            int nDia = [[validaSitios objectForKey:@"nDia"] intValue];
                            
                            if ([sitiosAsig insertaSitios:nOrden wIdCliente:nIdCliente wIdSitio:nIdSitio wRuta:nRuta wDia:nDia]) {
                                NSLog(@"SITIOS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS SITIOS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LOS SITIOS ASIGNADOS!!!!");
                    }
                    
                }
                
                //COPIA DETALLE DE LOS SITIOS ASIGNADOS
                if (mDetSitios.count == 0) {
                    NSLog(@"No se encontró el nodo de detSitiosAsignados");
                    informacion_incompleta = true;
                }
                else{
                    if ([detSitiosAsig borraDetSitios]) {
                        for (int i=0; i<mDetSitios.count; i++) {
                            NSDictionary *validaDetSitios = [mDetSitios objectAtIndex:i];
                            int nIdCliente = [[validaDetSitios objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaDetSitios objectForKey:@"nIdSitio"] intValue];
                            int nIdInstancia = [[validaDetSitios objectForKey:@"nIdInstancia"] intValue];
                            int nIdOrden = [[validaDetSitios objectForKey:@"nOrden"] intValue];
                            int nOrdenVisita = [[validaDetSitios objectForKey:@"nOrdenVisita"] intValue];
                            int nIdRuta = [[validaDetSitios objectForKey:@"nRuta"] intValue];
                            int nDia = [[validaDetSitios objectForKey:@"nDia"] intValue];
                            
                            if ([detSitiosAsig insertaDetSitios:nIdCliente wIdSitio:nIdSitio wIdInstancia:nIdInstancia wIdOrden:nIdOrden wOrdenVisita:nOrdenVisita wIdRuta:nIdRuta wDia:nDia]) {
                                NSLog(@"DET SITIOS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS DET SITIOS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LOS DETALLES SITIOS ASIGNADOS!!!!");
                    }
                    
                }
                
                //COPIA DEX_IV
                if (mDexIV.count == 0) {
                    NSLog(@"No se encontró el nodo de dexIV");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeDex borraDexIV]) {
                        for (int i=0; i<mDexIV.count; i++) {
                            NSDictionary *validaDexIV = [mDexIV objectAtIndex:i];
                            int nIdInstancia = [[validaDexIV objectForKey:@"nIdInstancia"] intValue];
                            NSString *sEspiral = [validaDexIV objectForKey:@"sEspiral"];
                            double dPrecioVenta = [[validaDexIV objectForKey:@"dPrecioVenta"] doubleValue];
                            NSString *sTitulo = [validaDexIV objectForKey:@"sTitulo"];
                            NSString *sDescripcion = [validaDexIV objectForKey:@"sDescripcion"];
                            
                            if ([traeDex insertaDexIV:nIdInstancia wEspiral:sEspiral wPrecioVta:dPrecioVenta wTitulo:sTitulo wDescripcion:sDescripcion]) {
                                NSLog(@"DEX IV GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS DEX!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO DEX_IV!!!!");
                    }
                    
                }
                //Progreso 30%
                HUD.progress = 0.3;
                //COPIA LOS MOVIMIENTOS DE LAS INSTANCIAS
                if (mPlanogramaMovs.count == 0) {
                    NSLog(@"No se encontró el nodo de planogramaMovs");
                    informacion_incompleta = true;
                }
                else{
                    if ([movsInstancias borraPlanogramaMovs]) {
                        for (int i=0; i<mPlanogramaMovs.count; i++) {
                            NSDictionary *validaMovsInstancia = [mPlanogramaMovs objectAtIndex:i];
                            int nIdCliente = [[validaMovsInstancia objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaMovsInstancia objectForKey:@"nIdSitio"] intValue];
                            int nIdInstancia = [[validaMovsInstancia objectForKey:@"nIdInstancia"] intValue];
                            int nIdArticulo = [[validaMovsInstancia objectForKey:@"nIdArticulo"] intValue];
                            double dInvIni = [[validaMovsInstancia objectForKey:@"dInvIni"] doubleValue];
                            double dSurtio = [[validaMovsInstancia objectForKey:@"dSurtio"] doubleValue];
                            double dRemovio = [[validaMovsInstancia objectForKey:@"dRemovio"] doubleValue];
                            double dCaducaron = [[validaMovsInstancia objectForKey:@"dCaducaron"] doubleValue];
                            int nClave = [[validaMovsInstancia objectForKey:@"nClave"] intValue];
                            int nInicio = [[validaMovsInstancia objectForKey:@"nInicio"] intValue];
                            int nOrdenVisita = [[validaMovsInstancia objectForKey:@"nOrdenVisita"] intValue];
                            int nIdOrdenServicio = [[validaMovsInstancia objectForKey:@"nIdOrdenServicio"] intValue];
                            int nSalteado = [[validaMovsInstancia objectForKey:@"nSalteado"] intValue];
                            double dExistencia = [[validaMovsInstancia objectForKey:@"dExistencia"] doubleValue];
                            int nIdEspiral = [[validaMovsInstancia objectForKey:@"nIdEspiral"] intValue];
                            int nIdCharola = [[validaMovsInstancia objectForKey:@"nIdCharola"] intValue];
                            int nCapacidadEspiral = [[validaMovsInstancia objectForKey:@"nCapacidadEspiral"] intValue];
                            int nIdUbicacion = [[validaMovsInstancia objectForKey:@"nIdUbicacion"] intValue];
                            int nIdPlanograma = [[validaMovsInstancia objectForKey:@"nIdPlanograma"] intValue];
                            NSString *sSitio = [validaMovsInstancia objectForKey:@"sSitio"];
                            NSString *sArticulo = [validaMovsInstancia objectForKey:@"sArticulo"];
                            double dPrecio = [[validaMovsInstancia objectForKey:@"dPrecio"] doubleValue];
                            int nIdRuta = [[validaMovsInstancia objectForKey:@"nIdRuta"] intValue];
                            double dExRuta = [[validaMovsInstancia objectForKey:@"dExRuta"] doubleValue];
                            NSString *sEspiral = [validaMovsInstancia objectForKey:@"sEspiral"];
                            NSString *sCambio = [validaMovsInstancia objectForKey:@"sCambio"];
                            NSString *sTipoArticulo = [validaMovsInstancia objectForKey:@"sTipoArticulo"];
                            double dSubsidio = [[validaMovsInstancia objectForKey:@"dSubsidio"] doubleValue];
                            int nPruebasSel = [[validaMovsInstancia objectForKey:@"nPruebasSel"] intValue];
                            int nVasos = [[validaMovsInstancia objectForKey:@"nVasos"] intValue];
                            int nVentaDexAnt = [[validaMovsInstancia objectForKey:@"nVentaDexAnt"] intValue];
                            double dImporteVtaDexAnt = [[validaMovsInstancia objectForKey:@"dImporteVtaDexAnt"] doubleValue];
                            double dVentaDex = [[validaMovsInstancia objectForKey:@"dVentaDex"] doubleValue];
                            double dImporteVtaDex = [[validaMovsInstancia objectForKey:@"dImporteVtaDex"] doubleValue];
                            int nIdSeleccion = [[validaMovsInstancia objectForKey:@"nIdSeleccion"] intValue];
                            int nIdCategoria = [[validaMovsInstancia objectForKey:@"nIdCategoria"] intValue];
                            int nCapacidadNva = [[validaMovsInstancia objectForKey:@"nCapacidadNva"] intValue];
                            NSString * sCategoria = [validaMovsInstancia objectForKey:@"sCategoria"];
                            
                            if ([movsInstancias instertaPlanogramaMovs:nIdCliente wIdSitio:nIdSitio wIdInstancia:nIdInstancia wIdArticulo:nIdArticulo wInvIni:dInvIni wSurtio:dSurtio wRemovio:dRemovio wCaducaron:dCaducaron wClave:nClave wInicio:nInicio wOrdenVisita:nOrdenVisita wIdOrdenServicio:nIdOrdenServicio wSalteado:nSalteado wExistencia:dExistencia wIdEspiral:nIdEspiral wIdCharola:nIdCharola wCapacidadEspiral:nCapacidadEspiral wIdUbicacion:nIdUbicacion wIdPlanograma:nIdPlanograma wSitio:sSitio wArticulo:sArticulo wPrecio:dPrecio wIdRuta:nIdRuta wExRuta:dExRuta wEspiral:sEspiral wCambio:sCambio wTipoArticulo:sTipoArticulo wSubsidio:dSubsidio wPruebasSel:nPruebasSel wVasos:nVasos wVentaDexAnt:nVentaDexAnt wImporteVtaDexAnt:dImporteVtaDexAnt wVentaDex:dVentaDex wImporteVtaDex:dImporteVtaDex wIdSeleccion:nIdSeleccion wIdCategoria:nIdCategoria wCapacidadNva:nCapacidadNva categoria:sCategoria]) {
                                NSLog(@"MOVIMIENTOS INSTANCIAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS MOVIMIENTOS DE INSTANCIA!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO MOVIMIENTOS INSTANCIAS!!!!");
                    }
                    
                }
                
                //COPIA ALMACENES
                if (mAlmacenes.count == 0) {
                    NSLog(@"No se encontró el nodo de almacenes");
                    informacion_incompleta = true;
                }
                else{
                    if ([copiaAlmacenes borraAlmacenes]) {
                        for (int i=0; i<mAlmacenes.count; i++) {
                            NSDictionary *validaAlmacenes = [mAlmacenes objectAtIndex:i];
                            int nIdAlmacen = [[validaAlmacenes objectForKey:@"nIdAlmacen"] intValue];
                            NSString *sNombre = [validaAlmacenes objectForKey:@"sNombre"];
                            
                            if ([copiaAlmacenes insertaAlmacenes:nIdAlmacen wNombre:sNombre]) {
                                NSLog(@"ALMACENES GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS ALMACENES!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO ALMACENES!!!!");
                    }
                    
                }
                
                //CARGA RUTAS
                if (mRutas.count == 0) {
                    NSLog(@"No se encontró el nodo de cargaRutas");
                    informacion_incompleta = true;
                }
                else{
                    if ([copiaRutas borraRutas]) {
                        for (int i=0; i<mRutas.count; i++) {
                            NSDictionary *validaRutas = [mRutas objectAtIndex:i];
                            int nIdRuta = [[validaRutas objectForKey:@"nIdRuta"] intValue];
                            int nIdAlmacen = [[validaRutas objectForKey:@"nIdAlmacen"] intValue];
                            int nInvNegativo = [[validaRutas objectForKey:@"nInvNegativo"] intValue];
                            NSString *sDescripcion = [validaRutas objectForKey:@"sDescripcion"];
                            
                            if ([copiaRutas insertaRutas:nIdRuta wIdAlmacen:nIdAlmacen wInvNegativo:nInvNegativo wDescripcion:sDescripcion]) {
                                NSLog(@"RUTAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS RUTAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO RUTAS!!!!");
                    }
                    
                }
                //Progreso 40%
                HUD.progress = 0.4;
                
                //SE TRAE LAS MAQUINAS
                if (mMaquinas.count == 0) {
                    NSLog(@"No se encontró el nodo de traeMaquinas");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeMaquinas borraMaquinas]) {
                        for (int i=0; i<mMaquinas.count; i++) {
                            NSDictionary *validaMaquinas = [mMaquinas objectAtIndex:i];
                            int nIdTipoMaquina = [[validaMaquinas objectForKey:@"nIdTipoMaquina"] intValue];
                            NSString *sDescripcionCorta = [validaMaquinas objectForKey:@"sDescripcionCorta"];
                            int nBebida = [[validaMaquinas objectForKey:@"nBebida"] intValue];
                            int nSnack = [[validaMaquinas objectForKey:@"nSnack"] intValue];
                            int nCafe = [[validaMaquinas objectForKey:@"nCafe"] intValue];
                            
                            if ([traeMaquinas insertaMaquinas:nIdTipoMaquina wDescripcionCorta:sDescripcionCorta wBebida:nBebida wSnack:nSnack wCafe:nCafe]) {
                                NSLog(@"MAQUINAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS MAQUINAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO MAQUINAS!!!!");
                    }
                    
                }
                
                //SE TRAE LA TABLA DE MONEDAS
                if (mMonedas.count == 0) {
                    NSLog(@"No se encontró el nodo de traeMonedas");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeMonedas borraMonedas]) {
                        for (int i=0; i<mMonedas.count; i++) {
                            NSDictionary *validaMonedas = [mMonedas objectAtIndex:i];
                            int nIdMoneda = [[validaMonedas objectForKey:@"nIdMoneda"] intValue];
                            double dDenominacion = [[validaMonedas objectForKey:@"dDenominacion"] doubleValue];
                            NSString *sDescripcion = [validaMonedas objectForKey:@"sDescripcion"];
                            int nEsMoneda = [[validaMonedas objectForKey:@"nEsMoneda"] intValue];
                            
                            if ([traeMonedas insertaMonedas:nIdMoneda wDenominacion:dDenominacion wDescripcion:sDescripcion wEsMoneda:nEsMoneda]) {
                                NSLog(@"MONEDAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS MONEDAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO MONEDAS!!!!");
                    }
                    
                }
                
                //GUARDA EL AUXILIAR DE VENTAS HISTORICO
                if (mVentasHistoricos.count == 0) {
                    NSLog(@"No se encontró el nodo de auxiliarVentasHist");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeVentasHist borraVentasHistoricos]) {
                        for (int i=0; i<mClavesCodBarras.count; i++) {
                        NSDictionary *validaCodigosAut = [mClavesCodBarras objectAtIndex:i];
                        int nOrdenVisita = [[validaCodigosAut objectForKey:@"nOrdenVisita"] intValue];
                        int nIdInstancia = [[validaCodigosAut objectForKey:@"nIdInstancia"] intValue];
                        [traeVentasHist insertainstanciashistoricos:(uint32_t)nIdInstancia ordenvisita:nOrdenVisita];
                        }
                        
                        for (int i=0; i<mVentasHistoricos.count; i++) {
                            int nOrdenVisitav = 0;
                            NSDictionary *validaVentasHist = [mVentasHistoricos objectAtIndex:i];
                            int nIdInstanciav =[[validaVentasHist objectForKey:@"nIdInstancia"]intValue];
                            int nUnidades = [[validaVentasHist objectForKey:@"nUnidades"] intValue];
                            double dMonto = [[validaVentasHist objectForKey:@"dMonto"] intValue];
                            //Buscar instancia y su orden
                            for (int i=0; i<mClavesCodBarras.count; i++) {
                                NSDictionary *validaCodigosAut = [mClavesCodBarras objectAtIndex:i];
                                int nOrdenVisita = [[validaCodigosAut objectForKey:@"nOrdenVisita"] intValue];
                                int nIdInstancia = [[validaCodigosAut objectForKey:@"nIdInstancia"] intValue];
                                if(nIdInstancia == nIdInstanciav){
                                    nOrdenVisitav = nOrdenVisita;
                                    [traeVentasHist insertaVentasHistoricos:nIdInstanciav wUnidades:nUnidades wMonto:dMonto orden_visita:nOrdenVisitav];
                                }
                                    }
                            if ([traeVentasHist insertaVentasHistoricos:nIdInstanciav wUnidades:nUnidades wMonto:dMonto orden_visita:nOrdenVisitav]) {
                                NSLog(@"VENTAS HISTORICAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS VENTAS HISTORICAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO VENTAS HISTORICAS!!!!");
                    }
                    
                }
                
                if ([traeGenerales eliminaDumpDex]) {
                    NSLog(@"DUMPDEX ELIMINADO");
                }else{
                    NSLog(@"NO SE ELIMINO DUMPDEX!!!");
                }
                
                if ([traeGenerales eliminaCambioMonedero]) {
                    NSLog(@"CAMBIO MONEDERO ELIMINADO");
                }else{
                    NSLog(@"NO SE ELIMINO CAMBIO MONEDERO!!!");
                }
                //Progreso 50%
                HUD.progress = 0.5;
                //COPIA PARAMETROS
                if (mParametros.count == 0) {
                    NSLog(@"No se encontró el nodo de parametros");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeParametros borraParametros]) {
                        for (int i=0; i<mParametros.count; i++) {
                            NSDictionary *validaParametros = [mParametros objectAtIndex:i];
                            int nEscanearMaquina = [[validaParametros objectForKey:@"nIdInstancia"] intValue];
                            int nConectaInternet = [[validaParametros objectForKey:@"nConectaInternet"] intValue];
                            int nLeerDex = [[validaParametros objectForKey:@"nLeerDex"] intValue];
                            int nValidaMonederos = [[validaParametros objectForKey:@"nValidaMonederos"] intValue];
                            int nMultiploCafe = [[validaParametros objectForKey:@"nMultiploCafe"] intValue];
                            int nLlenaSeleccion = [[validaParametros objectForKey:@"nLlenaSeleccion"] intValue];
                            int nCVPN = [[validaParametros objectForKey:@"nCVPN"] intValue];
                            int nAcumularMonedero = [[validaParametros objectForKey:@"nAcumularMonedero"] intValue];
                            int nBloqueoRecaudo = [[validaParametros objectForKey:@"nBloqueoRecaudo"] intValue];
                            int nPrekit = [[validaParametros objectForKey:@"nPrekit"] intValue];
                            int nInvSugDex = [[validaParametros objectForKey:@"nInvSugDex"] intValue];
                            int nQuitarValidacionesCafe = [[validaParametros objectForKey:@"nQuitarValidacionesCafe"] intValue];
                            int nOcultarContadores = [[validaParametros objectForKey:@"nOcultarContadores"] intValue];
                            int nMostrarVentasDex = [[validaParametros objectForKey:@"nMostrarVentasDex"] intValue];
                            
                            if ([traeParametros insertaParametros:nValidaMonederos wEscanearMaquina:nEscanearMaquina wConectaInternet:nConectaInternet wLeerDex:nLeerDex wMultiploCafe:nMultiploCafe wLlenaSeleccion:nLlenaSeleccion wCVPN:nCVPN wAcumularMonedero:nAcumularMonedero wBloqueoRecaudo:nBloqueoRecaudo wPrekit:nPrekit wMostrarVentaDex:nMostrarVentasDex wInvSugDex:nInvSugDex wQuitarValidacionesCafe:nQuitarValidacionesCafe wOcultarContadores:nOcultarContadores]) {
                                NSLog(@"PARAMETROS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS PARAMETROS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LOS PARAMETROS!!!!");
                    }
                    
                }
                
                //COPIA PLANOGRAMA
                if (mPlanograma.count == 0) {
                    NSLog(@"No se encontró el nodo de parametros");
                    informacion_incompleta = true;
                }
                else{
                    if ([traePlanograma borraPlanograma]) {
                        for (int i=0; i<mPlanograma.count; i++) {
                            NSDictionary *validaPlanograma = [mPlanograma objectAtIndex:i];
                            int nIdArticulo = [[validaPlanograma objectForKey:@"nIdArticulo"] intValue];
                            double dSurtio = [[validaPlanograma objectForKey:@"dSurtio"] doubleValue];
                            double dRemovio = [[validaPlanograma objectForKey:@"dRemovio"] intValue];
                            int nClave = [[validaPlanograma objectForKey:@"nClave"] intValue];
                            int nOrdenVisita = [[validaPlanograma objectForKey:@"nOrdenVisita"] intValue];
                            int nIdOrdenServicio = [[validaPlanograma objectForKey:@"nIdOrdenServicio"] intValue];
                            int nIdEspiral = [[validaPlanograma objectForKey:@"nIdEspiral"] intValue];
                            int nIdCharola = [[validaPlanograma objectForKey:@"nIdCharola"] intValue];
                            int nCapacidadEspiral = [[validaPlanograma objectForKey:@"nCapacidadEspiral"] intValue];
                            int nIdPlanograma = [[validaPlanograma objectForKey:@"nIdPlanograma"] intValue];
                            NSString *sArticulo = [validaPlanograma objectForKey:@"sArticulo"];
                            NSString *sEspiral = [validaPlanograma objectForKey:@"sEspiral"];
                            NSString *sCambio = [validaPlanograma objectForKey:@"sCambio"];
                            int nIdInstancia = [[validaPlanograma objectForKey:@"nIdInstancia"] intValue];
                            
                            if ([traePlanograma insertaPlanograma:nIdArticulo wSurtio:dSurtio wRemovio:dRemovio wClave:nClave wOrdenVisita:nOrdenVisita wIdOrdenServicio:nIdOrdenServicio wIdEspiral:nIdEspiral wIdCharola:nIdCharola wCapacidadEspiral:nCapacidadEspiral wIdPlanograma:nIdPlanograma wArticulo:sArticulo wEspiral:sEspiral wCambio:sCambio wIdInstancia:nIdInstancia]) {
                                NSLog(@"PLANOGRAMA GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS PLANOGRAMA!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LOS PLANOGRAMA!!!!");
                    }
                    
                }
                
                //COPIA LOS CAMBIOS DE PRECIO
                if (mCambioPrecios.count == 0) {
                    NSLog(@"No se encontró el nodo de cambioPrecio");
                    informacion_incompleta = true;
                }
                else{
                    if ([cambioPrecios borraCambioPrecios]) {
                        for (int i=0; i<mCambioPrecios.count; i++) {
                            NSDictionary *validaCambioPrecio = [mCambioPrecios objectAtIndex:i];
                            int nIdInstancia = [[validaCambioPrecio objectForKey:@"nIdInstancia"] intValue];
                            int nIdCharola = [[validaCambioPrecio objectForKey:@"nIdCharola"] intValue];
                            int nIdEspiral = [[validaCambioPrecio objectForKey:@"nIdEspiral"] intValue];
                            int nIdSeleccion = [[validaCambioPrecio objectForKey:@"nIdSeleccion"] intValue];
                            int nIdArticulo = [[validaCambioPrecio objectForKey:@"nIdArticulo"] intValue];
                            double dActual = [[validaCambioPrecio objectForKey:@"dActual"] doubleValue];
                            double dAnterior = [[validaCambioPrecio objectForKey:@"dAnterior"] doubleValue];
                            
                            if ([cambioPrecios insertaCambioPrecios:nIdInstancia wIdCharola:nIdCharola wIdEspiral:nIdEspiral wIdSeleccion:nIdSeleccion wIdArticulo:nIdArticulo wdActual:dActual wAnterior:dAnterior]) {
                                NSLog(@"CAMBIO DE PRECIOS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS CAMBIO DE PRECIOS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LOS CAMBIO DE PRECIOS!!!!");
                    }
                    
                }
                
                //Progreso 60%
                HUD.progress = 0.6;
                //COPIA LAS EXISTENCIAS DE RUTA
                if (mExistenciasRuta.count == 0) {
                    NSLog(@"No se encontró el nodo de existenciasRuta");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeExistencias borraExistenciasRuta]) {
                        for (int i=0; i<mExistenciasRuta.count; i++) {
                            NSDictionary *validaExistencias = [mExistenciasRuta objectAtIndex:i];
                            int nIdAlmacen = [[validaExistencias objectForKey:@"nIdAlmacen"] intValue];
                            int nIdArticulo = [[validaExistencias objectForKey:@"nIdArticulo"] intValue];
                            double dExistencia = [[validaExistencias objectForKey:@"dExistencia"] doubleValue];
                            
                            if ([traeExistencias insertaExistenciasRuta:nIdAlmacen wIdArticulo:nIdArticulo wExistencia:dExistencia]) {
                                NSLog(@"EXISTENCIAS GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDARON LAS EXISTENCIAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LAS EXISTENCIAS!!!!");
                    }
                    
                }
                
                //COPIA LOS CODIGOS DE AUTORIZACION
                if (mCodigosAutorizacion.count == 0) {
                    NSLog(@"No se encontró el nodo de codigosAutorizacion");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeCodigosAut borraCodigosAutorizacion]) {
                        for (int i=0; i<mCodigosAutorizacion.count; i++) {
                            NSDictionary *validaCodigosAut = [mCodigosAutorizacion objectAtIndex:i];
                            int nIdOrdenServicio = [[validaCodigosAut objectForKey:@"nIdOrdenServicio"] intValue];
                            int nOrdenVisita = [[validaCodigosAut objectForKey:@"nOrdenVisita"] intValue];
                            int nIdInstancia = [[validaCodigosAut objectForKey:@"nIdInstancia"] intValue];
                            int nCodigo = [[validaCodigosAut objectForKey:@"nCodigo"] intValue];
                            int nIdCliente = [[validaCodigosAut objectForKey:@"nIdCliente"] intValue];
                            int nIdSitio = [[validaCodigosAut objectForKey:@"nIdSitio"] intValue];
                            int nCodSitio = [[validaCodigosAut objectForKey:@"nCodSitio"] intValue];
                            int nCancelaCodigo = [[validaCodigosAut objectForKey:@"nCancelaCodigo"] intValue];
                            int nCancelaCodSitio = [[validaCodigosAut objectForKey:@"nCancelaCodSitio"] intValue];
                            
                            if ([traeCodigosAut insertaCodigosAutorizacion:nIdOrdenServicio wOrdenVisita:nOrdenVisita wIdInstancia:nIdInstancia wCodigo:nCodigo wIdCliente:nIdCliente wIdSitio:nIdSitio wCodSitio:nCodSitio wCancelaCodigo:nCancelaCodigo wCancelaCodSitio:nCancelaCodSitio]) {
                                NSLog(@"CODIGOS AUTORIZACION GUARDADAS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LOS CODIGOS AUTORIZACION!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO CODIGOS AUTORIZACION!!!!");
                    }
                    
                }
                
                //COPIA LAS CLAVES DE LOS CODIGOS DE BARRAS
                if (mClavesCodBarras.count == 0) {
                    NSLog(@"No se encontró el nodo de codigosAutorizacion");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeClaves borraClavesCodBarras]) {
                        for (int i=0; i<mClavesCodBarras.count; i++) {
                            NSDictionary *validaCodigosAut = [mClavesCodBarras objectAtIndex:i];
                            int nIdOrdenServicio = [[validaCodigosAut objectForKey:@"nIdOrdenServicio"] intValue];
                            int nOrdenVisita = [[validaCodigosAut objectForKey:@"nOrdenVisita"] intValue];
                            int nIdInstancia = [[validaCodigosAut objectForKey:@"nIdInstancia"] intValue];
                            int nClave = [[validaCodigosAut objectForKey:@"nClave"] intValue];
                            
                            if ([traeClaves insertaClavesCodBarras:nIdOrdenServicio wOrdenVisita:nOrdenVisita wIdInstancia:nIdInstancia wClave:nClave]) {
                                NSLog(@"CLAVES DE CODIGOS DE BARRAS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDON LAS CLAVES DE CODIGO DE BARRAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO LAS CLAVES DE CODIGO DE BARRAS!!!!");
                    }
                    
                }
                
                //COPIA LOS REGISTROS DEL PREKIT
                if (mRegistroPrekit.count == 0) {
                    NSLog(@"No se encontró el nodo de copiaPrekit");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeRegistrosPrekit borraRegistrosPrekit]) {
                        for (int i=0; i<mRegistroPrekit.count; i++) {
                            NSDictionary *validaRegistrosPrekit = [mRegistroPrekit objectAtIndex:i];
                            NSString *sFecha = [[validaRegistrosPrekit objectForKey:@"sFecha"]stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                            int nIdInstancia = [[validaRegistrosPrekit objectForKey:@"nIdInstancia"] intValue];
                            int nIdArticulo = [[validaRegistrosPrekit objectForKey:@"nIdArticulo"] intValue];
                            double dSugerido = [[validaRegistrosPrekit objectForKey:@"nIdInstancia"] doubleValue];
                            NSString *sNombre = [validaRegistrosPrekit objectForKey:@"sNombre"];
                            
                            if ([traeRegistrosPrekit insertaRegistrosPrekit:sFecha wIdInstancia:nIdInstancia wIdArticulo:nIdArticulo wSugerido:dSugerido wNombre:sNombre]) {
                                NSLog(@"REGISTRO PREKIT GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO REGISTRO PREKIT!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO REGISTRO PREKIT!!!!");
                    }
                    
                }
                
                //ELIMINA EL CABECERO DE LAS DEVOLUCIONES
                if ([devRuta borraCabDevolucionRuta]){
                    NSLog(@"CABECERO DE DEVOLUCIONES BORRADO");
                }
                else{
                    NSLog(@"NO SE BORRO EL CABECERO DE DEVOLUCIONES!!!!");
                }
                
                //ELIMINA EL DETALLE DE LAS DEVOLUCIONES
                if ([devRuta borraDetDevolucionRuta]){
                    NSLog(@"DETALLE DE DEVOLUCIONES BORRADO");
                }
                else{
                    NSLog(@"NO SE BORRO EL DETALLE DE DEVOLUCIONES!!!!");
                }
                
                //COPIA LAS CLASIFICACIONES DE LAS INCIDENCIAS
                if (mClasificacionIncidencia.count == 0) {
                    NSLog(@"No se encontró el nodo de clasificacionIncidencia");
                }
                else{
                    if ([traeIncidencias borraClasificacionIncidencias]) {
                        for (int i=0; i<mClasificacionIncidencia.count; i++) {
                            NSDictionary *validaIncidencias = [mClasificacionIncidencia objectAtIndex:i];
                            int nIdClasificacion = [[validaIncidencias objectForKey:@"nIdClasificacion"] intValue];
                            NSString *sDescripcion = [validaIncidencias objectForKey:@"sDescripcion"];
                            
                            if ([traeIncidencias insertaClasificacionIncidencias:nIdClasificacion wDescripcion:sDescripcion]) {
                                NSLog(@"CLASIFICACION INCIDENCIAS GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO CLASIFICACION INCIDENCIAS!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO CLASIFICACION INCIDENCIAS!!!!");
                    }
                    
                }
                
                //Progreso 70%
                HUD.progress = 0.7;
                //ELIMINA LOS REPORTES DE LAS INCIDENCIAS
                if ([borrar borraReporteIncidencias]){
                    NSLog(@"REPORTES DE LAS INCIDENCIAS BORRADO");
                }
                else{
                    NSLog(@"NO SE BORRO REPORTES DE LAS INCIDENCIAS!!!!");
                }
                
                //ELIMINA LOS CONTACTOS DE LAS DEVOLUCIONES
                if ([borrar borraDevolucionRuta]){
                    NSLog(@"CONTACTOS DE LAS DEVOLUCIONES BORRADO");
                }
                else{
                    NSLog(@"NO SE BORRO CONTACTOS DE LAS DEVOLUCIONES!!!!");
                }
                
                //COPIA LOS HISTORICOS DE LAS SELECCIONES DE CAFE
                if (mHistoricosCafe.count == 0) {
                    NSLog(@"No se encontró el nodo de historicosCafe");
                }
                else{
                    if ([traeHistCafe borraRegistrosPrekit]) {
                        for (int i=0; i<mHistoricosCafe.count; i++) {
                            NSDictionary *validaHistCafe = [mHistoricosCafe objectAtIndex:i];
                            int nIdInstancia = [[validaHistCafe objectForKey:@"nIdInstancia"] intValue];
                            int nOrdenVisita = [[validaHistCafe objectForKey:@"nOrdenVisita"] intValue];
                            int nIdArticulo = [[validaHistCafe objectForKey:@"nIdArticulo"] intValue];
                            int dSurtidoAnt = [[validaHistCafe objectForKey:@"dSurtidoAnt"] intValue];
                            int nPruebasAnt = [[validaHistCafe objectForKey:@"nPruebasAnt"] intValue];
                            int nIdSeleccion = [[validaHistCafe objectForKey:@"nIdSeleccion"] intValue];
                            
                            if ([traeHistCafe insertaRegistrosPrekit:nIdInstancia wOrdenVisita:nOrdenVisita wIdArticulo:nIdArticulo wSurtidoAnt:dSurtidoAnt wPruebasAnt:nPruebasAnt wIdSeleccion:nIdSeleccion]) {
                                NSLog(@"HISTORICOS CAFE GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO HISTORICOS CAFE!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO HISTORICOS CAFE!!!!");
                    }
                    
                }
                //Progreso 80%
                HUD.progress = 0.8;
                //COPIA LAS VERSIONES ACTUALES PARA TRABAJAR
                if (mCopiaVersiones.count == 0) {
                    NSLog(@"No se encontró el nodo de copiaVersiones");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeVersiones borraVersionesActuales]) {
                        for (int i=0; i<mCopiaVersiones.count; i++) {
                            NSDictionary *validaVersiones = [mCopiaVersiones objectAtIndex:i];
                            int nVersionMinTp = [[validaVersiones objectForKey:@"nVersionMinTp"] intValue];
                            int nVersionMaxTp = [[validaVersiones objectForKey:@"nVersionMaxTp"] intValue];
                            int nVersionMinAleatTp = [[validaVersiones objectForKey:@"nVersionMinAleatTp"] intValue];
                            int nVersionMaxAleatTp = [[validaVersiones objectForKey:@"nVersionMaxAleatTp"] intValue];
                            
                            if ([traeVersiones insertaVersionesActuales:nVersionMinTp wVersionMaxTp:nVersionMaxTp wVersionMinAleatTp:nVersionMinAleatTp wVersionMaxAleatTp:nVersionMaxAleatTp]) {
                                NSLog(@"VERSIONES GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO VERSIONES!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO VERSIONES!!!!");
                    }
                    
                }
                
                //COPIA LOS SITIOS DE LA TP
                if (mSitiosTp.count == 0) {
                    NSLog(@"No se encontró el nodo de copiaSitios");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeSitiosTp borraSitiosTp]) {
                        for (int i=0; i<mSitiosTp.count; i++) {
                            NSDictionary *validaSitiosTp = [mSitiosTp objectAtIndex:i];
                            NSString *sSitio = [validaSitiosTp objectForKey:@"sSitio"];
                            NSString *sInstancia = [validaSitiosTp objectForKey:@"sInstancia"];
                            int nIdPlanograma = [[validaSitiosTp objectForKey:@"nIdPlanograma"] intValue];
                            int nIdOrden = [[validaSitiosTp objectForKey:@"nIdOrden"] intValue];
                            int nIdInstancia = [[validaSitiosTp objectForKey:@"nIdInstancia"] intValue];
                            int nIdUbicacion = [[validaSitiosTp objectForKey:@"nIdUbicacion"] intValue];
                            int nOrdenVisita = [[validaSitiosTp objectForKey:@"nOrdenVisita"] intValue];
                            int nIdSitio = [[validaSitiosTp objectForKey:@"nIdSitio"] intValue];
                            int nIdCliente = [[validaSitiosTp objectForKey:@"nIdCliente"] intValue];
                            
                            if ([traeSitiosTp insertaSitiosTp:sSitio wInstancia:sInstancia wIdPlanograma:nIdPlanograma wIdOrden:nIdOrden wIdInstancia:nIdInstancia wIdUbicacion:nIdUbicacion wOrdenVisita:nOrdenVisita wIdSitio:nIdSitio wIdCliente:nIdCliente]) {
                                NSLog(@"SITIOS TP GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO SITIOS TP!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO SITIOS TP!!!!");
                    }
                    
                }
                //Progreso 90%
                HUD.progress = 0.9;
                //COPIA LOS INVENTARIOS DE INSTANCIA DE CAFE
                if (mInvInstancia.count == 0) {
                    NSLog(@"No se encontró el nodo de inventariosInstancia");
                    //informacion_incompleta = true;
                }
                else{
                    if ([traeInvInstancia borraInvInstancia]) {
                        for (int i=0; i<mInvInstancia.count; i++) {
                            NSDictionary *validaInvInstancia = [mInvInstancia objectAtIndex:i];
                            NSString *sDescripcion = [validaInvInstancia objectForKey:@"sDescripcion"];
                            int nIdInstancia = [[validaInvInstancia objectForKey:@"nIdInstancia"]intValue];
                            int nIdEspiral = [[validaInvInstancia objectForKey:@"nIdEspiral"]intValue];
                            int nClave = [[validaInvInstancia objectForKey:@"nClave"]intValue];
                            int nEistencia = [[validaInvInstancia objectForKey:@"nEistencia"]intValue];
                            int nIdOrdenServicio = [[validaInvInstancia objectForKey:@"nIdOrdenServicio"]intValue];
                            int nCapacidadEspiral = [[validaInvInstancia objectForKey:@"nCapacidadEspiral"]intValue];
                            int nFracciones = [[validaInvInstancia objectForKey:@"nFracciones"]intValue];
                            
                            if ([traeInvInstancia insertaInvInstancia:sDescripcion wIdInstancia:nIdInstancia wIdEspiral:nIdEspiral wClave:nClave wExistencia:nEistencia wIdOrdenServicio:nIdOrdenServicio wCapacidadEspiral:nCapacidadEspiral wFracciones:nFracciones]) {
                                NSLog(@"INV INSTANCIA GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO INV INSTANCIA!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO INV INSTANCIA!!!!");
                    }
                    
                }
                int nCargado = 0;
                
                //VERIFICA CARGAS
                /*if (mCargasIguales.count == 0) {
                    NSLog(@"No se encontró el nodo de verificaExistenciaRuta");
                }
                else{
                    for (int i=0; i<mCargasIguales.count; i++) {
                        NSDictionary *validaCargas = [mCargasIguales objectAtIndex:i];
                        double dRegistros = [[validaCargas objectForKey:@"dRegistros"] doubleValue];
                            
                        int diferencia = [verificaCargas traeCargasIguales:dRegistros];
                        
                        if (diferencia == 0) {
                            NSLog(@"CARGAS CORRECTAS");
                            nCargado = 1;
                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"La cantidad cargada en la tp no coincide con la cantidad del almacén ruta, cargue de nuevo" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
                            [alert show];
                            nCargado = 0;
                        }
                    }
                    
                }*/
                
                //COPIA LA FECHA DE LA ORDEN DE SERVICIO
                if (mFechaOrden.count == 0) {
                    NSLog(@"No se encontró el nodo de fechaOrden");
                    informacion_incompleta = true;
                }
                else{
                    if ([traeFechaOrden borraFechaOrdenServicio]) {
                        for (int i=0; i<mFechaOrden.count; i++) {
                            NSDictionary *validaFechaOrden = [mFechaOrden objectAtIndex:i];
                            NSString *sFecha = [validaFechaOrden objectForKey:@"sFecha"];
                            int nNoDia = [[validaFechaOrden objectForKey:@"nNoDia"]intValue];
                            
                            if ([traeFechaOrden insertaFechaOrdenServicio:nCargado wFecha:sFecha wNoDia:nNoDia]) {
                                NSLog(@"FECHA ORDEN GUARDADOS");
                            }
                            else{
                                NSLog(@"NO SE GUARDO FECHA ORDEN!!!!");
                            }
                        }
                    }
                    else{
                        NSLog(@"NO SE BORRO FECHA ORDEN!!!!");
                    }
                    
                }

                
                
                if ([cargado actualizaCargado]) {
                    NSLog(@"Actualiza campo cargado correcto!");
                }
                else{
                    NSLog(@"No se actualizó campo cargado!!");
                }
                
                
                
            }
            
        }
        //Progeso 100
        HUD.progress = 1.0;
        
        //Alerta si se carga toda la informacion
        UIAlertView *alertPrincipal;
        
        if (informacion_incompleta) {
             alertPrincipal = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No se cargaron todos los datos" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        else
        {
             alertPrincipal = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Datos cargados correctamente!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        
        
        //Creacion de arreglo vacio
        
        NSMutableArray *visitados = [[NSMutableArray alloc]init];
        for (int i = 0; i<40; i++) {
            [visitados addObject:@"0"];
        }
        
        NSMutableArray *salteadas = [[NSMutableArray alloc]init];
        for (int i = 0; i<40; i++) {
            [salteadas addObject:@"0"];
        }
        
        NSArray *visitados_inmutable = [NSArray arrayWithArray:visitados];
        NSArray *salteadas_inmutable = [NSArray arrayWithArray:salteadas];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:visitados_inmutable forKey:@"instancias_visitadas"];
        [userDefaults setObject:[NSNumber numberWithInt:0] forKey:@"posicion_actual_instancias_visitadas"];
        [userDefaults setObject:salteadas_inmutable forKey:@"instancias_salteadas"];
        [userDefaults setObject:[NSNumber numberWithInt:0] forKey:@"posicion_actual_instancias_salteadas"];
        [userDefaults synchronize];
        
        
        //NSString *hour = [self getHour];
        //NSInteger vlOrdendeservicio = [[cabecerodao CargarOrdendeservicio]integerValue];
        
        //[horasdao ActualizaHora_SALIDA:vlOrdendeservicio ID_RUTA:_sIdRuta HORA_SALIDA:hour HORA_SALIDA_ENVIADA:1];
        
        //[alertPrincipal show];
        
        NSString *descargado = @"1";
        [userDefaults setObject:descargado forKey:@"configuracion_descargado"];
        [userDefaults synchronize];
        btn_descarga.enabled = false;
        btn_surtir.enabled = true;
        btn_surtir_oculto_2.enabled = true;
        btn_surtir_oculto.enabled = true;
    }
        
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No se estableció la conexión" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    //}
    
    descargo_info = YES;
    [self Badge];
    
}

- (IBAction)btnDescargaInformacion:(id)sender {
   
    
    [self ObtenerDatos];
    
    //[self descargaInfo];
    
    [self showWithDetailsLabel];
}

-(void)surtir_ruta_action
{
    if (/*descargo_info == NO*/FALSE) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"No se ha descargado la informacion de esta ruta" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
        [alert show];
        perform_segue = false;
    }else{
        
        
        [self ObtenerDatos];
        revisaInfo *revisaInformacion = [revisaInfo alloc];
        
        if (revisaInformacion.RevisaInfoCargada) {
            if ([_nConectaInternet isEqualToString:@"1"]) {
                
                NSString *urlString = [NSString stringWithFormat:@"%@iniciaRuta?nIdRuta=%@",_sWebService,_sIdRuta];
                
                NSLog(@"%@",urlString);
                
                NSURL *jsonURL = [NSURL URLWithString:urlString];
                NSError *error = nil;
                
                NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
                
                if (!error) {
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:NSJSONReadingMutableContainers error:&error];
                    NSMutableArray *array = [json objectForKey:@"iniciaRutaResult"];
                    
                    if (array.count == 0) {
                        NSString *valor = @"No se encontro informacion";
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:valor delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        perform_segue = false;
                    }
                    else{
                        
                        
                        for (int i=0; i<array.count; i++) {
                            NSDictionary *validaDatos = [array objectAtIndex:i];
                            NSMutableArray *mRevisaEstatusOrden = [validaDatos objectForKey:@"revistaEstatusOrden"];
                            NSMutableArray *mTraeEncargado = [validaDatos objectForKey:@"traeEncargado"];
                            NSMutableArray *mVerificaSalidaRuta = [validaDatos objectForKey:@"verificaSalidaRuta"];
                            
                            //Trae encargado
                            if (mTraeEncargado.count == 0) {
                                NSLog(@"No se encontró el nodo de traeEncargado");
                            }
                            else{
                                for (int i=0; i<mTraeEncargado.count; i++) {
                                    NSDictionary *validaTraeEncargado = [mTraeEncargado objectAtIndex:i];
                                    NSString *nIdEncargado = [validaTraeEncargado objectForKey:@"nIdEncargado"];
                                    
                                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                    [prefs setObject:nIdEncargado forKey:@"nIdEncargado"];
                                    
                                    [prefs synchronize];
                                }
                            }
                            
                            //Verifica salida rutas
                            if (mVerificaSalidaRuta.count == 0) {
                                NSLog(@"No se encontró el nodo de verificaEstatusRuta");
                            }
                            else{
                                for (int i=0; i<mVerificaSalidaRuta.count; i++) {
                                    NSDictionary *validaSalidaRuta = [mVerificaSalidaRuta objectAtIndex:i];
                                    NSString *sSalio = [validaSalidaRuta objectForKey:@"sSalio"];
                                    
                                    if ([sSalio isEqualToString:@"N"]) {
                                        NSLog(@"MOSTRAR SALIDA RUTA");
                                    }
                                    else{
                                        if (mRevisaEstatusOrden.count == 0) {
                                            NSLog(@"No se encontró el nodo revisaEstatusOrden");
                                        }
                                        else{
                                            for (int j=0; j<mRevisaEstatusOrden.count; j++) {
                                                NSDictionary *validaEstatusOrden = [mRevisaEstatusOrden objectAtIndex:j];
                                                NSInteger nAbierta = [[validaEstatusOrden objectForKey:@"nAbierta"] intValue];
                                                if (nAbierta > 0) {
                                                    NSLog(@"Mostrar MnuPpl");
                                                }
                                                else{
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"La orden para esta ruta ya fue completada" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
                                                    [alert show];
                                                    perform_segue = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }else{
                //Conexion Local
                spGenerales *sps = [spGenerales alloc];
                NSString *vlauxVerifRuta;
                NSInteger vlAuxAbierto;
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:@"1" forKey:@"nIdEncargado"];
                [prefs synchronize];
                
                vlauxVerifRuta = [sps Sp_Verificar_SalidaRuta:[_sIdRuta intValue]];
                
                if ([vlauxVerifRuta isEqualToString:@"N"]) {
                    NSLog(@"frmSalidaRuta");
                }
                else{
                    
                    vlAuxAbierto = [sps sp_RevisaEstatusOrden:[_sIdRuta intValue] wAplica:@"N"];
                    if (vlAuxAbierto > 0) {
                        /*MnuPpalViewController *vMenuPrincipal = [self.storyboard instantiateViewControllerWithIdentifier:@"vMnuPpal"];
                         vMenuPrincipal.navigationItem.title = @"Ruta";
                         [self.navigationController pushViewController:vMenuPrincipal animated:YES];*/
                        //PruebaTableViewController *vMenuPrincipal = [self.storyboard instantiateViewControllerWithIdentifier:@"vPrueba"];
                        /*VC_Instancias *vMenuPrincipal = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Instancias"];
                         vMenuPrincipal.navigationItem.title = @"Ruta";
                         [self.navigationController pushViewController:vMenuPrincipal animated:YES];*/
                        perform_segue = true;
                    }
                    else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"La orden para esta ruta ya fue completada" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
                        [alert show];
                        perform_segue = false;
                    }
                }
                
            }
            
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"No se ha cargado aún la información local" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
            [alert show];
            perform_segue = false;
        }
    }
}

- (IBAction)btnSurtirRuta:(id)sender {
    [self surtir_ruta_action];
}

- (IBAction)btn_surtir_oculto:(id)sender {
    [self surtir_ruta_action];
    VC_Instancias *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Instancias"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)btn_surtir_oculto_2:(id)sender {
    [self surtir_ruta_action];
    VC_Instancias *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Instancias"];
    [self.navigationController pushViewController:controller animated:YES];
}



- (IBAction)btnupload:(id)sender{//BOTON DE CUANDO SE PICA UPLOAD    //[self uploadshowWithDetailsLabel];//HUD
     if([self validaruta]){
         UIAlertView *alertUpload = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Se va a subir la ruta" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alertUpload show];
    [self Upload];
    NSInteger vlOrdendeservicio = [[cabecerodao CargarOrdendeservicio]integerValue];
    NSString *hour = [self getHour];
   
   
    
    [horasdao ActualizaHora_LLEGADA:vlOrdendeservicio ID_RUTA:[_sIdRuta integerValue] HORA_LLEGADA:hour HORA_LLEGADA_ENVIADA:1];
     }
     else{
    UIAlertView *alertnoUpload = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Tiene que completar la ruta para poder subirla" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alertnoUpload show];
     }
}

-(void)Upload{

    /*UIAlertController * alert_exito=   [UIAlertController
                                        alertControllerWithTitle:@"Atención"
                                        message:@"Subiendo información de la ruta."
                                        preferredStyle:UIAlertControllerStyleAlert];
    [self presentViewController:alert_exito animated:YES completion:nil];*/
    
    if ([_nConectaInternet isEqualToString:@"0"]) {
        NSLog(@"Se necesita conexion para poder subir los datos");
    }
    BOOL hayinternet = YES;
    if(hayinternet){
    uHUD.progress = 0.2;
    NSString *vlOrdendeservicio = [cabecerodao CargarOrdendeservicio];
    //NSString *dif = [cabecerooperaciones obtenerdiferencia];//Preguntar que es diferencia//1
    NSString *var = [cabecerooperaciones newvarmedida];
    NSString *imtest = [NSString stringWithFormat:@"<ENVIA_INFORMACION><ORDEN_SERVICIO>%@</ORDEN_SERVICIO>%@</ENVIA_INFORMACION>",vlOrdendeservicio,var];
    [self webservicepost:imtest];
uHUD.progress = 0.5;

    pendientes = @"0";//MOVER CUANDO SEAN MAS DE UNA INSTANCIA
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:pendientes forKey:@"imagenes_pendientes"];
    [userDefaults synchronize];
        [self Badge];
      uHUD.progress = 0.7;
    }
    
    [self UploadImage];
    
    uHUD.progress = 1;
    //UIAlertView *alertUpload = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Se han subido los datos correctamente" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //[alertUpload show];
    
    //[alert_exito dismissViewControllerAnimated:YES completion:nil];
    pendientes = @"0";//MOVER CUANDO SEAN MAS DE UNA INSTANCIA
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:pendientes forKey:@"imagenes_pendientes"];
    [userDefaults synchronize];
    [self Badge];
}

-(void)UploadImage{
    [self _setupManager];
    [self Uploadall];
    [camaraupload Uploadall];
    [camaraupload Deleteall];
    
} 


-(NSString *)uploadstring: (NSString *)full{
    NSString *ienvia_informacion = @"<ENVIA_INFORMACION>";
    NSString *fenvia_informacion = @"</ENVIA_INFORMACION>";
    
    NSString *fullstring = [NSString stringWithFormat:@"%@%@%@",ienvia_informacion,full,fenvia_informacion];
    
    
    return fullstring;
}

-(NSString *)getHour{
    NSDate* date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"HH:mm:ss";
    NSString *fecha = [formatter stringFromDate:date];
    return fecha;
}

-(void)webservicepost:(NSString *)poststring{
    NSString *vlOrdendeservicio = [cabecerodao CargarOrdendeservicio];
   
    NSString *newpoststring = [NSString stringWithFormat: @"{\"sXML\":\"%@\"}",poststring];
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * sWebService = [prefs stringForKey:@"sWebService"];
    NSString * url = [NSString stringWithFormat:@"%@enviaInformacion?sXML=",sWebService];
    NSURL *remoteURL = [NSURL URLWithString:url];
    
    NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] initWithURL:remoteURL];
   
    [Request addValue:@"application/json; charset=utf-8"  forHTTPHeaderField: @"Content-Type"];
    
    
    NSMutableData *body = [NSMutableData data];

    [body appendData:[newpoststring dataUsingEncoding:NSUTF8StringEncoding]]; //your content itself
  
    
    [Request setHTTPMethod:@"POST"]; //set Method as POST
    [Request setHTTPBody:body];
    NSData *data = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
    NSString *datos = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *resp_Servidor = [NSString stringWithFormat:@"La respuesta del servidor de subida fue:%@ para la orden de servicio:%@",datos,vlOrdendeservicio];
    [logProceso escribir:resp_Servidor];
    pendientes = @"0";//MOVER CUANDO SEAN MAS DE UNA INSTANCIA
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:pendientes forKey:@"imagenes_pendientes"];
    [userDefaults synchronize];
    [self Badge];
    }


//CAMARA

-(void)Uploadall {
    imagenes = [dao CargarImagenes];
    
    while(list<[imagenes count]) {
        
        NSMutableDictionary *dic = [imagenes objectAtIndex:list];
        
        NSString *displayimg = [dic objectForKey:@"imagen"];
        NSString *Filename = [dic objectForKey:@"id_imagen"];
        UIImage *newimage = [self stringToUIImage:displayimg];
        
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",Filename]];
        NSData *imageData = UIImagePNGRepresentation(newimage);
        [imageData writeToFile:path atomically:YES];
        
        [self.requestsManager addRequestForUploadFileAtLocalPath:path toRemotePath:Filename];
        list++;
        
        //[NSThread sleepForTimeInterval:4.0f];
    }
    
    [self.requestsManager startProcessingRequests];
    [NSThread sleepForTimeInterval:5.0f];
    
}

//STRING TO IMAGE------------------------------------------------------------------------
- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

//CONVERTIR A BASE 64-------------------------------------------------------------------
- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (void)_setupManager
{
    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"ftp://173.192.80.226"//[self.hostnameTextField text]
                                                                  user:@"ftpImgBodies"//[self.usernameTextField text]
                                                              password:@"Ads720510."/*[self.passwordTextField text]*/];
    self.requestsManager.delegate = self;
}


//VALIDAR QUE LA RUTA SE HALLA COMPLETADO ANTES DE PERMITIR SUBIRLA
-(BOOL)validaruta{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    spGenerales *generales = [spGenerales alloc];
    NSDictionary *animals = [generales sp_revisaSitioPorRuta:[[prefs stringForKey:@"nIdRuta"] intValue] wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"]];
    NSArray *test = [animals allKeys];
    NSMutableArray *completeArray = [NSMutableArray array];
    
    for (int i =0; i<test.count; i++) {
        NSArray *prueba = [animals objectForKey:test[i]];
        [completeArray addObjectsFromArray:prueba];
    }     
    BOOL validado = YES;
    NSMutableArray *Ainstancias = [[NSMutableArray alloc]init];
    Ainstancias = completeArray;//[edao getInstancias];
    NSMutableArray *Avisitadas = [[NSMutableArray alloc]init];
    Avisitadas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    [Avisitadas removeObject:[Avisitadas objectAtIndex:([Avisitadas count] - 1)]];
    if ([Ainstancias count]==[Avisitadas count]) {
        validado = YES;
    }

    return validado;
}




@end
