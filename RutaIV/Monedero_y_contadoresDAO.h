//
//  Monedero_y_contadoresDAO.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 23/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface Monedero_y_contadoresDAO : NSObject{
    sqlite3 *bd;
    NSMutableArray *Monedas;
    NSMutableArray *Historicos;
    NSMutableArray *suma;
    NSString *Orden_visita;
}

-(NSMutableArray *)CargarHistoricos:(int) instancia orden_visita:(int)ordenvisita;
-(BOOL)InsertaMoneda:(int)ID_ORDEN_SERVICIO ID_INSTANCIA:(int)ID_INSTANCIA ID_MONEDA:(int)ID_MONEDA CANTIDAD:(int)CANTIDAD ORDEN_VISITA:(int)ORDEN_VISITA;
-(BOOL)actualizaHistoricos:(NSInteger)ID_ORDEN_SERVICIO INSTANCIA:(NSInteger)INSTANCIA ORDENVISITA:(NSInteger)ORDENVISITA UNIDADES:(NSInteger)UNIDADES MONTO:(double)MONTO RECOGIODINERO:(NSInteger)RECOGIODINERO;
-(NSMutableArray *)vlMontoMax:(int) instancia;
-(NSMutableArray *)vlUnidadMax:(int) instancia;
-(NSString*)obtenerorden_visita:(int)Instancia;
-(BOOL)Borraventas_hist_instancia;
@end