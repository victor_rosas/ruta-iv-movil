//
//  VC_Cambio_Planograma.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DATA_Cambio.h"

@interface VC_Cambio_Planograma : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    DATA_Cambio * data_cambio_manager;
    NSMutableArray * arreglo_cambio_parametros;
    int contador_rellenado;
    BOOL capacidad_espiral_superada;
    BOOL productos_orden;
    BOOL alerta_presente;
    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_orden;
    UIAlertController * alerta_capacidad;
    UIAlertController * alerta_planograma;
    
    UIToolbar* keyboardDoneButtonView;
    
    __weak IBOutlet UITableView *myTableView;
    __weak IBOutlet UIButton *btn_siguiente;
}
@property int id_instancia;
@property BOOL salida_ruta;
- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
