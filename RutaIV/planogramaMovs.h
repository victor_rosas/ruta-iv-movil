//
//  planogramaMovs.h
//  RutaIV
//
//  Created by Miguel Banderas on 30/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface planogramaMovs : NSObject

-(BOOL)borraPlanogramaMovs;

-(BOOL)instertaPlanogramaMovs:(int)nIdCliente wIdSitio:(int)nIdSitio wIdInstancia:(int)nIdInstancia wIdArticulo:(int)nIdArticulo wInvIni:(double)dInvIni wSurtio:(double)dSurtio wRemovio:(double)dRemovio wCaducaron:(double)dCaducaron wClave:(int)nClave wInicio:(int)nInicio wOrdenVisita:(int)nOrdenVisita wIdOrdenServicio:(int)nIdOrdenServicio wSalteado:(int)nSalteado wExistencia:(double)dExistencia wIdEspiral:(int)nIdEspiral wIdCharola:(int)nIdCharola wCapacidadEspiral:(int)nCapacidadEspiral wIdUbicacion:(int)nIdUbicacion wIdPlanograma:(int)nIdPlanograma wSitio:(NSString *)sSitio wArticulo:(NSString *)sArticulo wPrecio:(double)dPrecio wIdRuta:(int)nIdRuta wExRuta:(double)dExRuta wEspiral:(NSString *)sEspiral wCambio:(NSString *)sCambio wTipoArticulo:(NSString *)sTipoArticulo wSubsidio:(double)dSubsidio wPruebasSel:(int)nPruebasSel wVasos:(int)nVasos wVentaDexAnt:(int)nVentaDexAnt wImporteVtaDexAnt:(double)dImporteVtaDexAnt wVentaDex:(double)dVentaDex wImporteVtaDex:(double)dImporteVtaDex wIdSeleccion:(int)nIdSeleccion wIdCategoria:(int)nIdCategoria wCapacidadNva:(int)nCapacidadNva categoria:(NSString *)sCategoria;

@end
