//
//  VC_Configuracion_General.h
//  RutaIV
//
//  Created by Miguel Banderas on 05/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_Configuracion_General : UITableViewController{
    
    __weak IBOutlet UISwitch *switch_instancia_orden;
    __weak IBOutlet UISwitch *switch_producto_orden;
    __weak IBOutlet UISwitch *switch_red_celular;
    __weak IBOutlet UISwitch *switch_apariencia_mayusculas;
    __weak IBOutlet UISwitch *switch_historico;
    __weak IBOutlet UISwitch *switch_barra_progreso;
    __weak IBOutlet UISwitch *switch_historico_cafe;
    
    //Valores para el log de errores
    NSString  *_sNombreRuta;
    NSString  *_sIdRuta;
    NSString  *_sIdSucursal;
}
- (IBAction)action_btn_restaurar:(id)sender;
- (IBAction)enviarBD:(id)sender;
- (IBAction)back_pressed:(id)sender;
- (IBAction)enviarLog:(id)sender;

@end
