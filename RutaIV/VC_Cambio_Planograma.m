//
//  VC_Cambio_Planograma.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Cambio_Planograma.h"
#import "TC_Cambio_Planograma.h"
#import "OBJ_Cambio_Planograma.h"
#import "NextViewController.h"

@interface VC_Cambio_Planograma ()

@end

@implementation VC_Cambio_Planograma
@synthesize id_instancia;
@synthesize salida_ruta;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    arreglo_cambio_parametros = [[NSMutableArray alloc]init];
    data_cambio_manager = [[DATA_Cambio alloc]init];
    contador_rellenado = 0;
    capacidad_espiral_superada = false;
    alerta_presente = false;
    [self inicializacion_alertas];
    NSString * parametro_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_producto_orden"];
    if ([parametro_orden isEqualToString:@"0"]) {
        productos_orden = false;
    }
    else{
        productos_orden = true;
    }
    
    //Inicializacion de vistas
    btn_siguiente.enabled = false;
    [self button_setup];
    
    //Cargar datos
    arreglo_cambio_parametros = [data_cambio_manager cargar_cambio_planograma:id_instancia];
    
    //Alerta
    alerta_planograma=   [UIAlertController
                            alertControllerWithTitle:@"Atención"
                            message:@"Remover los productos indicados."
                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alerta_planograma dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alerta_planograma addAction:ok];
    [self presentViewController:alerta_planograma animated:YES completion:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
    NSIndexPath * index_path = [NSIndexPath indexPathForRow:0 inSection:0];
    [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:index_path];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
    
    [self update_en_cero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Aspecto y funciones de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_cambio_parametros count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView dequeueReusableCellWithIdentifier:@"tc_cambio_planograma" forIndexPath:indexPath];
    
    //Extraccion de objeto requerido por celda
    OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:indexPath.row];
    NSString * capacidad = [NSString stringWithFormat:@"C.E. - %@", cambio_planograma.capacidad];
    
    //Creacion de la celda
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    NSString * nombre_articulo = cambio_planograma.articulo;
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        nombre_articulo = [nombre_articulo capitalizedString];
    }
    
    [cell cell_setup:nombre_articulo espiral:cambio_planograma.espiral cantidad:cambio_planograma.cantidad_removida capacidad:capacidad];
    if (![cambio_planograma.cantidad_removida isEqualToString:@""])
    {
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:204.0/255.0f alpha:0.6];
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    //Se agrega boton a todos los text fields de las celdas
    cell.text_field_cantidad.inputAccessoryView = keyboardDoneButtonView;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Se extrae el text view de la celda para poder seleccionarlo
    TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:indexPath];
    UITextField * text_field = cell.text_field_cantidad;
    [text_field becomeFirstResponder];
}

//Seleccion de celda cuando se inicia modificacion en TextField
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    if (capacidad_espiral_superada) {
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_path posicion_regreso:(uint32_t)index_path_pasado.row];
            [myTableView selectRowAtIndexPath:index_path_pasado animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:index_path_pasado];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_path_pasado];
            
            [self presentViewController:alerta_capacidad animated:YES completion:nil];
        });
    }
    else
    {
        index_path_pasado = index_path;
        
        //Verificacion orden
        BOOL continuar;
        if (productos_orden) {
            continuar = [self verificacion_orden_otros:index_path];
        }
        else
        {
            continuar = true;
        }
        
        if (continuar) {
            [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionNone];
            
            [self ajuste_scroll:index_path];
        }
        
    }
    
}

//Guardar informacion de TextField y activacion de boton
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:index_path.row];
    cambio_planograma.cantidad_removida = [NSString stringWithFormat:@"%@%@",textField.text,string];

    if ([textField.text isEqualToString:@""]&&![string isEqualToString:@""]) {
        contador_rellenado++;
    }
    
    //En caso de borrar es necesario hacer esto
    if ([string isEqualToString:@""]) {
        int lenght =(uint32_t)[textField.text length];
        NSString * texto_actualizado = [textField.text substringToIndex:(lenght-1)];
        cambio_planograma.cantidad_removida = texto_actualizado;
        
        if ([texto_actualizado isEqualToString:@""]) {
            contador_rellenado --;
        }
    }
    [arreglo_cambio_parametros replaceObjectAtIndex:index_path.row withObject:cambio_planograma];
    
    //Decide cuando se desactiva el boton
    /*Original
     if (!(contador_rellenado == [arreglo_cambio_parametros count])) {//Siempre es diferente de?
     btn_siguiente.enabled = false;1
     }
     */
    if (contador_rellenado == [arreglo_cambio_parametros count]) {//Siempre es diferente de?
        btn_siguiente.enabled = true;
    }
    
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 5) ? NO : YES;
}

//Recargar informacion en cell recien abandonada //Si hay un solo articulo nunca va a abandonar el cell
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    TC_Cambio_Planograma * cell = (TC_Cambio_Planograma*)textField.superview.superview;
    NSIndexPath * index_path = [myTableView indexPathForCell:cell];
    
    NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
    
    [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self verificacion_capacidad_espiral:index_path_pasado];
    
    //Decide cuando se activa el boton
    if (contador_rellenado == [arreglo_cambio_parametros count]) {
        btn_siguiente.enabled = true;
    }
    else
    {
        btn_siguiente.enabled = false;
    }
    
}

//Setup del boton
- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 50)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    NSIndexPath * selected = [myTableView indexPathForSelectedRow];
    NSIndexPath * next;
    
    if ([arreglo_cambio_parametros count] == 1) {
        next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
        [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:next];
        UITextField * text_field = cell.text_field_cantidad;
        [text_field resignFirstResponder];
        [text_field becomeFirstResponder];
    }
    
    //Revision orden
    BOOL continuar;
    if (productos_orden) {
        continuar = [self verificacion_orden_next:selected];
    }
    else
    {
        continuar = true;
    }
    
    if (continuar) {
        if (selected.row+1 == [arreglo_cambio_parametros count])
        {
            next = [NSIndexPath indexPathForRow:0 inSection:selected.section];
            [self regresar_anterior:selected posicion_regreso:0];
        }
        else
        {
            next= [NSIndexPath indexPathForRow:selected.row+1 inSection:selected.section];
        }
        
        [myTableView selectRowAtIndexPath:next animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        
        [self ajuste_scroll:next];
        
        TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:next];
        UITextField * text_field = cell.text_field_cantidad;
        [text_field becomeFirstResponder];
    }
}

//Metodo para regresar a posicion lejana
-(void)regresar_anterior :(NSIndexPath *)selected posicion_regreso:(int)posicion
{
    while (selected.row > posicion+4) {
        selected = [NSIndexPath indexPathForRow:selected.row-3 inSection:selected.section];
        [myTableView selectRowAtIndexPath:selected animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
}

//Ajuste de scroll
-(void)ajuste_scroll :(NSIndexPath *)index_path
{
    int offset = -1;
    if (index_path.row+offset>=[arreglo_cambio_parametros count])
    {
        [myTableView selectRowAtIndexPath:index_path animated:YES scrollPosition:UITableViewScrollPositionTop];
    }
    else
    {
        NSIndexPath * scroll_offset = [NSIndexPath indexPathForRow:index_path.row+offset inSection:index_path.section];
        [myTableView scrollToRowAtIndexPath:scroll_offset atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

//Verificacion orden otros casos
-(BOOL)verificacion_orden_otros :(NSIndexPath *)index_seleccionado
{
    if (!(index_seleccionado.row <= contador_rellenado)) {
        NSIndexPath * index_verdadero = [NSIndexPath indexPathForRow:contador_rellenado inSection:index_seleccionado.section];
        
        dispatch_async(dispatch_get_main_queue(), ^ {
            [self regresar_anterior:index_seleccionado posicion_regreso:(uint32_t)index_verdadero.row];
            [myTableView selectRowAtIndexPath:index_verdadero animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            TC_Cambio_Planograma *cell = (TC_Cambio_Planograma *)[myTableView cellForRowAtIndexPath:index_verdadero];
            UITextField * text_field = cell.text_field_cantidad;
            [text_field becomeFirstResponder];
            
            [self ajuste_scroll:index_verdadero];
            if (!alerta_presente) {
                alerta_presente = true;
                [self presentViewController:alerta_orden animated:YES completion:nil];
            }
        });
        
        return false;
    }
    return true;
}

//Verificacion orden_next
-(BOOL)verificacion_orden_next :(NSIndexPath *)index_actual
{
    //Revisa si la celda actual tiene algo escrito, si no es asi no ejecuta el movimiento
    int posicion = (uint32_t)index_actual.row;
    
    OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:posicion];
    NSString * removio = cambio_planograma.cantidad_removida;
    
    if ([removio isEqualToString:@""]) {
        [myTableView scrollToRowAtIndexPath:index_actual atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
        [self ajuste_scroll:index_actual];
        [self presentViewController:alerta_orden animated:YES completion:nil];
        return false;
    }
    else
    {
        return true;
    }
}

//Verificacion capacidad de espiral
-(void)verificacion_capacidad_espiral :(NSIndexPath *)index_path
{
    OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:index_path.row];
    int cantidad_removida = (uint32_t)[cambio_planograma.cantidad_removida integerValue];
    int capacidad_espiral = (uint32_t)[cambio_planograma.capacidad integerValue];
    if (cantidad_removida>capacidad_espiral) {
        cambio_planograma.cantidad_removida = @"";
        contador_rellenado--;
        [arreglo_cambio_parametros replaceObjectAtIndex:index_path.row withObject:cambio_planograma];
        NSArray * arreglo_path = [[NSArray alloc]initWithObjects:index_path, nil];
        
        [myTableView reloadRowsAtIndexPaths:arreglo_path withRowAnimation:UITableViewRowAnimationAutomatic];
        
        capacidad_espiral_superada = true;
    }
    else
    {
        capacidad_espiral_superada = false;
    }
}

-(void)inicializacion_alertas
{
    alerta_capacidad = [UIAlertController
                        alertControllerWithTitle:@"Error"
                        message:@"Se ha superado la capacidad del espiral."
                        preferredStyle:UIAlertControllerStyleAlert];
    
    alerta_orden =  [UIAlertController
                     alertControllerWithTitle:@"Error"
                     message:@"Llenar la información en orden."
                     preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok_capacidad = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alerta_capacidad dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction* ok_orden = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             alerta_presente = false;
                             [alerta_orden dismissViewControllerAnimated:YES completion:nil];
                         }];
    
    [alerta_capacidad addAction:ok_capacidad];
    [alerta_orden addAction:ok_orden];
}


- (IBAction)action_back:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se perderá la información."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)action_siguiente:(id)sender {
    for (int i = 0; i<[arreglo_cambio_parametros count]; i++) {
        OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:i];
        NSInteger cantidad = [cambio_planograma.cantidad_removida integerValue];
        NSInteger id_articulo = [cambio_planograma.id_articulo integerValue];
        NSInteger id_charola = [cambio_planograma.id_charola integerValue];
        NSInteger id_espiral = [cambio_planograma.id_espiral integerValue];
        NSInteger orden_servicio = [cambio_planograma.id_orden_servicio integerValue];
        NSInteger orden_vsita = [cambio_planograma.orden integerValue];
        NSInteger instancia = id_instancia;
        
        [data_cambio_manager update_inicial_removio:cantidad id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
    }
    
    NextViewController * vc_inventario = [self.storyboard instantiateViewControllerWithIdentifier:@"inventario"];
    vc_inventario.instancia = id_instancia;
    vc_inventario.salida_sitio = salida_ruta;
    vc_inventario.arreglo_cambio_planograma = arreglo_cambio_parametros;
    [self.navigationController pushViewController:vc_inventario animated:YES];
}

-(void)update_en_cero
{
    for (int i = 0; i<[arreglo_cambio_parametros count]; i++) {
        OBJ_Cambio_Planograma * cambio_planograma = [arreglo_cambio_parametros objectAtIndex:i];
        NSInteger id_articulo = [cambio_planograma.id_articulo integerValue];
        NSInteger id_charola = [cambio_planograma.id_charola integerValue];
        NSInteger id_espiral = [cambio_planograma.id_espiral integerValue];
        NSInteger orden_servicio = [cambio_planograma.id_orden_servicio integerValue];
        NSInteger orden_vsita = [cambio_planograma.orden integerValue];
        NSInteger instancia = id_instancia;
        
        [data_cambio_manager update_inicial_removio:0 id_articulo:id_articulo id_charola:id_charola id_espiral:id_espiral orden_servicio:orden_servicio orden_visita:orden_vsita id_instancia:instancia];
    }
}
@end
