//
//  VC_Configuracion_Camara.h
//  RutaIV
//
//  Created by Miguel Banderas on 28/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VC_Configuracion_Camara : UIViewController{
    __weak IBOutlet UITextField *label_direccion;
    __weak IBOutlet UITextField *label_usuario;
    __weak IBOutlet UITextField *label_password;
    
    UIToolbar* keyboardDoneButtonView;
    NSMutableArray * array_text_fields;
    int current;
}
- (IBAction)action_guardar_cambios:(id)sender;
- (IBAction)back_pressed:(id)sender;

@end
