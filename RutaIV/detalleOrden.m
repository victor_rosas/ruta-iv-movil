//
//  detalleOrden.m
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "detalleOrden.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation detalleOrden

-(BOOL)borraDetalleOrden{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM DET_ORDEN_SERVICIO"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaDetalleOrden:(int)nIdOrdenServicio wOrden:(int)nOrden wCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wIdUbicacion:(int)nIdUbicacion wIdInstancia:(int)nIdInstancia wIdOperacion:(int)nIdOperacion wHoraInicio:(NSString *)sHoraInicio wCodigoAutorizacion:(int)nCodigoAutorizacion wTipoAutorizacion:(NSString *)sTipoAutorizacion wOrdenVisita:(int)nOrdenVisita wRecaudo:(int)nRecaudo{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
        sTipoAutorizacion = @"NULL";
    }
    
    if ([sHoraInicio isEqualToString:@""]) {
        sHoraInicio = @"NULL";
    }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO DET_ORDEN_SERVICIO(\"ID_ORDEN_SERVICIO\",\"ORDEN\",\"ID_CLIENTE\",\"ID_SITIO\",\"ID_UBICACION\",\"ID_INSTANCIA\",\"ID_OPERACION\",\"HORA_INICIO\",\"CODIGO_AUTORIZACION\",\"TIPO_AUTORIZACION\",\"ORDEN_VISITA\",\"RECAUDO\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\",\"%i\",\"%@\",\"%i\",\"%i\")",nIdOrdenServicio,nOrden,nIdCliente,nIdSitio,nIdUbicacion,nIdInstancia,nIdOperacion,sHoraInicio,nCodigoAutorizacion,sTipoAutorizacion,nOrdenVisita,nRecaudo];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
