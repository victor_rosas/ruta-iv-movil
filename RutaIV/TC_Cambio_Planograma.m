//
//  TC_Cambio_Planograma.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TC_Cambio_Planograma.h"

@implementation TC_Cambio_Planograma
@synthesize text_field_cantidad;

- (void)awakeFromNib {
    // Initialization code
    text_field_cantidad.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)cell_setup :(NSString *)articulo espiral:(NSString *)id_espiral cantidad:(NSString *)field_cantidad capacidad:(NSString *)capacidad_espiral
{
    label_espiral.layer.cornerRadius = 5;
    label_espiral.layer.masksToBounds = TRUE;
    
    label_articulo.text = articulo;
    label_espiral.text = id_espiral;
    text_field_cantidad.text = field_cantidad;
    label_capacidad.text = capacidad_espiral;
}

@end
