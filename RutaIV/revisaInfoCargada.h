//
//  revisaInfoCargada.h
//  RutaIV
//
//  Created by Miguel Banderas on 22/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface revisaInfoCargada : NSObject

-(BOOL)revisaCargada;

@end
