//
//  VC_Pruebas_Inventario.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 13/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface VC_Pruebas_Inventario : UIViewController
{
    __weak IBOutlet UITextField *label_instancia;
    __weak IBOutlet UITextField *label_orden;
    __weak IBOutlet UITextField *label_articulo;
}
- (IBAction)btn_buscar:(id)sender;

@end
