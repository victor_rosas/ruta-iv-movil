//
//  OBJ_Inventario_Producto.h
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Inventario_Producto : NSObject
{
   
    
}
-(id)init:(NSString *)nombre cantidad:(int)numero tipo:(NSString *)id_tipo;
@property NSString *producto;
@property int cantidad;
@property NSString *tipo_articulo;
@end
