//
//  sitiosAsignados.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface sitiosAsignados : NSObject

-(BOOL)borraSitios;

-(BOOL)insertaSitios:(int)nIdOrden wIdCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wRuta:(int)nRuta wDia:(int)nDia;

@end
