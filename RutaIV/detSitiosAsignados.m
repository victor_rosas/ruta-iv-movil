//
//  detSitiosAsignados.m
//  RutaIV
//
//  Created by Miguel Banderas on 28/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "detSitiosAsignados.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation detSitiosAsignados

-(BOOL)borraDetSitios{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM DET_SITIOS_ASIGNADOS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaDetSitios:(int)nIdCliente wIdSitio:(int)nIdSitio wIdInstancia:(int)nIdInstancia wIdOrden:(int)nIdOrden wOrdenVisita:(int)nOrdenVisita wIdRuta:(int)nIdRuta wDia:(int)nDia{
    BOOL bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO DET_SITIOS_ASIGNADOS(\"ID_CLIENTE\",\"ID_SITIO\",\"ID_INSTANCIA\",\"ID_ORDEN\",\"ORDEN_VISITA\",\"ID_RUTA\",\"DIA\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\")",nIdCliente,nIdSitio,nIdInstancia,nIdOrden,nOrdenVisita,nIdRuta,nDia];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
