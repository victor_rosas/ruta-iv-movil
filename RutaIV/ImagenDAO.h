//
//  ImagenDAO.h
//  Fotografia_vending
//
//  Created by Jose Miguel Banderas Lechuga on 10/12/14.
//  Copyright (c) 2014 Jose Miguel Banderas Lechuga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ImagenDAO : NSObject{
    sqlite3 *bd;
    NSMutableArray *imagenes;
    NSMutableArray *productos;
}


-(void)CrearImagen: (NSString *)Image Path:(NSString *)Path;
-(NSMutableArray *) obtenerImagenes;
-(void) actualizarImagenes: (NSInteger)image_id Imagen:(NSString *)Image Path:(NSString *)Path;
-(void) borrarImagenes;
- (void) borrarFotosVisita;
-(NSString *)obtenerRutaBD;
//-(void)Insertar:(NSString *)Imagedata Path:(NSString *)imgPath id_image:(NSString *)id_image id_image2:(NSString *)id_image2 id_image3:(NSString *)id_image3 id_image4:(NSString *)id_image4;
-(void)Insertar:(NSString *)IMAGEN PATH:(NSString *)PATH ID_IMAGEN:(NSString *)ID_IMAGEN ID_INSTANCIA:(NSString *)ID_INSTANCIA ID_ORDEN_SERVICIO:(NSString *)ID_ORDEN_SERVICIO ORDEN_VISITA:(NSString *)ORDEN_VISITA;
-(NSMutableArray *)CargarImagenes;
-(NSMutableArray *)CargarProductos;
-(void)InsertarFotos_visita:(int)ID_ORDEN_SERVICIO ID_INSTANCIA:(int)ID_INSTANCIA ORDEN_VISITA:(int)ORDEN_VISITA ID_FOTO:(int)ID_FOTO URL:(NSString*)URL;
@end
