//
//  VC_Devoluciones_Especie.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"
#import "DATA_Devoluciones.h"

@interface VC_Devoluciones_Especie : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    ProductosDAO * data_articulo_manager;
    DATA_Devoluciones * data_devoluciones_manager;
    NSMutableArray * arreglo_articulos;
    BOOL inventario_superado;
    BOOL alerta_presente;
    BOOL back;
    int field_seleccionado;

    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_capacidad;
    UIAlertController * alerta_cantidad;
    UIAlertController * alerta_incompleto;
    
    UIToolbar* keyboardDoneButtonView;
    __weak IBOutlet UIBarButtonItem *btn_siguiente;
    __weak IBOutlet UITableView *myTableView;
    __weak IBOutlet UITextField *field_contacto;
    __weak IBOutlet UITextField *field_telefono;
    __weak IBOutlet UITextField *field_cantidad;
}
@property int id_instancia;
@property BOOL salida_sitio;
@property NSInteger visitados;

- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
