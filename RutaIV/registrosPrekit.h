//
//  registrosPrekit.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface registrosPrekit : NSObject

-(BOOL)borraRegistrosPrekit;

-(BOOL)insertaRegistrosPrekit:(NSString *)sFecha wIdInstancia:(int)nIdInstacia wIdArticulo:(int)nIdArticulo wSugerido:(double)dSugerido wNombre:(NSString *)sNombre;

@end
