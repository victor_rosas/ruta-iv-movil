//
//  DATA_Progreso_Ruta.m
//  RutaIV
//
//  Created by Miguel Banderas on 25/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "DATA_Progreso_Ruta.h"
#import "AppDelegate.h"

@implementation DATA_Progreso_Ruta


-(NSMutableDictionary *)SELECT_datos_progreso:(int)id_instancia
{
    NSMutableDictionary * diccionario_resultados = [[NSMutableDictionary alloc]init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MAX(ID_ORDEN_SERVICIO), MAX(ORDEN), MAX(ORDEN_VISITA), MAX(ID_SITIO) FROM DET_ORDEN_SERVICIO WHERE ID_INSTANCIA = %d",id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString * id_orden_servicio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * orden = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * id_sitio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                
                [diccionario_resultados setObject:id_orden_servicio forKey:@"id_orden_servicio"];
                [diccionario_resultados setObject:orden forKey:@"orden"];
                [diccionario_resultados setObject:orden_visita forKey:@"orden_visita"];
                [diccionario_resultados setObject:id_sitio forKey:@"id_sitio"];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return diccionario_resultados;
}

-(NSMutableArray *)SELECT_instancias_sitio :(NSInteger)id_sitio
{
    NSMutableArray * arreglo_instancias_sitio = [[NSMutableArray alloc]init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT DISTINCT ID_INSTANCIA FROM DET_ORDEN_SERVICIO WHERE ID_SITIO = %ld",(long)id_sitio];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString * id_instancia = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                [arreglo_instancias_sitio addObject:id_instancia];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    return arreglo_instancias_sitio;
}

//Mandar progreso de la ruta a web service
-(void)mandar_hora :(int)id_orden_servicio orden:(int)orden orden_visita:(int)orden_visita id_instancia:(int)id_instancia id_operacion:(int)id_operacion
{
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString * hora = [dateFormatter stringFromDate:now];
    
    NSString * cadena = [NSString stringWithFormat:@"<ENVIA_HORA><DET_ORDEN_SERVICIO><ID_ORDEN_SERVICIO>%d</ID_ORDEN_SERVICIO><ORDEN>%d</ORDEN><ORDEN_VISITA>%d</ORDEN_VISITA><ID_INSTANCIA>%d</ID_INSTANCIA><ID_OPERACION>%d</ID_OPERACION><HORA_INICIO>01/01/1900 %@</HORA_INICIO></DET_ORDEN_SERVICIO></ENVIA_HORA>", id_orden_servicio, orden, orden_visita, id_instancia, id_operacion, hora];
    
    NSString *newpoststring = [NSString stringWithFormat: @"{\"sXML\":\"%@\"}",cadena];
    
    NSURL *remoteURL = [NSURL URLWithString:@"http://173.192.80.226/WCFMobileVending/Service1.svc/json/enviarhora"];
    
    NSMutableURLRequest *Request = [[NSMutableURLRequest alloc] initWithURL:remoteURL];
    
    [Request addValue:@"application/json; charset=utf-8"  forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[newpoststring dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [Request setHTTPMethod:@"POST"];
    [Request setHTTPBody:body];
   NSData *data = [NSURLConnection sendSynchronousRequest:Request returningResponse:nil error:nil];
   NSString *datos = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *logString = [NSString stringWithFormat:@"La respuesta del WS de enviarhora fue:%@ para la orden de servicio:%d instancia:%d",datos,id_orden_servicio,id_instancia];
    logProceso = [[OBJ_Log_Proceso alloc]init];
    [logProceso escribir:logString];
    
}

@end
