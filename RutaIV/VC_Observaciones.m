//
//  VC_Observaciones.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Observaciones.h"
#import "VC_Observaciones_Texto.h"

@interface VC_Observaciones ()

@end

@implementation VC_Observaciones
@synthesize instancia_observaciones;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de vista
    [switch_atencion setSelected:false];
    
    //Inicializacion de variables
    data_observaciones_manager = [[DATA_Observaciones alloc]init];
    diccionario_clasificaciones = [[NSMutableDictionary alloc]init];
    
    //Descarga de inforamcion
    diccionario_clasificaciones = [data_observaciones_manager traer_clasificacion];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [diccionario_clasificaciones count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSArray * array_clasificaciones = [[NSArray alloc]init];
    array_clasificaciones = [diccionario_clasificaciones allKeys];
    return [array_clasificaciones objectAtIndex:row];
}

- (IBAction)action_back_combo:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segue_observaciones"])
    {
        int picker_seleccionado = (uint32_t)[picker_categoria selectedRowInComponent:0];
        NSArray * array_clasificaciones = [diccionario_clasificaciones allKeys];
        NSString * clasificacion = [array_clasificaciones objectAtIndex:picker_seleccionado];
        int id_clasificacion = (uint32_t)[[diccionario_clasificaciones objectForKey:clasificacion] integerValue];
        VC_Observaciones_Texto *vc = [segue destinationViewController];
        vc.id_clasificacion = id_clasificacion;
        
        vc.instancia = instancia_observaciones;
        
        int atencion;
        if ([switch_atencion isOn]) {
            atencion = 1;
        }
        else
        {
            atencion = 0;
        }
        vc.atencion = atencion;
    }
}

@end
