//
//  TVC_Sucursal.h
//  RutaIV
//
//  Created by Miguel Banderas on 12/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVC_Sucursal : UITableViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    __weak IBOutlet UIButton *button_guardar;
    NSMutableArray * arreglo_sucursales;
    NSMutableArray * arreglo_rutas;
    __weak IBOutlet UILabel *txtRuta;
}

@property (weak, nonatomic) IBOutlet UILabel *txtSucursal;
@property (weak, nonatomic) IBOutlet UIPickerView *pRuta;
- (IBAction)btnGuardar:(id)sender;
- (IBAction)back_pressed:(id)sender;

@end
