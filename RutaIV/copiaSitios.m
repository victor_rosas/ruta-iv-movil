//
//  copiaSitios.m
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "copiaSitios.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation copiaSitios

-(BOOL)borrarSitios
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM SITIOS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }

    return  bValida;
    
}

-(BOOL)copiaSitios:(int)nIdCliente wIdSitio:(int)nIdSitio wNombreCorto:(NSString *)sNombreCorto wIdLista:(int)nIdLista{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {

        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO SITIOS(\"ID_CLIENTE\",\"ID_SITIO\",\"NOMBRE_CORTO\",\"ID_LISTA\") VALUES(\"%i\",\"%i\",\"%@\",\"%i\")",nIdCliente,nIdSitio,sNombreCorto,nIdLista];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
