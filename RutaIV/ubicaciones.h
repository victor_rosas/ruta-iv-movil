//
//  ubicaciones.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ubicaciones : NSObject

-(BOOL)borraUbicaciones;

-(BOOL)insertaUbicaciones:(int)nIdCliente wIdSitio:(int)nIdSitio wIdUbicacion:(int)nIdUbicacion wNombre:(NSString *)sNombre;

@end
