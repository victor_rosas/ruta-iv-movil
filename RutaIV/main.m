//
//  main.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
