//
//  CamaraUpload.m
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 13/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "CamaraUpload.h"
#import "GRRequestsManager.h"
#import "Reachability.h"
#import "Imagen.h"
#import "ImagenDAO.h" 
#import "Hora_inicioDAO.h"

@interface CamaraUpload ()
@property (nonatomic, strong) GRRequestsManager *requestsManager;
@end

@implementation CamaraUpload
@synthesize dao;
@synthesize imagenes;
@synthesize instancia;
@synthesize salida_sitio;


- (void)viewDidLoad {
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    instancia = [[prefs objectForKey:@"instancia"]integerValue];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    pendientes = [[userDefaults objectForKey:@"imagenes_pendientes"]integerValue];
    monederoycontadores = [[Monedero_y_contadoresDAO alloc]init];
    dao = [[ImagenDAO alloc]init];
    dao_hora = [[Hora_inicioDAO alloc]init];
    imagenes = [[NSMutableArray alloc]init];
    data_manager_progreso = [[DATA_Progreso_Ruta alloc]init];
    [super viewDidLoad];
    NSArray *elementos_camara = [[NSUserDefaults standardUserDefaults] objectForKey:@"elementos_camara"];
    if ([[elementos_camara objectAtIndex:0] isEqualToString:@"null"]) {
        
    }
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    //NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *redmovil =[[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_3G"];
    if ([redmovil integerValue] == 1) {
        datosmoviles = YES;
    }else{
        datosmoviles = NO;
    }
    reach.reachableOnWWAN = datosmoviles;
    [self isreachableobserver];
    reachable =reach.isReachable;
    if (reachable) {
        [_Wifiswitch setOn:YES];
    }
    
    //INICIAR CON CAMARA
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    picker.showsCameraControls = YES;
    [self presentViewController:picker animated:YES
                     completion:NULL];
    
    ////////////////////////////////
}

-(void)isreachableobserver{
    
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    reach.reachableOnWWAN = datosmoviles;
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [reach startNotifier];
    
}

-(void)reachabilityChanged:(NSNotification *)notification{
    
    if (reachable==YES) {
        [_Wifiswitch setOn:YES];
        Estatus.text = @"Conectado";
        if ([db_count.text integerValue]>0) {
            [NSThread sleepForTimeInterval:5.0f];
            NSLog(@"Uploading");
            Estatus.text = @"Uploading";
            [self Uploadall];
            [self Deleteall];
        }else{}
        
        reachable = NO;
        [_Wifiswitch setOn:YES];
        NSLog(@"Hay conexion");
        Estatus.text = @"Conectado";
        
    }
    else {
        reachable=YES;
        Estatus.text = @"Sin conexion";
        NSLog(@"No hay conexion");
        [_Wifiswitch setOn:NO];
    }
    
}

-(void)Deleteall{
    dao = [[ImagenDAO alloc]init];
    [dao borrarImagenes];
    [dao borrarFotosVisita];
}

-(void)Uploadall {
    imagenes = [[NSMutableArray alloc]init];
    dao = [[ImagenDAO alloc]init];
    imagenes = [dao CargarImagenes];
    
    while(list<[imagenes count]) {
        
        NSMutableDictionary *dic = [imagenes objectAtIndex:list];
        
        NSString *displayimg = [dic objectForKey:@"imagen"];
        NSString *Filename = [dic objectForKey:@"id_imagen"];
        UIImage *newimage = [self stringToUIImage:displayimg];
        
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",Filename]];
        NSData *imageData = UIImagePNGRepresentation(newimage);
        [imageData writeToFile:path atomically:YES];
        [self.requestsManager addRequestForUploadFileAtLocalPath:path toRemotePath:Filename];
        list++;
        pendientes = ([imagenes count] - list);
        NSString * pendientes_string = [@(pendientes)stringValue];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:pendientes_string forKey:@"imagenes_pendientes"];
        [userDefaults synchronize];
        
        [NSThread sleepForTimeInterval:4.0f];
    }
    [self.requestsManager startProcessingRequests];
    [NSThread sleepForTimeInterval:5.0f];
    NSLog(@"Termino Upload");
    
}

//UPLOAD MANUAL A LOS ARCHIVOS GUARDADOS EN BD
- (IBAction)listCLick:(id)sender {
    [self Uploadall];
    [self Deleteall];
    Estatus.text = @"Termino Upload";
}



//ESCALAR IMAGEN
-(UIImage *) imageWithImage:(UIImage *) image scaledTOSize:(CGSize) newsize
{
    UIGraphicsBeginImageContext(newsize);
    [image drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
    UIImage *newimag=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimag;
}

- (IBAction)FotoClick:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    picker.showsCameraControls = YES;
    [self presentViewController:picker animated:YES
                     completion:NULL];
}

- (IBAction)action_continuar:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    int numero_visitadas= (UInt32)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_visitadas"] integerValue];
    NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    
    
    NSString * orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"orden_visita_seleccionado"];
    NSString * instancia_compuesta = [NSString stringWithFormat:@"%d%@",instancia,orden];
    [visitas replaceObjectAtIndex:numero_visitadas withObject:instancia_compuesta];
    
    numero_visitadas++;
    NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
    
    [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
    [userDefaults setObject:[NSNumber numberWithInt:numero_visitadas] forKey:@"posicion_actual_instancias_visitadas"];
    [userDefaults synchronize];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    //NSString *dateString=[dateFormat stringFromDate:[NSDate date]];
    
    if (salida_sitio) {
        //Salida del sitio
        //[dao_hora ActualizaHora_Inicio:instancia ID_OPERACION:4 HORA_INICIO:dateString HORA_INICIO_ENVIADA:1];
    }
    //Salida de la instancia
    //[dao_hora ActualizaHora_Inicio:instancia ID_OPERACION:3 HORA_INICIO:dateString HORA_INICIO_ENVIADA:1];

    
    NSArray *array = [self.navigationController viewControllers];
    [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];
    
    
}


-(void)continuar{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    int numero_visitadas= (UInt32)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_visitadas"] integerValue];
    NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    
    NSString * orden_visita_instancia = [[NSUserDefaults standardUserDefaults] objectForKey:@"orden_visita_seleccionado"];
    NSString * instancia_compuesta = [NSString stringWithFormat:@"%d%@",instancia,orden_visita_instancia];
    [visitas replaceObjectAtIndex:numero_visitadas withObject:instancia_compuesta];
    
    numero_visitadas++;
    NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
    
    //NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];//orden de visita de la instancia
    
    [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
    [userDefaults setObject:[NSNumber numberWithInt:numero_visitadas] forKey:@"posicion_actual_instancias_visitadas"];
    [userDefaults synchronize];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    NSString *dateString=[dateFormat stringFromDate:[NSDate date]];
    
    if (salida_sitio) {
        //Salida del sitio
        if (ReachableViaWiFi) {
            horaenviada = 1;
        }
        else{horaenviada = 0;}
        
        NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
        diccionario_informacion = [data_manager_progreso SELECT_datos_progreso:instancia];
        
        NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];
        
        NSMutableArray * arreglo_instancias_sitio = [data_manager_progreso SELECT_instancias_sitio:[id_sitio integerValue]];
        for (int j = 0; j<[arreglo_instancias_sitio count]; j++) {
            
            NSString * instancia_sitio = [arreglo_instancias_sitio objectAtIndex:j];
            NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
            diccionario_informacion = [data_manager_progreso SELECT_datos_progreso:(uint32_t)[instancia_sitio integerValue]];
            
            NSString * id_orden_servicio = [diccionario_informacion objectForKey:@"id_orden_servicio"];
            NSString * orden = [diccionario_informacion objectForKey:@"orden"];
            NSString * orden_visita = [diccionario_informacion objectForKey:@"orden_visita"];
            //NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];
            
            [data_manager_progreso mandar_hora:(uint32_t)[id_orden_servicio integerValue] orden:(uint32_t)[orden integerValue] orden_visita:(uint32_t)[orden_visita integerValue] id_instancia:(uint32_t)[instancia_sitio integerValue] id_operacion:4];
        }
        
        [dao_hora ActualizaHora_Inicio:instancia ID_OPERACION:4 HORA_INICIO:dateString HORA_INICIO_ENVIADA:horaenviada id_sitio:[id_sitio integerValue] orden_visita:[orden_visita_instancia integerValue]];
        
    }
    //Salida de la instancia
    NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
    diccionario_informacion = [data_manager_progreso SELECT_datos_progreso:instancia];
    
    NSString * id_orden_servicio = [diccionario_informacion objectForKey:@"id_orden_servicio"];
    NSString * orden = [diccionario_informacion objectForKey:@"orden"];
    NSString * orden_visita = [diccionario_informacion objectForKey:@"orden_visita"];
    NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];

    [data_manager_progreso mandar_hora:(uint32_t)[id_orden_servicio integerValue] orden:(uint32_t)[orden integerValue] orden_visita:(uint32_t)[orden_visita integerValue] id_instancia:instancia id_operacion:3];
    [dao_hora ActualizaHora_Inicio:instancia ID_OPERACION:3 HORA_INICIO:dateString HORA_INICIO_ENVIADA:horaenviada id_sitio:[id_sitio integerValue] orden_visita:[orden_visita_instancia integerValue]];
    [dao_hora update_inicio_instancia:instancia];
    
    NSString * pendientes_string = [NSString stringWithFormat:@"%lu",(long)pendientes];
    [userDefaults setObject:pendientes_string forKey:@"imagenes_pendientes"];
    [userDefaults synchronize];

    
    NSArray *array = [self.navigationController viewControllers];
    [self.navigationController popToViewController:[array objectAtIndex:1] animated:YES];

}


//TERMINO DE TOMARSE LA FOTO*************************************
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    pendientes = pendientes + 1;
    UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    CGSize size= CGSizeMake(320.0,200.0);//RESOLUCION DE LA IMAGEN
    UIImage *newimage =[self imageWithImage:image scaledTOSize:size];
    [self dismissViewControllerAnimated:YES completion:NULL];
    [self.imageView setImage:newimage];
    
    
    //OBTENER FECHA Y HORA
    NSString *extension = @".bmp";//Extension del archivo
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    NSString *Filename = [resultString stringByAppendingString:extension];
    
    //Convertir imagen a texto
    NSString *encodedimage = [self imageToNSString:newimage];
    
    //Path de imagen
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",Filename]];
    NSData *imageData = UIImagePNGRepresentation(newimage);
    [imageData writeToFile:path atomically:YES];
    
    [self UploadImage:path :Filename];//Subir imagen al servidor
    
    
    //IS ONLINE?
    //if (reach.isReachable == NO){
        NSLog(@"Saving to BD");
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *orden_servicio =[prefs objectForKey:@"nIdOrdenServicio"];
        
       // NSArray *elementos_camara = [[NSUserDefaults standardUserDefaults] objectForKey:@"elementos_camara"];
    
    NSString *urlfoto = Filename;
        int orden_visita =(UInt32)[[monederoycontadores obtenerorden_visita:instancia]integerValue];
        
        [dao InsertarFotos_visita:(UInt32)[orden_servicio integerValue] ID_INSTANCIA:instancia ORDEN_VISITA:orden_visita ID_FOTO:Filename URL:urlfoto];
        [dao Insertar:encodedimage PATH:path ID_IMAGEN:Filename ID_INSTANCIA:[@(instancia) stringValue] ID_ORDEN_SERVICIO:orden_servicio ORDEN_VISITA:[@(orden_visita)stringValue]];
        
        //pendientes = pendientes + 1;
       
   // }
    
    
    [self continuar];
}

//UPLOAD TO SERVER************************************************************************

//CONFIGURAR A LA DIRECCION QUE SE VAN A SUBIR

- (void)_setupManager
{
    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"ftp://173.192.80.226"
                                                                  user:@"ftpImgBodies"
                                                              password:@"Ads720510."];
    self.requestsManager.delegate = self;
}

/*- (void)_setupManager
{
    NSArray *elementos_camara = [[NSUserDefaults standardUserDefaults] objectForKey:@"elementos_camara"];
    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"ftp://173.192.80.226"//[elementos_camara objectAtIndex:0]
                                                                  user:@"ftpImgBodies"//[elementos_camara objectAtIndex:1]
                                                              password:@"Ads720510."];//[elementos_camara objectAtIndex:2]];
    self.requestsManager.delegate = self;
}
*/

//METODO GENERAL PARA SUBIR UNA IMAGEN
-(void)UploadImage:(NSString *)BDPath :(NSString*)Filename{
    [self _setupManager];
    NSLog(@"Nombre archivo %@",Filename);
    [self.requestsManager addRequestForUploadFileAtLocalPath:BDPath toRemotePath:Filename];
    [self.requestsManager startProcessingRequests];
    NSLog(@"Termino upload");
}


#pragma Metodosdeconversion
//STRING TO IMAGE------------------------------------------------------------------------
- (UIImage *)stringToUIImage:(NSString *)string
{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:string options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

//IMAGE TO STRING
- (NSString *)imageToNSString:(UIImage *)image
{
    NSData *data = UIImagePNGRepresentation(image);
    
    return [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
}

//CONVERTIR A BASE 64-------------------------------------------------------------------
- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

//TERMINAN METODOS DE CONVERSION*******************************************************


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)action_back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}



@end
