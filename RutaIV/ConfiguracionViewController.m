//
//  ConfiguracionViewController.m
//  RutaIV
//
//  Created by Miguel Banderas on 05/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "ConfiguracionViewController.h"
#import "TablaConfigViewController.h"
#import "OBJ_Log_Proceso.h"

@interface ConfiguracionViewController ()

@end

@implementation ConfiguracionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) button_setup{
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView setFrame:CGRectMake(0, 50, 320, 30)];
    UIBarButtonItem *flexibleSpace =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Siguiente"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked:)];
    UIBarButtonItem* lelbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    UIBarButtonItem* lolbutton = [[UIBarButtonItem alloc] initWithTitle:@"               "
                                                                  style:UIBarButtonItemStyleBordered target:self
                                                                 action:@selector(doneClicked:)];
    [doneButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:@"Arial Bold" size:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:flexibleSpace,lelbutton,doneButton,lolbutton,flexibleSpace,nil]];
    //keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:209.0/255.0f green:214.0/255.0f blue:219.0/255.0f alpha:1.0];
    keyboardDoneButtonView.barTintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
}

- (IBAction)doneClicked:(id)sender
{
    UILabel * next = [array_text_fields objectAtIndex:current];
    [next becomeFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == txtUsuario) {
        current = 1;
    }
    if (textField == txtPassword) {
        current = 0;
    }
}


- (IBAction)btnGuardar:(id)sender {
    
    NSString *urlString = [NSString stringWithFormat:@"http://173.192.80.226/WCFMobileVending/Service1.svc/json/validaUsuario?sUsuario=%@&sPassword=%@&sUUID=%@&sSucursal=%@",txtUsuario.text,txtPassword.text,lblUUID.text,txtSucursal.text];
    
    NSLog(@"%@",urlString);
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers error:&error];
        NSMutableArray *array = [json objectForKey:@"validaLoginResult"];
        
        for (int i=0; i<array.count; i++) {
            NSDictionary *validaDatos = [array objectAtIndex:i];
            NSString *sUser = [[validaDatos objectForKey:@"sUsuario"] stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if (![sUser isEqual: @"NO"]) {
                
                sWebService = [[validaDatos objectForKey:@"sWebService"] stringByReplacingOccurrencesOfString:@"%\%" withString:@""];
                
                NSLog(@"%@",sWebService);
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:txtUsuario.text forKey:@"sUsuario"];
                [prefs setObject:txtPassword.text forKey:@"sContrasena"];
                [prefs setObject:txtSucursal.text forKey:@"sSucursal"];
                [prefs setObject:sWebService forKey:@"sWebService"];
                [prefs setObject:sUUID forKey:@"sUUID"];
                [prefs synchronize];
                
                UIAlertController * alert_exito=   [UIAlertController
                                                    alertControllerWithTitle:@"Atención"
                                                    message:@"Datos guardados correctamente."
                                                    preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* ok = [UIAlertAction
                                     actionWithTitle:@"Ok"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //se le avisa a el view principal que es necesario cambiar la sucursal.
                                         NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                         cambio_realizado = @"1";
                                         [userDefaults setObject:cambio_realizado forKey:@"configuracion_cambio_sesion"];
                                         [userDefaults synchronize];
                                         [self.navigationController popToRootViewControllerAnimated:true];
                                     }];
                [alert_exito addAction:ok];
                dispatch_async(dispatch_get_main_queue(), ^ {
                    [self presentViewController:alert_exito animated:YES completion:nil];
                });
                
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Usuario y/o password incorrectos" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No se estableció la conexión" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


- (NSString *)createNewUUID {
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef str = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    NSString *strUUID = (__bridge NSString *)str;
    return strUUID;
}

- (void) ObtenerDatos
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    txtUsuario.text = [prefs stringForKey:@"sUsuario"];
    txtPassword.text = [prefs stringForKey:@"sContrasena"];
    txtSucursal.text = [prefs stringForKey:@"sSucursal"];
    sWebService = [prefs stringForKey:@"sWebService"];
    sUUID = [prefs stringForKey:@"sUUID"];
    if (sUUID ==NULL) {
        sUUID = [self createNewUUID];
    }
    
    lblUUID.text = sUUID;
}

- (BOOL) validarDatos
{
    return true;
}

- (IBAction)back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self ObtenerDatos];
    
    [self button_setup];
    array_text_fields = [[NSMutableArray alloc]init];
    [array_text_fields addObject:txtUsuario];
    [array_text_fields addObject:txtPassword];
    
    txtUsuario.inputAccessoryView = keyboardDoneButtonView;
    txtPassword.inputAccessoryView = keyboardDoneButtonView;
    [txtUsuario becomeFirstResponder];
    
    //Revisar si se ha escrito en el archivo antes
    OBJ_Log_Proceso * guardar_archivo = [[OBJ_Log_Proceso alloc]init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * log_archivo = [prefs stringForKey:@"Log_Archivo"];
    if (![log_archivo isEqualToString:@"1"]) {
        [guardar_archivo escribir_titulo:@"INICIO"];
        
        NSString * iniciado = @"1";
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:iniciado forKey:@"Log_Archivo"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
