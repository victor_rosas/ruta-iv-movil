//
//  TC_Cambio_Planograma.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Cambio_Planograma : UITableViewCell
{
    __weak IBOutlet UILabel *label_articulo;
    __weak IBOutlet UILabel *label_espiral;
    __weak IBOutlet UILabel *label_capacidad;
}
-(void)cell_setup :(NSString *)articulo espiral:(NSString *)id_espiral cantidad:(NSString *)field_cantidad capacidad:(NSString *)capacidad_espiral;
@property (weak, nonatomic) IBOutlet UITextField *text_field_cantidad;

@end
