//
//  TableViewCell.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 15/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCell : UITableViewCell
{
    NSString *stringVariable;
    __weak IBOutlet UILabel *label_espiral;
}

@property (weak,nonatomic) IBOutlet UILabel *myLabel;
@property (weak,nonatomic) IBOutlet UITextField *myTextbox;
@property (weak,nonatomic) IBOutlet UILabel *Espiral;
@property (weak, nonatomic) IBOutlet UILabel *label_costo;
@property (weak, nonatomic) IBOutlet UILabel *label_capacidad;
@property (weak, nonatomic) IBOutlet UILabel *label_inv_ini;
@property (weak, nonatomic) IBOutlet UILabel *label_cantidad_pasada;

@end
