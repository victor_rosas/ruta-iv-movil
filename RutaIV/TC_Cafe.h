//
//  TC_Cafe.h
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Cafe : UITableViewCell
{
    __weak IBOutlet UILabel *label_numeracion;
    __weak IBOutlet UILabel *label_articulo;
    __weak IBOutlet UILabel *label_capacidad;
    __weak IBOutlet UILabel *label_costo;
}

@property (weak, nonatomic) IBOutlet UITextField *text_field_cantidad;
-(void)cell_setup :(NSString *)articulo numeracion:(NSString *)id_numeracion capacidad:(NSString *)capacidad_espiral costo:(NSString *)numero_costo;

@end
