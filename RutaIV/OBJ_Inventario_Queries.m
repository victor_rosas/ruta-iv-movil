//
//  OBJ_Inventario_Queries.m
//  RutaIV
//
//  Created by Miguel Banderas on 11/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Inventario_Queries.h"
#import "AppDelegate.h"
#import "OBJ_Inventario_Producto.h"

@implementation OBJ_Inventario_Queries

-(NSString *) obtenerRutaBD{
    logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(NSMutableArray *)cargar_inventario_cero{
    [logProceso escribir:@"#OBJ_Inventario_Queries entro cargar_localizacion"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray *inventario = [[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT DISTINCT EX.ID_ARTICULO, MOV.ARTICULO, MOV.CATEGORIA FROM EXISTENCIAS EX INNER JOIN MOVIMIENTOS_INSTANCIA MOV ON EX.ID_ARTICULO = MOV.ID_ARTICULO  WHERE EX.EXISTENCIA < 1"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString *articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *tipo_articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                OBJ_Inventario_Producto *obj_producto = [[OBJ_Inventario_Producto alloc]init:articulo cantidad:0 tipo:tipo_articulo];
                //[obj_producto init:articulo cantidad:0 tipo:tipo_articulo];
                [inventario addObject:obj_producto];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#OBJ_Inventario_Queries salio cargar_localizacion"];
    return inventario;
}

-(BOOL)borrar_surtido{
    [logProceso escribir:@"#OBJ_Inventario_Queries entro borrar_surtido"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM SURTIDO"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
            [logProceso escribir:[NSString stringWithFormat:@"#OBJ_Inventario_Queries #cargar_localizacion Problema al preparar el statement. %s",sqlite3_errmsg(dataBase)]];
        }
    }
    [logProceso escribir:@"#OBJ_Inventario_Queries salio borrar_surtido"];
    return  bValida;
}

-(BOOL)insert_surtido{
    BOOL bValida = FALSE;
    [logProceso escribir:@"#OBJ_Inventario_Queries entro insert_surtido"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"Insert Into SURTIDO(ID_ARTICULO,SURTIDO,ARTICULO, TIPO_ARTICULO) Select Id_Articulo,Sum(Surtio-Removio) Surtio,ARTICULO, CATEGORIA From MOVIMIENTOS_INSTANCIA Group By Id_Articulo,Articulo"];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#OBJ_Inventario_Queries #insert_surtido Error en la creacio del insert"];
        }
    }else{
        [logProceso escribir:@"#OBJ_Inventario_Queries #insert_surtido No se ha podido abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#OBJ_Inventario_Queries salio insert_surtido"];
    return bValida;
}

-(NSMutableArray *)inventario_actual{
    [logProceso escribir:@"#OBJ_Inventario_Queries entro inventario_actual"];
    NSMutableArray *productos = [[NSMutableArray alloc]init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"Select E.ID_ARTICULO,S.ARTICULO,E.EXISTENCIA - S.SURTIDO Surtio, S.TIPO_ARTICULO From EXISTENCIAS E Inner Join SURTIDO S On E.ID_ARTICULO = S.ID_ARTICULO Order By S.ARTICULO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString *articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *surtio = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
                NSString *tipo_articulo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
                OBJ_Inventario_Producto *obj_producto = [[OBJ_Inventario_Producto alloc]init:articulo cantidad:(uint32_t)[surtio integerValue] tipo:tipo_articulo];
                //[obj_producto init:articulo :[surtio integerValue]];
                //[obj_producto init:articulo cantidad:(uint32_t)[surtio integerValue] tipo:tipo_articulo];
        
                [productos addObject:obj_producto];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#OBJ_Inventario_Queries salio inventario_actual"];
    return productos;
}

-(NSMutableArray *)inventario_inicial{
    [logProceso escribir:@"#OBJ_Inventario_Queries entro inventario_inicial"];
    NSMutableArray *productos = [[NSMutableArray alloc]init];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"Select E.ID_ARTICULO,S.ARTICULO,E.EXISTENCIA, S.TIPO_ARTICULO From EXISTENCIAS E Inner Join SURTIDO S On E.ID_ARTICULO = S.ID_ARTICULO Order By S.ARTICULO"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSString *articulo = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *existencias = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
                NSString *tipo_articulo = [NSString stringWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
                OBJ_Inventario_Producto *obj_producto = [[OBJ_Inventario_Producto alloc]init:articulo cantidad:(uint32_t)[existencias integerValue] tipo:tipo_articulo];
                //[obj_producto init:articulo :[existencias integerValue]];
                //[obj_producto init:articulo cantidad:(uint32_t)[existencias integerValue] tipo:tipo_articulo];
                
                [productos addObject:obj_producto];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#OBJ_Inventario_Queries salio inventario_inicial"];
    return productos;
}

@end
