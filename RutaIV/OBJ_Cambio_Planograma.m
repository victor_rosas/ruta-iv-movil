//
//  OBJ_Cambio_Planograma.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Cambio_Planograma.h"

@implementation OBJ_Cambio_Planograma
@synthesize id_articulo;
@synthesize surtio;
@synthesize removio;
@synthesize clave;
@synthesize orden;
@synthesize id_orden_servicio;
@synthesize id_espiral;
@synthesize id_charola;
@synthesize capacidad;
@synthesize id_planograma;
@synthesize articulo;
@synthesize espiral;
@synthesize cambio;
@synthesize id_instancia;
@synthesize cantidad_removida;

-(id)init :(NSString *)articulo_id surtio:(NSString *)cantidad_surtio removio:(NSString *)cantidad_removio clave:(NSString *)numero_clave orden:(NSString *)numero_orden id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)espiral_id id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad id_planograma:(NSString *)planograma articulo:(NSString *)nombre_articulo espiral:(NSString *)nombre_espiral cambio:(NSString *)bool_cambio id_instancia:(NSString *)instancia
{
    self.id_articulo = articulo_id;
    self.surtio = cantidad_surtio;
    self.removio = cantidad_removio;
    self.clave = numero_clave;
    self.orden = numero_orden;
    self.id_orden_servicio = orden_servicio;
    self.id_espiral = espiral_id;
    self.id_charola = charola;
    self.capacidad = numero_capacidad;
    self.id_planograma = planograma;
    self.articulo = nombre_articulo;
    self.espiral = nombre_espiral;
    self.cambio = bool_cambio;
    self.id_instancia = instancia;
    self.cantidad_removida = @"";
    
    return self;
}

@end
