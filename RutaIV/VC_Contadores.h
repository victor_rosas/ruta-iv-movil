//
//  VC_Contadores.h
//  RutaIV
//
//  Created by Miguel Banderas on 11/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data_Cafe.h"

@interface VC_Contadores : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    Data_Cafe * data_cafe_manager;
    NSMutableArray * arreglo_articulos_cafe;
    int contador_rellenado;
    int cantidad_vasos;
    BOOL cantidad_vasos_superada;
    BOOL ventas_incongruentes;
    BOOL productos_orden;
    BOOL alerta_presente;
    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_orden;
    UIAlertController * alerta_ventas;
    UIAlertController * alerta_vasos;
    
    UIToolbar* keyboardDoneButtonView;
    __weak IBOutlet UIBarButtonItem *btn_siguiente;
    __weak IBOutlet UITableView *myTableView;
}
@property int id_instancia;
@property BOOL salida_sitio;
- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
