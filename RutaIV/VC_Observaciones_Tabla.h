//
//  VC_Observaciones_Tabla.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DATA_Observaciones.h"
#import "OBJ_Observaciones.h"

@interface VC_Observaciones_Tabla : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    DATA_Observaciones * data_observaciones_manager;
    NSIndexPath * current_indexPath;
    NSMutableArray * array_mutable_observaciones;
    NSMutableDictionary * diccionario_observaciones_completo;
    __weak IBOutlet UITableView *Tableview;
}

- (IBAction)action_back:(id)sender;

@end
