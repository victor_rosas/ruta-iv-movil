//
//  TVC_Sucursal.m
//  RutaIV
//
//  Created by Miguel Banderas on 12/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TVC_Sucursal.h"
#import "ViewController.h"

@interface TVC_Sucursal ()

@property (strong,nonatomic) NSArray *array, *arreglosId;
@property (strong,nonatomic) NSMutableArray *arregloNuevo, *arregloId;
@property (strong,nonatomic) NSString *nIdRuta,*nIdSucursal,*sWebService;
@property (strong,nonatomic) NSString *nIdPosPicker;

@end

@implementation TVC_Sucursal

- (void)viewDidLoad {
    [super viewDidLoad];
    arreglo_sucursales = [[NSMutableArray alloc]init];
    arreglo_rutas = [[NSMutableArray alloc]init];
    NSMutableArray * arreglo_nuevo = [[NSMutableArray alloc]init];
    NSString * nuevo = @"Seleccionar opcion";
    [arreglo_nuevo addObject:nuevo];
    [self crear_arreglo_sucursales];
    
    _arregloNuevo = [[NSMutableArray alloc]init];
    _arregloId = [[NSMutableArray alloc]init];
    
    self.arreglosId = [[NSArray alloc] init];
    self.array = [[NSArray alloc]init];
    
    [self ObtenerDatos];
    
    
    if (_nIdSucursal == NULL) {
        NSArray *data = [[NSArray alloc] initWithObjects: @"Seleccione una sucursal",nil];
        
        self.array = data;
    }
    else{
        _txtSucursal.text = _nIdSucursal;
        [self recargarPicker];
        self.array = arreglo_nuevo;
        //[_pRuta selectRow:[_nIdPosPicker integerValue] inComponent:0 animated:YES];
        txtRuta.text = [arreglo_rutas objectAtIndex:[_nIdPosPicker integerValue]];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString * cambio_realizado = @"0";
    [userDefaults setObject:cambio_realizado forKey:@"configuracion_cambio_sesion"];
    [userDefaults synchronize];
    
    if ([_txtSucursal.text isEqual:@"Sucursal"]) {
        button_guardar.enabled = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    ViewController *vPrincipal = (ViewController *)segue.destinationViewController;
    
    vPrincipal.lblRuta.text = [prefs stringForKey:@"sNombreRuta"];
}


#pragma mark Picker Data Source Methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [_array count];
}

#pragma mark Picker Delegate Methods
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [_array objectAtIndex:row];
}

- (IBAction)btnGuardar:(id)sender {
    NSUInteger numComponents = [[self.pRuta dataSource] numberOfComponentsInPickerView:self.pRuta];
    NSUInteger selectedRow = 0;
    
    if ([self.txtSucursal.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Debe de colocar una sucursal" delegate:nil cancelButtonTitle:@"Aceptar@" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    for(NSUInteger i = 0; i < numComponents; ++i) {
        selectedRow = [self.pRuta selectedRowInComponent:i];
    }
    
    //NSString *sNombreRuta = [_array objectAtIndex:selectedRow];
    NSString *sNombreRuta = txtRuta.text;
    NSLog(@"%@",sNombreRuta);
    _nIdPosPicker = [NSString stringWithFormat:@"%lul", (unsigned long)selectedRow];
    //NSString *sIdRuta = [self.arreglosId objectAtIndex:selectedRow];
    
    int posicion_objeto =(uint32_t)[arreglo_rutas indexOfObject:txtRuta.text];
    NSString *sIdRuta = [self.arregloId objectAtIndex:posicion_objeto];
    NSString *sIdSucursal = self.txtSucursal.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:sIdRuta forKey:@"nIdRuta"];
    [prefs setObject:sIdSucursal forKey:@"nIdSucursal"];
    [prefs setObject:_nIdPosPicker forKey:@"nIdPosPicker"];
    [prefs setObject:sNombreRuta forKey:@"sNombreRuta"];
    [prefs synchronize];
    
    ViewController *vPrincipal;
    
    vPrincipal.lblRuta.text = sNombreRuta;
    
    
    UIAlertController * alert_exito=   [UIAlertController
                                        alertControllerWithTitle:@"Atención"
                                        message:@"Datos guardados correctamente."
                                        preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self.navigationController popToRootViewControllerAnimated:true];
                         }];
    [alert_exito addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^ {
        [self presentViewController:alert_exito animated:YES completion:nil];
    });
    
    
}

- (IBAction)back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void) recargarPicker
{
    NSString *urlString = [NSString stringWithFormat:@"%@traeRuta?nIdSucursal=%@",_sWebService,_txtSucursal.text];
    
    NSURL *jsonURL = [NSURL URLWithString:urlString];
    NSError *error = nil;
    
    NSData *data = [NSData dataWithContentsOfURL:jsonURL options:NSDataReadingUncached error:&error];
    
    if (!error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers error:&error];
        NSMutableArray *array = [json objectForKey:@"traeRutasResult"];
        
        _arregloNuevo = [[NSMutableArray alloc] initWithCapacity:array.count];
        
        if (array.count == 0) {
            NSString *valor = @"No se encontraron rutas";
            [_arregloNuevo addObject:valor];
            
            self.array = _arregloNuevo;
            
            [self.pRuta reloadAllComponents];
            button_guardar.enabled = false;
        }
        else{
            for (int i=0; i<array.count; i++) {
                NSDictionary *validaDatos = [array objectAtIndex:i];
                NSString *sNombreRuta = [validaDatos objectForKey:@"sNombreRuta"];
                NSString *nIdRuta = [validaDatos objectForKey:@"nRuta"];
                
                [_arregloNuevo addObject:sNombreRuta];
                [_arregloId addObject:nIdRuta];
            }
            button_guardar.enabled = true;
        }
        
        self.array = _arregloNuevo;
        arreglo_rutas = _arregloNuevo;
        self.arreglosId = _arregloId;
        
        [self.pRuta reloadAllComponents];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No se estableció la conexión" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        button_guardar.enabled = false;
    }
}

- (void) ObtenerDatos
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    _sWebService = [prefs stringForKey:@"sWebService"];
    _nIdRuta = [prefs stringForKey:@"nIdRuta"];
    _nIdSucursal = [prefs stringForKey:@"nIdSucursal"];
    _nIdPosPicker = [prefs stringForKey:@"nIdPosPicker"];
}

-(void)crear_arreglo_sucursales
{
    for (int i = 1; i<10; i++) {
        NSString * sucursal = [NSString stringWithFormat:@"%d",i];
        [arreglo_sucursales addObject:sucursal];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int seleccion = 0;
    if (indexPath.row == 1) {
        [self recargarPicker];
        if ([self.array containsObject:txtRuta.text]) {
            seleccion = (uint32_t)[self.array indexOfObject:txtRuta.text];
        }
        else
        {
            txtRuta.text = [self.array objectAtIndex:0];
        }
        [self.pRuta selectRow:seleccion inComponent:0 animated:YES];
    }
    else
    {
        self.array = arreglo_sucursales;
        [self.pRuta reloadAllComponents];
        if ([arreglo_sucursales containsObject:self.txtSucursal.text]) {
            seleccion =(uint32_t) [arreglo_sucursales indexOfObject:self.txtSucursal.text];
        }
        else
        {
            self.txtSucursal.text = [arreglo_sucursales objectAtIndex:0];
        }
        [self.pRuta selectRow:seleccion inComponent:0 animated:YES];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSIndexPath * opcion_seleccionada = [self.tableView indexPathForSelectedRow];
    if (opcion_seleccionada.row == 0) {
        self.txtSucursal.text = [arreglo_sucursales objectAtIndex:row];
        txtRuta.text = @"";
        button_guardar.enabled = false;
    }
    else
    {
        txtRuta.text = [self.array objectAtIndex:row];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:TRUE];
}



@end
