//
//  CabeceroWS.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CabeceroWS : NSObject{
    
    
}


-(NSMutableArray *)borraCadenaGprs: (int)vlOrdenServicio boolean:(BOOL)boolean;
-(NSMutableArray *)insertaCadenaGprs: (NSString*)sCadenaCM;
-(NSMutableArray*)actualizaCabOrdenServicio:(NSString *)sCadenaHSE;
-(NSMutableArray *)interpretaGprs:(NSString *)sCadenaGprs;
-(NSMutableArray *)interpretaDumDex: (int)vlOrdenServicio boolean:(BOOL)boolean;


@end
