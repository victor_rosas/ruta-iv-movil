//
//  PruebaTableViewController.m
//  RutaIV
//
//  Created by Miguel Banderas on 19/08/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "PruebaTableViewController.h"
#import "SWTableViewCell.h"
#import "spGenerales.h"
#import "UMTableViewCell.h"
#import "UMPruebaTableViewCell.h"
#import "NextViewController.h"
#import "Hora_inicioDAO.h"

@interface PruebaTableViewController (){
    NSDictionary *animals;
    NSArray *animalSectionTitles;
    NSArray *_sections;
    NSMutableArray *_testArray;
    NSMutableDictionary *_SitiosAsignados;
    NSString *nInstancia, *nSitio;
    NSInteger *instancia_segue;
}

@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;


@end

@implementation PruebaTableViewController

@synthesize tableView2;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    spGenerales *generales = [spGenerales alloc];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [generales sp_RevisaFinInstancias:[[prefs stringForKey:@"nIdRuta"] intValue]];
    animals = [generales sp_revisaSitioPorRuta:[[prefs stringForKey:@"nIdRuta"] intValue] wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"]];
    NSLog(@"Sitios asignados = %lu",(unsigned long)animals.count);
    animalSectionTitles = [[animals allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    self.tableView.rowHeight = 40;
    self.navigationItem.title = @"Ruta";
    
    dao = [[Hora_inicioDAO alloc]init];
}

-(void)viewWillAppear:(BOOL)animated{
    arreglo_visitadas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    primer_blanco = true;
    [tableView2 reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getImageFilename:(NSString *)animal
{
    NSString *imageFilename = [[animal lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    imageFilename = [imageFilename stringByAppendingString:@".jpg"];
    
    return imageFilename;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [animalSectionTitles count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    spGenerales *generales = [spGenerales alloc];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:section];
    NSString *sections = [generales traeSitio:sectionTitle];
    
    return sections;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    view.tintColor = [UIColor colorWithRed:237.0/255.0f green:183.0/255.0f blue:57.0/255.0f alpha:0.6];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    return [sectionAnimals count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UMPruebaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    spGenerales *generales = [spGenerales alloc];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    // Configure the cell...
    [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:32.0f];
    //[cell setRightUtilityButtons:[self rightButtons] WithButtonWidth:58.0f];
    cell.delegate = self;
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    //NSString *sResult = [generales sp_revisaSitioPorRuta:[[prefs stringForKey:@"nIdRuta"] intValue] wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"]];
    NSMutableArray *busqueda_instancias = [generales traeInstancia:[[prefs stringForKey:@"nIdRuta"] intValue]  wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"] wIdOrden:sectionTitle wIdInstancia:animal];
    
    NSString *sResult = [busqueda_instancias objectAtIndex:0];
    NSString *tipo_maquina = [self tipoMaquina:[busqueda_instancias objectAtIndex:1]];
    
    //Separacion de sResult para mostrar informacion correctamente
    /*NSString *character;
    int contador = 0;
    while (![character  isEqual: @"-"]) {
        character = [sResult substringWithRange:NSMakeRange(contador, contador+1)];
        contador++;
        NSLog(character);
        NSLog(@"%d", contador);
    }*/
    
    NSArray *Array = [sResult componentsSeparatedByString:@"-"];
    NSString *sitio = [Array objectAtIndex:0];
    NSString *sResult2 = [Array objectAtIndex:1];
    Array = [sResult2 componentsSeparatedByString:@"("];
    //NSString *maquina = [Array objectAtIndex:0];
    NSString *IS0 = [Array objectAtIndex:1];
    IS0 = [IS0 substringToIndex:[IS0 length] - 2];
    BOOL pintar_blanco = true;
    
    NSString *instancia_visitada;
    for (int i=0; i<20; i++) {
         instancia_visitada = [[arreglo_visitadas objectAtIndex:i] stringValue];
         if ([instancia_visitada isEqualToString:animal]) {
             cell.backgroundColor = [UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6];
             cell.label_continuar.text = @"1";
             pintar_blanco = false;
             break;
         }
         else{
             pintar_blanco = true;
         }
    }
    if (pintar_blanco) {
        cell.backgroundColor = [UIColor whiteColor];
        if (primer_blanco) {
            cell.label_continuar.text = @"1";
            primer_blanco = false;
            activa = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        }
        else
        {
            cell.label_continuar.text = @"1";
        }
        
        if ([indexPath isEqual:activa]) {
            cell.label_continuar.text = @"1";
        }
    }
    
    
    cell.label_sitio.text = sitio;
    cell.label_codigo.text = IS0;
    cell.Label.text = tipo_maquina;
    cell.imageView.image = [UIImage imageNamed:[self getImageFilename:animal]];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    return cell;
}

-(NSString *)tipoMaquina:(NSString*)codigo_maquina{
    int id_maquina = [codigo_maquina integerValue];
    switch (id_maquina) {
        case 3:
            return @"- Snack";
            break;
        case 4:
            return @"- Combo";
            break;
        case 13:
            return @"- Café";
        default:
            return [NSString stringWithFormat:@"%d", id_maquina];
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    /*[leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
     icon:[UIImage imageNamed:@"check.png"]];
     [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:1.0]
     icon:[UIImage imageNamed:@"clock.png"]];
     [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188f alpha:1.0]
     icon:[UIImage imageNamed:@"cross.png"]];
     [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.55f green:0.27f blue:0.07f alpha:1.0]
     icon:[UIImage imageNamed:@"list.png"]];*/
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                               title:@"Sig."];
    
    return leftUtilityButtons;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Set background color of cell here if you don't want default white
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UMPruebaTableViewCell *cell = (UMPruebaTableViewCell *)[tableView2 cellForRowAtIndexPath:indexPath];
    if ([cell.label_continuar.text isEqualToString:@"0"]) {
        if([cell.backgroundColor isEqual:[UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6]])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Atención"
                                          message:@"Esta instancia ya fue visitada."
                                          preferredStyle:UIAlertActionStyleCancel];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [tableView2 deselectRowAtIndexPath:indexPath animated:YES];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Atención"
                                          message:@"Visitar las instancias en orden."
                                          preferredStyle:UIAlertActionStyleCancel];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [tableView2 deselectRowAtIndexPath:indexPath animated:YES];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else{
        NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
        NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
        NSInteger *instancia = [animal integerValue];
        instancia_segue = instancia;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString=[dateFormat stringFromDate:[NSDate date]];
        
        //NSString *algo = [NSString stringWithFormat:@"INSTANCIA: %@ SITIO:%@",animal,sectionTitle];
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sitio" message:algo delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //[alert show];
        
        if (indexPath.row == 0) {
            //Llegada al sitio, guardar hora de llegada a sitio
            //NSLog(@"%@", dateString);
            [dao ActualizaHora_Inicio:instancia ID_OPERACION:1 HORA_INICIO:dateString];
        }
        //Llegada a instancia
        //NSLog(@"%@", dateString);
        [dao ActualizaHora_Inicio:instancia ID_OPERACION:2 HORA_INICIO:dateString];
        
        if (indexPath.row == [tableView2 numberOfRowsInSection:indexPath.section] -1 ) {
            salida_sitio = true;
            //NSLog(@"true");
        }
        else
        {
            salida_sitio = false;
        }
        
        [self performSegueWithIdentifier:@"inventario" sender:self];
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{

    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    NSString *mensaje = [NSString stringWithFormat:@"Escriba el código correspondiente, instancia:%@",animal];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Codigo de autorización" message:mensaje delegate:self cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Aceptar", nil];
    
    
    //CAMBIAR A TECLADO NUMERICO
    UITextField *textField = [alert textFieldAtIndex:1];
    textField.keyboardType = UIKeyboardTypeNumberPad;

    nInstancia = animal;
    nSitio = sectionTitle;
    
    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alert textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [[alert textFieldAtIndex:0] becomeFirstResponder];

    switch (index) {
        case 0:
            [alert show];
            //cell.backgroundColor = [UIColor orangeColor];
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex]){
        //cancel clicked ...do your action
    }else{
        NSString *inputText = [[alertView textFieldAtIndex:0] text];
        NSLog(@"%@",inputText);
        [self.tableView reloadData];
    }
}

/*- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *inputText = [[alertView textFieldAtIndex:0] text];
    if( [inputText length] >= 10 )
    {
        return YES;
    }
    else
    {
        return NO;
    }
}*/

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            NSLog(@"More button was pressed");
            UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"More more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alertTest show];
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            // Delete button was pressed
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            [_testArray[cellIndexPath.section] removeObjectAtIndex:cellIndexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
     NextViewController *controller = (NextViewController *)segue.destinationViewController;
    NSLog(@"%d",instancia_segue);
    controller.instancia = instancia_segue;
    controller.salida_sitio = salida_sitio;
}


@end
