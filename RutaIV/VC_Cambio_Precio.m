//
//  VC_Cambio_Precio.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Cambio_Precio.h"
#import "OBJ_Cambio_Precio.h"
#import "TC_Cambio_Precio.h"
#import "NextViewController.h"
#import "VC_Cambio_Planograma.h"


@interface VC_Cambio_Precio ()

@end

@implementation VC_Cambio_Precio
@synthesize id_instancia;
@synthesize salida_sitio;

- (void)viewDidLoad {
    [super viewDidLoad];
    //Inicializacion de variables
    data_cambio_manager = [[DATA_Cambio alloc]init];
    arreglo_mutable_cambio_precio = [[NSMutableArray alloc]init];

    //Cargado de informacion
    arreglo_mutable_cambio_precio = [data_cambio_manager cargar_cambio_precio:id_instancia];
    
    //Alerta
    alert_cambio_precio=   [UIAlertController
                            alertControllerWithTitle:@"Atención"
                            message:@"Realizar los cambios de precio antes de continuar."
                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert_cambio_precio dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert_cambio_precio addAction:ok];
    [self presentViewController:alert_cambio_precio animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Aspecto de la tabla
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arreglo_mutable_cambio_precio count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TC_Cambio_Precio *cell = (TC_Cambio_Precio *)[myTableView dequeueReusableCellWithIdentifier:@"vc_cambio_precio" forIndexPath:indexPath];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    OBJ_Cambio_Precio * cambio_precio = [[OBJ_Cambio_Precio alloc]init];
    cambio_precio = [arreglo_mutable_cambio_precio objectAtIndex:indexPath.row];
    
    NSString * nombre_articulo = [data_cambio_manager cargar_nombre_articulo:(uint32_t)[cambio_precio.id_articulo integerValue]];
    NSString * nombre_espiral = [data_cambio_manager cargar_nombre_espiral:(uint32_t)[cambio_precio.id_espiral integerValue] charola:(uint32_t)[cambio_precio.id_charola integerValue] instancia:id_instancia];
    NSString * nombre_precio = [formatter stringFromNumber:[NSNumber numberWithFloat:[cambio_precio.actual integerValue]]];
    
    NSString *parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    
    if ([parametrizacion_mayusculas isEqualToString:@"0"]) {
        nombre_articulo = [nombre_articulo capitalizedString];
    }
    
    [cell cell_setup:nombre_articulo espiral:nombre_espiral precio:nombre_precio];
    
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NextViewController *controller = (NextViewController *)segue.destinationViewController;
    controller.instancia = id_instancia;
    controller.salida_sitio = salida_sitio;
}


- (IBAction)action_back:(id)sender {
   [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)action_next:(id)sender {
    NSMutableArray * arreglo_planograma = [data_cambio_manager cargar_cambio_planograma:id_instancia];
    if ([arreglo_planograma count] > 0) {
        VC_Cambio_Planograma * vc_planograma = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_cambio_planograma"];
        vc_planograma.id_instancia = id_instancia;
        vc_planograma.salida_ruta = salida_sitio;
        [self.navigationController pushViewController:vc_planograma animated:YES];
    }
    else
    {
        NextViewController * vc_inventario = [self.storyboard instantiateViewControllerWithIdentifier:@"inventario"];
        vc_inventario.instancia = id_instancia;
        vc_inventario.salida_sitio = salida_sitio;
        [self.navigationController pushViewController:vc_inventario animated:YES];
    }
}
@end
