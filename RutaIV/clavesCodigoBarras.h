//
//  clavesCodigoBarras.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface clavesCodigoBarras : NSObject{

    NSMutableArray *instancias;

}

-(BOOL)borraClavesCodBarras;

-(BOOL)insertaClavesCodBarras:(int)nIdOrdenServicio wOrdenVisita:(int)nOrdenVisita wIdInstancia:(int)nIdInstancia wClave:(int)nClave;

-(NSMutableArray *)cargarClavesCodBarras;

@end
