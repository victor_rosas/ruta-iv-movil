//
//  TVC_Inventario_Carga_Inicial.m
//  RutaIV
//
//  Created by Miguel Banderas on 12/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TVC_Inventario_Carga_Inicial.h"
#import "TC_Inventario.h"
#import "OBJ_Inventario_Queries.h"
#import "OBJ_Inventario_Producto.h"

@interface TVC_Inventario_Carga_Inicial ()

@end

@implementation TVC_Inventario_Carga_Inicial

- (void)viewDidLoad {
    [super viewDidLoad];
    metodos = [[OBJ_Inventario_Queries alloc]init];
    parametrizacion_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    [metodos borrar_surtido];
    [metodos insert_surtido];
    inventario = [metodos inventario_inicial];
    filtrado = [[NSMutableArray alloc]initWithCapacity:[inventario count]];
    
    dictionary_productos = [[NSMutableDictionary alloc]init];
    NSMutableArray *arreglo_productos = [[NSMutableArray alloc]init];
    
    for (int i = 0; [inventario count] > i; i++) {
        OBJ_Inventario_Producto *producto_temporal = [inventario objectAtIndex:i];
        arreglo_productos = [dictionary_productos objectForKey:producto_temporal.tipo_articulo];
        if (arreglo_productos == nil) {
            NSMutableArray *primero = [[NSMutableArray alloc]init];
            [primero addObject:producto_temporal];
            [dictionary_productos setObject:primero forKey:producto_temporal.tipo_articulo];
        }
        else
        {
            [arreglo_productos addObject:producto_temporal];
            [dictionary_productos setObject:arreglo_productos forKey:producto_temporal.tipo_articulo];
        }
    }
    headers = [[NSArray alloc]initWithArray:[dictionary_productos allKeys]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [filtrado count];
    } else {
        NSString *current_header = [headers objectAtIndex:section];
        NSMutableArray *current_arreglo = [[NSMutableArray alloc]init];
        current_arreglo = [dictionary_productos objectForKey:current_header];
        return [current_arreglo count];
    }
}

- (void) tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    // Background color
    view.tintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return 1;
    }
    else
    {
        return [headers count];
    }
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return @"";
    }
    else
    {
        NSString *current_header = [headers objectAtIndex:section];
        return current_header;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //TC_Inventario *cell = [self.tableView dequeueReusableCellWithIdentifier:@"vc_carga_actual" forIndexPath:indexPath];
    TC_Inventario *cell =
    [self.tableView dequeueReusableCellWithIdentifier:@"vc_carga_inicial"];
    OBJ_Inventario_Producto *producto;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        producto = [filtrado objectAtIndex:indexPath.row];
    }
    else
    {
        NSString *current_header = [headers objectAtIndex:indexPath.section];
        NSMutableArray *current_arreglo = [[NSMutableArray alloc]init];
        current_arreglo = [dictionary_productos objectForKey:current_header];
        producto = [current_arreglo objectAtIndex:indexPath.row];
    }
    
    if([parametrizacion_mayusculas isEqualToString:@"0"])
    {
        [cell cell_setup:[producto.producto capitalizedString] numero_cantidad:producto.cantidad];
    }
    else
    {
        [cell cell_setup:producto.producto numero_cantidad:producto.cantidad];
    }
    return cell;
}

#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    // Update the filtered array based on the search text and scope.
    // Remove all objects from the filtered search array
    [filtrado removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.producto CONTAINS[c] %@",searchText];
    filtrado = [NSMutableArray arrayWithArray:[inventario filteredArrayUsingPredicate:predicate]];
}

#pragma mark - UISearchDisplayController Delegate Methods
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}


- (IBAction)back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
@end
