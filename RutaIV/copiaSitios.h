//
//  copiaSitios.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface copiaSitios : NSObject

-(BOOL)copiaSitios:(int)nIdCliente wIdSitio:(int)nIdSitio wNombreCorto:(NSString *)sNombreCorto wIdLista:(int)nIdLista;
-(BOOL)borrarSitios;

@end
