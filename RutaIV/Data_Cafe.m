//
//  Data_Cafe.m
//  RutaIV
//
//  Created by Miguel Banderas on 10/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "Data_Cafe.h"
#import "AppDelegate.h"
#import "OBJ_Cafe.h"

@implementation Data_Cafe


-(NSString *) obtenerRutaBD
{
    logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}


-(NSMutableArray *)select_articulos:(NSInteger)id_instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro select_articulos"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_INSTANCIA, ID_ARTICULO, SURTIO, REMOVIO, CADUCARON, CLAVE, ORDEN_VISITA, ID_ORDEN_SERVICIO, ID_ESPIRAL, ID_CHAROLA, CAPACIDAD_ESPIRAL, ARTICULO, PRECIO, CAMBIO, VASOS, VENTAS_CAFE_ANT, ID_SELECCION, EXISTENCIA FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND CAMBIO != 'V' AND TIPO_ARTICULO = 'C' ORDER BY ID_ESPIRAL",(long)id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_articulo      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * surtio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * removio          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * caduco           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * clave            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * orden_visita     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * id_orden_serv    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * id_espiral       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_charola       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * capacidad        = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * nombre_articulo  = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * precio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * cambio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                NSString * vasos            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,14)];
                NSString * ventas_cafe      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,15)];
                NSString * id_seleccion     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,16)];
                //NSString * existencia       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,17)];
                
                OBJ_Cafe * articulo_cafe = [[OBJ_Cafe alloc]init:id_instancia id_articulo:id_articulo surtio:surtio removio:removio caduco:caduco clave:clave orden_visita:orden_visita id_orden_servicio:id_orden_serv id_espiral:id_espiral id_charola:id_charola capacidad:capacidad nombre_articulo:nombre_articulo precio:precio cambio:cambio vasos:vasos ventas_cafe:ventas_cafe ventas_pruebas:0 id_seleccion:id_seleccion];
                
                [arreglo_articulos addObject:articulo_cafe];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cafe:Salio select_articulos"];
    return arreglo_articulos;
}

-(NSMutableArray *)select_articulos_contadores:(NSInteger)id_instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro select_articulos_contadores"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSMutableArray * arreglo_articulos = [[NSMutableArray alloc]init];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MOV.ID_INSTANCIA, MOV.ID_ARTICULO, SURTIO, REMOVIO, CADUCARON, CLAVE, MOV.ORDEN_VISITA, ID_ORDEN_SERVICIO, ID_ESPIRAL, ID_CHAROLA, CAPACIDAD_ESPIRAL, ARTICULO, PRECIO, CAMBIO, VASOS, HIST.SURTIO_HIST, HIST.PRUEBAS_HIST, MOV.ID_SELECCION FROM MOVIMIENTOS_INSTANCIA MOV INNER JOIN HISTORICO_CAFE HIST ON MOV.ID_ARTICULO = HIST.ID_ARTICULO AND MOV.ID_INSTANCIA = HIST.ID_INSTANCIA WHERE MOV.ID_INSTANCIA = %ld AND CAMBIO != 'V' AND TIPO_ARTICULO = 'K' ORDER BY ID_ESPIRAL",(long)id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                NSString * id_instancia     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * id_articulo      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * surtio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * removio          = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * caduco           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * clave            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                NSString * orden_visita     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                NSString * id_orden_serv    = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                NSString * id_espiral       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                NSString * id_charola       = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                NSString * capacidad        = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                NSString * nombre_articulo  = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                NSString * precio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                NSString * cambio           = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                NSString * vasos            = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,14)];
                NSString * ventas_cafe      = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,15)];
                NSString * ventas_pruebas   = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,16)];
                NSString * id_seleccion     = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,17)];
                
                OBJ_Cafe * articulo_cafe = [[OBJ_Cafe alloc]init:id_instancia id_articulo:id_articulo surtio:surtio removio:removio caduco:caduco clave:clave orden_visita:orden_visita id_orden_servicio:id_orden_serv id_espiral:id_espiral id_charola:id_charola capacidad:capacidad nombre_articulo:nombre_articulo precio:precio cambio:cambio vasos:vasos ventas_cafe:ventas_cafe ventas_pruebas:ventas_pruebas id_seleccion:id_seleccion];
                
                [arreglo_articulos addObject:articulo_cafe];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
   [logProceso escribir:@"#Data_Cafe:Salio select_articulos_contadores"];
    return arreglo_articulos;
}

-(NSString *)select_cantidad_vasos:(NSInteger)id_instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro select_cantidad_vasos"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString * cantidad_vasos;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MIN(VASOS) FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND TIPO_ARTICULO = 'K'",(long)id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                cantidad_vasos = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cafe:Salio select_cantidad_vasos"];
    return cantidad_vasos;
}


-(NSString *)select_maximo_precio:(NSInteger)id_instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro select_maximo_precio"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString * precio;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MAX(PRECIO) FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %ld AND TIPO_ARTICULO = 'K'",(long)id_instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                
                precio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Cafe:Salio select_maximo_precio"];
    return precio;
}

-(BOOL)update_surtio_insumos:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro update_surtio_insumos"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET SURTIO = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V' AND TIPO_ARTICULO = 'C'",(long)surtio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#Data_Cafe #update_surtio_insumos Error en la creacion del update"];
        }
    }else{
        [logProceso escribir:@"#Data_Cafe #update_surtio_insumos No se pudo abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Data_Cafe:Salio update_surtio_insumos"];
    return bValida;
}

-(BOOL)update_surtio_contadores:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro update_surtio_contadores"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET SURTIO = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V' AND TIPO_ARTICULO = 'K'",(long)surtio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#Data_Cafe #update_surtio_contadores Error en la creacion del update"];
        }
    }else{
       [logProceso escribir:@"#Data_Cafe #update_surtio_contadores No se pudo abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Data_Cafe:Salio update_surtio_contadores"];
    return bValida;
}

-(BOOL)update_surtio_pruebas:(NSInteger)surtio id_articulo:(NSInteger)articulo id_charola:(NSInteger)charola id_espiral:(NSInteger)espiral orden_servicio:(NSInteger)servicio orden_visita:(NSInteger)visita id_instancia:(NSInteger)instancia
{
    [logProceso escribir:@"#Data_Cafe:Entro update_surtio_pruebas"];
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"UPDATE MOVIMIENTOS_INSTANCIA SET PRUEBAS_SEL = %ld WHERE ID_INSTANCIA = %ld AND ID_ARTICULO = %ld AND ID_CHAROLA = %ld AND ID_ESPIRAL = %ld AND ID_ORDEN_SERVICIO = %ld AND ORDEN_VISITA = %ld AND CAMBIO != 'V' AND TIPO_ARTICULO = 'K'",(long)surtio, (long)instancia, (long)articulo, (long)charola, (long)espiral, (long)servicio, (long)visita];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#Data_Cafe #update_surtio_pruebas Error en la creacion del update"];
        }
    }else{
       [logProceso escribir:@"#Data_Cafe #update_surtio_pruebas No se pudo abrir la base de datos"];
    }
    sqlite3_close(dataBase);
     [logProceso escribir:@"#Data_Cafe:Salio update_surtio_pruebas"];
    return bValida;
}

@end
