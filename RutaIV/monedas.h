//
//  monedas.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface monedas : NSObject

-(BOOL)borraMonedas;

-(BOOL)insertaMonedas:(int)nIdMoneda wDenominacion:(double)dDenominacion wDescripcion:(NSString *)sDescripcion wEsMoneda:(int)nEsMoneda;

@end
