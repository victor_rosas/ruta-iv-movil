//
//  OrdenServicio.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "OrdenServicio.h"

@implementation OrdenServicio

-(BOOL)ordenServ:(NSString *)nIdOrden{
    BOOL bValida = TRUE;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:NULL forKey:@"nIdOrdenServicio"];
    [prefs synchronize];
    [prefs setObject:nIdOrden forKey:@"nIdOrdenServicio"];
    [prefs synchronize];
    return bValida;
}

@end
