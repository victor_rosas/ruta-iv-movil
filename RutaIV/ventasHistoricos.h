//
//  ventasHistoricos.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ventasHistoricos : NSObject

-(BOOL)borraVentasHistoricos;
-(BOOL)insertainstanciashistoricos:(int)nIdInstancia ordenvisita:(int)ordenvisita;
-(BOOL)insertaVentasHistoricos:(int)nIdInstancia wUnidades:(int)nUnidades wMonto:(double)dMonto orden_visita:(int)ordenvisita;

@end
