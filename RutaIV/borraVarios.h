//
//  borraVarios.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface borraVarios : NSObject

-(BOOL)borraReporteIncidencias;
-(BOOL)borraDevolucionRuta;

@end
