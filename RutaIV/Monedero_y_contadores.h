//
//  Monedero_y_contadores.h
//  RutaIV
//
//  Created by Jose Miguel Banderas Lechuga on 23/01/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Monedero_y_contadores : NSObject{
    NSString *Monedas;
    NSString *Path;
    NSInteger Productos_id;
}

@property (nonatomic,retain) NSString *Monedas;
@property (nonatomic,retain) NSString *Path;
@property (nonatomic,assign) NSInteger Productos_id;



@end
