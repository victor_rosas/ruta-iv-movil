//
//  existenciasRuta.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface existenciasRuta : NSObject

-(BOOL)borraExistenciasRuta;

-(BOOL)insertaExistenciasRuta:(int)nIdAlmacen wIdArticulo:(int)nIdArticulo wExistencia:(double)dExistencia;

@end
