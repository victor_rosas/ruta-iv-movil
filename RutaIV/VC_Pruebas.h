//
//  VC_Pruebas.h
//  RutaIV
//
//  Created by Miguel Banderas on 11/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data_Cafe.h"

@interface VC_Pruebas : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    Data_Cafe * data_cafe_manager;
    NSMutableArray * arreglo_articulos_cafe;
    int contador_rellenado;
    BOOL ventas_incongruentes;
    BOOL productos_orden;
    BOOL alerta_presente;
    
    NSIndexPath * index_path_pasado;
    UIAlertController * alerta_orden;
    UIAlertController * alerta_ventas;
    
    UIToolbar* keyboardDoneButtonView;
    __weak IBOutlet UIBarButtonItem *btn_siguiente;
    __weak IBOutlet UITableView *myTableView;
}
@property int id_instancia;
@property BOOL salida_sitio;
- (IBAction)action_back:(id)sender;
- (IBAction)action_siguiente:(id)sender;

@end
