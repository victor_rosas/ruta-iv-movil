//
//  parametros.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface parametros : NSObject

-(BOOL)borraParametros;

-(BOOL)insertaParametros:(int)nValidaMonederos wEscanearMaquina:(int)nEscanearMaquina wConectaInternet:(int)nConectaInternet wLeerDex:(int)nLeerDex wMultiploCafe:(int)nMultiploCafe wLlenaSeleccion:(int)nLlenaSeleccion wCVPN:(int)nCVPN wAcumularMonedero:(int)nAcumularMonedero wBloqueoRecaudo:(int)nBloqueoRecaudo wPrekit:(int)nPrekit wMostrarVentaDex:(int)nMostrarVentaDex wInvSugDex:(int)nInvSugDex wQuitarValidacionesCafe:(int)nQuitarValidacionesCafe wOcultarContadores:(int)nOcultarContadores;

@end
