//
//  VC_Devolucion_Monedas.h
//  RutaIV
//
//  Created by Miguel Banderas on 17/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductosDAO.h"
#import "DATA_Devoluciones.h"

@interface VC_Devolucion_Monedas : UIViewController <UITextFieldDelegate>
{
    
    __weak IBOutlet UITextField *field_monedas_50;
    __weak IBOutlet UITextField *field_monedas_1;
    __weak IBOutlet UITextField *field_monedas_2;
    __weak IBOutlet UITextField *field_monedas_5;
    __weak IBOutlet UITextField *field_contacto;
    __weak IBOutlet UITextField *field_telefono;
    __weak IBOutlet UITextField *field_cantidad;
    
    int actual_seleccion;
    ProductosDAO * data_articulo_manager;
    DATA_Devoluciones * data_devoluciones_manager;
    BOOL back;
    
    UIAlertController * alerta_cantidad;
    UIAlertController * alerta_incompleto;
    
}
@property int id_instancia;
@property BOOL salida_sitio;
@property NSInteger visita;
@property NSString * tipo;
- (IBAction)action_back:(id)sender;
- (IBAction)action_next:(id)sender;

@end
