//
//  detSitiosAsignados.h
//  RutaIV
//
//  Created by Miguel Banderas on 28/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface detSitiosAsignados : NSObject

-(BOOL)borraDetSitios;

-(BOOL)insertaDetSitios:(int)nIdCliente wIdSitio:(int)nIdSitio wIdInstancia:(int)nIdInstancia wIdOrden:(int)nIdOrden wOrdenVisita:(int)nOrdenVisita wIdRuta:(int)nIdRuta wDia:(int)nDia;

@end
