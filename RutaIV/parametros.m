//
//  parametros.m
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "parametros.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation parametros

-(BOOL)borraParametros{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PARAMETROS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaParametros:(int)nValidaMonederos wEscanearMaquina:(int)nEscanearMaquina wConectaInternet:(int)nConectaInternet wLeerDex:(int)nLeerDex wMultiploCafe:(int)nMultiploCafe wLlenaSeleccion:(int)nLlenaSeleccion wCVPN:(int)nCVPN wAcumularMonedero:(int)nAcumularMonedero wBloqueoRecaudo:(int)nBloqueoRecaudo wPrekit:(int)nPrekit wMostrarVentaDex:(int)nMostrarVentaDex wInvSugDex:(int)nInvSugDex wQuitarValidacionesCafe:(int)nQuitarValidacionesCafe wOcultarContadores:(int)nOcultarContadores{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO PARAMETROS(\"VALIDA_MONEDEROS\",\"ESCANEAR_MAQUINA\",\"CONECTA_INTERNET\",\"LEER_DEX\",\"MULTIPLOCAFE\",\"LLENA_SELECCION_RECAUDO\",\"CVPN\",\"ACUMULAR_MONEDERO\",\"BLOQUEO_RECAUDO_TP\",\"PREKIT\",\"MOSTRAR_VENTA_DEX\",\"INV_SUG_DEX\",\"QUITAR_VALIDACIONES_CAFE\",\"OCULTAR_CONTADORES\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\")",nValidaMonederos,nEscanearMaquina,nConectaInternet,nLeerDex,nMultiploCafe,nLlenaSeleccion,nCVPN,nAcumularMonedero,nBloqueoRecaudo,nPrekit,nMostrarVentaDex,nInvSugDex,nQuitarValidacionesCafe,nOcultarContadores];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;

}

@end
