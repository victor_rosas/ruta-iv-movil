//
//  historicosCafe.h
//  RutaIV
//
//  Created by Miguel Banderas on 21/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface historicosCafe : NSObject

-(BOOL)borraRegistrosPrekit;

-(BOOL)insertaRegistrosPrekit:(int)nIdInstancia wOrdenVisita:(int)nOrdenVisita wIdArticulo:(int)nIdArticulo wSurtidoAnt:(double)dSurtidoAnt wPruebasAnt:(int)nPruebasAnt wIdSeleccion:(int)nIdSeleccion;

@end
