//
//  planogramaMovs.m
//  RutaIV
//
//  Created by Miguel Banderas on 30/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "planogramaMovs.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation planogramaMovs

-(BOOL)borraPlanogramaMovs{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM MOVIMIENTOS_INSTANCIA"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)instertaPlanogramaMovs:(int)nIdCliente wIdSitio:(int)nIdSitio wIdInstancia:(int)nIdInstancia wIdArticulo:(int)nIdArticulo wInvIni:(double)dInvIni wSurtio:(double)dSurtio wRemovio:(double)dRemovio wCaducaron:(double)dCaducaron wClave:(int)nClave wInicio:(int)nInicio wOrdenVisita:(int)nOrdenVisita wIdOrdenServicio:(int)nIdOrdenServicio wSalteado:(int)nSalteado wExistencia:(double)dExistencia wIdEspiral:(int)nIdEspiral wIdCharola:(int)nIdCharola wCapacidadEspiral:(int)nCapacidadEspiral wIdUbicacion:(int)nIdUbicacion wIdPlanograma:(int)nIdPlanograma wSitio:(NSString *)sSitio wArticulo:(NSString *)sArticulo wPrecio:(double)dPrecio wIdRuta:(int)nIdRuta wExRuta:(double)dExRuta wEspiral:(NSString *)sEspiral wCambio:(NSString *)sCambio wTipoArticulo:(NSString *)sTipoArticulo wSubsidio:(double)dSubsidio wPruebasSel:(int)nPruebasSel wVasos:(int)nVasos wVentaDexAnt:(int)nVentaDexAnt wImporteVtaDexAnt:(double)dImporteVtaDexAnt wVentaDex:(double)dVentaDex wImporteVtaDex:(double)dImporteVtaDex wIdSeleccion:(int)nIdSeleccion wIdCategoria:(int)nIdCategoria wCapacidadNva:(int)nCapacidadNva categoria:(NSString *)sCategoria{
    BOOL bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO MOVIMIENTOS_INSTANCIA(\"ID_CLIENTE\",\"ID_SITIO\",\"ID_INSTANCIA\",\"ID_ARTICULO\",\"INV_INI\",\"SURTIO\",\"REMOVIO\",\"CADUCARON\",\"CLAVE\",\"INICIO\",\"ORDEN_VISITA\",\"ID_ORDEN_SERVICIO\",\"SALTEADO\",\"EXISTENCIA\",\"ID_ESPIRAL\",\"ID_CHAROLA\",\"CAPACIDAD_ESPIRAL\",\"ID_UBICACION\",\"ID_PLANOGRAMA\",\"SITIO\",\"ARTICULO\",\"PRECIO\",\"ID_RUTA\",\"ExRuta\",\"Espiral\",\"CAMBIO\",\"TIPO_ARTICULO\",\"SUBSIDIO\",\"PRUEBAS_SEL\",\"VASOS\",\"VENTA_DEX_ANT\",\"IMPORTE_VTA_DEX_ANT\",\"VENTA_DEX\",\"IMPORTE_VTA_DEX\",\"ID_SELECCION\",\"INSTANCIA_NUEVA\",\"VENTAS_PRUEBAS_ANT\",\"VENTAS_CAFE_ANT\",\"ID_CATEGORIA\",\"CAPACIDAD_NVA\",\"CATEGORIA\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%f\",\"%f\",\"%f\",\"%f\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%f\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\",\"%@\",\"%f\",\"%i\",\"%f\",\"%@\",\"%@\",\"%@\",\"%f\",\"%i\",\"%i\",\"%i\",\"%f\",\"%f\",\"%f\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\")",nIdCliente,nIdSitio,nIdInstancia,nIdArticulo,dInvIni,dSurtio,dRemovio,dCaducaron,nClave,nInicio,nOrdenVisita,nIdOrdenServicio,nSalteado,dExistencia,nIdEspiral,nIdCharola,nCapacidadEspiral,nIdUbicacion,nIdPlanograma,sSitio,sArticulo,dPrecio,nIdRuta,dExRuta,sEspiral,sCambio,sTipoArticulo,dSubsidio,nPruebasSel,nVasos,nVentaDexAnt,dImporteVtaDexAnt,dVentaDex,dImporteVtaDex,nIdSeleccion,0,0,0,nIdCategoria,nCapacidadNva,sCategoria];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
