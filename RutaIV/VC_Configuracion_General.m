//
//  VC_Configuracion_General.m
//  RutaIV
//
//  Created by Miguel Banderas on 05/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Configuracion_General.h"
#import "TVC_Sucursal.h"
#import "ViewController.h"
#import "GRRequestsManager.h"
#import "OBJ_Log_Proceso.h"

@interface VC_Configuracion_General ()<GRRequestsManagerDelegate>
@property (nonatomic, strong) GRRequestsManager *requestsManager;

@end

@implementation VC_Configuracion_General

- (void)viewDidLoad {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    _sNombreRuta = [prefs stringForKey:@"sNombreRuta"];
    _sIdRuta = [prefs stringForKey:@"nIdRuta"];
    _sIdSucursal = [prefs stringForKey:@"nIdSucursal"];
    [super viewDidLoad];
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Configuración" style:UIBarButtonItemStyleBordered target:self action:@selector(home:)];
    self.navigationItem.leftBarButtonItem=newBackButton;
    
    NSString *instancia_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_instancia_orden"];
    NSString *producto_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_producto_orden"];
    NSString *historico = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_historico"];
    NSString *historico_cafe = [[NSUserDefaults standardUserDefaults] objectForKey:@"configurarion_historico_cafe"];
    NSString *nueva = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_3G"];
    NSString *apariencia_mayusculas = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_apariencia_mayusculas"];
    NSString *barra_progreso = [[NSUserDefaults standardUserDefaults] objectForKey:@"configurarion_apariencia_progreso"];
    
    
    
    if([instancia_orden isEqualToString:@"1"])
    {
        [switch_instancia_orden setOn:YES];
    }
    if([producto_orden isEqualToString:@"1"])
    {
        [switch_producto_orden setOn:YES];
    }
    if ([historico isEqualToString:@"1"]) {
        [switch_historico setOn:YES];
    }
    if([nueva isEqualToString:@"1"])
    {
        [switch_red_celular setOn:YES];
    }
    if([apariencia_mayusculas isEqualToString:@"1"])
    {
        [switch_apariencia_mayusculas setOn:YES];
    }
    if([barra_progreso isEqualToString:@"1"])
    {
        [switch_barra_progreso setOn:YES];
    }
    if ([historico_cafe isEqualToString:@"1"]) {
        [switch_historico_cafe setOn:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(void)home:(UIBarButtonItem *)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Atención"
                                  message:@"Se guardarán los cambios."
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Ok"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             NSString *instancias_orden = @"0";
                             NSString *productos_orden = @"0";
                             NSString *mostrar_historico = @"0";//mostrar historicos en placeholder
                             NSString *movil_datos = @"0";//Subida de datos en 3G
                             NSString *apariencia_mayusculas = @"0";
                             NSString *barra_progreso = @"0"; // mostrar barra de progreso
                             NSString *historico_cafe = @"0";
                             
                             if([switch_instancia_orden isOn]){
                                 instancias_orden = @"1";
                             }
                             if([switch_producto_orden isOn]){
                                 productos_orden = @"1";
                             }
                             if ([switch_historico isOn]) {
                                 mostrar_historico = @"1";
                             }
                             if ([switch_red_celular isOn]) {
                                 movil_datos = @"1";
                             }
                             if([switch_apariencia_mayusculas isOn])
                             {
                                 apariencia_mayusculas = @"1";
                             }
                             if([switch_barra_progreso isOn])
                             {
                                 barra_progreso = @"1";
                             }
                             if ([switch_historico_cafe isOn]) {
                                 historico_cafe = @"1";
                             }
                             
                             NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                             
                             [userDefaults setObject:instancias_orden forKey:@"configuracion_instancia_orden"];
                             [userDefaults setObject:productos_orden forKey:@"configuracion_producto_orden"];
                             [userDefaults setObject:historico_cafe forKey:@"configurarion_historico_cafe"];
                             [userDefaults setObject:mostrar_historico forKey:@"configuracion_historico"];
                             [userDefaults setObject:movil_datos forKey:@"configuracion_3G"];
                             [userDefaults setObject:apariencia_mayusculas forKey:@"configuracion_apariencia_mayusculas"];
                             [userDefaults setObject:barra_progreso forKey:@"configurarion_apariencia_progreso"];
                             
                             [userDefaults synchronize];
                             [self.navigationController popViewControllerAnimated:true];
                         }];
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    [alert addAction:ok];
    [alert addAction:Cancelar];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)action_btn_restaurar:(id)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Restaurado de Bajada"
                                          message:@"Introduzca la contraseña"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Password", @"Password");
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction* OK = [UIAlertAction
                               actionWithTitle:@"Aceptar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   UITextField *password = alertController.textFields.lastObject;
                                   [self revision_password:password.text];
                               }];
    [alertController addAction:OK];
    [alertController addAction:Cancelar];
    [self presentViewController:alertController animated:YES completion:nil];
    OBJ_Log_Proceso *logProceso = [[OBJ_Log_Proceso alloc]init];
    [logProceso escribir:[NSString stringWithFormat:@"Se restauro la baja de la base de datos de %@: Id_ruta:%@ Sucursal:%@",_sNombreRuta,_sIdRuta,_sIdSucursal]];
     TVC_Sucursal *vConfiguracion = [self.storyboard instantiateViewControllerWithIdentifier:@"vSucursalConfig"];
     vConfiguracion.navigationItem.title = @"Configurar Sucursal";
     [self.navigationController pushViewController:vConfiguracion animated:YES];
}

- (void)_setupManager
{
    self.requestsManager = [[GRRequestsManager alloc] initWithHostname:@"ftp://173.192.80.226"//[self.hostnameTextField text]
                                                                  user:@"ftpImgBodies"//[self.usernameTextField text]
                                                              password:@"Ads720510."/*[self.passwordTextField text]*/];
    self.requestsManager.delegate = self;
}

- (IBAction)enviarBD:(id)sender {
    OBJ_Log_Proceso *logProceso = [[OBJ_Log_Proceso alloc]init];
    [logProceso escribir:[NSString stringWithFormat:@"Se ha subido la base de datos para la ruta %@: Id_ruta:%@ Sucursal:%@",_sNombreRuta,_sIdRuta,_sIdSucursal]];
        [self _setupManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"Vending.sqlite"];
        
        
        // NSError* error;
        
        
        [self.requestsManager addRequestForUploadFileAtLocalPath:writableDBPath toRemotePath:@"Vending.sqlite"];
        
        [self.requestsManager startProcessingRequests];
        
        UIAlertView *DBUpload = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Se ha cargado la base de datos" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
        [DBUpload show];

}

- (IBAction)back_pressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)enviarLog:(id)sender {
    [self _setupManager];
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* fileName = @"Log.txt";
    NSString* fileAtPath = [filePath stringByAppendingPathComponent:fileName];
    
    [self.requestsManager addRequestForUploadFileAtLocalPath:fileAtPath toRemotePath:@"ErrorLog.txt"];
    
    [self.requestsManager startProcessingRequests];
    UIAlertView *logUpload = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Se ha cargado el log de errores" delegate:nil cancelButtonTitle:@"Aceptar" otherButtonTitles:nil, nil];
    [logUpload show];

}

-(void)revision_password :(NSString *)password
{
    if ([password isEqualToString:@"pass"]) {
        UIAlertController * alert_correcto=   [UIAlertController
                                               alertControllerWithTitle:@"Restaurado de Bajada"
                                               message:@"Se puede volver a bajar la ruta."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 TVC_Sucursal *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"vSucursalConfig"];
                                 [self.navigationController pushViewController:controller animated:YES];
                                 [alert_correcto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_correcto addAction:ok];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *descargado = @"0";
        [userDefaults setObject:descargado forKey:@"configuracion_descargado"];
        //[userDefaults setObject:@"0" forKey:@"imagenes_pendientes"];
        [userDefaults synchronize];
        [self presentViewController:alert_correcto animated:YES completion:nil];
    }
    else
    {
        UIAlertController * alert_incorrecto=   [UIAlertController
                                                 alertControllerWithTitle:@"Restaurado de Bajada"
                                                 message:@"Contraseña incorrecta."
                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_incorrecto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_incorrecto addAction:ok];
        [self presentViewController:alert_incorrecto animated:YES completion:nil];
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath * index_bajar_ruta = [NSIndexPath indexPathForRow:0 inSection:3];
    if ([indexPath isEqual:index_bajar_ruta]) {
        [self action_btn_restaurar:self];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end
