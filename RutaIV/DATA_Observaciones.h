//
//  DATA_Observaciones.h
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "OBJ_Log_Proceso.h"

@interface DATA_Observaciones : NSObject {
    sqlite3 *bd;
    OBJ_Log_Proceso *logProceso;
}

-(NSString *) obtenerRutaBD;
-(NSMutableDictionary *) traer_clasificacion;
-(BOOL)insertar_observacion:(int)id_reporte clasificacion:(int)id_clasificacion empleado:(int)id_empleado fecha:(NSString *)fecha_incidencia texto_descripcion:(NSString *)descripcion orden_servicio:(int)id_orden_servicio instancia:(int)id_instancia visita:(int)orden_visita maquina:(int)id_maquina bit_servicio:(int)servicio fecha_atencion:(NSString *)fecha_de_atencion contacto:(NSString *)texto_contacto tel_contacto:(int)telefono_contacto bit_subido:(int)subido;
-(NSString *)traer_id_reporte;
-(NSString*)trae_orden_visita:(int)instancia;
-(NSString*)trae_id_maquina:(int)instancia;
-(NSMutableArray *)traer_observaciones;
-(NSString*)trae_sitio:(int)instancia;

//Metodos para pruebas
-(BOOL)prueba_borrar;

@end
