//
//  detalleOrden.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/06/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface detalleOrden : NSObject

-(BOOL)borraDetalleOrden;

-(BOOL)insertaDetalleOrden:(int)nIdOrdenServicio wOrden:(int)nOrden wCliente:(int)nIdCliente wIdSitio:(int)nIdSitio wIdUbicacion:(int)nIdUbicacion wIdInstancia:(int)nIdInstancia wIdOperacion:(int)nIdOperacion wHoraInicio:(NSString *)sHoraInicio wCodigoAutorizacion:(int)nCodigoAutorizacion wTipoAutorizacion:(NSString *)sTipoAutorizacion wOrdenVisita:(int)nOrdenVisita wRecaudo:(int)nRecaudo;

@end
