//
//  OBJ_Cambio_Precio.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Cambio_Precio : NSObject

@property NSString * id_instancia;
@property NSString * id_charola;
@property NSString * id_espiral;
@property NSString * id_seleccion;
@property NSString * id_articulo;
@property NSString * actual;
@property NSString * anterior;

-(id)init :(NSString *)instancia id_charola:(NSString *)charola id_espiral:(NSString *)espiral id_seleccion:(NSString *)seleccion id_articulo:(NSString *)articulo actual:(NSString *)precio_actual anterio:(NSString *)precio_anterior;

@end
