//
//  OBJ_Cambio_Planograma.h
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Cambio_Planograma : NSObject
@property NSString * id_articulo;
@property NSString * surtio;
@property NSString * removio;
@property NSString * clave;
@property NSString * orden;
@property NSString * id_orden_servicio;
@property NSString * id_espiral;
@property NSString * id_charola;
@property NSString * capacidad;
@property NSString * id_planograma;
@property NSString * articulo;
@property NSString * espiral;
@property NSString * cambio;
@property NSString * id_instancia;
@property NSString * cantidad_removida;

-(id)init :(NSString *)articulo_id surtio:(NSString *)cantidad_surtio removio:(NSString *)cantidad_removio clave:(NSString *)numero_clave orden:(NSString *)numero_orden id_orden_servicio:(NSString *)orden_servicio id_espiral:(NSString *)espiral_id id_charola:(NSString *)charola capacidad:(NSString *)numero_capacidad id_planograma:(NSString *)planograma articulo:(NSString *)nombre_articulo espiral:(NSString *)nombre_espiral cambio:(NSString *)bool_cambio id_instancia:(NSString *)instancia;
@end
