//
//  TC_Cambio_Precio.m
//  RutaIV
//
//  Created by Miguel Banderas on 03/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "TC_Cambio_Precio.h"

@implementation TC_Cambio_Precio

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)cell_setup :(NSString *)articulo espiral:(NSString *)nombre_espiral precio:(NSString *)precio_nuevo
{
    label_espiral.layer.cornerRadius = 5;
    label_espiral.layer.masksToBounds = TRUE;
    
    label_articulo.text = articulo;
    label_espiral.text = nombre_espiral;
    label_precio.text = precio_nuevo;
}

@end
