//
//  rutas.h
//  RutaIV
//
//  Created by Miguel Banderas on 02/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface rutas : NSObject

-(BOOL)borraRutas;

-(BOOL)insertaRutas:(int)nIdRuta wIdAlmacen:(int)nIdAlmacen wInvNegativo:(int)nInvNegativo wDescripcion:(NSString *)sDescripcion;

@end
