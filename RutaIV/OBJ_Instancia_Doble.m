//
//  OBJ_Instancia_Doble.m
//  RutaIV
//
//  Created by Miguel Banderas on 19/05/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "OBJ_Instancia_Doble.h"

@implementation OBJ_Instancia_Doble
@synthesize instancia;
@synthesize minimo;
@synthesize maximo;

-(id)init :(NSString *)id_instancia orden_minima:(NSString *)minima orden_maxima:(NSString *)maxima
{
    self.instancia = id_instancia;
    self.minimo = minima;
    self.maximo = maxima;
    return self;
}

@end
