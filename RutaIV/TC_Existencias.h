//
//  TC_Existencias.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TC_Existencias : UITableViewCell{
    
    __weak IBOutlet UILabel *label_articulo;
    __weak IBOutlet UILabel *label_cantidad;
    __weak IBOutlet UILabel *label_id;
}
-(void)setupCell:(NSString*)articulo cantidad:(int)cantidad id_articulo:(NSString *)id_articulo;

@end
