//
//  DATA_Observaciones.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "DATA_Observaciones.h"
#import "AppDelegate.h"
#import "OBJ_Observaciones.h"

@implementation DATA_Observaciones

-(NSString *) obtenerRutaBD{
    logProceso = [[OBJ_Log_Proceso alloc]init];
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
    }
    return rutaBD;
}

-(NSMutableDictionary *)traer_clasificacion
{   [logProceso escribir:@"#Data_Observaciones Entro traer_clasificacion"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSMutableDictionary * diccionario_clasificacion = [[NSMutableDictionary alloc]init];
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CLASIFICACION_INCIDENCIA ORDER BY DESCRIPCION"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW)
            {
                NSString *id_clasificacion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString *descripcion = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                
                [diccionario_clasificacion setObject:id_clasificacion forKey:descripcion];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Observaciones salio traer_clasificacion"];
    return diccionario_clasificacion;
}

-(BOOL)insertar_observacion:(int)id_reporte clasificacion:(int)id_clasificacion empleado:(int)id_empleado fecha:(NSString *)fecha_incidencia texto_descripcion:(NSString *)descripcion orden_servicio:(int)id_orden_servicio instancia:(int)id_instancia visita:(int)orden_visita maquina:(int)id_maquina bit_servicio:(int)servicio fecha_atencion:(NSString *)fecha_de_atencion contacto:(NSString *)texto_contacto tel_contacto:(int)telefono_contacto bit_subido:(int)subido
{[logProceso escribir:@"#Data_Observaciones Entro insertar_observacion"];
    BOOL bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO REPORTE_INCIDENCIAS(\"ID_REPORTE\",\"ID_CLASIFICACION\",\"ID_EMPLEADO\",\"FECHA_INCIDENCIA\",\"DESCRIPCION\",\"ID_ORDEN_SERVICIO\",\"ID_INSTANCIA\",\"ORDEN_VISITA\",\"ID_MAQUINA\",\"SERVICIO\",\"FECHA_ATENCION\",\"CONTACTO\",\"TEL_CONTACTO\",\"SUBIDO\") VALUES(\"%i\",\"%i\",\"%i\",\"%@\",\"%@\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\",\"%@\",\"%i\",\"%i\")",id_reporte, id_clasificacion, id_empleado, fecha_incidencia, descripcion, id_orden_servicio, id_instancia, orden_visita, id_maquina, servicio, fecha_de_atencion, texto_contacto, telefono_contacto, subido];
        
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            [logProceso escribir:@"#Data_Observaciones #insertar_observacion error en la creacion del insert"];
        }
    }else{
       [logProceso escribir:@"#Data_Observaciones #insertar_observacion No se ha podido abrir la base de datos"];
    }
    sqlite3_close(dataBase);
    [logProceso escribir:@"#Data_Observaciones Salio insertar_observacion"];
    return bValida;
}


-(NSString *)traer_id_reporte
{[logProceso escribir:@"#Data_Observaciones Entro traer_id_reporte"];
    NSString * id_reporte;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MAX(ID_REPORTE) FROM REPORTE_INCIDENCIAS"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW)
            {
                const unsigned char *id_reporte_check = sqlite3_column_text(sentencia, 0);
                if (id_reporte_check)
                {
                    id_reporte = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                }
                else
                {
                    id_reporte_check = 0;
                }
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Observaciones Salio traer_id_reporte"];
    return id_reporte;
}

-(NSString*)trae_orden_visita:(int)instancia
{
    [logProceso escribir:@"#Data_Observaciones Entro traer_orden_visita"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSString * orden_visita;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MIN(ORDEN_VISITA) FROM MOVIMIENTOS_INSTANCIA WHERE ID_INSTANCIA = %d",instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
               orden_visita = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Observaciones Salio traer_orden_visita"];
    return orden_visita;
    
}

-(NSString*)trae_id_maquina:(int)instancia
{   [logProceso escribir:@"#Data_Observaciones Entro traer_id_maquina"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSString * id_maquina;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT ID_MAQUINA FROM INSTANCIAS_SERVICIO WHERE ID_INSTANCIA = %d",instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                id_maquina = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Observaciones Salio traer_id_maquina"];
    return id_maquina;
    
}

-(NSString*)trae_sitio:(int)instancia
{   [logProceso escribir:@"#Data_Observaciones Entro trae_sitio"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    NSString * sitio;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT SITIO FROM SITIOS_TP WHERE ID_INSTANCIA = %d",instancia];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                sitio = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
            }
        }
        sqlite3_finalize(sentencia);
    }
    sqlite3_close(database);
    [logProceso escribir:@"#Data_Observaciones Salio traer_sitio"];
    return sitio;
    
}



-(NSMutableArray *)traer_observaciones
 {[logProceso escribir:@"#Data_Observaciones Entro traer_observaciones"];
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     sqlite3 *database;
     sqlite3_stmt *sentencia;
     NSMutableArray * array_mutable_observaciones = [[NSMutableArray alloc]init];
     
     if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK)
     {
         NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM REPORTE_INCIDENCIAS"];
         if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK)
         {
             while (sqlite3_step(sentencia) == SQLITE_ROW)
             {
                 NSString * ID_REPORTE = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                 NSString * ID_CLASIFICACION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                 NSString * ID_EMPLEADO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                 NSString * FECHA_INCIDENCIA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                 NSString * DESCRIPCION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                 NSString * ID_ORDEN_SERVICIO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                 NSString * ID_INSTANCIA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];
                 NSString * ORDEN_VISITA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,7)];
                 NSString * ID_MAQUINA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,8)];
                 NSString * SERVICIO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,9)];
                 NSString * FECHA_ATENCION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,10)];
                 NSString * CONTACTO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,11)];
                 NSString * TEL_CONTACTO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,12)];
                 NSString * SUBIDO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,13)];
                 
                 OBJ_Observaciones * observacion = [[OBJ_Observaciones alloc]init:ID_REPORTE id_clasificacion:ID_CLASIFICACION id_empleado:ID_EMPLEADO fecha_incidencia:FECHA_INCIDENCIA descripcion:DESCRIPCION id_orden_servicio:ID_ORDEN_SERVICIO id_instancia:ID_INSTANCIA orden_visita:ORDEN_VISITA id_maquina:ID_MAQUINA servicio:SERVICIO fecha_atencion:FECHA_ATENCION contacto:CONTACTO tel_contacto:TEL_CONTACTO subido:SUBIDO];
                 
                 [array_mutable_observaciones addObject:observacion];
             }
         }
         sqlite3_finalize(sentencia);
     }
     sqlite3_close(database);
     [logProceso escribir:@"#Data_Observaciones Salio traer_observaciones"];
     return array_mutable_observaciones;
 }

-(BOOL)prueba_borrar
{   [logProceso escribir:@"#Data_Observaciones entro prueba_borrar"];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM REPORTE_INCIDENCIAS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    [logProceso escribir:@"#Data_Observaciones salio prueba_borrar"];
    return  bValida;
}

@end
