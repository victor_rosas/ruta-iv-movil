//
//  OBJ_Log_Proceso.h
//  RutaIV
//
//  Created by Miguel Banderas on 27/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBJ_Log_Proceso : NSObject
-(void)escribir :(NSString *)texto;
-(void)escribir_titulo :(NSString *)texto;
-(NSString *)leer;
@end
