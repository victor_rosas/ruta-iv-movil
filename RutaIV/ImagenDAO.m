//
//  ImagenDAO.m
//  Fotografia_vending
//
//  Created by Jose Miguel Banderas Lechuga on 10/12/14.
//  Copyright (c) 2014 Jose Miguel Banderas Lechuga. All rights reserved.
//

#import "ImagenDAO.h"
#import "Imagen.h"
#import "AppDelegate.h"

@implementation ImagenDAO

-(NSString *) obtenerRutaBD{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(void) CrearImagen:(NSString *)Image Path:(NSString *)Path{
    NSLog(@"Entra crear imagen");
    NSString *ubicacionDB = [self obtenerRutaBD];
    
    if (!(sqlite3_open([ubicacionDB UTF8String], &bd) == SQLITE_OK)) {
        NSLog(@"No se puede conectar con la BD");
        return;
    }else{
        NSString *sqlInsert = [NSString stringWithFormat:@"Insert into imageinfo (Imagen) VALUES ('%@')",Image];
        const char *sql = [sqlInsert UTF8String];
        sqlite3_stmt *sqlStatement;
        
        if (sqlite3_prepare_v2(bd, sql, -1, &sqlStatement, NULL) != SQLITE_OK) {
            NSLog(@"Problema al preparar el statement %s",sqlite3_errmsg(bd));
            return;
        }else{
            if (sqlite3_step(sqlStatement) == SQLITE_DONE) {
                sqlite3_finalize(sqlStatement);
                sqlite3_close(bd);
            }
            
            
        }
        
    }
    
}

-(NSMutableArray *)obtenerImagenes{
    NSLog(@"Entro a obtenerImagenes");
    NSMutableArray *listaImagenes =[[NSMutableArray alloc]init];
    NSString *ubicacionDB = [self obtenerRutaBD];
    
    if (!(sqlite3_open([ubicacionDB UTF8String], &bd) == SQLITE_OK)) {
        NSLog(@"No se puede conectar a la BD");
    }
    const char *sentenciaSQL = "SELECT Imagen, Path, Image_id FROM ImageBD";
    sqlite3_stmt *sqlStatement;
    
    if(sqlite3_prepare_v2(bd, sentenciaSQL, -1, &sqlStatement, NULL) != SQLITE_OK){
        NSLog(@"Problema para preparar el statement");
    }
    
    while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
        Imagen *imagen = [[Imagen alloc]init];
        imagen.Imagen = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 0)];
        imagen.Path = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sqlStatement, 1)];
        imagen.Image_id = sqlite3_column_int(sqlStatement, 2);
        [listaImagenes addObject:imagen];
    }
    
    return listaImagenes;
    
}

-(void)actualizarImagenes:(NSInteger)images_id Imagen:(NSString *)Image Path:(NSString *)Path{
    NSLog(@"Entro a actualizarImagenes");
    NSString *ubicacionBD = [self obtenerRutaBD];
    
    if(!(sqlite3_open([ubicacionBD UTF8String], &bd) == SQLITE_OK)){
        NSLog(@"No se puede conectar con la BD");
        return;
    } else {
        NSString *sqlUpdate = [NSString stringWithFormat:@"UPDATE ImageBD SET Imagen = '%@', Path = '%@' WHERE id = %ld", Image, Path, (long)images_id];
        const char *sql = [sqlUpdate UTF8String];
        sqlite3_stmt *sqlStatement;
        
        if(sqlite3_prepare_v2(bd, sql, -1, &sqlStatement, NULL) != SQLITE_OK){
            NSLog(@"Problema al preparar el statement.");
            return;
        } else {
            if(sqlite3_step(sqlStatement) == SQLITE_DONE){
                sqlite3_finalize(sqlStatement);
                sqlite3_close(bd);
            }
        }
    }
}

- (void) borrarImagenes{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    
    if(!(sqlite3_open([appDelegate.dataBasePath UTF8String], &database) == SQLITE_OK)){
        NSLog(@"No se puede conectar con la BD");
        return;
    } else {
        NSString *sqlDelete = [NSString stringWithFormat:@"DELETE FROM IMAGEN"];
        const char *sql = [sqlDelete UTF8String];
 
        
        if(sqlite3_prepare_v2(database, sql, -1, &sentencia, NULL) != SQLITE_OK){
            NSLog(@"Problema al preparar el statement. %s",sqlite3_errmsg(database));
            return;
        } else {
            if(sqlite3_step(sentencia) == SQLITE_DONE){
                
                sqlite3_finalize(sentencia);
                sqlite3_close(database);
            }
        }
    }
}

- (void) borrarFotosVisita{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    
    if(!(sqlite3_open([appDelegate.dataBasePath UTF8String], &database) == SQLITE_OK)){
        NSLog(@"No se puede conectar con la BD");
        return;
    } else {
        NSString *sqlDelete = [NSString stringWithFormat:@"DELETE FROM FOTOS_VISITA"];
        const char *sql = [sqlDelete UTF8String];
        
        
        if(sqlite3_prepare_v2(database, sql, -1, &sentencia, NULL) != SQLITE_OK){
            NSLog(@"Problema al preparar el statement. %s",sqlite3_errmsg(database));
            return;
        } else {
            if(sqlite3_step(sentencia) == SQLITE_DONE){
                
                sqlite3_finalize(sentencia);
                sqlite3_close(database);
            }
        }
    }
}


-(void)Insertar:(NSString *)IMAGEN PATH:(NSString *)PATH ID_IMAGEN:(NSString *)ID_IMAGEN ID_INSTANCIA:(NSString *)ID_INSTANCIA ID_ORDEN_SERVICIO:(NSString *)ID_ORDEN_SERVICIO ORDEN_VISITA:(NSString *)ORDEN_VISITA{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if(sqlite3_open([appDelegate.dataBasePath UTF8String], &database)== SQLITE_OK){
        NSString *sql = [NSString stringWithFormat:@"insert into IMAGEN (\"ID_IMAGEN\",\"ID_INSTANCIA\",\"ID_ORDEN_SERVICIO\",\"ORDEN_VISITA\",\"IMAGEN\",\"PATH\") VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",ID_IMAGEN,ID_INSTANCIA,ID_ORDEN_SERVICIO,ORDEN_VISITA,IMAGEN,PATH];//id_image,id_image2,id_image3,id_image4,Imagedata,imgPath];
        if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &sentencia, NULL) == SQLITE_OK){
            if(sqlite3_step(sentencia)){
                NSLog(@"Insertado");
            }else{
                NSLog(@"No insertado");
            }
            sqlite3_finalize(sentencia);
        }else{
            NSLog(@"Error en la creacion del insert %s",sqlite3_errmsg(database));
        }
    }else{
        NSLog(@"No se ha podido abrir la BD");
    }
    sqlite3_close(database);
}

-(NSMutableArray *)CargarImagenes{
    NSLog(@"Entro a cargar imagenes");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    imagenes =[[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * from IMAGEN"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                NSString *ID_IMAGEN = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 0)];
                NSString *ID_INSTANCIA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 1)];
                NSString *ID_ORDEN_SERVICIO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 2)];
                NSString *ORDEN_VISITA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 3)];
                NSString *IMAGEN = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 4)];
                NSString *PATH= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia, 5)];
                
                [dic setValue:ID_IMAGEN forKey:@"id_imagen"];
                [dic setValue:ID_INSTANCIA forKey:@"id_instancia"];
                [dic setValue:ID_ORDEN_SERVICIO forKey:@"id_orden_servicio"];
                [dic setValue:ORDEN_VISITA forKey:@"orden_visita"];
                [dic setValue:IMAGEN forKey:@"imagen"];
                [dic setValue:PATH forKey:@"path"];
                [imagenes addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar imagenes");
    return imagenes;
}


//METODO PARA CARGAR PRODUCTOS 17/01/2015

-(NSMutableArray *)CargarProductos{

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    productos =[[NSMutableArray alloc]init];
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"select DESCRIPCION,NOMENCLATURA_IV from DEX_IV"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                 NSString *nomenclatura_iv = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString *Producto= [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                [dic setValue:nomenclatura_iv forKey:@"nomenclatura_iv"];
                [dic setValue:Producto forKey:@"producto"];
               
                [productos addObject:dic];
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
    return productos;
}

-(void)InsertarFotos_visita:(int)ID_ORDEN_SERVICIO ID_INSTANCIA:(int)ID_INSTANCIA ORDEN_VISITA:(int)ORDEN_VISITA ID_FOTO:(int)ID_FOTO URL:(NSString*)URL{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if(sqlite3_open([appDelegate.dataBasePath UTF8String], &database)== SQLITE_OK){
        NSString *sql = [NSString stringWithFormat:@"insert into FOTOS_VISITA (\"ID_ORDEN_SERVICIO\",\"ID_INSTANCIA\",\"ORDEN_VISITA\",\"ID_FOTO\",\"URL\") VALUES (\"%d\",\"%d\",\"%d\",\"%d\",\"%@\")",ID_ORDEN_SERVICIO,ID_INSTANCIA,ORDEN_VISITA,ID_FOTO,URL];
        if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &sentencia, NULL) == SQLITE_OK){
            if(sqlite3_step(sentencia)){
                NSLog(@"Insertado");
            }else{
                NSLog(@"No insertado");
            }
            sqlite3_finalize(sentencia);
        }else{
            NSLog(@"Error en la creacion del insert %s",sqlite3_errmsg(database));
        }
    }else{
        NSLog(@"No se ha podido abrir la BD");
    }
    sqlite3_close(database);
}



@end
