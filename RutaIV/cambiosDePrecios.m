//
//  cambiosDePrecios.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "cambiosDePrecios.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation cambiosDePrecios

-(BOOL)borraCambioPrecios{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CAMBIO_PRECIOS"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaCambioPrecios:(int)nIdInstancia wIdCharola:(int)nIdCharola wIdEspiral:(int)nIdEspiral wIdSeleccion:(int)nIdSeleccion wIdArticulo:(int)nIdArticulo wdActual:(double)dActual wAnterior:(double)dAnterior{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CAMBIO_PRECIOS(\"ID_INSTANCIA\",\"ID_CHAROLA\",\"ID_ESPIRAL\",\"ID_SELECCION\",\"ID_ARTICULO\",\"ACTUAL\",\"ANTERIOR\") VALUES(\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%f\",\"%f\")",nIdInstancia,nIdCharola,nIdEspiral,nIdSeleccion,nIdArticulo,dActual,dAnterior];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

@end
