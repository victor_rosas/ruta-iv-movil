//
//  VC_Instancias.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Instancias.h"
#import "SWTableViewCell.h"
#import "spGenerales.h"
#import "UMTableViewCell.h"
#import "UMPruebaTableViewCell.h"
#import "NextViewController.h"
#import "Hora_inicioDAO.h"
#import "TVC_Inventario_Carga_Actual.h"
#import "ProductosDAO.h"
#import "VC_Mapa.h"
#import "Reachability.h"
#import "VC_Mapa_Unico.h"
#import "VC_Observaciones.h"
#import "VC_Cambio_Precio.h"
#import "DATA_Cambio.h"
#import "VC_Cambio_Planograma.h"
#import "VC_Insumos.h"
#import "VC_Devolucion_Monedas.h"
#import "VC_Devoluciones_Especie.h"

@interface VC_Instancias (){
    NSDictionary *animals;
    NSArray *animalSectionTitles;
    NSArray *_sections;
    NSMutableArray *_testArray;
    NSMutableDictionary *_SitiosAsignados;
    NSString *nInstancia, *nSitio;
    NSInteger instancia_segue;
}

;
@property (nonatomic) BOOL useCustomCells;
@property (nonatomic, weak) UIRefreshControl *refreshControl;

@end

@implementation VC_Instancias
@synthesize tableView2;

- (void)viewDidLoad {
    Reachability *reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    reachable =reach.isReachable;
    [super viewDidLoad];
    spGenerales *generales = [spGenerales alloc];
    cabecerooperaciones =[CabeceroOperaciones alloc];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [generales sp_RevisaFinInstancias:[[prefs stringForKey:@"nIdRuta"] intValue]];
    animals = [generales sp_revisaSitioPorRuta:[[prefs stringForKey:@"nIdRuta"] intValue] wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"]];
    NSLog(@"Sitios asignados = %lu",(unsigned long)animals.count);
    animalSectionTitles = [[animals allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    animalSectionTitles = [animalSectionTitles sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([obj1 intValue]==[obj2 intValue])
            return NSOrderedSame;
        
        else if ([obj1 intValue]<[obj2 intValue])
            return NSOrderedAscending;
        else
            return NSOrderedDescending;
        
    }];
    self.tableView2.rowHeight = 40;
    self.navigationItem.title = @"Ruta";
    
    dao = [[Hora_inicioDAO alloc]init];
    dao_producto = [[ProductosDAO alloc]init];
    data_manager_progreso = [[DATA_Progreso_Ruta alloc]init];
    
    // Do any additional setup after loading the view.
    imagenes = [imagendao obtenerImagenes];
    NSInteger pendientes = [imagenes count];
    if(pendientes < 0){
        //NSString * var = [cabecerooperaciones newvarmedida];
       // NSString * full = [cabecerooperaciones fullstring:var];
    }
    
    //PRUEBA
    //ExistenciasDAO * dao_meh = [[ExistenciasDAO alloc]init];
    //[dao_meh update_todas_existencias:10];
    
}

/*-(void)isreachableobserver{
    
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [reach startNotifier];
    
}*/

-(void)viewWillAppear:(BOOL)animated{
    arreglo_visitadas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    arreglo_salteadas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_salteadas"]];
    primer_blanco = true;
    [tableView2 reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return [animalSectionTitles count];
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    spGenerales *generales = [spGenerales alloc];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:section];
    NSString *sections = [generales traeSitio:sectionTitle];
    
    return sections;
}

- (void) tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    // Background color
    view.tintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:55.0/255.0f green:123.0/255.0f blue:250.0/255.0f alpha:1.0]
                                                icon:[UIImage imageNamed:@"103-map_white.png"]];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:242.0/255.0f green:215.0/255.0f blue:41.0/255.0f alpha:1.0]
                                                icon:[UIImage imageNamed:@"216-compose_white.png"]];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.0/255.0f green:153.0/255.0f blue:76.0/255.0f alpha:1.0]
                                                icon:[UIImage imageNamed:@"02-redo.png"]];
    return leftUtilityButtons;
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath * index_path_selected = [tableView2 indexPathForCell:cell];
    UMPruebaTableViewCell *celda = (UMPruebaTableViewCell *)[tableView2 cellForRowAtIndexPath:index_path_selected];
    VC_Mapa_Unico *vc_mapa = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_mapa_unico"];
    VC_Observaciones * vc_observaciones = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_observaciones_combo"];
    VC_Devolucion_Monedas * vc_devolucion_monedas = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devolucion_Moneda"];
    VC_Devoluciones_Especie * vc_devolucion_especie =[self.storyboard instantiateViewControllerWithIdentifier:@"VC_Devolucion_Especie"];
    
    spGenerales *generales = [spGenerales alloc];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:index_path_selected.section];
    NSString *sitio_completo = [generales traeSitio:sectionTitle];
    
    NSString * sitio_espacio = [[sitio_completo componentsSeparatedByString:@"."] objectAtIndex:1];
    NSString * sitio = [sitio_espacio substringWithRange:NSMakeRange(1, sitio_espacio.length - 1)];

    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *instancia = [sectionAnimals objectAtIndex:index_path_selected.row];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:instancia forKey:@"instancia"];
    [prefs synchronize];
    
    NSString * orden_visita_seleccionado = celda.label_orden.text;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:orden_visita_seleccionado forKey:@"orden_visita_seleccionado"];
    [userDefaults synchronize];
    
    switch (index) {
        case 0:
            vc_mapa.navigationItem.title = @"Localización";
            vc_mapa.sitio = sitio;
            vc_mapa.instancia = instancia;
            [self.navigationController pushViewController:vc_mapa animated:YES];
            break;
        case 1:
            vc_observaciones.instancia_observaciones = (uint32_t)[instancia integerValue];
            [self.navigationController pushViewController:vc_observaciones animated:YES];
            break;
            
        case 2:
        {
            UIAlertController * view=   [UIAlertController
                                         alertControllerWithTitle:@"Continuar"
                                         message:@"Seleccionar una opción."
                                         preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction* Dinero = [UIAlertAction
                                      actionWithTitle:@"Devolucion Dinero"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action)
                                      {
                                          vc_devolucion_monedas.id_instancia = [instancia integerValue];
                                          [self.navigationController pushViewController:vc_devolucion_monedas animated:YES];
                                          [view dismissViewControllerAnimated:YES completion:nil];
                                          
                                      }];
            NSString * tipo_maquina = celda.Label.text;
            if(![tipo_maquina isEqualToString:@"- Café"])
            {
                UIAlertAction* especie = [UIAlertAction
                                          actionWithTitle:@"Devolucion especie"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action)
                                          {
                                              
                                              vc_devolucion_especie.id_instancia = [instancia integerValue];
                                              [self.navigationController pushViewController:vc_devolucion_especie animated:YES];
                                              [view dismissViewControllerAnimated :YES completion:nil];
                                              
                                          }];
                 [view addAction:especie];
            }
           
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancelar"
                                     style:UIAlertActionStyleDestructive
                                     handler:^(UIAlertAction * action)
                                     {
                                         [view dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            
            [view addAction:Dinero];
            [view addAction:cancel];
            [self presentViewController:view animated:YES completion:nil];
        }
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //UMPruebaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UMPruebaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    spGenerales *generales = [spGenerales alloc];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    cell.delegate = self;
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    cell.label_orden.text = sectionTitle;
    
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    //NSString *sResult = [generales sp_revisaSitioPorRuta:[[prefs stringForKey:@"nIdRuta"] intVfinalue] wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"]];
    NSMutableArray *busqueda_instancias = [generales traeInstancia:[[prefs stringForKey:@"nIdRuta"] intValue]  wIdOrdenServicio:[prefs stringForKey:@"nIdOrdenServicio"] wIdOrden:sectionTitle wIdInstancia:animal];
    
    NSString *sResult = [busqueda_instancias objectAtIndex:0];
    NSString *tipo_maquina = [self tipoMaquina:[busqueda_instancias objectAtIndex:1]];

    [cell setLeftUtilityButtons:[self leftButtons] WithButtonWidth:50.0];
    
    BOOL primera_visita = [dao_producto verificacion_primera_visita:[animal integerValue]];
    if (primera_visita) {
        cell.label_nueva.hidden = false;
    }
    else
    {
        cell.label_nueva.hidden = true;
    }
    

    NSArray *Array = [sResult componentsSeparatedByString:@"-"];
    NSString *sitio = [Array objectAtIndex:0];
    NSString *sResult2 = [Array objectAtIndex:1];
    Array = [sResult2 componentsSeparatedByString:@"("];
    //NSString *maquina = [Array objectAtIndex:0];
    NSString *IS0 = [Array objectAtIndex:1];
    IS0 = [IS0 substringToIndex:[IS0 length] - 2];
    BOOL pintar_blanco = true;
    
    NSString *instancia_orden = [[NSUserDefaults standardUserDefaults] objectForKey:@"configuracion_instancia_orden"];
    
    //Visitar en orden
    if([instancia_orden isEqualToString:@"1"])
    {
        NSString * instancia_visitada;
        for (int i=0; i<20; i++) {
            
            instancia_visitada = [arreglo_visitadas objectAtIndex:i];
            if ([instancia_visitada isEqualToString:[NSString stringWithFormat:@"%@%@",animal,cell.label_orden.text]])
            {
                cell.backgroundColor = [UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6];
                cell.label_continuar.text =  @"0";
                pintar_blanco = false;
                break;
            }
            else{
                pintar_blanco = true;
            }
        }
        if (pintar_blanco) {
            cell.backgroundColor = [UIColor whiteColor];
            if (primer_blanco) {
                cell.label_continuar.text = @"1";
                primer_blanco = false;
                activa = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
            }
            else
            {
                cell.label_continuar.text = @"0";
            }
            
            if ([indexPath isEqual:activa]) {
                cell.label_continuar.text = @"1";
            }
        }
    }
    //no necesario visitar en orden
    else
    {
        NSString * instancia_visitada;
        for (int i=0; i<20; i++) {
            
            instancia_visitada = [arreglo_visitadas objectAtIndex:i];
            
            if ([instancia_visitada isEqualToString:[NSString stringWithFormat:@"%@%@",animal,cell.label_orden.text]])
            {
                cell.backgroundColor = [UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6];
                cell.label_continuar.text = @"0";
                pintar_blanco = false;
                break;
            }
            else{
                pintar_blanco = true;
            }
        }
        if (pintar_blanco) {
            cell.backgroundColor = [UIColor whiteColor];
            if (primer_blanco) {
                cell.label_continuar.text = @"1";
                primer_blanco = false;
                activa = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
            }
            else
            {
                cell.label_continuar.text = @"1";
            }
            
            if ([indexPath isEqual:activa]) {
                cell.label_continuar.text = @"1";
            }
        }
    }
    
    for (int i=0; i<20; i++) {
        
        NSString *instancia_salteada = [arreglo_salteadas objectAtIndex:i];
        
        if ([instancia_salteada isEqualToString:[NSString stringWithFormat:@"%@%@",animal,cell.label_orden.text]])
        {
            cell.backgroundColor = [UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0];//[UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6];
            cell.label_continuar.text = @"0";
            break;
        }
    }
    
    cell.label_sitio.text = sitio;
    cell.label_codigo.text = IS0;
    cell.Label.text = tipo_maquina;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:tipo_maquina forKey:@"tipo_maquina"];
    [userDefaults synchronize];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    return cell;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UMPruebaTableViewCell *cell = (UMPruebaTableViewCell *)[tableView2 cellForRowAtIndexPath:indexPath];
    
    NSString * orden_visita_seleccionado = cell.label_orden.text;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:orden_visita_seleccionado forKey:@"orden_visita_seleccionado"];
    [userDefaults synchronize];
    
    if ([cell.backgroundColor isEqual:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0]]) {
        if (indexPath.row == 0) {
            UITableViewRowAction *button3 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Volver Sitio" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                [self autorizar_ruta_volver:indexPath];
                                            }];
            button3.backgroundColor = [UIColor greenColor]; //arbitrary color
            UITableViewRowAction *button4 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Volver" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 [self autorizar_instancia_volver:indexPath];
                                             }];
            button4.backgroundColor = [UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0]; //arbitrary color
            
            return @[button3, button4]; //array with all the buttons you want. 1,2,3, etc...
        }
        else
        {
            UITableViewRowAction *button3 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Volver" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                [self autorizar_instancia_volver:indexPath];
                                            }];
            button3.backgroundColor = [UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0]; //arbitrary color
            return @[button3];
        }
    }
    else if([cell.backgroundColor isEqual:[UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6]])
    {
        return @[];
    }
    else
    {
        if (indexPath.row == 0) {
            UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Saltar Sitio" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                [self autorizar_ruta:indexPath];
                                            }];
            button.backgroundColor = [UIColor redColor]; //arbitrary color
            UITableViewRowAction *button2 = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Saltar" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 [self autorizar_instancia:indexPath];
                                             }];
            button2.backgroundColor = [UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0]; //arbitrary color
            
            return @[button, button2]; //array with all the buttons you want. 1,2,3, etc...
        }
        else
        {
            UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Saltar" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                            {
                                                [self autorizar_instancia:indexPath];
                                            }];
            button.backgroundColor = [UIColor colorWithRed:170.0/255.0f green:170.0/255.0f blue:170.0/255.0f alpha:1.0]; //arbitrary color
            return @[button];
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:

    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    UMPruebaTableViewCell *cell = (UMPruebaTableViewCell *)[tableView2 cellForRowAtIndexPath:indexPath];
    if ([cell.backgroundColor isEqual:[UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6]]) {
        return NO;
    }
    else
    {
        return YES; //tableview must be editable or nothing will work...
    }
}

//Codigo de inicio de autorizacion de instancia, extraccion de codigo
-(void)autorizar_instancia:(NSIndexPath *)indexPath
{
    NSString *orden_servicio = [[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"];
    int orden = (uint32_t)[orden_servicio integerValue];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    int instancia = (uint32_t)[animal integerValue];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    NSString *codigo = [dao_producto obtener_codigo_autorizacion:instancia orden_servicio:orden orden_visita:[orden_visita_instancia integerValue]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Autorizar Instancia"
                                          message:[NSString stringWithFormat:@"Introduzca el código de autorización Instancia %d.", instancia]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Código Autorización", @"Código");
         [textField setKeyboardType:UIKeyboardTypeNumberPad];
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"Aceptar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UITextField *password = alertController.textFields.lastObject;
                             [self revision_password:password.text password_verdadero:codigo instancia:instancia orden:sectionTitle];
                         }];
    [alertController addAction:OK];
    [alertController addAction:Cancelar];
    [self presentViewController:alertController animated:YES completion:nil];
}

//Codigo de inicio de autorizacion de sitio, extraccion de codigo
-(void)autorizar_ruta:(NSIndexPath *)indexPath
{
    NSString *orden_servicio = [[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"];
    int orden = (uint32_t)[orden_servicio integerValue];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    int instancia = (uint32_t)[animal integerValue];
    
    spGenerales *generales = [spGenerales alloc];
    NSString * sections = [generales traeSitio:sectionTitle];
    NSString * seccion = [sections componentsSeparatedByString:@"."][1];
    seccion = [seccion capitalizedString];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    NSString *codigo = [dao_producto obtener_codigo_autorizacion_sitio:instancia orden_servicio:orden orden_visita:[orden_visita_instancia integerValue]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Autorizar Sitio"
                                          message:[NSString stringWithFormat:@"Introduzca el código de autorización Sitio%@.", seccion]
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Código Autorización", @"Código");
         [textField setKeyboardType:UIKeyboardTypeNumberPad];
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"Aceptar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UITextField *password = alertController.textFields.lastObject;
                             [self revision_password_sitio:password.text password_verdadero:codigo indexPath:indexPath];
                         }];
    [alertController addAction:OK];
    [alertController addAction:Cancelar];
    [self presentViewController:alertController animated:YES completion:nil];
}

//Codigo de inicio de retorno de instancia, extraccion de codigo
-(void)autorizar_instancia_volver:(NSIndexPath *)indexPath
{
    NSString *orden_servicio = [[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"];
    int orden = (uint32_t)[orden_servicio integerValue];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    int instancia = (uint32_t)[animal integerValue];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    NSString *codigo = [dao_producto obtener_codigo_revisita_instancia:instancia orden_servicio:orden orden_visita:[orden_visita_instancia integerValue]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Revisitar Instancia"
                                          message:@"Introduzca el código de autorización."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Código Autorización", @"Código");
         [textField setKeyboardType:UIKeyboardTypeNumberPad];
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"Aceptar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UITextField *password = alertController.textFields.lastObject;
                             [self revision_password_volver:password.text password_verdadero:codigo instancia:instancia orden:sectionTitle];
                         }];
    [alertController addAction:OK];
    [alertController addAction:Cancelar];
    [self presentViewController:alertController animated:YES completion:nil];
}

//Codigo de inicio de retorno de sitio, extraccion de codigo
-(void)autorizar_ruta_volver:(NSIndexPath *)indexPath
{
    NSString *orden_servicio = [[NSUserDefaults standardUserDefaults] objectForKey:@"nIdOrdenServicio"];
    int orden = (uint32_t)[orden_servicio integerValue];
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    int instancia = (uint32_t)[animal integerValue];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString * orden_visita_instancia = [prefs stringForKey:@"orden_visita_seleccionado"];
    NSString *codigo = [dao_producto obtener_codigo_revisita_sitio:instancia orden_servicio:orden orden_visita:[orden_visita_instancia integerValue]];
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Revisitar Sitio"
                                          message:@"Introduzca el código de autorización."
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = NSLocalizedString(@"Código Autorización", @"Código");
         [textField setKeyboardType:UIKeyboardTypeNumberPad];
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction* Cancelar = [UIAlertAction
                               actionWithTitle:@"Cancelar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction* OK = [UIAlertAction
                         actionWithTitle:@"Aceptar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             UITextField *password = alertController.textFields.lastObject;
                             [self revision_password_sitio_volver:password.text password_verdadero:codigo indexPath:indexPath];
                         }];
    [alertController addAction:OK];
    [alertController addAction:Cancelar];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(NSString *)tipoMaquina:(NSString*)codigo_maquina{
    int id_maquina = (uint32_t)[codigo_maquina integerValue];
    switch (id_maquina) {
        case 2:
            return @"- Bebida";
            break;
        case 3:
            return @"- Snack";
            break;
        case 4:
            return @"- Combo";
            break;
        case 13:
            return @"- Café";
        default:
            return [NSString stringWithFormat:@"%d", id_maquina];
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UMPruebaTableViewCell *cell = (UMPruebaTableViewCell *)[tableView2 cellForRowAtIndexPath:indexPath];
    if ([cell.label_continuar.text isEqualToString:@"0"]) {
        if([cell.backgroundColor isEqual:[UIColor colorWithRed:250.0/255.0f green:244.0/255.0f blue:205.0/255.0f alpha:0.6]])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Atención"
                                          message:@"Esta instancia ya fue visitada."
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [tableView2 deselectRowAtIndexPath:indexPath animated:YES];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if([cell.backgroundColor isEqual:[UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0]])
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Atención"
                                          message:@"Instancia autorizada."
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [tableView2 deselectRowAtIndexPath:indexPath animated:YES];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Atención"
                                          message:@"Visitar las instancias en orden."
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Ok"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     [tableView2 deselectRowAtIndexPath:indexPath animated:YES];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else{
        NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
        NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
        NSInteger instancia = [animal integerValue];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:@(instancia) forKey:@"instancia"];
        [prefs synchronize];
        instancia_segue = instancia;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSString *dateString=[dateFormat stringFromDate:[NSDate date]];
        
        //NSString *algo = [NSString stringWithFormat:@"INSTANCIA: %@ SITIO:%@",animal,sectionTitle];
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sitio" message:algo delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //[alert show];
        
        if (indexPath.row == 0) {
            //Llegada al sitio, guardar hora de llegada a sitio
            //NSLog(@"%@", dateString);
            if (ReachableViaWiFi) {
                horaenviada = 1;
            }
            else{horaenviada = 0;}
            
            
           NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
            diccionario_informacion =[data_manager_progreso SELECT_datos_progreso:(uint32_t)instancia];
            
            //NSString * id_orden_servicio = [diccionario_informacion objectForKey:@"id_orden_servicio"];
            //NSString * orden = [diccionario_informacion objectForKey:@"orden"];
            //NSString * orden_visita = [diccionario_informacion objectForKey:@"orden_visita"];
            NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];
            NSInteger orden_servicio = [cell.label_orden.text integerValue];
            [dao ActualizaHora_Inicio:instancia ID_OPERACION:1 HORA_INICIO:dateString HORA_INICIO_ENVIADA:horaenviada id_sitio:[id_sitio integerValue] orden_visita:orden_servicio];
            
            NSMutableArray * arreglo_instancias_sitio = [data_manager_progreso SELECT_instancias_sitio:[id_sitio integerValue]];
            for (int j = 0; j<[arreglo_instancias_sitio count]; j++) {
                
                NSString * instancia_sitio = [arreglo_instancias_sitio objectAtIndex:j];
                NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
                diccionario_informacion = [data_manager_progreso SELECT_datos_progreso:(uint32_t)[instancia_sitio integerValue]];
                
                NSString * id_orden_servicio = [diccionario_informacion objectForKey:@"id_orden_servicio"];
                NSString * orden = [diccionario_informacion objectForKey:@"orden"];
                NSString * orden_visita = [diccionario_informacion objectForKey:@"orden_visita"];
                //NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];
                
                [data_manager_progreso mandar_hora:(uint32_t)[id_orden_servicio integerValue] orden:(uint32_t)[orden integerValue] orden_visita:(uint32_t)[orden_visita integerValue] id_instancia:(uint32_t)[instancia_sitio integerValue] id_operacion:1];
            }
            
        }
        //Llegada a instancia
        //NSLog(@"%@", dateString);
        
        //[dao ActualizaHora_Inicio:instancia ID_OPERACION:1 HORA_INICIO:dateString HORA_INICIO_ENVIADA:horaenviada];
        
       NSMutableDictionary * diccionario_informacion = [[NSMutableDictionary alloc]init];
        diccionario_informacion = [data_manager_progreso SELECT_datos_progreso:(uint32_t)instancia];
        
        NSString * id_orden_servicio = [diccionario_informacion objectForKey:@"id_orden_servicio"];
        NSString * orden = [diccionario_informacion objectForKey:@"orden"];
        NSString * orden_visita = [diccionario_informacion objectForKey:@"orden_visita"];
        NSString * id_sitio = [diccionario_informacion objectForKey:@"id_sitio"];
        
        NSString * orden_visita_seleccionado = cell.label_orden.text;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:orden_visita_seleccionado forKey:@"orden_visita_seleccionado"];
        [userDefaults synchronize];
        
        NSInteger orden_servicio = [cell.label_orden.text integerValue];
        [dao ActualizaHora_Inicio:instancia ID_OPERACION:2 HORA_INICIO:dateString HORA_INICIO_ENVIADA:horaenviada id_sitio:[id_sitio integerValue] orden_visita:orden_servicio];
       [data_manager_progreso mandar_hora:(uint32_t)[id_orden_servicio integerValue] orden:(uint32_t)[orden integerValue] orden_visita:(uint32_t)[orden_visita integerValue] id_instancia:(uint32_t)instancia id_operacion:2];
        
        if (indexPath.row == [tableView2 numberOfRowsInSection:indexPath.section] -1 ) {
            salida_sitio = true;
            //NSLog(@"true");
        }
        else
        {
            salida_sitio = false;
        }
        
        //Revision si hay cambio de precio o maquina de cafe
        DATA_Cambio * data_cambio_manager = [[DATA_Cambio alloc]init];
        NSMutableArray * arreglo_precios = [data_cambio_manager cargar_cambio_precio:(uint32_t)instancia];
        NSMutableArray * arreglo_planogramas = [data_cambio_manager cargar_cambio_planograma:(uint32_t)instancia];
        NSString * tipo_maquina = cell.Label.text;
        
        if ([arreglo_precios count] > 0) {
            
            VC_Cambio_Precio * vc_precio = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_precio"];
            vc_precio.id_instancia = (uint32_t)instancia_segue;
            vc_precio.salida_sitio = salida_sitio;
            [self.navigationController pushViewController:vc_precio animated:YES];
        }
        else if ([arreglo_planogramas count] > 0 )
        {
            VC_Cambio_Planograma * vc_planograma = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_cambio_planograma"];
            vc_planograma.id_instancia = (uint32_t)instancia_segue;
            vc_planograma.salida_ruta = salida_sitio;
            [self.navigationController pushViewController:vc_planograma animated:YES];
        }
        else if ([tipo_maquina isEqualToString:@"- Café"])
        {
            
            VC_Insumos * vc_insumos = [self.storyboard instantiateViewControllerWithIdentifier:@"VC_Insumos"];
            vc_insumos.id_instancia = (uint32_t)instancia_segue;
            vc_insumos.salida_sitio = salida_sitio;
            [self.navigationController pushViewController:vc_insumos animated:YES];
        }
        else
        {
            [self performSegueWithIdentifier:@"inventario" sender:self];
        }
    }
}


//METODO QUE SE ENCARGA DE GUARDAR LOS CAMBIOS QUE SE REALIZAN CUANDO UNA INSTANCIA ES TERMINADA
-(void)instancia_salteada:(NSString *)instancia orden_visita:(NSString *)orden_visita
{
    //Se hace lo mismo que en visitadas pero con un nuevo arreglo llamado salteadas
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    int numero_visitadas= (uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_visitadas"] integerValue];
    int numero_salteadas= (uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_salteadas"] integerValue];
    
    NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    NSMutableArray *saltos = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_salteadas"]];
    
    NSString * obj_instancia = [NSString stringWithFormat:@"%@%@",instancia,orden_visita];
    
    [visitas replaceObjectAtIndex:numero_visitadas withObject:obj_instancia];
    [saltos replaceObjectAtIndex:numero_salteadas withObject:obj_instancia];
    
    numero_visitadas++;
    numero_salteadas++;
    
    NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
    NSArray *saltos_inmutable = [NSArray arrayWithArray:saltos];
    
    [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
    [userDefaults setObject:saltos_inmutable forKey:@"instancias_salteadas"];
    
    [userDefaults setObject:[NSNumber numberWithInt:numero_visitadas] forKey:@"posicion_actual_instancias_visitadas"];
    [userDefaults setObject:[NSNumber numberWithInt:numero_salteadas] forKey:@"posicion_actual_instancias_salteadas"];
    [userDefaults synchronize];
    
    [dao_producto actualizar_salteado:1 instancia:[instancia integerValue]];
    
}

//METODO QUE SE ENCARGA DE GUARDAR LOS CAMBIOS QUE SE REALIZAN CUANDO UNA INSTANCIA ES TERMINADA EN TODAS LAS INSTANCIAS DE EL SITIO
-(void)sitio_salteado:(NSIndexPath *)indexPath
{
    int numberOfRows = (uint32_t)[tableView2 numberOfRowsInSection:[indexPath section]];
    for (int i = 0; i<numberOfRows; i++) {
        NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
        NSString *animal = [sectionAnimals objectAtIndex:i];
        int instancia = (uint32_t)[animal integerValue];
        
        //Metodo cuando se extrae la instancia en el ciclo
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        int numero_visitadas= (uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_visitadas"] integerValue];
        int numero_salteadas= (uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:@"posicion_actual_instancias_salteadas"] integerValue];
        
        NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
        NSMutableArray *saltos = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_salteadas"]];
        
        NSString * obj_instancia = [NSString stringWithFormat:@"%@%@",animal,sectionTitle];
        
        [visitas replaceObjectAtIndex:numero_visitadas withObject:obj_instancia];
        [saltos replaceObjectAtIndex:numero_salteadas withObject:obj_instancia];
        
        numero_visitadas++;
        numero_salteadas++;
        
        NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
        NSArray *saltos_inmutable = [NSArray arrayWithArray:saltos];
        
        [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
        [userDefaults setObject:saltos_inmutable forKey:@"instancias_salteadas"];
        
        [userDefaults setObject:[NSNumber numberWithInt:numero_visitadas] forKey:@"posicion_actual_instancias_visitadas"];
        [userDefaults setObject:[NSNumber numberWithInt:numero_salteadas] forKey:@"posicion_actual_instancias_salteadas"];
        [userDefaults synchronize];
        
        [dao_producto actualizar_salteado:1 instancia:instancia];
    }
    
    
}

//METODO QUE SE ENCARGA DE GUARDAR LOS CAMBIOS QUE SE REALIZAN CUANDO UNA INSTANCIA SE VUELVE A ABRIR
-(void)instancia_salteada_volver:(NSString *)instancia orden_visita:(NSString *)orden_visita
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
    NSMutableArray *saltos = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_salteadas"]];
    
    for (int i = 0; i<[visitas count]; i++) {
        NSString * obj_instancia = [visitas objectAtIndex:i];
        if ([obj_instancia isEqualToString:[NSString stringWithFormat:@"%@%@",instancia, orden_visita]])
        {
            NSString * obj_cero = @"0";
            [visitas replaceObjectAtIndex:i withObject:obj_cero];
        }
    }
    
    for (int i = 0; i<[saltos count]; i++) {
        NSString * obj_instancia = [saltos objectAtIndex:i];
        if ([obj_instancia isEqualToString:[NSString stringWithFormat:@"%@%@",instancia, orden_visita]])
        {
            NSString * obj_cero = @"0";
            [saltos replaceObjectAtIndex:i withObject:obj_cero];
        }
    }
    
    NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
    NSArray *saltos_inmutable = [NSArray arrayWithArray:saltos];
    
    [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
    [userDefaults setObject:saltos_inmutable forKey:@"instancias_salteadas"];
    [userDefaults synchronize];
    
    [dao_producto actualizar_salteado:0 instancia:[instancia integerValue]];
}

//METODO QUE SE ENCARGA DE GUARDAR LOS CAMBIOS QUE SE REALIZAN CUANDO UNA INSTANCIA SE VUELVE A ABRIR EN TODAS LAS INSTANCIAS DE EL SITIO
-(void)sitio_salteado_volver:(NSIndexPath *)indexPath
{
    int numberOfRows = (uint32_t)[tableView2 numberOfRowsInSection:[indexPath section]];
    for (int i = 0; i<numberOfRows; i++) {
        NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
        NSString *animal = [sectionAnimals objectAtIndex:i];
        int instancia = (uint32_t)[animal integerValue];
        
        //Metodo cuando se extrae la instancia en el ciclo
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray *visitas = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_visitadas"]];
        NSMutableArray *saltos = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"instancias_salteadas"]];
        
        for (int i = 0; i<[visitas count]; i++) {
            NSString * obj_instancia = [visitas objectAtIndex:i];
            if ([obj_instancia isEqualToString:[NSString stringWithFormat:@"%@%@",animal, sectionTitle]])
            {
                NSString * obj_cero = @"0";
                [visitas replaceObjectAtIndex:i withObject:obj_cero];
            }
        }
        
        for (int i = 0; i<[saltos count]; i++) {
            NSString * obj_instancia = [saltos objectAtIndex:i];
            if ([obj_instancia isEqualToString:[NSString stringWithFormat:@"%@%@",animal, sectionTitle]])
            {
                NSString * obj_cero = @"0";
                [saltos replaceObjectAtIndex:i withObject:obj_cero];
            }
        }
        
        NSArray *visitas_inmutable = [NSArray arrayWithArray:visitas];
        NSArray *saltos_inmutable = [NSArray arrayWithArray:saltos];
        
        [userDefaults setObject:visitas_inmutable forKey:@"instancias_visitadas"];
        [userDefaults setObject:saltos_inmutable forKey:@"instancias_salteadas"];
        [userDefaults synchronize];
        
        [dao_producto actualizar_salteado:0 instancia:instancia];
    }
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"segue_observaciones"])
    {
        
    }
    else if ([[segue identifier] isEqualToString:@"inventario"])
    {
        NextViewController *controller = (NextViewController *)segue.destinationViewController;
        NSLog(@"%d",(uint32_t)instancia_segue);
        controller.instancia = (uint32_t)instancia_segue;
        controller.salida_sitio = salida_sitio;
    }
    else
    {
        
    }
}

//REVISION DE LA CONTRASEñA CUANDO SE SALTA UNA INSTANCIA
-(void)revision_password :(NSString *)password password_verdadero:(NSString *)password_correcto instancia:(int)instancia orden:(NSString *)orden
{
    if ([password isEqualToString:@"1"]) {
        UIAlertController * alert_correcto=   [UIAlertController
                                               alertControllerWithTitle:@"Autorización"
                                               message:@"Autorización correcta."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_correcto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_correcto addAction:ok];
        [self presentViewController:alert_correcto animated:YES completion:nil];
        [self instancia_salteada:[NSString stringWithFormat:@"%d",instancia] orden_visita:orden];
        [self viewDidLoad];
        [self viewWillAppear:true];
    }
    else
    {
        UIAlertController * alert_incorrecto=   [UIAlertController
                                                 alertControllerWithTitle:@"Autorización"
                                                 message:@"Código incorrecto."
                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_incorrecto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_incorrecto addAction:ok];
        [self presentViewController:alert_incorrecto animated:YES completion:nil];
    }
}

//REVISION DE LA CONTRASEÑA CUANDO SE SALTA UN SITIO
-(void)revision_password_sitio :(NSString *)password password_verdadero:(NSString *)password_correcto indexPath:(NSIndexPath *)index
{
    if ([password isEqualToString:@"1"]) {
        UIAlertController * alert_correcto=   [UIAlertController
                                               alertControllerWithTitle:@"Autorización"
                                               message:@"Autorización correcta."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_correcto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_correcto addAction:ok];
        [self presentViewController:alert_correcto animated:YES completion:nil];
        [self sitio_salteado:index];
        [self viewDidLoad];
        [self viewWillAppear:true];
        
        //Agregar pendientes
    }
    else
    {
        UIAlertController * alert_incorrecto=   [UIAlertController
                                                 alertControllerWithTitle:@"Autorización"
                                                 message:@"Código incorrecto."
                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_incorrecto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_incorrecto addAction:ok];
        [self presentViewController:alert_incorrecto animated:YES completion:nil];
    }
    
}

//REVISION DE LA CONTRASEñA CUANDO SE VUELVE UNA INSTANCIA
-(void)revision_password_volver :(NSString *)password password_verdadero:(NSString *)password_correcto instancia:(int)instancia orden:(NSString*)orden
{
    if ([password isEqualToString:@"1"]) {
        UIAlertController * alert_correcto=   [UIAlertController
                                               alertControllerWithTitle:@"Autorización"
                                               message:@"Autorización correcta."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_correcto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_correcto addAction:ok];
        [self presentViewController:alert_correcto animated:YES completion:nil];
        [self instancia_salteada_volver:[NSString stringWithFormat:@"%d",instancia] orden_visita:orden];
        [self viewDidLoad];
        [self viewWillAppear:true];
    }
    else
    {
        UIAlertController * alert_incorrecto=   [UIAlertController
                                                 alertControllerWithTitle:@"Autorización"
                                                 message:@"Código incorrecto."
                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_incorrecto dismissViewControllerAnimated:YES completion:nil];
                             }]; 
        [alert_incorrecto addAction:ok];
        [self presentViewController:alert_incorrecto animated:YES completion:nil];
    }
}

//REVISION DE LA CONTRASEÑA CUANDO SE VUELVE UN SITIO
-(void)revision_password_sitio_volver :(NSString *)password password_verdadero:(NSString *)password_correcto indexPath:(NSIndexPath *)index
{
    if ([password isEqualToString:@"1"]) {
        UIAlertController * alert_correcto=   [UIAlertController
                                               alertControllerWithTitle:@"Autorización"
                                               message:@"Autorización correcta."
                                               preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_correcto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_correcto addAction:ok];
        [self presentViewController:alert_correcto animated:YES completion:nil];
        [self sitio_salteado_volver:index];
        [self viewDidLoad];
        [self viewWillAppear:true];
    }
    else
    {
        UIAlertController * alert_incorrecto=   [UIAlertController
                                                 alertControllerWithTitle:@"Autorización"
                                                 message:@"Código incorrecto."
                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert_incorrecto dismissViewControllerAnimated:YES completion:nil];
                             }];
        [alert_incorrecto addAction:ok];
        [self presentViewController:alert_incorrecto animated:YES completion:nil];
    }
    
}

- (IBAction)action_back_button:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)action_inventario_pressed:(id)sender {
    TVC_Inventario_Carga_Actual *vInventarioActual = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_carga_actual"];
    vInventarioActual.navigationItem.title = @"Inventario Actual";
    [self.navigationController pushViewController:vInventarioActual animated:YES];
}

- (IBAction)action_map_pressed:(id)sender {
    VC_Mapa *vc_mapa = [self.storyboard instantiateViewControllerWithIdentifier:@"vc_mapa"];
    vc_mapa.navigationItem.title = @"Localización";
    [self.navigationController pushViewController:vc_mapa animated:YES];
}
@end
