//
//  VC_Observaciones_Tabla.m
//  RutaIV
//
//  Created by Miguel Banderas on 26/02/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "VC_Observaciones_Tabla.h"
#import "VC_Observacion_Detalle.h"

@interface VC_Observaciones_Tabla ()

@end

@implementation VC_Observaciones_Tabla

- (void)viewDidLoad {
    [super viewDidLoad];
    //inicializacion de variables
    data_observaciones_manager = [[DATA_Observaciones alloc]init];
    array_mutable_observaciones = [[NSMutableArray alloc]init];
    diccionario_observaciones_completo = [[NSMutableDictionary alloc]init];
    
    //Cargado de informacion
    array_mutable_observaciones = [data_observaciones_manager traer_observaciones];
    diccionario_observaciones_completo= [self crear_diccionario_headers:array_mutable_observaciones];
    
    [Tableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//Creacion de diccionario con headers
-(NSMutableDictionary *)crear_diccionario_headers:(NSMutableArray *)arreglo_mutable_observaciones
{
    NSMutableDictionary * diccionario_observaciones = [[NSMutableDictionary alloc]init];
    
    for (int i = 0; i<[arreglo_mutable_observaciones count]; i++) {
        OBJ_Observaciones * observacion = [arreglo_mutable_observaciones objectAtIndex:i];
        NSString * sitio = [data_observaciones_manager trae_sitio:(uint32_t)[observacion.id_instancia integerValue]];
        
        //Revisar si el objeto ya existe
        NSMutableArray * arreglo_observaciones_en_diccionario = [[NSMutableArray alloc]init];
        arreglo_observaciones_en_diccionario = [diccionario_observaciones objectForKeyedSubscript:sitio];
        //No existe
        if (arreglo_observaciones_en_diccionario == nil) {
            //Se agrega como nuevo
            NSMutableArray * arreglo_observaciones_en_diccionario_nuevo = [[NSMutableArray alloc]init];
            [arreglo_observaciones_en_diccionario_nuevo addObject:observacion];
            [diccionario_observaciones setObject:arreglo_observaciones_en_diccionario_nuevo forKey:sitio];
        }
        else
        {
            //A el objeto existente se le agrega el elemento nuevo
            [arreglo_observaciones_en_diccionario addObject:observacion];
            [diccionario_observaciones setObject:arreglo_observaciones_en_diccionario forKey:sitio];
        }
    }
    return diccionario_observaciones;
}

//Aspecto de la tabla
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [diccionario_observaciones_completo count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray * arreglo_sitios = [[NSArray alloc]init];
    arreglo_sitios = [diccionario_observaciones_completo allKeys];
    return [arreglo_sitios objectAtIndex:section];
}

- (void) tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    // Background color
    view.tintColor = [UIColor colorWithRed:239.0/255.0f green:155.0/255.0f blue:6.0/255.0f alpha:1.0];
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor whiteColor]];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray * arreglo_sitios = [[NSArray alloc]init];
    NSMutableArray * arreglo_observaciones_temporal = [[NSMutableArray alloc]init];
    
    arreglo_sitios = [diccionario_observaciones_completo allKeys];
    arreglo_observaciones_temporal = [diccionario_observaciones_completo objectForKey:[arreglo_sitios objectAtIndex:section]];
    return [arreglo_observaciones_temporal count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TVC_Observaciones" forIndexPath:indexPath];
    NSArray * arreglo_sitios = [[NSArray alloc]init];
    NSMutableArray * arreglo_observaciones_temporal = [[NSMutableArray alloc]init];
    arreglo_sitios = [diccionario_observaciones_completo allKeys];
    arreglo_observaciones_temporal = [diccionario_observaciones_completo objectForKey:[arreglo_sitios objectAtIndex:indexPath.section]];
    
    OBJ_Observaciones * observacion = [arreglo_observaciones_temporal objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"Observación Instancia: %@",observacion.id_instancia];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

- (IBAction)action_back:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath * selected = [Tableview indexPathForSelectedRow];
    VC_Observacion_Detalle *vc = [segue destinationViewController];
    
    NSArray * arreglo_sitios = [[NSArray alloc]init];
    NSMutableArray * arreglo_observaciones_temporal = [[NSMutableArray alloc]init];
    arreglo_sitios = [diccionario_observaciones_completo allKeys];
    arreglo_observaciones_temporal = [diccionario_observaciones_completo objectForKey:[arreglo_sitios objectAtIndex:selected.section]];
    OBJ_Observaciones * observacion = [arreglo_observaciones_temporal objectAtIndex:selected.row];
    vc.navigationItem.title = [NSString stringWithFormat:@"Reporte Instancia: %@",observacion.id_instancia];
    vc.observacion = observacion;
}

@end
