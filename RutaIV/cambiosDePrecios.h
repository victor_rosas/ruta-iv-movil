//
//  cambiosDePrecios.h
//  RutaIV
//
//  Created by Miguel Banderas on 04/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cambiosDePrecios : NSObject

-(BOOL)borraCambioPrecios;

-(BOOL)insertaCambioPrecios:(int)nIdInstancia wIdCharola:(int)nIdCharola wIdEspiral:(int)nIdEspiral wIdSeleccion:(int)nIdSeleccion wIdArticulo:(int)nIdArticulo wdActual:(double)dActual wAnterior:(double)dAnterior;

@end
