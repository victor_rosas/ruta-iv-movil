//
//  DATA_Devoluciones.m
//  RutaIV
//
//  Created by Miguel Banderas on 18/03/15.
//  Copyright (c) 2015 Miguel Banderas. All rights reserved.
//

#import "DATA_Devoluciones.h"
#import "AppDelegate.h"

@implementation DATA_Devoluciones

-(NSString *) obtenerRutaBD
{
    NSLog(@"Entro a rutaBD");
    NSString *dirDocs;
    NSArray *rutas;
    NSString *rutaBD;
    
    rutas = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    dirDocs = [rutas objectAtIndex:0];
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    rutaBD = [[NSString alloc] initWithString:[dirDocs stringByAppendingPathComponent:@"Vending.sqlite"]];
    
    if([fileMgr fileExistsAtPath:rutaBD]== NO){
        [fileMgr copyItemAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Vending.sqlite"] toPath:rutaBD error:NULL];
        
    }
    return rutaBD;
}

-(BOOL)insert_contacto_devolucion_ruta:(NSString *)id_devolucion nombre:(NSString *)nombre telefono:(NSString *)telefono cantidad:(NSString *)cantidad
{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CONTACTO_DEVOLUCION_RUTA(\"ID_DEVOLUCION\",\"NOMBRE\",\"TELEFONO\",\"CANTIDAD\") VALUES(\"%@\",\"%@\",\"%@\",\"%@\")",id_devolucion, nombre, telefono, cantidad];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)insert_cab_devolucion_ruta:(NSString *)id_devolucion id_orden_servicio:(NSString *)id_orden_servicio id_instancia:(NSString *)instancia orden:(NSString *)orden_visita tipo:(NSString *)tipo
{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CAB_DEVOLUCION_RUTA(\"ID_DEVOLUCION\",\"ID_ORDEN_SERVICIO\",\"ID_INSTANCIA\",\"ORDEN_VISITA\",\"TIPO\") VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",id_devolucion, id_orden_servicio, instancia, orden_visita, tipo];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(BOOL)insert_det_devolucion_ruta:(NSString *)id_devolucion id_devuleto:(NSString *)devuelto cantidad:(NSString *)numero_cantidad charola:(NSString *)nombre_charola espiral:(NSString *)nombre_espiral inv_in:(NSString *)inventario_inicial
{
    BOOL bValida = FALSE;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO DET_DEVOLUCION_RUTA(\"ID_DEVOLUCION\",\"ID_DEVUELTO\",\"CANTIDAD\",\"CHAROLA\",\"ESPIRAL\",\"INV_INI\") VALUES(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",id_devolucion, devuelto, numero_cantidad, nombre_charola, nombre_espiral, inventario_inicial];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;
}

-(NSString *)select_id_devolucion
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString * id_devolucion;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MAX(ID_DEVOLUCION) FROM CONTACTO_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                char * maximo = (char *) sqlite3_column_text(sentencia,0);
                if (maximo != nil) {
                    id_devolucion = [NSString stringWithUTF8String:maximo];
                }
                else
                {
                    id_devolucion = @"0";
                }
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
    return id_devolucion;
}

-(NSString *)select_id_devolucion_det
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    NSString * id_devolucion;
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT MAX(ID_DEVOLUCION) FROM DET_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW) {
                char * maximo = (char *) sqlite3_column_text(sentencia,0);
                if (maximo != nil) {
                    id_devolucion = [NSString stringWithUTF8String:maximo];
                }
                else
                {
                    id_devolucion = @"0";
                }
                
            }
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
    return id_devolucion;
}

-(BOOL)delete_ultimo_contacto:(NSString *)id_devolucion
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CONTACTO_DEVOLUCION_RUTA WHERE ID_DEVOLUCION = %@", id_devolucion];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}


-(BOOL)delete_ultimo_cab:(NSString *)id_orden_servicio instancia:(NSString * )id_instancia orden_visita:(NSString *)id_orden_visita
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CAB_DEVOLUCION_RUTA WHERE ID_ORDEN_SERVICIO = %@ AND ID_INSTANCIA = %@ AND ORDEN_VISITA = %@", id_orden_servicio, id_instancia, id_orden_visita];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)delete_det_devolucion:(NSString *)id_devolucion
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM DET_DEVOLUCION_RUTA WHERE ID_DEVOLUCION = %@",id_devolucion];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(void)select_prueba_contacto
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CONTACTO_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW)
            {
                //NSString * ID_DEVOLUCION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                //NSString * CONTACOT = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                //NSString * TELEFONO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                //NSString * CANTIDAD = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                //NSString * CLAVE = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
            }

        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
}

-(void)select_prueba_cab
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM CAB_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW)
            {
                /*NSString * ID_DEVOLUCION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * ID_ORDEN_SERVICIO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * ID_INSTANCIA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * ID_ORDEN_VISITA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * TIPO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];*/
            }
            
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
}

-(void)select_prueba_det
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    sqlite3 *database;
    sqlite3_stmt *sentencia;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &database) ==SQLITE_OK) {
        NSString *sentenciaSQL = [NSString stringWithFormat:@"SELECT * FROM DET_DEVOLUCION_RUTA"];
        if (sqlite3_prepare_v2(database, [sentenciaSQL UTF8String], -1, &sentencia, NULL) == SQLITE_OK) {
            while (sqlite3_step(sentencia) == SQLITE_ROW)
            {
                /*NSString * ID_DEVOLUCION = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,0)];
                NSString * ID_DEVUELTO = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,1)];
                NSString * CANTIDAD = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,2)];
                NSString * CHAROLA = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,3)];
                NSString * ESPIRAL = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,4)];
                NSString * INICIAL = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,5)];
                //NSString * CLAVE = [NSString stringWithUTF8String:(char *) sqlite3_column_text(sentencia,6)];*/
            }
            
        }
        sqlite3_finalize(sentencia);
    }
    
    sqlite3_close(database);
    NSLog(@"Salio cargar productos");
}


@end
