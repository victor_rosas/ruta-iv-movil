//
//  planograma.m
//  RutaIV
//
//  Created by Miguel Banderas on 04/07/14.
//  Copyright (c) 2014 Miguel Banderas. All rights reserved.
//

#import "planograma.h"
#import <sqlite3.h>
#import "AppDelegate.h"

@implementation planograma

-(BOOL)borraPlanograma{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3_stmt *sentencia;
    sqlite3 *dataBase;
    bool bValida = FALSE;
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CAMBIO_PLANOGRAMA"];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dataBase, delete_stmt, -1, &sentencia, NULL );
        if (sqlite3_step(sentencia) == SQLITE_DONE)
        {
            bValida = TRUE;
        }
        else{
            bValida = FALSE;
        }
    }
    
    return  bValida;
}

-(BOOL)insertaPlanograma:(int)nIdArticulo wSurtio:(double)dSurtio wRemovio:(double)dRemovio wClave:(int)nClave wOrdenVisita:(int)nOrdenVisita wIdOrdenServicio:(int)nIdOrdenServicio wIdEspiral:(int)nIdEspiral wIdCharola:(int)nIdCharola wCapacidadEspiral:(int)nCapacidadEspiral wIdPlanograma:(int)nIdPlanograma wArticulo:(NSString *)sArticulo wEspiral:(NSString *)sEspiral wCambio:(NSString *)sCambio wIdInstancia:(int)nIdInstancia{
    bool bValida = FALSE;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *dataBase;
    sqlite3_stmt *sentenciaInsert;
    
    /*if ([sTipoAutorizacion isEqualToString:@""]) {
     sTipoAutorizacion = @"NULL";
     }
     
     if ([sHoraInicio isEqualToString:@""]) {
     sHoraInicio = @"NULL";
     }*/
    
    if (sqlite3_open([appDelegate.dataBasePath UTF8String], &dataBase) == SQLITE_OK) {
        //stringByReplacingOccurrencesOfString:@"%\%" withString:@""]
        NSString *sqlInsert = [NSString stringWithFormat:@"INSERT INTO CAMBIO_PLANOGRAMA(\"ID_ARTICULO\",\"SURTIO\",\"REMOVIO\",\"CLAVE\",\"ORDEN_VISITA\",\"ID_ORDEN_SERVICIO\",\"ID_ESPIRAL\",\"ID_CHAROLA\",\"CAPACIDAD\",\"ID_PLANOGRAMA\",\"ARTICULO\",\"ESPIRAL\",\"CAMBIO\",\"ID_INSTANCIA\") VALUES(\"%i\",\"%f\",\"%f\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%i\",\"%@\",\"%@\",\"%@\",\"%i\")",nIdArticulo,dSurtio,dRemovio,nClave,nOrdenVisita,nIdOrdenServicio,nIdEspiral,nIdCharola,nCapacidadEspiral,nIdPlanograma,sArticulo,sEspiral,sCambio,nIdInstancia];
        if(sqlite3_prepare_v2(dataBase, [sqlInsert UTF8String], -1, &sentenciaInsert, NULL) == SQLITE_OK){
            if(sqlite3_step(sentenciaInsert)){
                bValida = TRUE;
            }
            else{
                bValida = FALSE;
            }
            sqlite3_finalize(sentenciaInsert);
        }
        else{
            NSLog(@"Error en la creacion del insert");
        }
    }else{
        NSLog(@"No se ha podido abrir la base de datos");
    }
    sqlite3_close(dataBase);
    
    return bValida;

}

@end
